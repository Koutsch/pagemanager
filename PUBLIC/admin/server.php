<?php
	define("PHP_DIR_PATH", __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR);

	require_once PHP_DIR_PATH. 'login.php';
	require_once PHP_DIR_PATH . 'error.php';

	// Allow from any origin HIER WEITER: FOR DEVELOPMENT
	if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
	}
	// Access-Control headers are received during OPTIONS requests
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
			header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,HEAD,OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
	}

	$login_handler = new LoginHandler();
	if ($login_handler->is_admin() === false) exit_error(403, FORBIDDEN_ERR);

	if (array_key_exists("jsonData", $_POST)) {
		$request = json_decode($_POST["jsonData"]);
		if ($request === null) exit_error(400, INVALID_REQUEST_ERR);
		// handle get
		if (property_exists($request, "get")) {
			require_once PHP_DIR_PATH . 'get.php';
			$handler = new Get_Handler($request->get);
			echo json_encode($handler->response());
			exit();
		}
		// or handle set
		else if (property_exists($request, "set")) {
			require_once PHP_DIR_PATH . 'set.php';
			$handler = new Set_Handler($request->set);
			echo json_encode($handler->response());
			exit();
		}
	}
	// if no data, issue error
	exit_error(400, INVALID_DATA_ERR);
?>
