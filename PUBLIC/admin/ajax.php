<?php
  // Allow from any originc HIER WEITER: FOR DEVELOPMENT
  if (isset($_SERVER['HTTP_ORIGIN'])) {
      header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
      header('Access-Control-Allow-Credentials: true');
      header('Access-Control-Max-Age: 86400');    // cache for 1 day
  }
  // Access-Control headers are received during OPTIONS requests
  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
          header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,HEAD,OPTIONS");

      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
          header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

      exit(0);
  }

  define("PHP_DIR_PATH", __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "php" . DIRECTORY_SEPARATOR);
  require_once PHP_DIR_PATH . "ajax.php";
  require_once PHP_DIR_PATH. 'login.php';

  $login_handler = new LoginHandler();
	if ($login_handler->is_admin() === false) exit_error(403, FORBIDDEN_ERR);

  $ajax_response = new AjaxResponse();
  echo $ajax_response->process_request();
?>
