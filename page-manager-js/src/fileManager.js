import React from 'react';
import Button from 'react-bootstrap/Button';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Stack from 'react-bootstrap/Stack';
import Nav from 'react-bootstrap/Nav';
import Delete from '@mui/icons-material/Delete';
import Preview from '@mui/icons-material/Preview';
import FileChooser from './fileChooser.js';

export default function FileManager(props) {
  const [fileList, setFileList] = React.useState(null);
  const [activeKey, setActiveKey] = React.useState("files");
  const [preview, setPreview] = React.useState(null);

  const getFileList = () => {
    props.onLoadFileList((list) => setFileList(list));
  };

  React.useEffect(() => {
    getFileList();
  },[props.data]);

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>{props.labels.get(props.title)}</Breadcrumb.Item>
      </Breadcrumb>
      <Nav variant="tabs" className="mb-3" activeKey={activeKey}>
        <Nav.Item>
          <Nav.Link eventKey="files" onClick={() => setActiveKey("files")}>{props.labels.get("files")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="uploadNewFile" onClick={() => setActiveKey("uploadNewFile")}>{props.labels.get("uploadNewFile")}</Nav.Link>
        </Nav.Item>
      </Nav>
      {
        preview !== null ?
        <div className='preview-screen'>
          <div className='preview-curtain'></div>
          <button onClick={() => setPreview(null)} className='preview-close'>&times;</button>
          <img src={preview} />
        </div>
        :
        null
      }
      {
        activeKey === "files" && fileList !== null ?
        <Stack gap={3}>
          {
            fileList.filter((file) => file.remove !== true).sort((a, b) => {
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            }).map((file, i) => <Stack key={i} direction="horizontal" gap={3}>
              <div className="p-2">{file.name} ({file.type})</div>
              <Button onClick={() => props.onRemove(file.id, getFileList)} className="ms-auto p-2 btn-danger"><Delete /> {props.labels.get("remove")}</Button>
              {
                file.type.startsWith("image") ?
                <Button onClick={() => props.onGetThumbnail(file.id, "template", 300, (thumbnailData) => setPreview(thumbnailData))}><Preview /> {props.labels.get("preview")}</Button>
                :
                null
              }              
            </Stack>)
          }
        </Stack>
        :
        activeKey === "uploadNewFile" ?
        <FileChooser
          label={props.labels.get("chooseFile")}
          submitLabel={props.labels.get("upload")}
          onSubmit={(file) => props.onUpload(file, getFileList)}
          accept={props.accept}
        />
        :
        null
      }
    </>
  );
}
