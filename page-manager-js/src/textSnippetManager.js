import React from 'react';
import Nav from 'react-bootstrap/Nav';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import InputGroup from 'react-bootstrap/InputGroup';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Stack from 'react-bootstrap/Stack';
import ListAlt from '@mui/icons-material/ListAlt';
import Title from '@mui/icons-material/Title';
import Edit from '@mui/icons-material/Edit';
import Delete from '@mui/icons-material/Delete';
import ReactQuill from 'react-quill';

function TextSnippetEditor(props) {
	const [rerender, setRerender] = React.useState(false);
  React.useEffect(() => {
		if (props.snippet === null) {
			setName("");
			setType(null);
			setText(null);
		}
  },[props.snippet]);
  React.useEffect(() => {
    if (rerender === true) setRerender(false);
  },[rerender]);
	const [name, setName] = React.useState((props.snippet !== null ? props.snippet.name : ""));
	const [type, setType] = React.useState((props.snippet !== null ? props.snippet.type : null));
	const [text, setText] = React.useState((props.snippet !== null ? props.snippet.text : null));
	const modules = {
		toolbar: [
			[{ 'header': [1, 2, false] }],
			['bold', 'italic', 'underline','strike', 'blockquote'],
			[{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
			['link'],
			['clean']
		],
	};
	const formats = [
		'header',
		'bold', 'italic', 'underline', 'strike', 'blockquote',
		'list', 'bullet', 'indent',
		'link'
	];
	return (
		<>
			{
				props.snippet !== null ?
				<>
					<h4>{props.snippet.name}</h4>
					<p>{props.labels.get(props.snippet.type)}</p>
				</>
				:
				<InputGroup>
					<Form.Control value={name} onInput={(e) => props.registry.setRegistry(() => setName(e.target.value))} type="text" placeholder={props.labels.get("name")} />
					<Button onClick={() => props.registry.setRegistry(() => setType("singleLine"))}>{props.labels.get("singleLine")}</Button>
					<Button onClick={() => props.registry.setRegistry(() => setType("multiLine"))}>{props.labels.get("multiLine")}</Button>
				</InputGroup>
			}
			{
				<Stack className="my-3" gap={3}>
					{
						props.languages.languages.map((language, i) => {
							const mandatory = (language.iso639 === props.languages.defaultLanguage ? "*" : "");
							let languageText = (text !== null && text[language.iso639] !== undefined ? text[language.iso639] : "");
							if (type === "singleLine") return (
								<Form.Control value={languageText} key={i} onInput={(e) => props.registry.setRegistry(() => setText({...text, [language.iso639]: e.target.value}))} type="text" placeholder={language.name + mandatory} />
							)
							else if (type === "multiLine") return (
								<React.Fragment key={i}>
									<span>{language.name}{mandatory}</span>
									<ReactQuill
										theme="snow"
										modules={modules}
										formats={formats}
										value={languageText}
										onChange={(value) => props.registry.setRegistry(() => setText({...text, [language.iso639]: value}))}
									/>
								</React.Fragment>
							)
							else return null;
						})
					}
				</Stack>
			}
			<Button onClick={() => props.registry.unsetRegistry(() => props.onSubmitEditor(type, name, text))}>{props.labels.get("submit")}</Button>
		</>
	)
}

export default function TextSnippetManager(props) {
	const [rerender, setRerender] = React.useState(false);
  React.useEffect(() => {
		if (activeKey.key === "editText" && (props.textSnippets[activeKey.name] === undefined || props.textSnippets[activeKey.name].remove !== undefined)) setActiveKey({key: "textList", nav: "textList"});
    else setRerender(true);
  },[props.textSnippets]);
  React.useEffect(() => {
    if (rerender === true) setRerender(false);
  },[rerender]);
	const [activeKey, setActiveKey] = React.useState({key: null});
	if (rerender === true) return null;
	return (
		<>
			<Breadcrumb>
				<Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setActiveKey({key: null, nav: null}))}>{props.labels.get("textSnippets")}</Breadcrumb.Item>
				{
					activeKey.key === "newText" ?
					<Breadcrumb.Item active>{props.labels.get("newText")}</Breadcrumb.Item>
					:
					activeKey.key === "textList" ?
					<Breadcrumb.Item active>{props.labels.get("textList")}</Breadcrumb.Item>
					:
					activeKey.key === "editText" ?
					<>
						<Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setActiveKey({key: "textList", nav: "textList"}))}>{props.labels.get("textList")}</Breadcrumb.Item>
						<Breadcrumb.Item active>{activeKey.name}</Breadcrumb.Item>
					</>
					:
					null
				}
			</Breadcrumb>
			<Nav 
				activeKey={activeKey.nav} 
				className="my-3"
        		variant="tabs"
			>
				<Nav.Item>
					<Nav.Link onClick={() => props.registry.getRegistry(() => setActiveKey({key: "newText", nav: "newText"}))} eventKey="newText"><Title /> {props.labels.get("newText")}</Nav.Link>
				</Nav.Item>
				<Nav.Item>
					<Nav.Link onClick={() => props.registry.getRegistry(() => props.onGetTextSnippets(() => setActiveKey({key: "textList", nav: "textList"})))} eventKey="textList"><ListAlt /> {props.labels.get("textList")}</Nav.Link>
				</Nav.Item>
			</Nav>
			{
				activeKey.key === "newText" ?
				<TextSnippetEditor
					snippet={null}
					onSubmitEditor={(type, name, text) => props.onSubmitText(type, name, text, () => props.onGetTextSnippets(() => setActiveKey({key: "textList", nav: "textList"})))}
					{...props}
				/>
				:
				activeKey.key === "textList" ?
				<Stack gap={3}>
					{
						Object.keys(props.textSnippets).filter((snippetName) => props.textSnippets[snippetName].remove === undefined).sort().map((snippetName, i) => <ButtonGroup key={i}>
							<Button onClick={() => props.onGetTextSnippet(snippetName, () => setActiveKey({key: "editText", nav: "textList", name: snippetName}))}><Edit /> {snippetName}</Button>
							<Button className="btn-danger" onClick={() => props.onRemoveTextSnippet(snippetName)}><Delete /> {props.labels.get("remove")}</Button>
						</ButtonGroup>)
					}
				</Stack>
				:
				activeKey.key === "editText" ?
				<TextSnippetEditor
					snippet={{name: activeKey.name, ...props.textSnippets[activeKey.name]}}
					onSubmitEditor={(type, name, text) => props.onEditTextSnippet(type, name, text, () => props.onGetTextSnippets(() => setActiveKey({key: "textList", nav: "textList"})))}
					{...props}
				/>
				:
				null
			}
		</>
	)
}