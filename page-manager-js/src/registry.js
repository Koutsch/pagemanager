export default class Registry {
  constructor(dialogRef, title, text) {
    this.dialogRef = dialogRef;
    this.registered = false;
    this.supRegistries = {};
    this.title = title;
    this.text = text;
  }
  
  splitRegistry(name) {
    if (this.supRegistries[name] === undefined) this.supRegistries[name] = new Registry(this.dialogRef, this.title, this.text);
    return this.supRegistries[name];
  }

  getRegistry(callback) {
    if ((this.registered || Object.values(this.supRegistries).filter((sup) => sup.registered === true).length > 0) && this.dialogRef.current !== null) {
      this.dialogRef.current.showDialog({
        "title": this.title,
        "text": this.text,
        "callback": () => {
          this.registered = false;
          if (callback !== undefined) callback();
        }
      });
    }
    else if (callback !== undefined) callback();
  }

  setRegistry(callback) {
    this.registered = true;
    if (callback !== undefined) callback();
  }

  unsetRegistry(callback) {
    this.registered = false;
    callback();
  }
}
