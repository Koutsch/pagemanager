const jp = require('jsonpath');

function createJsonpath(path) {
  let jsonPath = "$";
  if (path !== null && path.length > 1) {
    path = path.slice(1);
    for (let section of path) jsonPath += `.sections[?(@.id==${section})]`;
  }
  return jsonPath;
}

export default class Page {
  constructor(pageTree, pageSchema, sectionSchemas, customSectionSchemas) {
    this.pageTree = pageTree;
    this.pageSchema = pageSchema;
    this.sectionSchemas = sectionSchemas;
    this.customSectionSchemas = customSectionSchemas;
  }

  setProperty(name, value) {
    this.pageTree[name] = value;
  }

  hasProperty(name) {
    if (name in this.pageTree) return true;
    return false;
  }

  setPageTree(pageTree) {
    pageTree.sections = [];
    this.pageTree = pageTree;
  }

  addSection(path, data) {
    const section = this.getSection(path);
    if (section === false) return false;
    if (!("sections" in section)) section.sections = [];
    section.sections.push(data);
    return true;
  }

  replaceSection(path, data) {
    const section = this.getSection(path);
    if (section === false) return false;
    Object.keys(section).forEach(function(s) {delete section[s];});
    Object.assign(section, data);
    return true;
  }

  repositionSection(parentPath, moved, target) {
    const parentSection = this.getSection(parentPath);
    if (parentSection === false) return false;
    const movedSection = parentSection.sections.splice(moved, 1)[0];
    parentSection.sections.splice(target, 0, movedSection);
    return true;
  }

  isPage(path) {
    return path.length === 1 && this.pageTree.id === path[path.length-1];
  }

  removeSection(path) {
    if (this.isPage(path)) this.pageTree = null;
    else {
      const parentPath = path.slice(0,path.length-1);
      const parentSection = this.getSection(parentPath);
      if (parentSection === false) return false;
      const filteredSections = parentSection.sections.filter((section) => {
        return section.id !== path[path.length-1];
      });
      parentSection.sections = filteredSections;
    }
    return true;
  }

  addContent(path, name, type, custom) {
    const section = this.getSection(path);
    if (section === false) return false;
    if (!("content" in section)) section.content = {};
    const part = (custom === true ? "custom" : "predefined");
    if (!(part in section.content)) section.content[part] = {};
    section.content[part][name] = {type: type};
    return true;
  }

  removeContent(path, name, custom) {
    const section = this.getSection(path);
    if (section === false) return false;
    const part = (custom === true ? "custom" : "predefined");
    if ("content" in section && part in section.content) delete section.content[part][name];
    return true;
  }

  getSection(path) {
    if (path === undefined || this.isPage(path)) {
      return this.pageTree;
    }
    else if (path.length > 1) {
      const section = jp.query(this.pageTree, createJsonpath(path));
      if (section.length === 0) return false;
      return section[0];
    }
    else return false;
  }

  getSectionSchema(name, custom) {
    let schemas;
    if (custom === true) schemas = this.customSectionSchemas;
    else schemas = this.sectionSchemas;
    const schema = schemas.filter((schema) => schema.name === name);
    if (schema.length < 1) return false;
    return schema[0];
  }

  getSchema(path) {
    if (path === undefined || this.isPage(path)) return this.pageSchema;
    else {
      const section = this.getSection(path);
      if (section === false) return false;
      return this.getSectionSchema(section.type, section.custom);
    }
  }
}
