import React from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Form from "@rjsf/material-ui";
import validator from '@rjsf/validator-ajv8';
import { addLangInput, addTitles } from './schemas.js';

export default function InformationHandler(props) {
  const schema = React.useMemo(() => {
    const schema = addTitles(props.informationTextSchema, props.labels);
    schema.$defs.langInput = addLangInput(props.languages);
    return schema;
  }, [props.informationTextSchema, props.labels, props.languages]);
  const [informationTexts, setInformationTexts] = React.useState(false);
  React.useEffect(() => {
    if (informationTexts === false) props.onGetInformationTexts(() => setInformationTexts(true));
  });
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>{props.labels.get("informationTexts")}</Breadcrumb.Item>
      </Breadcrumb>
      <Form
          schema={schema}
          validator={validator}       
          formData={props.informationTexts}
          onSubmit={(data) => props.registry.unsetRegistry(() => props.onSubmitInformationTexts(data.formData))}
          onChange={() => props.registry.setRegistry()}
      />
    </>
  );
}