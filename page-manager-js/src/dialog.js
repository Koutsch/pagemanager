import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Stack from 'react-bootstrap/Stack';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export default class Dialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: null,
      input: null
    }
  }

  handleChange = (e) => this.setState({input: e.target.value});
  setData = (data) => this.setState({input: data});

  showDialog = (modal) => {
    this.setState({
      modal: modal
    });
  }

  render() {
    if (this.state.modal === null) return null;
    return (
      <Modal show={this.state.modal !== null} onHide={() => this.setState({modal: null})}>
        <Modal.Header closeButton>
          <Modal.Title>{this.state.modal.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Stack direction="horizontal" gap={3}>
            <img
              alt=""
              src="logo_main.png"
              width="100"
              height="100"
              className="d-inline-block align-top"
            />
            {this.state.modal.text}
          </Stack>
          {
            this.state.modal.input === true ?
            <Form.Group>
              <Form.Control type="text" onChange={this.handleChange} value={this.state.name} />
            </Form.Group>
            :
            null
          }
          {
            "component" in this.state.modal && "componentProps" in this.state.modal ?
            React.createElement(this.state.modal.component, {setData: this.setData, ...this.state.modal.componentProps})
            :
            null
          }
          </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => this.setState({modal: null})}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => {
            if (this.state.modal.callback !== undefined) {
              if ("checkOnSubmit" in this.state.modal && this.state.modal.checkOnSubmit === true) {
                this.state.modal.callback(this.state.input, () => this.setState({modal: null}));
                return;
              }
              else this.state.modal.callback(this.state.input);
            }
            this.setState({modal: null});
          }}>OK</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
