import React from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import InputGroup from 'react-bootstrap/InputGroup';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Stack from 'react-bootstrap/Stack';
import Container from 'react-bootstrap/Container';
import Dialog from './dialog.js';
import Form from 'react-bootstrap/Form';
import SchemaTreeView from './schemaTreeView.js';
import Check from '@mui/icons-material/Check';
import Close from '@mui/icons-material/Close';
const jp = require('jsonpath');

const inputTypes = [
  {
    "name": "singleLine"
  },
  {
    "name": "multiLine"
  },
  {
    "name": "number"
  },
  {
    "name": "regex"
  },
  {
    "name": "date"
  },
  {
    "name": "date-time"
  },
  {
    "name": "email"
  },
  {
    "name": "uri"
  },
  {
    "name": "selector"
  },
  {
    "name": "array"
  },
  {
    "name": "object"
  }
];

const inputSort = (a, b) => {
  if (a.title < b.title) return -1;
  if (a.title > b.title) return 1;
  return 0;
};

function InputList(props) {
  const [chosen, setChosen] = React.useState({});
  const addName = (name) => {
    chosen.name = name;
    setChosen({...chosen});
    props.setData(chosen);
  };
  const addType = (type) => {
    chosen.type = type;
    setChosen({...chosen});
    props.setData(chosen);
  };
  return (
    <Stack gap={2}>
      <p>
        {`${chosen.name !== undefined ? chosen.name : ""} (${chosen.type !== undefined ? props.labels.get(chosen.type) : ""})`}
      </p>
      <Form.Control type="text" onChange={(e) => addName(e.target.value)} />
      <div>
        {
          props.inputTypes.map((inputType) => {
            return {name: inputType.name, title: props.labels.get(inputType.name)};
          }).sort(inputSort).map((input, i) => {
            if (input.name === "multiLine" && props.allowMultiLine === false) return null;
            else return (
              <Button className="m-2" key={i} onClick={() => addType(input.name)}>{input.title}</Button>
            );
          })
        }
      </div>
    </Stack>
  );
}

function checkName(name) {
  var re = new RegExp("^([a-zA-Z0-9_ ]+)$");
  if (!re.test(name)) {
    return false;
  }
  return true;
}

function SchemaCreator(props) {
  const [schema, setSchema] = React.useState((props.editSchema !== null ? props.editSchema : {
    "name": "schema",
    "type": "object"
  }));
  const dialogRef = React.createRef();
  const selectDialogRef = React.createRef();
  const regexDialogRef = React.createRef();

  const getByPath = (path) => {
    let curSchema = schema;
    for (let index = 0; index < path.length; index++) {
      if (curSchema.type === "object") {
        const subschema = jp.query(curSchema, "$.properties[?(@.name=='" + path[index] + "')]");
        if (subschema.length === 0) return null;
        curSchema = subschema[0];
      }
      else if (curSchema.type === "array") {
        curSchema = curSchema.items;
        if (curSchema.name !== path[index]) return null;
      }
      else return null;
    }
    return curSchema;
  }

  const checkData = (data) => {
    if (data === null || !data.name || !data.type) {
      return false;
    }
    return checkName(data.name);
  }

  const createInputDialog = (action) => {
    selectDialogRef.current.showDialog({
      title: props.labels.get("add"),
      text: props.labels.get("selectInput"),
      component: InputList,
      componentProps: {
        inputTypes: inputTypes,
        allowMultiLine: props.allowMultiLine,
        labels: props.labels
      },
      checkOnSubmit: true,
      callback: (data, callback) => {
        if (!checkData(data)) {
          dialogRef.current.showDialog({
            title: props.labels.get("error"),
            text: props.labels.get("invalid")
          });
          return;
        }
        const status = action(data);
        if (!status) return;
        setRegistry(props.registry, {...schema}, setSchema);
        callback();
      }
    });
  }

  const handleAddToObject = (path) => {
    createInputDialog((data) => {
      const pathArr = path.split("/");
      let curSchema;
      if (pathArr.length === 1) curSchema = schema;
      else curSchema = getByPath(pathArr.slice(1));
      if (!("properties" in curSchema)) curSchema.properties = [];
      else {
        const existing = curSchema.properties.filter((property) => property.name === data.name);
        if (existing.length > 0) {
          dialogRef.current.showDialog({
            title: props.labels.get("error"),
            text: props.labels.get("alreadyExists")
          });
          return false;
        }
      }
      curSchema.properties.push(data);
      return true;
    });
  }

  const setItems = (path) => {
    createInputDialog((data) => {
      const pathArr = path.split("/");
      const parentObj = getByPath(pathArr.slice(1));
      parentObj.items = data;
      return true;
    });
  }

  const addOption = (path) => {
    regexDialogRef.current.showDialog({
      title: props.labels.get("option"),
      text: props.labels.get("addOption"),
      input: true,
      checkOnSubmit: true,
      callback: (data, callback) => {
        const pathArr = path.split("/");
        const selectObj = getByPath(pathArr.slice(1));
        if (data === null || data === "") {
          dialogRef.current.showDialog({
            title: props.labels.get("error"),
            text: props.labels.get("emptyField")
          });
          return;
        }
        if (!("options" in selectObj)) selectObj.options = [];
        selectObj.options.push({
          "name": data,
          "type": "option"
        });
        setSchema({...schema});
        callback();
      }
    });
  }

  const setRegex = (path) => {
    regexDialogRef.current.showDialog({
      title: props.labels.get("regex"),
      text: props.labels.get("setRegex"),
      input: true,
      checkOnSubmit: true,
      callback: (data, callback) => {
        const pathArr = path.split("/");
        const regexObj = getByPath(pathArr.slice(1));
        if (data === null || data === "") {
          dialogRef.current.showDialog({
            title: props.labels.get("error"),
            text: props.labels.get("emptyField")
          });
          return;
        }
        regexObj.regex = data;
        setSchema({...schema});
        callback();
      }
    });
  }

  const handleRemove = (path) => {
    dialogRef.current.showDialog({
      title: props.labels.get("remove"),
      text: props.labels.get("removeInput"),
      callback: () => {
        const pathArr = path.split("/");
        const name = pathArr[pathArr.length -1];
        const parentObj = getByPath(pathArr.slice(1,-1));
        if ("items" in parentObj && parentObj.items.name === name) delete parentObj.items;
        else if ("options" in parentObj) {
          const index = parentObj.options.findIndex((property) => property.name === name);
          parentObj.options.splice(index, 1);
        }
        else {
          const index = parentObj.properties.findIndex((property) => property.name === name);
          parentObj.properties.splice(index, 1);
        }
        setSchema({...schema});
      }
    });
  }

  const handleRequire = (path) => {
    const pathArr = path.split("/");
    const obj = getByPath(pathArr.slice(1));
    if (!("required" in obj)) obj.required = true;
    else obj.required = ! obj.required;
    setSchema({...schema});
  }

  return (
    <>
      <Dialog
        ref={dialogRef}
      />
      <Dialog
        ref={selectDialogRef}
      />
      <Dialog
        ref={regexDialogRef}
      />
      <h4>{props.labels.get("createSchema")}</h4>
      <SchemaTreeView
        defaultExpanded={schema.name}
        schema={schema}
        onRemoveInput={handleRemove}
        onRequire={handleRequire}
        addToObject={handleAddToObject}
        setItems={setItems}
        setRegex={setRegex}
        addOption={addOption}
        languages={props.languages}
        labels={props.labels}
      />
      <Button onClick={() => props.onSave(schema)}>{props.labels.get("save")}</Button>
    </>
  );
}

function setRegistry(registry, data, callback) {
  registry.setRegistry();
  callback(data);
}

function FileCreator(props) {
  const [name, setName] = React.useState("");
  const [occurrence, setOccurrence] = React.useState("single");
  const [fileType, setFileType] = React.useState("");

  return (
    <>
      <h3>{props.labels.get("createFileCollection")}</h3>
      <Form.Group className="mb-3">
        <Form.Label>{props.labels.get("name")}</Form.Label>
        <Form.Control type="text" onChange={(e) => setRegistry(props.registry, e.target.value, setName)} />
        <Form.Text className="text-muted">
          {props.labels.get("enterContentName")}
        </Form.Text>
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>{props.labels.get("occurrence")}</Form.Label>
        <Form.Select onChange={(e) => setRegistry(props.registry, e.target.value, setOccurrence)}>
          <option value="single">{props.labels.get("single")}</option>
          <option value="multiple">{props.labels.get("multiple")}</option>
        </Form.Select>
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>{props.labels.get("fileType")}</Form.Label>
        <Form.Control type="text" onChange={(e) => setRegistry(props.registry, e.target.value, setFileType)} />
        <Form.Text className="text-muted">
          {props.labels.get("enterContentName")}
        </Form.Text>
      </Form.Group>
      <Button onClick={() => props.onSave({name: name, occurrence: occurrence, fileType: fileType})}>{props.labels.get("save")}</Button>
    </>
  );
}

export function ContentAdder(props) {
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>{props.labels.get("createContentSchema")}</Breadcrumb.Item>
      </Breadcrumb>
      <ContentCreator
        {...props}
      />
    </>
  );
}

function ContentCreator(props) {
  const dialogRef = React.createRef();
  const [name, setName] = React.useState((props.edit !== undefined ? props.edit.name : ""));
  const [schema, setSchema] = React.useState((props.edit !== undefined ? props.edit.schemaUpload.schema : null));
  const [action, setAction] = React.useState(null);

  return (
    <>
      <Dialog
        ref={dialogRef}
      />
      <Nav
        className="my-3"
        variant="tabs"
        activeKey={action}
      > 
        {
          props.edit === undefined ?
          <Nav.Item>
            <Nav.Link eventKey="schemaName" onClick={() => setAction("schemaName")}>{props.labels.get("schemaName")}</Nav.Link>
          </Nav.Item>
          :
          <Nav.Item>
            <Nav.Link eventKey="schemaName" className="text-dark">{name}</Nav.Link>
          </Nav.Item>
        }
        <Nav.Item>
          <Nav.Link eventKey="schema" onClick={() => setAction("schema")}>{props.labels.get("schema")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="upload" className={(name !== "" && (schema !== null) ? "text-success" : "text-secondary")} disabled={(name !== "" && (schema !== null) ? false : true)} onClick={() => props.onUploadContentSchema({name: name, schema: schema, edit: (props.edit !== undefined ? props.edit.name : false)}, () => props.registry.unsetRegistry(props.onSetList))}>{props.labels.get("upload")}</Nav.Link>
        </Nav.Item>
      </Nav>
      <InputGroup className="my-3">
        {
          props.edit === undefined && name !== "" ?
          <>
            <InputGroup.Text>{props.labels.get("schemaName")}: {name}</InputGroup.Text>
            {
              props.edit === undefined ?
              <Button className="me-2" onClick={() => setRegistry(props.registry, "", setName)}>{props.labels.get("reset")}</Button>
              :
              null
            }            
          </>        
          :
          null
        }
        {
          schema !== null ?
          <>
            <InputGroup.Text>{props.labels.get("schema")}</InputGroup.Text>
            <Button className="me-2" onClick={() => setRegistry(props.registry, null, setSchema)}>{props.labels.get("reset")}</Button>
          </>
          :
          null
        }   
      </InputGroup>
      {
        action === "schemaName" ?
        <Form.Control type="text" value={name} onChange={(e) => setRegistry(props.registry, e.target.value, setName)} placeholder={props.labels.get("enterContentName")} />
        :
        action === "schema" ?
        <SchemaCreator
          onSave={(savedSchema) => setRegistry(props.registry, savedSchema, setSchema)}
          allowMultiLine={true}
          editSchema={(props.edit !== undefined ? props.edit.schemaUpload.schema : null)}
          {...props}
        />
        :
        null
      }
    </>
  );
}

function SchemaList(props) {
  const schemas = props.getSchemas();
  return (
    <Stack gap={3}>
      {
        schemas.sort().map((schema, i) => <div key={i}>
          <h4>{schema}</h4>
          <ButtonGroup key={i}>
            <Button onClick={() => props.onSetEdit(schema, false)}>{props.labels.get("edit")}</Button>
            <Button className="btn btn-danger" onClick={() => props.onRemove(schema)}>{props.labels.get("remove")}</Button>
          </ButtonGroup>
        </div>)
      }
    </Stack>
  );
}

export function SectionAdder(props) {
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>{props.labels.get("createSectionSchema")}</Breadcrumb.Item>
      </Breadcrumb>
      <SectionCreator
        {...props}
      />
    </>
  );
}

function SectionCreator(props) {
  const dialogRef = React.createRef();
  const existDialogRef = React.createRef();
  const [name, setName] = React.useState((props.edit !== undefined ? props.edit.name : ""));
  const [subsections, setSubsections] = React.useState((props.edit !== undefined ? props.edit.schemaUpload.subsections : false));
  const [schema, setSchema] = React.useState((props.edit !== undefined ? props.edit.schemaUpload.schema : null));
  const [contents, setContents] = React.useState((props.edit !== undefined ? props.edit.schemaUpload.content : false));
  const [fileCollections, setFileCollections] = React.useState((props.edit !== undefined ? props.edit.schemaUpload.fileCollections : []));
  const [forms, setForms] = React.useState((props.edit !== undefined ? props.edit.schemaUpload.forms : false));
  const [action, setAction] = React.useState(null);

  const saveFileCollection = (fileCollection) => {
    if (fileCollection.name === "" || fileCollection.occurrence === null) {
      dialogRef.current.showDialog({
        title: props.labels.get("error"),
        text: props.labels.get("emptyField")
      });
      return;
    }
    if (fileCollections.filter((coll) => coll.name === fileCollection.name).length > 0) {
      dialogRef.current.showDialog({
        title: props.labels.get("error"),
        text: props.labels.get("alreadyExists")
      });
      return;
    }
    if (checkName(fileCollection.name) === false) {
      dialogRef.current.showDialog({
        title: props.labels.get("error"),
        text: props.labels.get("invalid")
      });
      return;
    }
    fileCollections.push(fileCollection);
    setFileCollections([].concat(fileCollections));
  }

  const createSectionSchemaList = (schemas, sectionArray) => <Container className="p-3 border">
    <h4>{props.labels.get(sectionArray)}</h4>
    <Stack gap={3}>
      {
        schemas.sort((a, b) => {
          if (a.name < b.name) return -1;
          if (a.name > b.name) return 1;
          return 0;
        }).map((existingSchema, i) => <Button key={i} onClick={() => handleAddSection(existingSchema.name, sectionArray)}>{(sectionArray === "predefined" ? props.labels.get(existingSchema.name) : existingSchema.name)}</Button>)
      }
    </Stack>
  </Container>;

  const handleAddSection = (schemaName, sectionArray) => {
    const tmpSections = (typeof subsections === "boolean" ? {} : subsections);
    if (!(sectionArray in tmpSections)) tmpSections[sectionArray] = [];
    if (tmpSections[sectionArray].includes(schemaName)) {
      existDialogRef.current.showDialog({
        title: props.labels.get("error"),
        text: props.labels.get("alreadyExists")
      });
      return;
    }
    props.registry.setRegistry();
    tmpSections[sectionArray] = tmpSections[sectionArray].concat(schemaName);
    setSubsections({...tmpSections});
  }

  const createContentSchemaList = (schemas, contentArray) => <Container className="p-3 border">
    <h4>{props.labels.get(contentArray)}</h4>
    <Stack gap={3}>
      {
        Object.keys(schemas).sort().map((schemaName, i) => <Button key={i} onClick={() => handleAddContent(schemaName, contentArray)}>{(contentArray === "predefined" ? props.labels.get(schemaName) : schemaName)}</Button>)
      }
    </Stack>
  </Container>;

  const handleAddContent = (schemaName, contentArray) => {
    dialogRef.current.showDialog({
      title: props.labels.get("confirmation"),
      text: props.labels.get("name"),
      input: true,
      callback: (data) => {
        const tmpContents = (typeof contents === "boolean" ? {} : contents);
        if (!(contentArray in tmpContents)) tmpContents[contentArray] = [];
        if (tmpContents[contentArray].filter((content) => content.name === data).length > 0) {
          existDialogRef.current.showDialog({
            title: props.labels.get("error"),
            text: props.labels.get("alreadyExists")
          });
          return;
        }
        props.registry.setRegistry();
        tmpContents[contentArray] = tmpContents[contentArray].concat({name: data, type: schemaName});
        setContents({...tmpContents});
      }
    });
  }
  return (
    <>
      <Dialog
        ref={dialogRef}
      />
      <Dialog
        ref={existDialogRef}
      />
      <Nav
        className="my-3"
        variant="tabs"
        activeKey={action}
      >
        {
          props.edit === undefined ?
          <Nav.Item>
            <Nav.Link eventKey="schemaName" onClick={() => setAction("schemaName")}>{props.labels.get("schemaName")}</Nav.Link>
          </Nav.Item>
          :
          <Nav.Item>
            <Nav.Link eventKey="schemaName" className="text-dark">{name}</Nav.Link>
          </Nav.Item>
        }
        <Nav.Item>
          <Nav.Link eventKey="subsections" onClick={() => setAction("subsections")}>{props.labels.get("subsections")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="contents" onClick={() => setAction("contents")}>{props.labels.get("contents")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="files" onClick={() => setAction("files")}>{props.labels.get("files")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="forms" onClick={() => setRegistry(props.registry, (forms !== true ? true : false), setForms)}>
            {
              forms === true ?
              <Check />
              :
              <Close />
            }
            {' '}{props.labels.get("forms")}
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="schema" onClick={() => setAction("schema")}>{props.labels.get("schema")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link className={(name !== "" ? "text-success" : "text-secondary")} disabled={(name !== "" ? false : true)} eventKey="upload" onClick={() => props.onUploadSectionSchema({name: name, subsections: subsections, contents: contents, schema: schema, fileCollections: fileCollections, forms: forms, edit: (props.edit !== undefined ? props.edit.name : false)}, () => props.registry.unsetRegistry(props.onSetList))}>{props.labels.get("upload")}</Nav.Link>
        </Nav.Item>
      </Nav>
      <InputGroup className="my-3">
        {
          props.edit === undefined && name !== "" ?
          <>
            <InputGroup.Text>{props.labels.get("schemaName")}: {name}</InputGroup.Text>
            {
              props.edit === undefined ?
              <Button className="me-2" onClick={() => setRegistry(props.registry, "", setName)}>{props.labels.get("reset")}</Button>
              :
              null
            }            
          </>        
          :
          null
        }
        {
          subsections === false ?
          null
          :
          <>
            {
              subsections === true ?
              <InputGroup.Text>{props.labels.get("subsections")}: {props.labels.get("all")}</InputGroup.Text>
              :
              <InputGroup.Text>{props.labels.get("subsections")}: {Object.keys(subsections).map((key) => {
                const names = [];
                subsections[key].forEach(subsection => {
                  names.push((key === "predefined" ? props.labels.get(subsection) : subsection));
                });
                return names.join(", ");
              }).join(", ")}</InputGroup.Text>
            }
            <Button className="me-2" onClick={() => setRegistry(props.registry, false, setSubsections)}>{props.labels.get("reset")}</Button>
          </>          
        }
        {
          contents === false ?
          null
          :
          <>
            {
              contents === true ?
              <InputGroup.Text>{props.labels.get("contents")}: {props.labels.get("all")}</InputGroup.Text>
              :
              <InputGroup.Text>{props.labels.get("contents")}: {Object.keys(contents).map((key) => {
                const contentsArr = [];
                contents[key].map((content) => contentsArr.push(`${content.name} (${(key === "predefined" ? props.labels.get(content.type) : content.type)})`));
                return contentsArr.join(", ");
              }).join(", ")}</InputGroup.Text>
            }
            <Button className="me-2" onClick={() => setRegistry(props.registry, false, setContents)}>{props.labels.get("reset")}</Button>
          </>          
        }
        {
          fileCollections.length > 0 ?
          <>
            <InputGroup.Text>{props.labels.get("files")}: </InputGroup.Text>
            {
              fileCollections.map((fileCollection, i) => <React.Fragment key={i}>
                <InputGroup.Text>{fileCollection.name}</InputGroup.Text>
                <Button onClick={() => {
                  fileCollections.splice(i, 1);
                  setFileCollections([].concat(fileCollections));
                }}>{props.labels.get("remove")}</Button>
              </React.Fragment>)
            }
            <Button className="me-2" onClick={() => setRegistry(props.registry, [], setFileCollections)}>{props.labels.get("reset")}</Button>           
          </>
          :
          null    
        }
        {
          forms === true ?
          <>
            <InputGroup.Text>{props.labels.get("forms")} </InputGroup.Text>
            <Button onClick={() => setRegistry(props.registry, false, setForms)}>{props.labels.get("reset")}</Button>
          </>          
          :
          null
        } 
        {
          schema !== null ?
          <>
            <InputGroup.Text>{props.labels.get("schema")}</InputGroup.Text>
            <Button className="me-2" onClick={() => setRegistry(props.registry, null, setSchema)}>{props.labels.get("reset")}</Button>
          </>
          :
          null
        }
      </InputGroup>
      {
        action === "schemaName" ?
        <Form.Control type="text" value={name} onChange={(e) => setRegistry(props.registry, e.target.value, setName)} aria-label={props.labels.get("enterSectionName")} placeholder={props.labels.get("enterSectionName")} />
        :
        action === "subsections" ?
        <Stack gap={3}>
          <Form.Group className="mt-3">
            <Form.Check type="checkbox" label={props.labels.get("chooseAll")} checked={(subsections === true ? true : false)} onChange={(e) => setRegistry(props.registry, e.target.checked, setSubsections)} />
          </Form.Group>
          <Row>
            <Col className="my-3" sm>
              {
                createSectionSchemaList(props.sectionSchemas, "predefined")
              }
            </Col>
            <Col className="my-3" sm>
              {
                createSectionSchemaList(props.customSectionSchemas, "custom")
              }
            </Col>
          </Row>
        </Stack>
        :
        action === "contents" ?
        <Stack gap={3}>
          <Form.Group className="mt-3">
            <Form.Check type="checkbox" label={props.labels.get("chooseAll")} checked={(contents === true ? true : false)} onChange={(e) => setRegistry(props.registry, e.target.checked, setContents)} />
          </Form.Group>
          <Row>
            <Col className="my-3" sm>
              {
                createContentSchemaList(props.contentSchemas, "predefined")
              }
            </Col>
            <Col className="my-3" sm>
              {
                createContentSchemaList(props.customContentSchemas, "custom")
              }
            </Col>
          </Row>
        </Stack>
        :
        action === "files" ?
        <FileCreator
          onSave={saveFileCollection}
          {...props}
        />
        :
        action === "schema" ?
        <SchemaCreator
          onSave={(savedSchema) => setRegistry(props.registry, savedSchema, setSchema)}
          allowMultiLine={false}
          editSchema={(props.edit !== undefined ? props.edit.schemaUpload.schema : null)}
          {...props}
        />
        :
        null
      }
    </>
  );
}

export function SchemaEditor(props) {
  const [chosenSchema, setChosenSchema] = React.useState(null);
  const getChosenSchemaUpload = (schema, direct) => {
    if (!direct) props.onGetSchemaUpload(props.schemaType, schema, (schemaUpload) => {
      setChosenSchema({schema: schema, schemaUpload: schemaUpload});
    });
    else setChosenSchema({schema: schema});
  }
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setChosenSchema(null))}>{props.labels.get(props.schemaType === "section" ? "editSectionSchemas" : "editContentSchemas")}</Breadcrumb.Item>
        {
          chosenSchema !== null ?
          <Breadcrumb.Item active>{props.labels.get("editSchema", chosenSchema.schema)}</Breadcrumb.Item>
          :
          null
        }
      </Breadcrumb>
      {
        chosenSchema === null ?
        <SchemaList
          getSchemas={() => (props.schemaType === "section" ? props.schemas.map((sectionSchema) => sectionSchema.name) : Object.keys(props.schemas))}
          onRemove={(schema) => props.onRemoveSchema(schema, false, () => setChosenSchema(null))}
          onSetEdit={(schema, direct) => getChosenSchemaUpload(schema, direct)}
          labels={props.labels}
        />
        :
        chosenSchema.schemaUpload !== undefined ?
          props.schemaType === "section" ?
          <SectionCreator
            onSetList={() => {}}
            edit={{name: chosenSchema.schema, schemaUpload: chosenSchema.schemaUpload}}
            {...props}
          />
          :
          props.schemaType === "content" ?
          <ContentCreator
            onSetList={() => {}}
            edit={{name: chosenSchema.schema, schemaUpload: chosenSchema.schemaUpload}}
            {...props}
          />
          :
          null
        :
        null
      }
    </>
  );
}
