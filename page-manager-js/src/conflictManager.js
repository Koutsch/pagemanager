import React from 'react';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import CardGroup from 'react-bootstrap/CardGroup';
import Form from 'react-bootstrap/Form';
import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';

function CheckBox(props) {
  const [checked, setChecked] = React.useState(props.checked);
  return (
    <Form.Group className="mb-3" controlId="check">
      <Form.Check
        name={props.id}
        checked={checked}
        onChange={(e) => setChecked(e.target.checked)}
        type="checkbox"
        label={props.labels.get("acceptChange")}
      />
    </Form.Group>
  );
}

function StringConflict(props) {

  function deescape(string) {
    return string.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
  }

  function styleLine(line) {
    const allLines = [], parts = line.split(/<ins>|<del>/);
    parts.forEach((part, i) => {
      const index = part.search(/<\/ins>|<\/del>/);
      if (index === -1) allLines.push(<span key={i}>{deescape(part)}</span>)
      else {
        const modifiedText = deescape(part.substring(0, index));
        const tag = part.substring(index, index + 6);
        const textAfter = deescape(part.substring(index + 6));
        const className = (tag === "</ins>" ? "inserted" : tag === "</del>" ? "deleted" : "modified");
        allLines.push(<span key={i+"mod"} className={className}>{modifiedText}</span>);
        allLines.push(<span key={i}>{textAfter}</span>);
      }
    });
    return allLines;
  }

  function styleLines(lines) {
    return lines.map((line, i) => <span key={i}>{styleLine(line)}<br/></span>);
  }

  return (
    <ListGroup>
      {
        props.stringConflict.map((diff, i) => <ListGroup.Item key={i}>
          <CardGroup>
            <Card>
              <Card.Body>
                <Card.Title>{props.labels.get("old")}</Card.Title>
                <Card.Text>{styleLines(diff.diff.old.lines)}</Card.Text>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body>
                <Card.Title>{props.labels.get("new")}</Card.Title>
                <Card.Text>{styleLines(diff.diff.new.lines)}</Card.Text>
              </Card.Body>
            </Card>
          </CardGroup>
          <CheckBox
            checked={true}
            id={diff.id}
            {...props}
          />
        </ListGroup.Item>)
      }
    </ListGroup>
  );
}

function Conflict(props) {
  const modifications = [];

  function getModLocation() {
    const modification = {};
    if (props.request === "PAGE") modification.text = props.labels.get("inSection", props.conflict.oldSectionName);
    else if (props.request === "CONTENTS" || props.request === "FILES") {
      const section = props.page.getSection(props.conflict.sectionPath.split("/"));
      modification.text = props.labels.get("inContent", section.name, props.labels.get(props.conflict.contentName));
    }
    else if (props.request === "TEMPLATES") {
      if ("sectionPath" in props.conflict) {
        const section = props.page.getSection(props.conflict.sectionPath.split("/"));
        modification.text = props.labels.get("inSectionTemplate", section.name);
      }
      else modification.text = props.labels.get("inPredefinedTemplate", (props.conflict.templateType.custom === true ? props.conflict.templateType.name : props.labels.get(props.conflict.templateType.name)));
    }
    return modification;
  }
  const modLocation = getModLocation();
  if ("objectRemovals" in props.conflict) {
    modifications.push({title: props.labels.get("dataRemoved"), checked: false, id: props.conflict.objectRemovals.id, ...modLocation});
  }
  if ("objectModifications" in props.conflict) {
    modifications.push({title: props.labels.get("dataModified"), checked: true, id: props.conflict.objectModifications.id, ...modLocation});
  }
  if ("stringModifications" in props.conflict) {
    const modification = {title: props.labels.get("stringModified"), checked: true, ...modLocation};
    let actions = [];
    props.conflict.stringModifications.forEach((stringModification, i) => {
      actions = actions.concat(<StringConflict key={i} stringConflict={stringModification.diff} labels={props.labels} {...props} />);
    });
    modification.actions = actions;
    modifications.push(modification);
  }
  if ("head" in props.conflict) {
    const modification = {title: props.labels.get("head"), checked: true, ...modLocation};
    modification.actions = <StringConflict stringConflict={props.conflict.head} labels={props.labels} {...props} />;
    modifications.push(modification);
  }
  if ("body" in props.conflict) {
    const modification = {title: props.labels.get("body"), checked: true, ...modLocation};
    modification.actions = <StringConflict stringConflict={props.conflict.body} labels={props.labels} {...props} />;
    modifications.push(modification);
  }
  if ("default" in props.conflict) {
    const modification = {title: props.labels.get("defaultFile"), checked: true, ...modLocation};
    modification.actions = <StringConflict stringConflict={props.conflict.default} labels={props.labels} {...props} />;
    modifications.push(modification);
  }
  return (
    <>
      {
        modifications.map((modification, i) => <Card key={i}>
          <Card.Body>
            <Card.Title>{modification.title}</Card.Title>
            <Card.Text>{modification.text}</Card.Text>
            {
              modification.actions ?
              modification.actions
              :
              <CheckBox
                checked={modification.checked}
                id={modification.id}
                {...props}
              />
            }
          </Card.Body>
        </Card>)
      }
    </>
  );
}

function Conflicts(props) {
  return (
    <>
      {
        props.conflicts.map((conflict, i) => <Conflict
          key={i}
          conflict={conflict}
          {...props}
        />)
      }
    </>
  );
}

export default function ConflictList(props) {
  const handleSubmit = (event) => {
    event.preventDefault();
    const decisions = {};
    for (var i = 0; i < event.target.length; i++) {
      if ("name" in event.target[i]) decisions[event.target[i].name] = event.target[i].checked;
    }
    props.onResolveConflict(decisions);
  }

  return (
    <Form onSubmit={handleSubmit}>
      <Accordion>
        {
          Object.keys(props.uploadConflict.conflicts).map((request, i) => <Accordion.Item key={i} eventKey={i}>
            <Accordion.Header>{props.labels.get(request)}</Accordion.Header>
            <Accordion.Body>
              <Conflicts
                request={request}
                conflicts={props.uploadConflict.conflicts[request]}
                labels={props.labels}
                page={props.page}
              />
            </Accordion.Body>
          </Accordion.Item>)
        }
      </Accordion>
      <Button variant="primary" className="my-3 btn-right" type="submit">{props.labels.get("confirm")}</Button>
    </Form>
  );
}
