export default function timely(time, callback) {
	const process = setTimeout(() => {
		callback();
	}, time);
	return () => clearTimeout(process);
}