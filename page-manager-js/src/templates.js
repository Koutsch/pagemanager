export default class Templates {
  constructor() {
    this.templates = [];
  }

  append(data) {
    let template = this.getTemplate(data);
    if (template === null) {
      this.templates.push(data);
    }
    else template.data = data.data;
    return this;
  }

  getTemplate(selection) {
    let match;
    if (selection.ref === "path") match = this.templates.filter((template) => template.ref === selection.ref && template.rel === selection.rel);
    else if (selection.ref === "type") match = this.templates.filter((template) => template.ref === selection.ref && template.rel.name === selection.rel.name && template.rel.custom === selection.rel.custom);
    else return null;
    if (match.length === 0) return null;
    else match = match[0];
    return match;
  }

  get(selection) {
    const template = this.getTemplate(selection);
    if (template === null) return null;
    return template.data;
  }

  getAll() {
    return this.templates;
  }
}
