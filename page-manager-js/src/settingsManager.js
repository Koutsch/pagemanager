import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Stack from 'react-bootstrap/Stack';

export default function SettingsManager(props) {
  const [settings, setSettings] = React.useState(null);
  const [values, setValues] = React.useState({});
  React.useEffect(() => {
    if (settings === null) props.onGetSettings((s) => {
      const tmpValues = values;
      s.forEach(setting => {
        if (setting.value !== undefined && setting.value !== null) tmpValues[setting.name] = setting.value;
        else if (setting.default !== undefined) tmpValues[setting.name] = setting.default;
      });
      setSettings(s);
    });
  });
  const changeValue = (name, value) => {
    setValues({...values, [name]: value})
  }

  return (
		<Stack gap={3}>
      {
        settings !== null ?
        settings.map((setting) => <div key={setting.name}>
          <Form.Label htmlFor="basic-url">{props.labels.get(setting.name)}</Form.Label>
          <InputGroup className="mb-3">
            <Form.Control value={values[setting.name]} onChange={(e) => changeValue(setting.name, e.target.value)} />
            <Button onClick={() => props.onUpdateSetting(setting.name, values[setting.name], () => setSettings(null))} variant="outline-primary">{props.labels.get("submit")}</Button>
            {
              "default" in setting ?
              <Button variant="outline-secondary" onClick={() => props.onUpdateSetting(setting.name, null, () => setSettings(null))}>
                {props.labels.get("useDefault")}: {setting.default}
              </Button>
              :
              null
            }
          </InputGroup>
        </div>)
        :
        null
      }
		</Stack>
	)
}