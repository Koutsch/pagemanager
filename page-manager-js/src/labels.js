export default class Labels {
  constructor(labels) {
    this.labels = labels;
  }

  get(labelName, ...args) {
    let label = this.labels[labelName];
    if (label === undefined || label === null) return labelName;
    for (let i = 0; i < args.length; i++) {
      label = label.replaceAll(`{{{${i}}}}`, args[i]);
    }
    return label;
  }
}
