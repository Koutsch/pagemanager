import React from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import Stack from 'react-bootstrap/Stack';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Alert from 'react-bootstrap/Alert';
import BForm from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from "@rjsf/material-ui";
import validator from '@rjsf/validator-ajv8';
import PageTreeView from './pageTreeView.js';
import Delete from '@mui/icons-material/Delete';
import Edit from '@mui/icons-material/Edit';
import AssignmentTurnedIn from '@mui/icons-material/AssignmentTurnedIn';
import { addTitles } from './schemas.js';

function userSort(a, b) {
  let nameA = a[1].fullName.toUpperCase();
  let nameB = b[1].fullName.toUpperCase();
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  return 0;
}

export function UserAdder(props) {
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>{props.labels.get("addUser")}</Breadcrumb.Item>
      </Breadcrumb>
      <Form
        validator={validator}
        schema={addTitles(props.userSchema, props.labels)}
        onSubmit={(data) => props.onAddUser(data.formData, () => props.registry.unsetRegistry(() => {}))}
        onChange={() => props.registry.setRegistry()}
      />
    </>
  );
}

export function UserEditor(props) {
  const [usersLoaded, setUsersLoaded] = React.useState({});
  const [view, setView] = React.useState(["l"]);
  const handleSetView = (id, action) => {
    props.onGetUser(id, (user) => {
      setView([action, id, user]);
    });
  };
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setView(["l"]))}>{props.labels.get("editUsers")}</Breadcrumb.Item>
        {
          view[0] === "e" ?
          <Breadcrumb.Item active>{props.labels.get("editUser", view[2].username)}</Breadcrumb.Item>
          :
          view[0] === "p" ?
          <Breadcrumb.Item active>{props.labels.get("assignPrivilegesToUser", view[2].username)}</Breadcrumb.Item>
          :
          null
        }
      </Breadcrumb>
      {
        view[0] === "l" ?
        <UserSearch
          onUserSearch={(searchStr) => props.onUserSearch(searchStr, false, (filteredUsers) => {
            setUsersLoaded({...usersLoaded, ...filteredUsers});
          })}
          onClearUsers={() => setUsersLoaded({})}
          usersLoaded={usersLoaded}
          labels={props.labels}
          actions={(user) => <>
            <Button onClick={() => handleSetView(user[0], "e")}><Edit /> {props.labels.get("edit")}</Button>
            <Button onClick={() => handleSetView(user[0], "p")}><AssignmentTurnedIn /> {props.labels.get("assignPrivileges")}</Button>
            <Button className="btn-danger" onClick={() => props.onRemoveUser(user[0], user[1].username, () => {
              delete usersLoaded[user[0]];
              setUsersLoaded({...usersLoaded});
            })}><Delete /> {props.labels.get("remove")}</Button>
          </>}
        />
        :
        view[0] === "e" ?
        <>
          {
            view[2].pin !== undefined ?
            <Alert>{props.labels.get("noPassword")} {props.labels.get("pin")}: {view[2].pin}</Alert>
            :
            null
          }
          <Form
            validator={validator}
            schema={addTitles(props.userSchema, props.labels)}
            onSubmit={(data) => props.onEditUser(data.formData, view[1], () => {
              props.registry.unsetRegistry(() => {
                usersLoaded[view[1]] = data.formData;
                setUsersLoaded({...usersLoaded});
                handleSetView(view[1], "e");
              });
            })}
            formData={view[2]}
            onChange={() => props.registry.setRegistry()}
          />
        </>        
        :
        view[0] === "p" ?
        <PrivilegeAssigner
          user={view[2]}
          onAssignPrivileges={(path) => props.onPrivilegeAssigned(path, view[1], () => handleSetView(view[1], "p"))}
          onRemovePrivilege={(path) => props.onPrivilegeRemoved(path, view[1], view[2].fullName, () => handleSetView(view[1], "p"))}
          {...props}
        />
        :
        null
      }
    </>
  );
}

export function UserList(props) {
  return (
    <Stack gap={3}>
    {
      props.usersLoaded !== null ?
      Object.entries(props.usersLoaded).sort(userSort).map((user, i) =>
      <Row key={i}>
        <Col sm={4} className="mb-1 align-items-center d-grid">{user[1].fullName} ({user[1].username})</Col>
        <Col sm={8} className="mb-2 align-items-center d-grid">
          <ButtonGroup>
            {React.createElement(props.actions, user)}
          </ButtonGroup>
        </Col>
      </Row>
      )
      :
      null
    }
    </Stack>
  );
}

export function UserSearch(props) {
  const [searchStr, setSearchStr] = React.useState("");
  return (
    <>
      <InputGroup className="mb-3">
        <BForm.Control type="text" value={searchStr} onChange={(e) => setSearchStr(e.target.value)} />
        <Button onClick={() => props.onUserSearch(searchStr)}>{props.labels.get("search")}</Button>
        <Button onClick={props.onClearUsers}>{props.labels.get("clear")}</Button>
      </InputGroup>
      <UserList
        {...props}
      />
    </>
  );
}

function PrivilegeAssigner(props) {
  return (
    props.page.getSection() !== null ?
    <Row>
      <Col sm>
        <Container className="p-3 my-3 border">
          <h4>{props.labels.get("assignPrivileges")}</h4>
          <PageTreeView
            languages={props.languages}
            page={props.page}
            onNodeSelect={(path) => props.onAssignPrivileges(path)}
            defaultExpanded={[props.page.getSection().id.toString()]}
            height={300}
            buttonText={props.labels.get("select")}
          />
        </Container>
      </Col>
      <Col sm>
        <Container className="p-3 my-3 border">
          <h4>{props.labels.get("privileges")}</h4>
          <Stack gap={3}>
            {
              "privileges" in props.user && props.user.privileges !== null ?
              props.user.privileges.sort().map((privilege, i) => {
                const section = props.page.getSection(privilege.split("/").map(Number));
                return (<InputGroup key={i}>
                  <InputGroup.Text>{section.title[props.languages.defaultLanguage]}</InputGroup.Text>
                  <Button className="btn-danger" onClick={() => props.onRemovePrivilege(privilege)}><Delete /> {props.labels.get("remove")}</Button>
                </InputGroup>);
              })
              :
              null
            }
          </Stack>
        </Container>
      </Col>
    </Row>
    :
    null
  );
}
