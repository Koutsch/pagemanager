import React from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Form from 'react-bootstrap/Form';
import Stack from 'react-bootstrap/Stack';
import InputGroup from 'react-bootstrap/InputGroup';
import Edit from '@mui/icons-material/Edit';
import Delete from '@mui/icons-material/Delete';
import Editor from "@monaco-editor/react";
import FileChooser from './fileChooser.js';
import timely from './timely.js';

export default function FileDataManager(props) {
  const editorRef = React.useRef(null);
  const monacoRef = React.useRef();
  const [emptyFileName, setEmptyFileName] = React.useState("");
  const [view, setView] = React.useState({view: null, data: null, nav: null});

  const setData = (view, name) => {
    const newView = {view: view, data: null, nav: null};
    if (view === "default") props.onLoadDefault((defaultData) => {
      newView.data = {file: defaultData};
      setView(newView);
    });
    else if (view === "file") props.onLoadFile(name, (fileLoaded) => {
      newView.data = {name: name, file: fileLoaded};
      newView.nav = "list";
      setView(newView);
    });
    else if (view === "list") props.onLoadList((list) => {
      newView.data = list;
      setView(newView);
    });
    else setView(newView);
  };

  React.useEffect(() => {
    if (view.view === "file") setData(view.view, view.data.name);
    else if (view.view !== null) setData(view.view);
  }, [props.data]);

  React.useEffect(() => {
    const timed = timely(3000, () => {
      if ((view.view === "file" || view.view === "default") && view.data.change === true) handleSubmit();
    });
    return timed;
  }, [view]);

  const handleInsert = (text) => {
    if (editorRef.current === null) return;
    const line = editorRef.current.getPosition();
    editorRef.current.executeEdits("",
    [
      {
        range: new monacoRef.current.Range(line.lineNumber,
          line.column,
          line.lineNumber,
          line.column),
        text: text
      }
    ]);
  }

  const handleSubmit = () => {
    if (editorRef.current === null) return;
    const file = editorRef.current.getValue();
    const name = view.data.name;
    if (name === undefined) props.onSubmitDefault(file, () => props.registry.unsetRegistry(() => {}));
    else props.onSubmitDataFile(name, file, () => props.registry.unsetRegistry(() => {}));
  };

  const handleRemoveFile = (name) => {
    props.onRemoveFile(name, () => {
      setData("list");
    });
  };

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setData(null))}>{props.labels.get(props.title)}</Breadcrumb.Item>
        {
          view.nav === "list" ?
          <>
            <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setData("list"))}>{props.labels.get("fileList")}</Breadcrumb.Item>
            <Breadcrumb.Item active>{view.data.name}</Breadcrumb.Item>
          </>          
          :
          view.view === "list" ?
          <Breadcrumb.Item active>{props.labels.get("fileList")}</Breadcrumb.Item>
          :
          view.view === "default" ?
          <Breadcrumb.Item active>{props.labels.get(props.defaultName)}</Breadcrumb.Item>
          :
          view.view === "upload" ?
          <Breadcrumb.Item active>{props.labels.get(props.uploadLabel)}</Breadcrumb.Item>
          :
          view.view === "empty" ?
          <Breadcrumb.Item active>{props.labels.get("emptyFile")}</Breadcrumb.Item>
          :
          null
        }
      </Breadcrumb>
      {
        view.view === null ?
        <Stack gap={3}>
          <Button onClick={() => props.registry.getRegistry(() => setData("default"))}>{props.labels.get(props.defaultName)}</Button>
          <Button onClick={() => props.registry.getRegistry(() => setData("list"))}>{props.labels.get(props.listName)}</Button>
          <Button onClick={() => props.registry.getRegistry(() => setData("upload"))}>{props.labels.get(props.uploadLabel)}</Button>
          <Button onClick={() => props.registry.getRegistry(() => setData("empty"))}>{props.labels.get("emptyFile")}</Button>
        </Stack>
        :
        view.view === "list" ?
        <Stack gap={3}>
          {
            view.data.filter((file) => file.remove !== true).sort((a, b) => {
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            }).map((file, i) => <Stack key={i} direction="horizontal" gap={3}>
              <div className="p-2">{file.name}</div>
              <ButtonGroup className="ms-auto p-2">
                <Button onClick={() => setData("file", file.name)}><Edit /> {props.labels.get("edit")}</Button>
                <Button className="btn-danger" onClick={() => handleRemoveFile(file.name)}><Delete /> {props.labels.get("remove")}</Button>
              </ButtonGroup>
            </Stack>)
          }
        </Stack>
        :
        view.view === "default" || view.view === "file" ?
        <>
          {
            "editor" in props && typeof props.editor === "function" ?
            <div>
              {
                React.createElement(props.editor, {
                  insert: handleInsert
                })
              }
            </div>            
            :
            null
          }
          <Editor
            className="my-3"
            height="100vh"
            defaultLanguage={props.syntax}
            value={view.data.file}
            onMount={(e, m) => {
              editorRef.current = e; 
              monacoRef.current = m; 
              editorRef.current.getModel().updateOptions({tabSize: 2})
            }}
            onChange={() => {
              props.registry.setRegistry();
              const newView = {...view};
              newView.data.file = editorRef.current.getValue();
              newView.data.change = true;
              setView(newView);
            }}
          />
        </>
        :
        view.view === "upload" ?
        <FileChooser
          label={props.labels.get(props.uploadLabel)}
          submitLabel={props.labels.get("upload")}
          onSubmit={(file) => props.onUpload(file, () => setData("list"))}
          accept={props.accept}
        />
        :
        view.view === "empty" ?
        <InputGroup className="my-3">
          <Form.Control type="text" onChange={(e) => setEmptyFileName(e.target.value)} placeholder={props.labels.get("filename")} />
          <Button onClick={() => props.onCreateEmptyFile(emptyFileName, () => setData("list"))}>{props.labels.get("submit")}</Button>
        </InputGroup>
        :
        null
      }
    </>
  )
}
