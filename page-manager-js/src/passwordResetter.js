import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function PasswordResetter(props) {
	const [curP, setCurP] = React.useState("");
	const [newP, setNewP] = React.useState("");
	const [repeatP, setRepeatP] = React.useState("");
  const [passwordStrength, setPasswordStrength] = React.useState("");
	const handleSubmit = (e) => {
		props.onSubmitPassword(curP, newP, repeatP);
		e.preventDefault();
	}
  const strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
  const mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
  React.useEffect(() => {
    if (newP.length < 1) setPasswordStrength("");
    else if (strongPassword.test(newP)) {
      setPasswordStrength(props.labels.get("strongPassword"));
    }
    else if (mediumPassword.test(newP)) {
      setPasswordStrength(props.labels.get("mediumPassword"));
    }
    else {
      setPasswordStrength(props.labels.get("weakPassword"));
    }
  }, [newP]);

  return (
		<Form onSubmit={handleSubmit}>
      <Form.Group className="mb-3" controlId="currentPassword">
        <Form.Label>{props.labels.get("currentPassword")}</Form.Label>
        <Form.Control value={curP} onChange={(e) => setCurP(e.target.value)} type="password" />
      </Form.Group>
			<Form.Group className="mb-1" controlId="newPassword">
        <Form.Label>{props.labels.get("newPassword")}</Form.Label>
        <Form.Control value={newP} onChange={(e) => setNewP(e.target.value)} type="password" />
      </Form.Group>
      <Form.Text className="mb-3">{passwordStrength}</Form.Text>
			<Form.Group className="mb-3" controlId="repeatPassword">
        <Form.Label>{props.labels.get("passwordLabelNewRepeat")}</Form.Label>
        <Form.Control value={repeatP} onChange={(e) => setRepeatP(e.target.value)} type="password" />
      </Form.Group>
      <Button variant="success" type="submit">
				{props.labels.get("submit")}
      </Button>
    </Form>
	);
}