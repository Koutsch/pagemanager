import './App.css';
import PageManager from './PageManager.js';

function App() {
  return (
    <PageManager />
  );
}

export default App;
