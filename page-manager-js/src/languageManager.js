import * as React from 'react';
import BForm from 'react-bootstrap/Form';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Alert from 'react-bootstrap/Alert';
import Form from "@rjsf/material-ui";
import validator from '@rjsf/validator-ajv8';
import { addTitles } from './schemas.js';

export function LanguageEditor(props) {
  const languages = props.languages;
  let formData = (props.languages !== null && "languages" in languages ? languages.languages : null);
  const languageSchema = addTitles(props.languageSchema, props.labels);
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>{props.labels.get("editLangs")}</Breadcrumb.Item>
      </Breadcrumb>
      <Form
        schema={languageSchema}
        formData={formData}
        onChange={() => props.registry.setRegistry()}
        onSubmit={(data) => props.registry.unsetRegistry(() => props.onLanguagesEdit(data.formData))}
        validator={validator}
      />
    </>
  );
}

export function DefaultLanguageChooser(props) {
  let listItems = null;
  
  if (props.languages !== null && "languages" in props.languages) {
    listItems = props.languages.languages.map((language) =>
      <option key={language.iso639} value={language.iso639}>{language.name} ({language.iso639})</option>
    );
  }
  return (
    <>
      {
        props.languages !== null && "languages" in props.languages ?
        <>
          <Breadcrumb>
            <Breadcrumb.Item>{props.labels.get("chooseDefaultLang")}</Breadcrumb.Item>
          </Breadcrumb>
          <BForm.Select onChange={(e) => props.onChooseDefaultLang(e.target.value)} name="chosenLanguage" value={props.languages.defaultLanguage}>
            <option></option>
            { listItems }
          </BForm.Select>
        </>
        :
        <Alert variant="warning">{props.labels.get("noLanguages")}</Alert>
      }
    </>
  );
}
