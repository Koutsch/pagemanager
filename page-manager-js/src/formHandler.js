import React, { useEffect } from 'react';
import Form from "@rjsf/material-ui";
import validator from '@rjsf/validator-ajv8';
import Table from 'react-bootstrap/Table'
import { addLangInput, addTitles } from './schemas.js';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Nav from 'react-bootstrap/Nav';
import Stack from 'react-bootstrap/Stack';
import InputGroup from 'react-bootstrap/InputGroup';

class InputTypeField extends React.Component {
	constructor(props) {
		super(props);
		this.state = {value: props.formData};
	}

	onChange(value) {
		this.setState({
			value: value
		}, () => this.props.onChange(value));
	}

	render() {
		return (
			<>
				<p>{this.props.formContext.labels.get("formType")}{(this.props.required === true? "*" : "")}</p>
				<select value={this.state.value} onChange={(e) => this.onChange(e.target.value)}>
					<option value=""></option>
					<option value="text">{this.props.formContext.labels.get("singleLine")}</option>
					<option value="textarea">{this.props.formContext.labels.get("multiLine")}</option>
					<option value="email">{this.props.formContext.labels.get("email")}</option>
					<option value="checkbox">{this.props.formContext.labels.get("checkbox")}</option>
					<option value="select">{this.props.formContext.labels.get("select")}</option>
				</select>
			</>      
		);
	}
}

function FormEditor(props) {
	const schema = React.useMemo(() => {
		const schema = addTitles(props.formSchema, props.labels);
		schema.$defs.langInput = addLangInput(props.languages);
		return schema;
	}, [props.formSchema, props.labels, props.languages]);
	const [formData, setFormData] = React.useState(props.formData);
	return (
		<Form
			schema={schema}
			formData={formData}
			validator={validator}
			uiSchema={{
				"inputs": {
					"items": {
						"type": {
							"ui:field": "inputTypeField"
						}
					}
				}
			}}
			fields={{inputTypeField: InputTypeField}}
			formContext={{labels: props.labels}}
			onChange={(data) => props.registry.setRegistry(() => setFormData(data.formData))}
			onSubmit={(data) => props.registry.unsetRegistry(() => props.onSubmitForm(data.formData, props.currentSectionPath, props.index))}
		/>
	);
}

function FormDataHandler(props) {
	const [formData, setFormData] = React.useState(props.formData);

	const downloadAsCSV = () => {
		const header = props.form.inputs;
		const csv = [
			header.map(input => input.title[props.languages.defaultLanguage]).join(", "),
			...formData.map(row => row.join(", "))
		].join('\r\n');
		const MIME_TYPE = "text/csv";
    const blob = new Blob([csv], {type: MIME_TYPE});
    window.open(window.URL.createObjectURL(blob), '_blank');
	};

	return (
		<>
			<h4>{props.form.title[props.languages.defaultLanguage]}</h4>
			<ButtonGroup>
				<Button className="btn-danger" onClick={() => props.onClearFormData(props.currentSectionPath, props.form.id, () => setFormData([]))}>{props.labels.get("clear")}</Button>
				<Button onClick={downloadAsCSV}>{props.labels.get("asCSV")}</Button>
			</ButtonGroup>			
			<Table responsive="md">
				<thead>
					<tr>
						{
							props.form.inputs.map((input, i) => <th key={i}>{input.title[props.languages.defaultLanguage]}</th>)
						}
					</tr>          
				</thead>
				<tbody>
					{
						formData.map((data, j) => <tr key={j}>
							{
								data.map((field, k) => 
								{
									if (props.form.inputs[k].type === "select")
									return (
										<td key={k}>{props.form.inputs[k].options[field]?.title[props.languages.defaultLanguage]}</td>
									);
									else return (
										<td key={k}>{field}</td>
									);                 
								})
							}
						</tr>)
					}
				</tbody>
			</Table>
		</>
	)
}

function getForms(forms, sectionPath) {
	const strPath = sectionPath.join("/");
	if (forms[strPath] === undefined) return null;
	return forms[strPath];
}

export default function FormHandler(props) {
	const [activeKey, setActiveKey] = React.useState({key: "formList", nav: "formList"});
	const [rerender, setRerender] = React.useState(false);
	useEffect(() => {
		if (activeKey.key === "editForm") {
			const curForms = getForms(props.forms, props.currentSectionPath);
			if (curForms === null || curForms.length <= activeKey.index) setActiveKey({key: "formList", nav: "formList"});
			else setRerender(true);
		}
		else setRerender(true);
	},[activeKey, props.forms, props.currentSectionPath]);
	useEffect(() => {
		if (rerender === true) setRerender(false);
	},[rerender]);
	return (
		rerender === false ?
		<>
			<Nav variant="tabs" className="mb-3" activeKey={activeKey.nav}>
				<Nav.Item>
					<Nav.Link eventKey="addForm" onClick={() => props.registry.getRegistry(() => setActiveKey({key: "addForm", nav: "addForm"}))}>{props.labels.get("addForm")}</Nav.Link>
				</Nav.Item>
				<Nav.Item>
					<Nav.Link eventKey="formList" onClick={() => props.registry.getRegistry(() => props.onGetForms(props.currentSectionPath, () => setActiveKey({key: "formList", nav: "formList"})))}>{props.labels.get("formList")}</Nav.Link>
				</Nav.Item>
			</Nav>
			{
				activeKey.key === "addForm" ?
				<FormEditor
					formData={null}
					index={null}
					{...props}
				/>
				:
				activeKey.key === "formList" ?
				<Stack gap={3}>
					{
						getForms(props.forms, props.currentSectionPath).map((form, i) => <InputGroup key={i}>
						<InputGroup.Text>{form.title[props.languages.defaultLanguage]}</InputGroup.Text>
							<Button onClick={() => props.onChangeActiveStatus(props.currentSectionPath, i)} className={(form.active === true ? "btn-success" : "btn-danger")}>{(form.active === true ? props.labels.get("active") : props.labels.get("inactive"))}</Button>
							{
								form.hasData !== true ?
								<>
									<Button onClick={() => setActiveKey({key: "editForm", nav: "formList", index: i})}>{props.labels.get("edit")}</Button>
									<Button onClick={() => props.onRemoveForm(props.currentSectionPath, i)} className='btn-danger'>{props.labels.get("remove")}</Button>
								</>
								:
								<Button onClick={() => props.onGetFormData(props.currentSectionPath, form.id, (formData) => setActiveKey({key: "formData", nav: "formList", formData: formData, index: i}))}>{props.labels.get("formData")}</Button>
							}              
						</InputGroup>)
					}
				</Stack>
				:
				activeKey.key === "editForm" ?
				<FormEditor
					formData={getForms(props.forms, props.currentSectionPath)[activeKey.index]}
					index={activeKey.index}
					{...props}
				/>
				:
				activeKey.key === "formData" ?
					<FormDataHandler
						form={getForms(props.forms, props.currentSectionPath)[activeKey.index]}
						formData={activeKey.formData}
						{...props}
					/>
				:
				null
			}
		</>
		:
		null
	);
}