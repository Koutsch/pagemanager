import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export default function FileChooser(props) {
  const [selectedFile, setSelectedFile] = React.useState(null);

  const onFileChange = event => {
    setSelectedFile(event.target.files[0]);
  };

  const attributes = {
    type: "file",
    onChange: onFileChange
  };
  if ("accept" in props) attributes.accept = props.accept;
  return (
    <>
      <Form.Group controlId="formFileLg" className="mb-3">
        <Form.Label>{props.label}</Form.Label>
        <Form.Control {...attributes} />
      </Form.Group>
      <Button onClick={() => props.onSubmit(selectedFile)}>{props.submitLabel}</Button>
    </>
  );
}
