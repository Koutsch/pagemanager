import React, { useRef, useEffect } from 'react';
import Form from "@rjsf/material-ui";
import validator from '@rjsf/validator-ajv8';
import BForm from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Stack from 'react-bootstrap/Stack';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Add from '@mui/icons-material/Add';
import Delete from '@mui/icons-material/Delete';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { addLangInput, addTitles } from './schemas.js';
import Dialog from './dialog.js';
import CustomFileWidget from './customFileWidget.js';

const contentSort = (a, b) => {
  if (a.title < b.title) return -1;
  if (a.title > b.title) return 1;
  return 0;
};

export function ContentList(props) {
  let addContent, contents = {};
  if (props.currentSectionSchema.content === true) {
    addContent = true;
    if ("content" in props.currentSection) contents = props.currentSection.content;
  }
  else {
    addContent = false;
    contents = props.currentSectionSchema.content;
  }
  const sortedContent = [];
  if (contents.predefined !== undefined) Object.keys(contents.predefined).forEach((key) => sortedContent.push({title: props.labels.get(key), content: contents.predefined[key], key: key, custom: false}));
  if (contents.custom !== undefined) Object.keys(contents.custom).forEach((key) => sortedContent.push({title: key, content: contents.custom[key], key: key, custom: true}));
  sortedContent.sort(contentSort);
  return (
    <Stack gap={3}>
      {
        addContent ?
        <Button className="btn-success" onClick={props.onAddContent}><Add /> {props.labels.get("addContent")}</Button>
        :
        null
      }
      {
        sortedContent.map((content, i) => <Button key={i} onClick={() => props.onChooseContent({name: content.key, type: content.content.type, custom: content.custom, addContent: addContent})}>{content.title}</Button>)
      }
    </Stack>
  );
}

class QuillTextarea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {...props.formData};
    this.createTextarea = this.createTextarea.bind(this);
  }

  onChange(value, name) {
    if (value === "<p><br></p>") value = "";
    this.setState({
      [name]: value
    }, () => this.props.onChange(this.state));
  }

  createTextarea(language) {
    const curValue = this.state[language.iso639];
    const modules = {
      toolbar: [
        [{ 'header': [1, 2, 3, 4, 5, false] }],
        [{ 'size': ['small', false, 'large', 'huge'] }],
        ['bold', 'italic', 'underline','strike', 'blockquote'],
        [{ 'color': [] }, { 'background': [] }], 
        [{'list': 'ordered'}, {'list': 'bullet'}],
        [{'indent': '-1'}, {'indent': '+1'}],
        [{ 'align': [] }],
        ['link', 'image', 'video'],
        ['clean']
      ],
    };
    const formats = [
      'header',
      'size',
      'bold', 'italic', 'underline', 'strike', 'blockquote',
      'color', 'background',
      'list', 'bullet', 'indent',
      'link', 'image', 'video',
      'align'
    ];
    return (
      <Container key={language.iso639} className="px-0 mx-0 my-3">
        <p>{language.name}{this.props.schema.required.includes(language.iso639) ? "*" : ""}</p>
        <ReactQuill
          theme="snow"
          modules={modules}
          formats={formats}
          defaultValue={curValue}
          onChange={(value, delta, source, editor) => {
            if (source === "user") this.onChange(value, language.iso639);
          }}
        />
      </Container>
    );
  }

  render() {
    return (
      <Container className="py-2 my-2 border">
        <h4>{this.props.schema.title}</h4>
        {
          this.props.formContext.languages.map((language) => this.createTextarea(language))
        }
      </Container>
    );
  }
}
const fields = {quillTextarea: QuillTextarea};
const widgets = {FileWidget: CustomFileWidget};

export function ContentAdder(props) {
  const inputRef = useRef(null);
  const contentSchemas = [];
  Object.keys(props.contentSchemas).forEach((key) => {
    contentSchemas.push({title: props.labels.get(key), key: key});
  });
  contentSchemas.sort(contentSort);
  return (
    <>
      <BForm.Group className="mb-3">
        <BForm.Label>{props.labels.get("contentName")}</BForm.Label>
        <BForm.Control name="contentName" ref={inputRef} type="text" />
      </BForm.Group>
      <Row>
        <Col sm className="p-3 m-3 border">
          <h4>{props.labels.get("predefined")}</h4>
          <Stack gap={3}>
            {
              contentSchemas.map((contentSchema, i) => <Button key={i} onClick={() => props.onContentAdded(contentSchema.key, inputRef.current.value, false)}>{contentSchema.title}</Button>)
            }
          </Stack>
        </Col>
        <Col sm className="p-3 m-3 border">
          <h4>{props.labels.get("custom")}</h4>
          <Stack gap={3}>
            {
              Object.keys(props.customContentSchemas).sort().map((contentSchema, i) => <Button key={i} onClick={() => props.onContentAdded(contentSchema, inputRef.current.value, true)}>{contentSchema}</Button>)
            }
          </Stack>
        </Col>
      </Row>
    </>
  );
}

function getContentSchema(schemas, type, labels, languages) {
  const contentSchema = schemas[type];
  const schema = addTitles(contentSchema.schema, labels);
  schema.$defs.langInput =
    addLangInput(languages);
  schema.labels = labels;
  return {schema: schema, uiSchema: contentSchema.uiSchema};
}

function ContentForm(props) {
  const [rerender, setRerender] = React.useState(false);
  useEffect(() => {
    if (props.contentSchema !== null) setRerender(true);
  },[props.contents, props.contentSchema]);
  useEffect(() => {
    if (rerender === true) setRerender(false);
  },[rerender]);
  if (rerender === false && props.contentSchema !== null) {
    return (
      <Form
        schema={props.contentSchema.schema}
        uiSchema={props.contentSchema.uiSchema}
        validator={validator}
        fields={fields}
        widgets={widgets}
        formContext={{languages: props.languages.languages}}
        formData={props.contentData}
        onSubmit={(data) => props.onDataChange({action: "ce", formData: data.formData, path: props.currentSectionPath, name: props.content.name, custom: props.content.custom}, () => props.registry.unsetRegistry(() => {}))}
        onChange={() => props.registry.setRegistry()}
      />
    );
  }
  return null;
}

export function ContentEditor(props) {
  const dialogRef = useRef(null);
  const [activeKey, setActiveKey] = React.useState(null);
  let contentSchema = null, contentData = null, schemas, part;
  if (props.content.custom === true) {
    schemas = props.customContentSchemas;
    part = "custom";
  }
  else {
    schemas = props.contentSchemas;
    part = "predefined";
  }
  if (schemas[props.content.type].schema !== false) {
    contentSchema = getContentSchema(schemas, props.content.type, props.labels, props.languages);
    const pathString = props.currentSectionPath.join("/");
    if (pathString in props.contents && part in props.contents[pathString] && props.content.name in props.contents[pathString][part]) contentData = props.contents[pathString][part][props.content.name];
  }
  return (
    <>
      <Dialog
        ref={dialogRef}
      />
      <Nav variant="tabs" className="mb-3" activeKey={activeKey}>
        <Nav.Item>
          <Nav.Link eventKey="title" className="text-dark" onClick={() => props.registry.getRegistry(() => setActiveKey(null))}>{(props.content.addContent === false && props.content.custom === false ? props.labels.get(props.content.name) :  props.content.name)} ({(props.content.custom === true ? props.content.type : props.labels.get(props.content.type))})</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="remove" className="text-danger" onClick={() => dialogRef.current.showDialog({
            "title": props.labels.get("remove"),
            "text": props.labels.get("removeContentConfirm"),
            "callback": props.onRemoveContent
          })}><Delete /> {props.labels.get("remove")}</Nav.Link>
        </Nav.Item>
      </Nav>
      {
        schemas[props.content.type].schema !== false ?
        <ContentForm
          contentSchema={contentSchema}
          contentData={contentData}
          {...props}
        />
        :
        null
      }
    </>
  );
}
