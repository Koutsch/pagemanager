import * as React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TreeView from '@mui/lab/TreeView';
import TreeItem, { treeItemClasses } from '@mui/lab/TreeItem';
import Typography from '@mui/material/Typography';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

const StyledTreeItemRoot = styled(TreeItem)(({ theme }) => ({
  color: theme.palette.text.secondary,
  [`& .${treeItemClasses.content}`]: {
    color: theme.palette.text.secondary,
    borderTopRightRadius: theme.spacing(2),
    borderBottomRightRadius: theme.spacing(2),
    paddingRight: theme.spacing(1),
    fontWeight: theme.typography.fontWeightMedium,
    '&.Mui-expanded': {
      fontWeight: theme.typography.fontWeightRegular,
    },
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
    },
    '&.Mui-focused, &.Mui-selected, &.Mui-selected.Mui-focused': {
      backgroundColor: `var(--tree-view-bg-color, ${theme.palette.action.selected})`,
      color: 'var(--tree-view-color)',
    },
    [`& .${treeItemClasses.label}`]: {
      fontWeight: 'inherit',
      color: 'inherit',
    },
  },
  [`& .${treeItemClasses.group}`]: {
    marginLeft: theme.spacing(1),
    [`& .${treeItemClasses.content}`]: {
      paddingLeft: theme.spacing(1),
    },
  },
}));

function StyledTreeItem(props) {
  const {
    nodeTitle,
    actions,
    toggleExpand,
    onRemoveInput,
    removeText,
    showRemove,
    requireable,
    onRequire,
    requireText,
    color,
    ...other
  } = props;

  return (
    <StyledTreeItemRoot
      label={
        <Box sx={{ display: 'flex', alignItems: 'center', p: 0.5, pr: 0 }}>
          <Typography onClick={() => props.toggleExpand(props.nodeId)} variant="body2" sx={{color: color, fontWeight: 'inherit', flexGrow: 1 }}>
            {nodeTitle}
          </Typography>
          {
            props.actions.map((action, i) => <Typography pl={2} key={i} variant="caption" color="inherit">
              <div onClick={() => {
                action.func(props.nodeId)
              }}>{action.name}</div>
            </Typography>)
          }
          {
            showRemove ?
            <Typography pl={2} variant="caption" color="inherit">
              <div onClick={() => {
                props.onRemoveInput(props.nodeId)
              }}>{removeText}</div>
            </Typography>
            :
            null
          }
          {
            requireable ?
            <Typography pl={2} variant="caption" color="inherit">
              <div onClick={() => {
                props.onRequire(props.nodeId)
              }}>{requireText}</div>
            </Typography>
            :
            null
          }
        </Box>
      }
      style={{
        '--tree-view-color': "black",
        '--tree-view-bg-color': "white",
      }}
      {...other}
    />
  );
}

StyledTreeItem.propTypes = {
  nodeTitle: PropTypes.string,
  actions: PropTypes.array,
  toggleExpand: PropTypes.func,
  onRemoveInput: PropTypes.func,
  onRequire: PropTypes.func,
  removeText: PropTypes.string,
  showRemove: PropTypes.bool,
  color: PropTypes.string,
  requireable: PropTypes.bool,
  requireText: PropTypes.string
};

export default function SchemaTreeView(props) {
  const [expanded, setExpanded] = React.useState([]);
  const toggleExpand = (id) => {
    const index = expanded.indexOf(id);
    const copyExpanded = [...expanded];
    if (index === -1) copyExpanded.push(id);
    else copyExpanded.splice(index, 1);
    setExpanded(copyExpanded);
  };

  const renderTree = (node, path) => {
    if (path === undefined) path = [node.name];
    else path = path.concat(node.name);
    const pathStr = path.join("/");

    const actions = [];
    let showRemove = true;
    if (path.length === 1) showRemove = false;
    let nodeTitle = `${node.name} (${props.labels.get(node.type)})`;
    let color = "black";
    let requireable = false;
    if (node.type !== "option" && path.length > 1) requireable = true;
    switch (node.type) {
      case 'object':
        actions.push({
          "name": props.labels.get("add"),
          "func": props.addToObject
        });
        color = "blue";
        break;
      case 'array':
        actions.push({
          "name": props.labels.get("setItems"),
          "func": props.setItems
        });
        color = "red";
        break;
      case 'regex':
        actions.push({
          "name": props.labels.get("setRegex"),
          "func": props.setRegex
        });
        if ("regex" in node) nodeTitle += ` – ${node.regex}`;
        break;
      case 'selector':
        actions.push({
          "name": props.labels.get("addOption"),
          "func": props.addOption
        });
        color = "green";
        break;
      default:
        break;
    }

    let isRequired;
    if (node.required === true) isRequired = props.labels.get("required");
    else isRequired = props.labels.get("optional");

    return (
      <StyledTreeItem
        key={pathStr}
        toggleExpand={toggleExpand}
        onRemoveInput={props.onRemoveInput}
        onRequire={props.onRequire}
        removeText={props.labels.get("remove")}
        showRemove={showRemove}
        nodeId={pathStr}
        nodeTitle={nodeTitle}
        actions={actions}
        color={color}
        requireable={requireable}
        requireText={isRequired}
      >
        {
          Array.isArray(node.properties) ?
          node.properties.map((subnode) => renderTree(subnode, path))
          : "items" in node ?
          renderTree(node.items, path)
          : "options" in node ?
          node.options.map((subnode) => renderTree(subnode, path))
          : null
        }
      </StyledTreeItem>
    );
  };

  return (
    <TreeView
      expanded={expanded}
      defaultExpanded={[props.defaultExpanded]}
      onNodeSelect={(e, id) => {
      }}
      defaultCollapseIcon={<ArrowDropDownIcon />}
      defaultExpandIcon={<ArrowRightIcon />}
      defaultEndIcon={<div style={{ width: 24 }} />}
      sx={{ height: 264, flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
    >
      {renderTree(props.schema)}
    </TreeView>
  );
}
