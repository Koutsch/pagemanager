import React from 'react';
import Form from "@rjsf/material-ui";
import validator from '@rjsf/validator-ajv8';
import { createBasicSectionSchema } from './schemas.js';
import { ContentList, ContentAdder, ContentEditor } from './contentEditor.js';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Stack from 'react-bootstrap/Stack';
import Container from 'react-bootstrap/Container';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Add from '@mui/icons-material/Add';
import Delete from '@mui/icons-material/Delete';
import ArtTrackIcon from '@mui/icons-material/ArtTrack';
import Check from '@mui/icons-material/Check';
import Close from '@mui/icons-material/Close';
import Edit from '@mui/icons-material/Edit';
import AccountTree from '@mui/icons-material/AccountTree';
import Preview from '@mui/icons-material/Preview';
import AssignmentTurnedIn from '@mui/icons-material/AssignmentTurnedIn';
import FilePresent from '@mui/icons-material/FilePresent';
import ListAlt from '@mui/icons-material/ListAlt';
import Dialog from './dialog.js';
import { UserSearch, UserList } from './userManager.js';
import PageTreeView from './pageTreeView.js';
import DragAndDrop from './dragAndDrop.js';
import FileHandler from './fileHandler.js';
import FormHandler from './formHandler.js';

function PageCreator(props) {
  const schema = createBasicSectionSchema(props.labels.get("newPage"),
    props.pageSchema.schema, props.languages, props.labels);
  return (
    <Form
      schema={schema}
      validator={validator}
      onSubmit={(data) => props.registry.unsetRegistry(() => props.onDataChange({action: "pc", formData: data.formData}, () => props.onSetCurrentSectionPath(null)))}
      onChange={() => props.registry.setRegistry()}
    />
  );
}

function SchemaDescription(props) {
  const [showDesc, setShowDesc] = React.useState(false);
  return (
    <>
      <ButtonGroup>
        <Button onClick={() => props.onSetChosenSchema({custom: (props.custom === true), schema: props.schema})}>{(props.custom === true ? props.schema.name : props.labels.get(props.schema.name))}</Button>
        <Button onClick={() => setShowDesc(!showDesc)} className="btn-secondary">{props.labels.get("schemaDesc")}</Button>
      </ButtonGroup>
      {
        showDesc === true ?
        <Stack gap={2}>
          <section>
            <h4>{props.labels.get("subsections")}</h4>
            {
              props.schema.subsections === false ?
              <p>{props.labels.get("notAvailable")}</p>
              :
              props.schema.subsections === true ?
              <p>{props.labels.get("freeChoice")}</p>
              :
              <>
                {
                  props.schema.subsections.predefined !== undefined ?
                  <>
                    <h5>{props.labels.get("predefined")}</h5>
                    {
                      props.schema.subsections.predefined.map((section, i) => <p key={i}>{props.labels.get(section)}</p>)
                    }
                  </>
                  :
                  null
                }
                {
                  props.schema.subsections.custom !== undefined ?
                  <>
                    <h5>{props.labels.get("custom")}</h5>
                    {
                      props.schema.subsections.custom.map((section, i) => <p key={i}>{section}</p>)
                    }
                  </>
                  :
                  null
                }
              </>
            }
          </section>
          <section>
            <h4>{props.labels.get("contents")}</h4>
            {
              props.schema.content === false ?
              <p>{props.labels.get("notAvailable")}</p>
              :
              props.schema.content === true ?
              <p>{props.labels.get("freeChoice")}</p>
              :
              <>
                {
                  props.schema.content.predefined !== undefined ?
                  <>
                    <h5>{props.labels.get("predefined")}</h5>
                    {
                      Object.keys(props.schema.content.predefined).map((content, i) => <p key={i}>{(props.custom ? content : props.labels.get(content))} ({props.labels.get(props.schema.content.predefined[content].type)})</p>)
                    }
                  </>
                  :
                  null
                }
                {
                  props.schema.content.custom !== undefined ?
                  <>
                    <h5>{props.labels.get("custom")}</h5>
                    {
                      Object.keys(props.schema.content.custom).map((content, i) => <p key={i}>{content} ({props.schema.content.custom[content].type})</p>)
                    }
                  </>
                  :
                  null
                }
              </>
            }
          </section>
          <section>
            <h4>{props.labels.get("files")}</h4>
            {
              props.schema.files !== undefined && props.schema.files !== false ?
              Object.keys(props.schema.files).map((filename, i) => <p key={i}>{(props.custom ? filename : props.labels.get(filename))} ({props.labels.get(props.schema.files[filename].occurrence)})</p>)
              :
              <p>{props.labels.get("notAvailable")}</p>
            }
          </section>
          <section>
            <h4>{props.labels.get("forms")}</h4>
            <p>{(props.schema.forms ? props.labels.get("yes") : props.labels.get("no"))}</p>
          </section>
        </Stack>
        :
        null
      }
    </>
  );
}

function SectionList(props) {
  const schemaList = (custom) => {
    let schemas, part;
    if (custom === true) {
      schemas = props.customSectionSchemas;
      part = "custom";
    }
    else {
      schemas = props.sectionSchemas;
      part = "predefined";
    }
    const schemaDesc = props.page.getSchema(props.currentSectionPath);
    if (schemaDesc.subsections === false) return null;
    let schemaList;
    if (schemaDesc.subsections === true) schemaList = schemas;
    else if (part in schemaDesc.subsections) schemaList = schemas.filter((schema) => {
      return schemaDesc.subsections[part].includes(schema.name);
    });
    else return null;
    if (schemaList.length === 0) return null;
    return schemaList.sort((a,b) => {
      let strA, strB;
      if (custom === true) {
        strA = a.name;
        strB = b.name;
      }
      else {
        strA = props.labels.get(a.name);
        strB = props.labels.get(b.name);
      }
      if (strA > strB) return 1;
      if (strA < strB) return -1;
      return 0;
    }).map((schema, i) => <SchemaDescription
      key={i}
      onSetChosenSchema={props.onSetChosenSchema}
      schema={schema}
      labels={props.labels}
      custom={custom}
    />);
  };
  return (
    <Row>
      <Col sm>
        <Container className="p-3 my-3 border">
          <h4>{props.labels.get("predefined")}</h4>
          <Stack gap={3}>
            { schemaList() }
          </Stack>
        </Container>
      </Col>
      <Col sm>
        <Container className="p-3 my-3 border">
          <h4>{props.labels.get("custom")}</h4>
          <Stack gap={3}>
            { schemaList(true) }
          </Stack>
        </Container>
      </Col>
    </Row>
  );
}

function UserAssigner(props) {
  const [filtered, setFiltered] = React.useState(null);
  const [privileged, setPrivileged] = React.useState(null);
  const getPrivilegedUsers = () => {
    props.onPrivilegeSearch(props.currentSectionPath.join("/"), (privilegedUsers) => setPrivileged(privilegedUsers));
  };
  const getFilteredUsers = (searchStr) => {
    props.onUserSearch(searchStr, true, (filteredUsers) => {
      Object.keys(privileged).forEach((id) => {
        delete filteredUsers[id];
      });
      setFiltered(filteredUsers);
    });
  };
  React.useEffect(() => {
    if (privileged !== null && filtered !== null) {
      Object.keys(privileged).forEach((id) => {
        delete filtered[id];
      });
      setFiltered({...filtered});
    }
  }, [privileged]);
  React.useEffect(() => {
    if (privileged === null) getPrivilegedUsers();
  });
  return (
    <Row>
      <Col md>
        <Container className="p-3 my-3 border">
          <h4>{props.labels.get("searchUser")}</h4>
          <UserSearch
            onUserSearch={(searchStr) => getFilteredUsers(searchStr)}
            onClearUsers={() => setFiltered(null)}
            usersLoaded={filtered}
            labels={props.labels}
            actions={(user) => <Button className="btn-success" onClick={() => props.onPrivilegeAssigned(props.currentSectionPath.join("/"), user[0], () => getPrivilegedUsers())}><Add /> {props.labels.get("add")}</Button>}
          />
        </Container>
      </Col>
      <Col md>
        <Container className="p-3 my-3 border">
          <h4>{props.labels.get("privilegedUsers")}</h4>
          <UserList
            usersLoaded={privileged}
            actions={(user) => <Button className="btn-danger" onClick={() => props.onPrivilegeRemoved(props.currentSectionPath.join("/"), user[0], user[1].username, () => getPrivilegedUsers())}><Delete /> {props.labels.get("remove")}</Button>}
          />
        </Container>
      </Col>
    </Row>
  );
}

function SectionEditor(props) {
  const [action, setAction] = React.useState({action: null, nav: null});
  const registrySetAction = (newAction, data, navAction) => {
    let tmpAction = {action: newAction};
    if (data !== undefined) tmpAction = {...tmpAction, ...data};
    if (navAction !== undefined) tmpAction.nav = navAction;
    else tmpAction.nav = newAction;
    props.registry.getRegistry(() => setAction(tmpAction));
  };
  const onChooseSection = (path) => {
    setAction({action: null});
    props.onChooseSection(path);
  };
  const dialogRef = React.useRef(null);
  const section = props.page.getSection(props.path);
  const currentSectionSchema = props.page.getSchema(props.path);
  const sectionType = ("type" in section) ? props.labels.get(section.type) : props.labels.get("page");
  const title = `${section.title[props.languages.defaultLanguage]} (${section.name} - ${sectionType})`;
  const schema = createBasicSectionSchema("",
    currentSectionSchema.schema, props.languages, props.labels);
  return (
    <>
      <Dialog
        ref={dialogRef}
      />
      <Nav variant="tabs" className="mb-3" activeKey={action.nav}>
        <Nav.Item>
          <Nav.Link eventKey="title" className="text-dark" onClick={() => registrySetAction(null)}>{title}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="dataEditor" onClick={() => registrySetAction("dataEditor")}><Edit /> {props.labels.get("editSectionData")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link className="text-danger" eventKey="remove-section" onClick={() => dialogRef.current.showDialog({
            title: props.labels.get("remove"),
            text: props.labels.get("removeSectionConfirm"),
            callback: () => {
              props.onDataChange({action: "sd", path: props.path}, () => onChooseSection(null));
            }
          })}><Delete /> {props.labels.get("removeSection")}</Nav.Link>
        </Nav.Item>
        {
          currentSectionSchema.subsections !== false ?
          <>
            <Nav.Item>
              <Nav.Link className="text-success" eventKey="sectionList" onClick={() => registrySetAction("sectionList")}><Add /> {props.labels.get("addSection")}</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="subsections" onClick={() => registrySetAction("subsections")}><AccountTree /> {props.labels.get("subsections")}</Nav.Link>
            </Nav.Item>
          </>
          :
          null
        }
        {
          currentSectionSchema.content !== undefined && currentSectionSchema.content !== false ?
          <Nav.Item>
            <Nav.Link eventKey="contents" onClick={() => registrySetAction("contents")}><ArtTrackIcon /> {props.labels.get("contents")}</Nav.Link>
          </Nav.Item>
          :
          null
        }
        {
          currentSectionSchema.files !== undefined ?
          <Nav.Item>
            <Nav.Link eventKey="files" onClick={() => registrySetAction("files")}><FilePresent /> {props.labels.get("files")}</Nav.Link>
          </Nav.Item>
          :
          null
        }
        {
          currentSectionSchema.forms !== undefined && currentSectionSchema.forms !== false ?
          <Nav.Item>
            <Nav.Link eventKey="forms" onClick={() => props.onGetForms(props.path, () => registrySetAction("forms"))}><ListAlt /> {props.labels.get("forms")}</Nav.Link>
          </Nav.Item>
          :
          null
        }
        <Nav.Item>
          <Nav.Link eventKey="assignUsers" onClick={() => registrySetAction("assignUsers")}><AssignmentTurnedIn /> {props.labels.get("assignUsers")}</Nav.Link>
        </Nav.Item>
        {
          !props.page.isPage(props.path) ?
          <Nav.Item>
            <Nav.Link eventKey="useSectionTemplate" onClick={() => props.onUseSectionTemplate(props.path, section.useSectionTemplate !== true ? true : false)}>
              {
                section.useSectionTemplate === true ?
                <Check />
                :
                <Close />
              }
              {' '}{props.labels.get("useSectionTemplate")}
            </Nav.Link>
          </Nav.Item>
          :
          null
        }
        <Nav.Item>
          <Nav.Link eventKey="preview" onClick={() => props.onGetPreview("path", props.path.join("/"), null)}><Preview /> {props.labels.get("preview")}</Nav.Link>
        </Nav.Item>
      </Nav>
      {
        action.action === "dataEditor" ?
        <Form
          validator={validator}
          schema={schema}
          onSubmit={(data) => props.registry.unsetRegistry(() => props.onDataChange({action: "se", formData: data.formData, path: props.path}, () => {}))}
          onChange={() => props.registry.setRegistry()}
          formData={section}
        />
        :
        action.action === "subsections" ?
        <>
          {
            "sections" in section && section.sections ?
              <DragAndDrop
                onReposition={(dropped, current) => props.onDataChange({action: "sm", dropped: dropped, current: current, path: props.path}, () => {})}
                elements={section.sections.map((subsection, i) => <Button key={i} className="my-2" onClick={() => onChooseSection(props.path.concat(subsection.id))}>{subsection.title[props.languages.defaultLanguage]} ({subsection.name})</Button>)}
              />
            :
            null
          }
        </>
        :
        action.action === "sectionList" ?
        <SectionList
          currentSectionPath={props.path}
          onSetChosenSchema={(chosenSchema) => registrySetAction("addSection", {chosenSchema: chosenSchema}, "sectionList")}
          {...props}
        />
        :
        action.action === "addSection" ?
        <Form
          validator={validator}
          schema={createBasicSectionSchema((action.chosenSchema.schema.custom ? action.chosenSchema.schema.name : props.labels.get(action.chosenSchema.schema.name)),
            action.chosenSchema.schema.schema, props.languages, props.labels)}
          onSubmit={(data) => props.onDataChange({action: "sa", formData: data.formData, path: props.path, custom: action.chosenSchema.custom, type: action.chosenSchema.schema.name}, (id) => props.registry.unsetRegistry(() => onChooseSection(props.path.concat(id))))}
          onChange={() => props.registry.setRegistry()}
        />
        :
        action.action === "contents" ?
        <ContentList
          currentSection={section}
          currentSectionSchema={currentSectionSchema}
          onChooseContent={(content) => props.onContentChosen(props.path, content.name, content.custom, () => registrySetAction("editContent", {content: content}, "contents"))}
          onAddContent={() => registrySetAction("addContent", undefined, "contents")}
          {...props}
        />
        :
        action.action === "addContent" ?
        <ContentAdder
          currentSectionPath={props.path}
          onContentAdded={(type, name, custom) => props.onDataChange({action: "ca", type: type, name: name, custom: custom, path: props.path}, () => props.registry.unsetRegistry(() => registrySetAction("contents")))}
          {...props}
        />
        :
        action.action === "editContent" ?
        <ContentEditor
          currentSectionPath={props.path}
          content={action.content}
          onRemoveContent={() => props.onDataChange({action: "cr", path: props.path, name: action.content.name, custom: action.content.custom}, () => props.registry.unsetRegistry(registrySetAction("contents")))}
          {...props}
        />
        :
        action.action === "files" ?
        <FileHandler
          fileCollections={currentSectionSchema.files}
          currentSectionPath={props.path}
          {...props}
        />
        :
        action.action === "forms" ?
        <FormHandler
          currentSectionPath={props.path}
          {...props}
        />
        :
        action.action === "assignUsers" ?
        <UserAssigner
          currentSectionPath={props.path}
          {...props}
        />
        :
        null
      }
    </>
  );
}

export default function PageEditor(props) {
  const [path, setPath] = React.useState(null);
  const [prevPath, setPrevPath] = React.useState([]);
  React.useEffect(() => {
    if (path !== null) setPrevPath(path);
  }, [path]);
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setPath(null))}>{props.labels.get("pageSelector")}</Breadcrumb.Item>
        {
          path !== null ?
          <Breadcrumb.Item active>{props.labels.get("editSection")}</Breadcrumb.Item>
          :
          null
        }
      </Breadcrumb>
      {
        props.page.getSection() === null ?
        <PageCreator
          onSetCurrentSectionPath={setPath}
          {...props}
        />
        :
        path === null ?
        <PageTreeView
          page={props.page}
          onNodeSelect={(selectedPath) => setPath(selectedPath.split("/").map(Number))}
          languages={props.languages}
          defaultExpanded={prevPath.map((item, i) => {
            return prevPath.slice(0, i+1).join("/");
          })}
          defaultSelected={prevPath.join("/")}
          buttonText={props.labels.get("edit")}
        />
        :
        <SectionEditor
          path={path}
          onChooseSection={(path) => setPath(path)}
          {...props}
        />
      }
    </>
  )
}
