import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Container from 'react-bootstrap/Container';
import FileChooser from './fileChooser.js';
import DragAndDrop from './dragAndDrop.js';
import humanFileSize from './humanFileSize.js';

function FileCard(props) {
  const [captionEdit, setCaptionEdit] = React.useState(false);
  const [captions, setCaptions] = React.useState({...props.file.caption});
  const [thumbnail, setThumbnail] = React.useState(null);
  React.useEffect(() => {
    setCaptions({...props.file.caption});
    setCaptionEdit(false);
  }, [props.file.caption]);
  React.useEffect(() => {
    props.onGetThumbnail(props.file.id, "content", 100, (thumbnailData) => {
      setThumbnail(thumbnailData);
    });
  }, []);
  return (
    <Card key={props.i} style={{ width: '18rem' }}>
      <Card.Img variant="top" className='mx-auto' style={{ width: '100px' }} src={thumbnail} />
      <Card.Body>
        <Card.Title>{props.file.caption[props.languages.defaultLanguage]}</Card.Title>
        <Card.Subtitle>{props.file.name}</Card.Subtitle>
        <Card.Text>
          {props.file.type}, {humanFileSize(props.file.size)}
        </Card.Text>
        <ButtonGroup>
          <Button className="btn-danger" size="sm" onClick={() => props.onRemoveFile(props.currentSectionPath.join("/"), props.name, props.file.id, props.onReload)}>{props.labels.get("remove")}</Button>
          <Button size="sm" onClick={() => setCaptionEdit(captionEdit === true ? false : true)}>{props.labels.get("editCaption")}</Button>
        </ButtonGroup>
        {
          captionEdit === true ?
          <Container className="my-3" fluid>
            <CaptionEditor
              captions={captions}
              setCaptions={(captions) => setCaptions(captions)}
              {...props}
            />
            <Button onClick={() => props.onEditFileCaptions(props.currentSectionPath.join("/"), props.name, props.file.id, captions, () => setCaptionEdit(false))}>{props.labels.get("submit")}</Button>
          </Container>
          :
          null
        }
      </Card.Body>
    </Card>
  );
}

function CaptionEditor(props) {
  return (
    <>
      <h6>{props.labels.get("caption")}</h6>
      {
        props.languages.languages.map((language, i) => <Form.Group key={i} controlId="formFileLg" className="mb-3">
          <Form.Label>{language.name}{(language.iso639 === props.languages.defaultLanguage ? "*" : "")}</Form.Label>
          <Form.Control type="text" defaultValue={props.captions[language.iso639]} onChange={(e) => {
            const curCaptions = props.captions;
            curCaptions[language.iso639] = e.target.value;
            props.setCaptions(curCaptions)
          }} />
        </Form.Group>)
      }
    </>
  );
}

function FileDesc(props) {
  const [captions, setCaptions] = React.useState({});
  const [showFileAdder, setShowFileAdder] = React.useState(false);

  function toggleFileAdder() {
    setShowFileAdder(!showFileAdder);
  }

  function handleFileUpload(file) {
    const curCaption = {};
    Object.keys(captions).forEach((language, i) => {
      curCaption[language] = captions[language];
    });
    props.onAddFile(props.currentSectionPath, props.name, curCaption, file, props.fileDesc.occurrence, () => {
      toggleFileAdder();
      setCaptions({});
      props.onReload();
    });
  }

  let fileInfo = props.fileInfo;
  if (fileInfo !== null && props.name in fileInfo) fileInfo = fileInfo[props.name];
  else fileInfo = null;
  let mime = [];
  if ("mimeTypes" in props.fileDesc) mime = mime.concat(props.fileDesc.mimeTypes);
  if ("mimeSubtypes" in props.fileDesc) mime = mime.concat(props.fileDesc.mimeSubtypes);
  return (
    <Card>
      <Card.Body>
        <Card.Title>{props.labels.get(props.name)}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">{props.labels.get(props.fileDesc.occurrence)}{
          mime.length > 0 ?
          `, ${props.labels.get("fileType")}: ${mime.join(" / ")}`
          :
          null
        }</Card.Subtitle>
        <ListGroup variant="flush">
          {
            fileInfo !== null ?
            <ListGroup.Item>
              <DragAndDrop
                onReposition={(dropped, current) => props.onRepositionFile(props.currentSectionPath.join("/"), props.name, dropped, current, props.onReload)}
                elements={fileInfo.map((file, i) => <FileCard file={file} i={i} {...props} />)}
              />
            </ListGroup.Item>
            :
            null
          }
          <ListGroup.Item>
            <Button className="my-3" onClick={toggleFileAdder}>{props.labels.get((props.fileDesc.occurrence === "single" ? "uploadFile": "addFile"))}</Button>
            {
              showFileAdder ?
              <>
                <CaptionEditor
                  captions={captions}
                  setCaptions={setCaptions}
                  {...props}
                />
                <FileChooser
                  label={props.labels.get("file")}
                  submitLabel={props.labels.get("upload")}
                  onSubmit={(file) => handleFileUpload(file)}
                />
              </>
              :
              null
            }
          </ListGroup.Item>
        </ListGroup>
      </Card.Body>
    </Card>
  );
}

export default function FileHandler(props) {
  const [fileInfo, setFileInfo] = React.useState(false);
  const getFileInfo = () => props.onGetContentFiles(props.currentSectionPath.join("/"), (files) => {
    setFileInfo(files);
  });
  React.useEffect(() => {
    if (fileInfo === false) getFileInfo();
  }, [fileInfo]);
  React.useEffect(() => {
    getFileInfo();
  }, [props.filesLoaded]);
  return (
    fileInfo !== false ?
    Object.keys(props.fileCollections).map((name, i) => <FileDesc
      key={i}
      name={name}
      fileInfo={fileInfo}
      fileDesc={props.fileCollections[name]}
      onReload={() => getFileInfo()}
      {...props}
    />)
    :
    null
  );
}
