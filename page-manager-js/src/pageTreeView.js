import * as React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TreeView from '@mui/lab/TreeView';
import TreeItem, { treeItemClasses } from '@mui/lab/TreeItem';
import Typography from '@mui/material/Typography';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

const StyledTreeItemRoot = styled(TreeItem)(({ theme }) => ({
  color: theme.palette.text.secondary,
  [`& .${treeItemClasses.content}`]: {
    color: theme.palette.text.secondary,
    borderTopRightRadius: theme.spacing(2),
    borderBottomRightRadius: theme.spacing(2),
    paddingRight: theme.spacing(1),
    fontWeight: theme.typography.fontWeightMedium,
    '&.Mui-expanded': {
      fontWeight: theme.typography.fontWeightRegular,
    },
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
    },
    '&.Mui-focused, &.Mui-selected, &.Mui-selected.Mui-focused': {
      backgroundColor: `var(--tree-view-bg-color, ${theme.palette.action.selected})`,
      color: 'var(--tree-view-color)',
    },
    [`& .${treeItemClasses.label}`]: {
      fontWeight: 'inherit',
      color: 'inherit',
    },
  },
  [`& .${treeItemClasses.group}`]: {
    marginLeft: theme.spacing(1),
    [`& .${treeItemClasses.content}`]: {
      paddingLeft: theme.spacing(1),
    },
  },
}));

function StyledTreeItem(props) {
  const {
    nodeTitle,
    buttonIcon,
    exclude,
    onSelectFunction,
    ...other
  } = props;

  return (
    <StyledTreeItemRoot
      label={
        <Box sx={{ display: 'flex', alignItems: 'center', p: 0.5, pr: 0 }}>
          <Typography variant="body2" sx={{ fontWeight: 'inherit', flexGrow: 1 }}>
            {nodeTitle}
          </Typography>
          {
            !props.exclude.includes(props.nodeId) ?
            <Typography variant="caption" color="inherit">
              <div onClick={() => props.onSelectFunction(props.nodeId)}>{buttonIcon}</div>
            </Typography>
            :
            null
          }
        </Box>
      }
      style={{
        '--tree-view-color': "black",
        '--tree-view-bg-color': "white"
      }}
      {...other}
    />
  );
}

StyledTreeItem.propTypes = {
  nodeTitle: PropTypes.string,
  buttonIcon: PropTypes.string,
  exclude: PropTypes.array,
  onSelectFunction: PropTypes.func
};

export default function PageTreeView(props) {
  let exclude = [];
  if ("exclude" in props) exclude = props.exclude;

  const renderTree = (node, path) => {
    if (path === undefined) path = [node.id];
    else path = path.concat(node.id);
    const pathJson = path.join("/");
    return (
      <StyledTreeItem key={pathJson} onSelectFunction={props.onNodeSelect} nodeId={pathJson} nodeTitle={node.title[props.languages.defaultLanguage]} buttonIcon={props.buttonText} exclude={exclude}>
        {
          Array.isArray(node.sections)
          ? node.sections.map((subnode) => renderTree(subnode, path))
          : null
        }
      </StyledTreeItem>
    );
  };

  return (
    <TreeView
      defaultExpanded={props.defaultExpanded}
      defaultSelected={props.defaultSelected}
      defaultCollapseIcon={<ArrowDropDownIcon />}
      defaultExpandIcon={<ArrowRightIcon />}
      defaultEndIcon={<div style={{ width: 24 }} />}
      sx={{ height: props.height, flexGrow: 1, maxWidth: '100%', overflowY: 'auto' }}
    >
      {renderTree(props.page.getSection())}
    </TreeView>
  );
}
