import React from 'react';
import Container from 'react-bootstrap/Container';
import Dialog from './dialog.js';

export default class CustomFileWidget extends React.Component {
  constructor(props) {
    super(props);
    this.dialogRef = React.createRef();
  }

  render() {
    const dataUrl = this.props.value;
    let fileInfo = "";
    if (dataUrl !== undefined) {
      const dataParts = dataUrl.split(";");
      if (dataParts.length > 2) fileInfo = `${dataParts[1].substr(dataParts[1].indexOf("=") + 1)} (${dataParts[0].substr(dataParts[0].indexOf(":") + 1)})`;
    }
    return (
      <Container>
        <h5>{this.props.label}</h5>
        <input type="file" onChange={(event) => {
          const file = event.target.files[0];
          const reader = new FileReader();
          reader.onload = (readerEvent) => {
            try {
              let newDataUrl = readerEvent.target.result.split(";");
              if ("mime" in this.props.schema) {
                const mimeParts = this.props.schema.mime.split("/");
                const urlMimeParts = newDataUrl[0].substr(newDataUrl[0].indexOf(":") + 1).split("/");
                for (let i = 0; i < mimeParts.length; i++) {
                  if (mimeParts[i] !== urlMimeParts[i]) throw new Error();
                }
              }
              newDataUrl.splice(1, 0, `name=${file.name}`);
              this.props.onChange(newDataUrl.join(";"));
            }
            catch (error) {
              this.dialogRef.current.showDialog({
                "title": this.props.registry.rootSchema.labels.get("error"),
                "text": this.props.registry.rootSchema.labels.get("invalidFile")
              });
            }
          }
          reader.readAsDataURL(file);
        }} />
        <p>{fileInfo}</p>
        <Dialog
          ref={this.dialogRef}
        />
      </Container>
    );
  }
}
