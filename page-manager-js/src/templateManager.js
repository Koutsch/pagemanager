import React from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ListGroup from 'react-bootstrap/ListGroup';
import Stack from 'react-bootstrap/Stack';
import Accordion from 'react-bootstrap/Accordion';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Alert from 'react-bootstrap/Alert';
import Loop from '@mui/icons-material/Loop';
import Home from '@mui/icons-material/Home';
import QuestionMark from '@mui/icons-material/QuestionMark';
import VpnKey from '@mui/icons-material/VpnKey';
import VpnKeyOff from '@mui/icons-material/VpnKeyOff';
import Login from '@mui/icons-material/Login';
import Logout from '@mui/icons-material/Logout';
import Lock from '@mui/icons-material/Lock';
import LockOpen from '@mui/icons-material/LockOpen';
import NoEncryptionGmailerrorred from '@mui/icons-material/NoEncryptionGmailerrorred';
import Link from '@mui/icons-material/Link';
import ArtTrack from '@mui/icons-material/ArtTrack';
import SubdirectoryArrowRight from '@mui/icons-material/SubdirectoryArrowRight';
import DataObject from '@mui/icons-material/DataObject';
import AddLink from '@mui/icons-material/AddLink';
import DownloadingIcon from '@mui/icons-material/Downloading';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import CalendarMonth from '@mui/icons-material/CalendarMonth';
import AccountTree from '@mui/icons-material/AccountTree';
import Css from '@mui/icons-material/Css';
import FontDownload from '@mui/icons-material/FontDownload';
import Image from '@mui/icons-material/Image';
import Javascript from '@mui/icons-material/Javascript';
import TextSnippetIcon from '@mui/icons-material/TextSnippet';
import Title from '@mui/icons-material/Title';
import Pages from '@mui/icons-material/Pages';
import Preview from '@mui/icons-material/Preview';
import ClosedCaption from '@mui/icons-material/ClosedCaption';
import Route from '@mui/icons-material/Route';
import ListAlt from '@mui/icons-material/ListAlt';
import Translate from '@mui/icons-material/Translate';
import Edit from '@mui/icons-material/Edit';
import Delete from '@mui/icons-material/Delete';
import Archive from '@mui/icons-material/Archive';
import Editor from "@monaco-editor/react";
import PageTreeView from './pageTreeView.js';
import { addLangInput, createBasicSectionSchema, derivePathsFromSchema, addTitles } from './schemas.js';
import Dialog from './dialog.js';
import humanFileSize from './humanFileSize.js';
import FileDataManager from './fileDataManager.js';
import FileManager from './fileManager.js';
import PageEditor from './pageEditor.js';
import timely from './timely.js';
import TextSnippetManager from './textSnippetManager';
import {
  ReflexContainer,
  ReflexSplitter,
  ReflexElement
} from 'react-reflex';
import 'react-reflex/styles.css';

function createTemplateAction(action, ...values) {
  for (let i = 0; i < values.length; i++) {
    action = action.replaceAll(`[[[${i}]]]`, values[i]);
  }
  return action;
}

function PathDisplayer(props) {
  const [showTranslations, setShowTranslations] = React.useState(false);
  const [translations, setTranslations] = React.useState({});
  const addTranslation = (item, language, value) => {
    if (translations[item] === undefined) translations[item] = {};
    translations[item][language] = value;
    setTranslations({...translations});
  };
  const getTranslationKey = (type, custom, name, path) => {
    if (props.savedTranslations === null) return "";
    let key = Object.keys(props.savedTranslations).filter((translation) => {
      if (props.savedTranslations[translation].type === type && props.savedTranslations[translation].custom === custom && props.savedTranslations[translation].name === name && props.savedTranslations[translation].path === path) return true;
      return false;
    });
    if (key.length > 0) return key[0];
    else return "";
  };
  const createFullPath = (parentPath, pathStr) => {
    return pathStr.replace("[PATH]", parentPath);
  }
  return (
    <div>
      {
        props.paths.map((path, i) => {
          let title, type = "", pathStr = `${path.path}`, key = false, list = [], formatter = null;
          if ("def" in path) {
            title = ("title" in path.def.schema ? path.def.schema.title : path.property.title);
            type = path.def.name;
            if (type === "langInput") pathStr = createTemplateAction(props.actions.getText, path.path);
          }
          else {
            title = path.property.title;
            const typeDesc = [];
            if ("type" in path.property) {
              typeDesc.push(`${path.property.type}`);
            }
            if ("format" in path.property) {
              formatter = path.property.format;
              typeDesc.push(`${props.labels.get("format")}: ${path.property.format}`);
            }
            else if ("const" in path.property) {
              typeDesc.push(`${props.labels.get("constant")}: ${path.property.const}`);
            }
            else if ("pattern" in path.property) {
              typeDesc.push(`${props.labels.get("pattern")}: ${path.property.pattern}`);
            }
            else if ("default" in path.property) {
              typeDesc.push(`${props.labels.get("default")}: ${path.property.default}`);
            }
            else if ("enum" in path.property) {
              key = getTranslationKey(props.selector.type, props.selector.custom, props.selector.name, path.path);
              pathStr = createTemplateAction(props.actions.getSelector, path.path, key);
              list = path.property.enum;
              typeDesc.push(`${props.labels.get("enum")}: ${list.join(", ")}`);
            }
            type = typeDesc.join(" – ");
          }
          return (
            <div key={i} className="my-3" style={{paddingLeft: 10}}>
              <span>{title} ({path.path})</span><br />
              <div>
                <span>{type}</span>
              </div>
              <div>
                <Button variant="link" style={{margin:0, padding:0}} onClick={() => props.onClick(createFullPath(props.relativePath, pathStr))}>{props.labels.get("addRelativePath")}</Button>
                <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(createFullPath(props.parentPath, pathStr))}>{props.labels.get("addFullPath")}</Button>
                {
                  formatter === "date-time" ?
                    <>
                      <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(createTemplateAction(props.actions.getDateTime, createFullPath(props.relativePath, path.path)))}>{props.labels.get("addFormattedDateRelative")}</Button>
                      <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(createTemplateAction(props.actions.getDateTime, createFullPath(props.parentPath, path.path)))}>{props.labels.get("addFormattedDateFull")}</Button>
                    </>
                  :
                  null
                }
                {
                  key !== false ?
                  <>
                    <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => setShowTranslations(!showTranslations)}>{props.labels.get("addTranslations")}</Button>
                    {
                      showTranslations === true ?
                      <>
                        <ListGroup className="my-1">
                          {
                            list.map((item, i) => <ListGroup.Item key={i}>
                              <p>{item}</p>
                              {
                                props.languages.languages.map((language, j) => {
                                  let defaultValue = "";
                                  if (props.savedTranslations !== null && props.savedTranslations[key] !== undefined && props.savedTranslations[key].translations[item] !== undefined && props.savedTranslations[key].translations[item][language.iso639] !== undefined) {
                                    defaultValue = props.savedTranslations[key].translations[item][language.iso639];
                                  }
                                  return (
                                    <Form.Group key={j} className="mb-3">
                                      <Form.Label>{language.name}</Form.Label>
                                      <Form.Control type="text" defaultValue={defaultValue} onChange={(e) => addTranslation(item, language.iso639, e.target.value)} />
                                    </Form.Group>
                                  )
                                })
                              }
                            </ListGroup.Item>)
                          }
                        </ListGroup>
                        <Button onClick={() => props.addTranslations({path: path.path, translations: translations})}>{props.labels.get("save")}</Button>
                      </>
                      :
                      null
                    }
                  </>
                  :
                  null
                }
              </div>
              {
                path.subnodes && path.subnodes.length > 0 ?
                <div className="my-3">
                  <PathDisplayer
                    paths={path.subnodes}
                    onClick={props.onClick}
                    addTranslations={props.addTranslations}
                    labels={props.labels}
                    relativePath={props.relativePath}
                    parentPath={props.parentPath}
                    actions={props.actions}
                    languages={props.languages}
                    savedTranslations={props.savedTranslations}
                    selector={props.selector}
                  />
                </div>
                :
                null
              }
            </div>
          );
        })
      }
    </div>
  );
}

const sortSectionSchemas = (a, b, labels) => {
  if (labels !== undefined) {
    a = labels.get(a);
    b = labels.get(b);
  }
  if (a < b) return -1;
  if (a > b) return 1;
  return 0;
};

function SavedSections(props) {
  const sectionPattern = (path) => createTemplateAction(props.templateActions.actions.getSavedSection, path);
  return (
    <ListGroup>
      <ListGroup.Item>
        <p>{props.labels.get("getSections")}</p>
        <ButtonGroup>
          <Button onClick={() => props.onClick(createTemplateAction(props.templateActions.actions.getSections, sectionPattern(props.parentPath)))}>{props.labels.get("addFullPath")}</Button>
        </ButtonGroup>
      </ListGroup.Item>
      {
        props.section.sections.map((section, i) => {
          const path = `${props.parentPath}/${section.id}`;
          const title = `${section.title[props.languages.defaultLanguage]} (${props.labels.get(section.type)})`;
          return (
            <ListGroup.Item key={i}>
              <p>{title}</p>
              <ButtonGroup>
                <Button onClick={() => props.onClick(sectionPattern(path))}>{props.labels.get("addFullPath")}</Button>
                <Button onClick={() => props.onClick(createTemplateAction(props.templateActions.actions.getLink, path))}>{props.labels.get("addLink")}</Button>
                <Button variant="secondary" onClick={() => props.onSelectSection(path, sectionPattern(path), sectionPattern(path), title)}><ArrowForwardIosIcon /></Button>
              </ButtonGroup>
            </ListGroup.Item>
          );
        })
      }
    </ListGroup>
  );
}

function Subsections(props) {
  let subsections = [];
  if (Array.isArray(props.schema.subsections)) subsections = props.schema.subsections;
  if (props.schema.subsections === true) {
    subsections = [...props.sectionSchemas.map((subsection) => { return {name: subsection.name, custom: false}}), ...props.customSectionSchemas.map((subsection) => { return {name: subsection.name, custom: true}})];
  }
  else {
    if ("predefined" in props.schema.subsections) subsections = [...subsections, ...props.schema.subsections.predefined.map((subsection) => { return {name: subsection, custom: false}})];
    if ("custom" in props.schema.subsections) subsections = [...subsections, ...props.schema.subsections.custom.map((subsection) => { return {name: subsection, custom: true}})];
  }
  const pattern = (parentPath, custom) => createTemplateAction(props.templateActions.actions.getType, parentPath, custom);
  return (
    <ListGroup>
      {
        subsections.sort((a, b) => sortSectionSchemas(a.name, b.name, props.labels)).map((subsection, i) => {
          const schema = `'${subsection.name}', ${subsection.custom}`
          const relativePath = `${props.relativePath}${pattern(subsection.name, subsection.custom)}`;
          const fullPath = `${props.parentPath}${pattern(subsection.name, subsection.custom)}`;
          const title = props.labels.get(subsection.name);
          return (
            <ListGroup.Item key={i}>
              <p>{title}</p>
              <ButtonGroup>
                <Button onClick={() => props.onClick(schema)}>{props.labels.get("schema")}</Button>
                <Button onClick={() => props.onClick(relativePath)}>{props.labels.get("addRelativePath")}</Button>
                <Button onClick={() => props.onClick(fullPath)}>{props.labels.get("addFullPath")}</Button>
                <Button onClick={() => props.onClick(createTemplateAction(props.templateActions.actions.getUnspecifiedLink, relativePath))}>{props.labels.get("addLinkRelative")}</Button>
                <Button onClick={() => props.onClick(createTemplateAction(props.templateActions.actions.getUnspecifiedLink, fullPath))}>{props.labels.get("addLinkFull")}</Button>
                <Button variant="secondary" onClick={() => props.onSelectSection(subsection, props.relativePath, fullPath, title)}><ArrowForwardIosIcon /></Button>
              </ButtonGroup>
            </ListGroup.Item>
          );
        })
      }
    </ListGroup>
  );
}

function ContentFiles(props) {
  const [files, setFiles] = React.useState(null);
  React.useEffect(() => {
    if (props.sectionPath !== null) props.onGetContentFiles(props.sectionPath, (filesLoaded) => {
      setFiles(filesLoaded);
    });
  }, [props.sectionPath]);
  return (<div>
    {
      Object.keys(props.schema.files).map((name, i) => {
        const contentFiles = props.schema.files[name];
        let mime = [];
        if ("mimeTypes" in contentFiles) mime = mime.concat(contentFiles.mimeTypes);
        if ("mimeSubtypes" in contentFiles) mime = mime.concat(contentFiles.mimeSubtypes);
        const path = createTemplateAction(props.templateActions.actions.getContentFiles, name);
        const relativePath = `${props.relativePath}${path}`;
        const fullPath = `${props.parentPath}${path}`;
        const linkPath = createTemplateAction(props.templateActions.actions.getContentFileLinks, name);
        const relativeLinkPath = `${props.relativePath}${linkPath}`;
        const fullLinkPath = `${props.parentPath}${linkPath}`;
        return (
          <div key={i} className="my-3" style={{paddingLeft: 10}}>
            <div>{props.labels.get(name)}, {props.labels.get(contentFiles.occurrence)}</div>
            <div>{mime.join(" / ")}</div>
            <Button variant="link" style={{margin:0, padding:0}} onClick={() => props.onClick(relativePath)}>{props.labels.get("addRelativePath")}</Button>
            <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(fullPath)}>{props.labels.get("addFullPath")}</Button>
            <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(relativeLinkPath)}>{props.labels.get("addRelativeLinkPath")}</Button>
            <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(fullLinkPath)}>{props.labels.get("addFullLinkPath")}</Button>
            {
              files !== null && name in files && files[name].length > 0 ?
              <div className="my-2">
                <h6>{props.labels.get("savedFiles")}</h6>
                {
                  files[name].map((file, i) => {
                    const getFunction = createTemplateAction(props.templateActions.actions.getContentFile, name, file.id);
                    const getCaption = createTemplateAction(props.templateActions.actions.getCaption, name, file.id);
                    return (
                      <div key={i}>
                        <div>{file.name}, {file.type}, {humanFileSize(file.size)}</div>
                        <Button variant="link" style={{margin:0, padding:0}} onClick={() => props.onClick(`${props.relativePath}${getFunction}`)}>{props.labels.get("addRelativePath")}</Button>
                        <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(`${props.parentPath}${getFunction}`)}>{props.labels.get("addFullPath")}</Button>
                        <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(`${props.relativePath}${getCaption}`)}>{props.labels.get("captionRelative")}</Button>
                        <Button variant="link" style={{margin:0, padding:0, paddingLeft: "0.5em"}} onClick={() => props.onClick(`${props.parentPath}${getCaption}`)}>{props.labels.get("captionFull")}</Button>
                      </div>
                    );
                  })
                }
              </div>
              :
              null
            }
          </div>
        );
      })
    }
  </div>);
}

function Contents(props) {
  const createContentList = (schemaContents, custom, schemas) => {
    const part = (custom === true ? "custom" : "predefined");
    const contents = Object.keys(schemaContents[part]).map((contentName) => {
      const type = schemaContents[part][contentName].type;
      return {
        title: (custom === true ? contentName : props.labels.get(contentName)), name: contentName, custom: custom, part: part, type: type, schema: schemas[type].schema, files: schemas[type].files, forms: schemas[type].forms
      };
    });
    return contents;
  };
  let contentList = [];
  if (props.schemaContents !== undefined && typeof props.schemaContents !== "boolean") {
    if ("predefined" in props.schemaContents) contentList = createContentList(props.schemaContents, false, props.contentSchemas);
    if ("custom" in props.schemaContents) contentList = [...contentList, ...createContentList(props.schemaContents, true, props.customContentSchemas)];
  }
  if (contentList.length === 0) return null;
  contentList.sort((a, b) => {
    if (a.title < b.title) return -1;
    if (a.title > b.title) return 1;
    return 0;
  });
  return (
    <ListGroup>
      {
        contentList.map((content, id) => {
          let contentView = null;
          if (content.schema !== false && content.schema !== undefined) {
            const contentSchema = addTitles(content.schema, props.labels);
            const path = createTemplateAction(props.templateActions.actions.getContent, content.name, content.custom);
            const relativePath = `${props.relativePath}${path}`;
            const fullPath = `${props.parentPath}${path}`;
            contentSchema.$defs.langInput = addLangInput(props.languages);
            const contentPaths = derivePathsFromSchema(contentSchema, "[PATH]");
            contentView = () => <div key={id}>
              <ButtonGroup>
                <Button onClick={() => props.onClick(relativePath)}>{props.labels.get("addRelativePath")}</Button>
                <Button onClick={() => props.onClick(fullPath)}>{props.labels.get("addFullPath")}</Button>
              </ButtonGroup>
              <PathDisplayer
                paths={contentPaths}
                onClick={props.onClick}
                addTranslations={(added) => props.addTranslations({type: "content", custom: content.custom, name: content.type, ...added})}
                labels={props.labels}
                relativePath={relativePath}
                parentPath={fullPath}
                actions={props.templateActions.actions}
                languages={props.languages}
                savedTranslations={props.translations}
                selector={{type: "content", custom: content.custom, name: content.type}}
              />
            </div>;
          }
          return (
            <ListGroup.Item key={id}>
              <h5>{props.labels.get(content.name)}</h5>
              {
                contentView !== null ?
                contentView()
                :
                null
              }
            </ListGroup.Item>
          );
        })
      }
    </ListGroup>
  );
}

function PossiblePaths(props) {
  const sectionSchema = createBasicSectionSchema(props.labels.get(props.schema.name), props.schema.schema, props.languages, props.labels);
  const possiblePaths = derivePathsFromSchema(sectionSchema, "[PATH]");
  const custom = (props.schemaDesc !== null && props.schemaDesc.custom === true ? true : false);
  return (
    <Container>
      <PathDisplayer
        paths={possiblePaths}
        onClick={props.onClick}
        addTranslations={(added) => props.addTranslations({type: "section", custom: custom, name: props.schema.name, ...added})}
        labels={props.labels}
        relativePath={props.relativePath}
        parentPath={props.parentPath}
        actions={props.templateActions.actions}
        languages={props.languages}
        savedTranslations={props.translations}
        selector={{type: "section", custom: custom, name: props.schema.name}}
      />
    </Container>
  );
}

function PathList(props) {
  const path = (props.selection.ref === "path" && props.selection.rel === props.page.getSection().id.toString() ? "page" : "section");
  const [selections, setSelections] = React.useState({data: [{
    ref: props.selection.ref,
    rel: props.selection.rel,
    parentPath: path,
    relativePath: path,
    title: (props.selection.ref === "path" ? props.page.getSection(props.selection.rel.split("/").map(Number)).title[props.languages.defaultLanguage] : props.labels.get(props.selection.rel.name))
  }], current: 0});
  const [fonts, setFonts] = React.useState(false);
  const [templateFiles, setTemplateFiles] = React.useState(false);
  React.useEffect(() => {
    if (templateFiles === false) props.onLoadList("templateFiles", (files) => setTemplateFiles(files));
    if (fonts === false) props.onLoadList("fonts", (fonts) => setFonts(fonts));
  });

  function addSelection(selection) {
    setSelections({data: selections.data.concat(selection), current: selections.current+1});
  }

  function navigate(i) {
    if (i < selections.current) setSelections({data: selections.data.slice(0, i + 1), current: i});
  }

  const view = () => {
    const selection = selections.data[selections.current];
    let schema, section = null, content = null, path = null, custom = false;
    if (selection.ref === "type") {
      schema = props.page.getSectionSchema(selection.rel.name, selection.rel.custom);
      custom = selection.rel.custom;
      if (schema.content !== undefined && typeof schema.content !== "boolean") content = schema.content;
    }
    else {
      const sectionPath = selection.rel.split("/").map(Number);
      schema = props.page.getSchema(sectionPath);
      section = props.page.getSection(sectionPath);
      custom = section.custom;
      path = selection.rel;
      if (schema.content !== undefined && typeof schema.content !== "boolean") content = schema.content;
      else if (schema.content === true && section.content !== undefined) content = section.content;
    }
    if (schema === false) return (
      <Alert variant="warning">{props.labels.get("dataMissingReload")}</Alert>
    )
    return (
      <>
        <Breadcrumb>
          {
            selections.data.map((selection, i) => <Breadcrumb.Item key={i} onClick={() => navigate(i)}>{selection.title}</Breadcrumb.Item>)
          }
        </Breadcrumb>
        <Accordion>
          <Accordion.Item eventKey="0">
            <Accordion.Header>{props.labels.get("sectionPaths")}</Accordion.Header>
            <Accordion.Body>
              <PossiblePaths
                schema={schema}
                schemaDesc={{name: schema.name, custom: custom}}
                onClick={props.onChoosePath}
                parentPath={selection.parentPath}
                relativePath={selection.relativePath}
                {...props}
              />
            </Accordion.Body>
          </Accordion.Item>
          {
            content !== null ?
            <Accordion.Item eventKey="1">
              <Accordion.Header>{props.labels.get("contents")}</Accordion.Header>
              <Accordion.Body>
                <Contents
                  parentPath={selection.parentPath}
                  relativePath={selection.relativePath}
                  sectionPath={path}
                  schemaContents={content}
                  onClick={props.onChoosePath}
                  templateRef={selection.ref}
                  {...props}
                />
              </Accordion.Body>
            </Accordion.Item>
            :
            null
          }
          {
            schema.files !== undefined ?
            <Accordion.Item eventKey="2">
              <Accordion.Header>{props.labels.get("contentFiles")}</Accordion.Header>
              <Accordion.Body>
                <ContentFiles
                  parentPath={selection.parentPath}
                  relativePath={selection.relativePath}
                  schema={schema}
                  sectionPath={path}
                  onClick={props.onChoosePath}
                  {...props}
                />
              </Accordion.Body>
            </Accordion.Item>
            :
            null
          }
          {
            schema.forms === true ?
            <Accordion.Item eventKey="3">
              <Accordion.Header>{props.labels.get("forms")}</Accordion.Header>
              <Accordion.Body>
                <Button onClick={() => props.onChoosePath(createTemplateAction(props.templateActions.actions.getCustomForms, selection.relativePath, "callback", "errCallback"))}>{props.labels.get("addAllFormsRelative")}</Button>
                <Button onClick={() => props.onChoosePath(createTemplateAction(props.templateActions.actions.getCustomForms, selection.parentPath, "callback", "errCallback"))}>{props.labels.get("addAllFormsFull")}</Button>
              </Accordion.Body>
            </Accordion.Item>
            :
            null
          }
          {
            "subsections" in schema && schema.subsections !== false ?
            <Accordion.Item eventKey="4">
              <Accordion.Header>{props.labels.get("subsections")}</Accordion.Header>
              <Accordion.Body>
                <Subsections
                  parentPath={selection.parentPath}
                  relativePath={selection.relativePath}
                  schema={schema}
                  onClick={props.onChoosePath}
                  onSelectSection={(subsection, relativePath, fullPath, title) => addSelection({ref: "type", rel: subsection, parentPath: fullPath, relativePath: relativePath, title: title})}
                  {...props} />
              </Accordion.Body>
            </Accordion.Item>
            :
            null
          }
          {
            section !== null && "sections" in section && section.sections.length > 0 ?
            <Accordion.Item eventKey="5">
              <Accordion.Header>{props.labels.get("savedSections")}</Accordion.Header>
              <Accordion.Body>
                <SavedSections
                  parentPath={selection.rel}
                  relativePath={selection.relativePath}
                  section={section}
                  onSelectSection={(path, relativePath, fullPath, title) => addSelection({ref: "path", rel: path, parentPath: fullPath, relativePath: relativePath, title: title})}
                  onClick={props.onChoosePath}
                  {...props} />
              </Accordion.Body>
            </Accordion.Item>
            :
            null
          }
          {
            selection.parentPath !== "page" ?
            <Accordion.Item eventKey="6">
              <Accordion.Header>{props.labels.get("page")}</Accordion.Header>
              <Accordion.Body>
                <PossiblePaths
                  schema={props.pageSchema}
                  schemaDesc={null}
                  onClick={props.onChoosePath}
                  relativePath={"page"}
                  parentPath={"page"}
                  {...props}
                />
              </Accordion.Body>
            </Accordion.Item>
            :
            null
          }
          <Accordion.Item eventKey="7">
            <Accordion.Header>{props.labels.get("languages")}</Accordion.Header>
            <Accordion.Body>
              <ButtonGroup>
                {
                  props.languages.languages.map((language, i) => <Button key={i} onClick={() => props.onChoosePath(language.iso639)}>{language.name}</Button>)
                }
              </ButtonGroup>
            </Accordion.Body>
          </Accordion.Item>
          {
            templateFiles !== false && templateFiles !== null ?
            <Accordion.Item eventKey="8">
              <Accordion.Header>{props.labels.get("files")}</Accordion.Header>
              <Accordion.Body>
                <ListGroup>
                  {
                    templateFiles.sort((a, b) => {
                      if (a.name > b.name) return 1;
                      if (a.name < b.name) return -1;
                      return 0;
                    }).map((file, i) => <ListGroup.Item key={i}>
                      <Button onClick={() => props.onChoosePath(createTemplateAction(props.templateActions.actions.getTemplateFile, file.id))} size="sm">{file.name} ({file.type})</Button>
                    </ListGroup.Item>)
                  }
                </ListGroup>
              </Accordion.Body>
            </Accordion.Item>
            :
            null
          }
          {
            fonts !== false && fonts !== null ?
            <Accordion.Item eventKey="9">
              <Accordion.Header>{props.labels.get("fonts")}</Accordion.Header>
              <Accordion.Body>
                <ListGroup>
                  {
                    fonts.map((font, i) => <ListGroup.Item key={i}>
                      <Button onClick={() => props.onChoosePath(font.name)} size="sm">{font.name} ({font.type})</Button>
                    </ListGroup.Item>)
                  }
                </ListGroup>
              </Accordion.Body>
            </Accordion.Item>
            :
            null
          }
        </Accordion>
      </>
    );
  };
  return view();
}

function TemplateToolbar(props) {
  const logos = {
    ArtTrack: ArtTrack,
    SubdirectoryArrowRight: SubdirectoryArrowRight,
    DataObject: DataObject,
    AddLink: AddLink,
    QuestionMark: QuestionMark,
    Loop: Loop,
    CalendarTodayIcon: CalendarTodayIcon,
    CalendarMonth: CalendarMonth,
    DownloadingIcon: DownloadingIcon,
    VpnKey: VpnKey,
    VpnKeyOff: VpnKeyOff,
    Home: Home,
    Login: Login,
    Logout: Logout,
    LockOpen: LockOpen,
    NoEncryptionGmailerrorred: NoEncryptionGmailerrorred,
    Lock: Lock,
    Link: Link,
    ClosedCaption: ClosedCaption,
    ListAlt: ListAlt,
    Route: Route,
    Translate: Translate,
    Archive: Archive
  };
  const areas = {};

  props.templateActions.toolbar.filter((tool) => tool.tool.apply.includes(props.templateType)).forEach((tool) => {
    if (areas[tool.tool.area] === undefined) areas[tool.tool.area] = [tool];
    else areas[tool.tool.area].push(tool);
  });

  return (
    <div className="templateToolbar">
      {
        Object.keys(areas).map((area, i) => <div className="toolbarCategory" key={i}>
          <div className="toolbarTitle">{props.labels.get(area)}</div>
          {
            areas[area].map((tool, j) => <button key={j} onClick={() => props.onChoose(tool.template)}>{React.createElement(logos[tool.tool.logo])} {props.labels.get(tool.tool.label)}</button>)
          }
        </div>)
      }
    </div>
  );
}

const CodeEditor = React.forwardRef((props, ref) => {
  const editorRef = React.useRef();
  const monacoRef = React.useRef();
  React.useImperativeHandle(ref, () => ({
    set(text) {
      editorRef.current.setValue(text)
    },
    insert(text) {
      handleInsert(text);
    }
  }));
  function handleInsert(text) {
    const line = editorRef.current.getPosition();
    editorRef.current.executeEdits("",
    [
      {
        range: new monacoRef.current.Range(line.lineNumber,
          line.column,
          line.lineNumber,
          line.column),
        text: text
      }
    ]);
  }
  return (
    <>
      <TemplateToolbar
        onChoose={handleInsert}
        templateType={props.templateType}
        labels={props.labels}
        templateActions={props.templateActions}
      />
      <Editor
        className="my-3"
        height={props.height}
        defaultLanguage="html"
        defaultValue={props.defaultValue}
        onMount={(e, m) => {
          editorRef.current = e; 
          monacoRef.current = m; 
          editorRef.current.getModel().updateOptions({tabSize: 2})
        }}
        onChange={() => props.onChange(editorRef.current.getValue(), props.templatePart)}
      />      
     </>
  );
});

function TemplateSnippetChooser(props) {
  const [snippets, setSnippets] = React.useState(null);
  React.useEffect(() => {
    if (snippets === null) props.onGetSnippetList((list) => setSnippets(list));
  });
  if (snippets === null) return null;
  const pattern = (name) => `{{ include('${name}_snippet.template') }}`;
  return (
    <>
      <Breadcrumb className="mb-3">
        <Breadcrumb.Item>{props.labels.get("templateSnippets")}</Breadcrumb.Item>
      </Breadcrumb>
      <Stack gap={3}>
        {
          snippets.sort().map((snippetName, i) => <ButtonGroup key={i}>
            <Button onClick={() => props.onGetSnippet(snippetName, (snippet) => props.insert(snippet))} >{snippetName}</Button>
            <Button className='btn-secondary' onClick={() => props.insert(pattern(snippetName))} >{props.labels.get("include")}</Button>
          </ButtonGroup>)
        }
      </Stack>
    </>
  );
}

function TextSnippetChooser(props) {
  const [snippets, setSnippets] = React.useState(null);
  React.useEffect(() => {
    if (snippets === null) props.onGetTextSnippets((textSnippets) => setSnippets(textSnippets));
  });
  if (snippets === null) return null;
  return (
    <>
      <Breadcrumb className="mb-3">
        <Breadcrumb.Item>{props.labels.get("textSnippets")}</Breadcrumb.Item>
      </Breadcrumb>
      <Stack gap={3}>
        {
          snippets.sort().map((snippetName, i) => <Button onClick={() => props.insert(createTemplateAction(props.templateActions.actions.getTextSnippet, snippetName))} key={i}>{snippetName}</Button>)
        }
      </Stack>
    </>
  );
}

function SecondEditor(props) {
  const [action, setAction] = React.useState(false);
  const [editor, setEditor] = React.useState(null);

  React.useEffect(() => {
    setEditor(false);
  }, [props.editor]);

  React.useEffect(() => {
    setEditor(props.editor);
  }, [editor]);

  return (
    <>
      {
        editor === "css" ?
        <FileDataManager
          editor={(editorProps) => 
            <>
              <Nav className="my-1" style={{fontSize: '12px'}}>
                <Nav.Item>
                  <Nav.Link eventKey="fonts" onClick={() => {
                    if (action !== false && action.action === "fonts") setAction(false);
                    else props.onLoadList("fonts", (fonts) => setAction({action: "fonts", data: fonts}));
                  }}><FontDownload /> {props.labels.get("fonts")}</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="images" onClick={() => {
                    if (action !== false && action.action === "images") setAction(false);
                    else props.onLoadImages(props.selection, (images) => setAction({action: "images", data: images}));
                  }}><Image /> {props.labels.get("images")}</Nav.Link>
                </Nav.Item>
              </Nav>
              {
                action !== false && action.action === "fonts" ?
                <Nav style={{fontSize: '12px'}}>
                  <Nav.Item>
                    <Nav.Link className="text-dark" eventKey={"fontsLabel"}>{props.labels.get("fonts")}</Nav.Link>
                  </Nav.Item>
                  {
                    action.data.map((font, i) => <Nav.Item key={i}>
                      <Nav.Link eventKey={i} onClick={() => editorProps.insert(font.name)}>{font.name}</Nav.Link>
                    </Nav.Item>)
                  }
                </Nav>
                :
                null
              }
              {
                action !== false && action.action === "images" ?
                <>
                  <Nav className="my-1" style={{fontSize: '12px'}}>
                    <Nav.Item>
                      <Nav.Link className="text-dark" eventKey={"templateLabel"}>{props.labels.get("templateFiles")}</Nav.Link>
                    </Nav.Item>
                    {
                      action.data.template.map((image, i) => <Nav.Item key={i}>
                        <Nav.Link eventKey={i} onClick={() => editorProps.insert(`url('{{template ${image.id}}}')`)}>{image.name}</Nav.Link>
                      </Nav.Item>)
                    }
                  </Nav>
                  {
                    action.data.section !== null ?
                    <Nav style={{fontSize: '12px'}}>
                      <Nav.Item>
                        <Nav.Link className="text-dark" eventKey={"templateLabel"}>{props.labels.get("contentFiles")}</Nav.Link>
                      </Nav.Item>
                      {
                        action.data.section.map((image, i) => <Nav.Item key={i}>
                          <Nav.Link eventKey={i} onClick={() => editorProps.insert(`url('{{section ${image.path} ${image.fileCollection}}}')`)}>{props.labels.get(image.fileCollection)}</Nav.Link>
                        </Nav.Item>)
                      }
                    </Nav>
                    :
                    null
                  }
                </>
                :
                null
              }
            </>
          }
          onLoadFile={(name, callback) => props.onLoadFile("css", name, callback)}
          onLoadDefault={(callback) => props.onLoadDefault("css", callback)}
          onLoadList={(callback) => props.onLoadList("css", callback)}
          onSubmitDefault={(defaultData, callback) => props.onSubmitDefault("css", defaultData, callback)}
          onUpload={(file, callback) => props.onUpload("css", file, callback)}
          onSubmitDataFile={(name, file, callback) => props.onSubmitDataFile("css", name, file, callback)}
          onRemoveFile={(name, callback) => props.onRemoveFile("css", "name", name, callback)}
          onCreateEmptyFile={(filename, callback) => props.onCreateEmptyFile("css", filename, callback)}
          syntax="css"
          managerName="cssManager"
          title="cssManager"
          defaultName="defaultCSS"
          listName="cssList"
          uploadLabel="uploadCSSFile"
          accept=".css,text/css"
          data={props.cssData}
          registry={props.secondRegistry}
          labels={props.labels}
        />
        :
        editor === "page" ?
        <PageEditor
          page={props.page}
          pageSchema={props.pageSchema}
          sectionSchemas={props.sectionSchemas}
          customSectionSchemas={props.customSectionSchemas}
          contentSchemas={props.contentSchemas}
          customContentSchemas={props.customContentSchemas}
          contents={props.contents}
          languages={props.languages}
          onGetPreview={props.onGetPreview}
          onDataChange={props.onDataChange}
          onContentChosen={props.onContentChosen}
          onUserSearch={props.onUserSearch}
          onPrivilegeSearch={props.onPrivilegeSearch}
          onPrivilegeAssigned={props.onPrivilegeAssigned}
          onPrivilegeRemoved={props.onPrivilegeRemoved}
          files={props.files}
          filesLoaded={props.filesLoaded}
          onUnlistFile={props.onUnlistFile}
          onAddFile={props.onAddFile}
          onGetContentFiles={props.onGetContentFiles}
          onRemoveFile={props.onRemoveContentFile}
          onRepositionFile={props.onRepositionFile}
          onEditFileCaptions={props.onEditFileCaptions}
          labels={props.labels}
          onUseSectionTemplate={props.onUseSectionTemplate}
          registry={props.secondRegistry}
        />
        :
        editor === "fonts" ?
        <FileManager
          onLoadFileList={(callback) => props.onLoadList("fonts", callback)}
          onRemove={(id, callback) => props.onRemoveFile("fonts", "id", id, callback)}
          onUpload={(file, name, callback) => props.onUploadFile("fonts", file, name, callback)}
          managerName="cssManager"
          title="fonts"
          fileNameLabel="fontName"
          accept="font/collection,font/otf,font/sfnt,font/ttf,font/woff,font/woff2"
          data={props.fontData}
          labels={props.labels}
        />
        :
        editor === "js" ?
        <FileDataManager
          editor={(editorProps) => 
            <>
              <Nav className="my-1" style={{fontSize: '12px'}}>
                <Nav.Item>
                  <Nav.Link eventKey="images" onClick={() => {
                    if (action !== false && action.action === "images") setAction(false);
                    else props.onLoadImages(props.selection, (images) => setAction({action: "images", data: images}));
                  }}><Image /> {props.labels.get("images")}</Nav.Link>
                </Nav.Item>
              </Nav>
              {
                action !== false && action.action === "images" ?
                <>
                  <Nav className="my-1" style={{fontSize: '12px'}}>
                    <Nav.Item>
                      <Nav.Link className="text-dark" eventKey={"templateLabel"}>{props.labels.get("templateFiles")}</Nav.Link>
                    </Nav.Item>
                    {
                      action.data.template.map((image, i) => <Nav.Item key={i}>
                        <Nav.Link eventKey={i} onClick={() => editorProps.insert(`'|||template ${image.id}|||'`)}>{image.name}</Nav.Link>
                      </Nav.Item>)
                    }
                  </Nav>
                </>
                :
                null
              }
            </>
          }
          onLoadFile={(name, callback) => props.onLoadFile("js", name, callback)}
          onLoadDefault={(callback) => props.onLoadDefault("js", callback)}
          onLoadList={(callback) => props.onLoadList("js", callback)}
          onSubmitDefault={(defaultData, callback) => props.onSubmitDefault("js", defaultData, callback)}
          onUpload={(file, callback) => props.onUpload("js", file, callback)}
          onSubmitDataFile={(name, file, callback) => props.onSubmitDataFile("js", name, file, callback)}
          onRemoveFile={(name, callback) => props.onRemoveFile("js", "name", name, callback)}
          onCreateEmptyFile={(filename, callback) => props.onCreateEmptyFile("js", filename, callback)}
          syntax="javascript"
          managerName="jsManager"
          title="jsManager"
          defaultName="defaultJS"
          listName="jsList"
          uploadLabel="uploadJSFile"
          accept=".js,text/javascript"
          data={props.jsData}
          registry={props.secondRegistry}
          labels={props.labels}
        />
        :
        editor === "possiblePaths" ?
        <PathList
          onChoosePath={(path) => props.insert(path)}
          {...props}
        />
        :
        editor === "selectPath" ?
        <PageTreeView
          languages={props.languages}
          page={props.page}
          defaultExpanded={[props.page.getSection().id.toString()]}
          onNodeSelect={(path) => props.insert(`'${path}'`)}
          buttonText={props.labels.get("select")}
        />
        :
        editor === "templateSnippets" ?
        <TemplateSnippetHandler
          onGetSnippetList={props.onGetSnippetList}
          snippets={props.snippets}
          registry={props.secondRegistry}
          onEditSnippet={props.onEditSnippet}
          labels={props.labels}
          onAddSnippet={props.onAddSnippet}
          onGetSnippet={props.onGetSnippet}
          onRemoveSnippet={props.onRemoveSnippet}
        />
        :
        editor === "importTemplateSnippet" ?
        <TemplateSnippetChooser
          {...props}
        />
        :
        editor === "textSnippets" ?
        <TextSnippetManager
          textSnippets={props.textSnippets}
          registry={props.secondRegistry}
          labels={props.labels}
          onGetTextSnippets={props.onGetTextSnippets}
          onSubmitText={props.onSubmitText}
          onGetTextSnippet={props.onGetTextSnippet}
          onRemoveTextSnippet={props.onRemoveTextSnippet}
          onEditTextSnippet={props.onEditTextSnippet}
          languages={props.languages}
        />
        :
        editor === "importTextSnippet" ?
        <TextSnippetChooser
          {...props}
        />
        :
        null
      }
    </>
  );
}

function TemplateEditor(props) {
  const [templateData, setTemplateData] = React.useState({template: props.templates.get(props.selection), change: false});
  const [secondEditor, setSecondEditor] = React.useState({editor: null});
  const [htmlActiveKey, setHtmlActiveKey] = React.useState("body");
  const dialogRef = React.useRef();
  const registryDialogRef = React.useRef();
  const [secondRegistry, ] = React.useState(props.registry.splitRegistry("second"));
  const templateType = (props.selection.ref === "path" && props.selection.rel.split("/").length === 1) ? "page" : "section";
  const editorRefs = {body: React.useRef(), head: React.useRef()};

  React.useEffect(() => {
    const currentTemplate = props.templates.get(props.selection);
    Object.keys(currentTemplate).forEach((c) => {
      if (editorRefs[c].current !== undefined) editorRefs[c].current.set(currentTemplate[c]);
    });
    props.registry.unsetRegistry(() => setTemplateData((prevTemplate) => {
      return ({
        template: prevTemplate.template,
        change: false
      });
    }));
  }, [props.templates]);

  React.useEffect(() => {
    const timed = timely(3000, () => {
      if (templateData.change !== false) handleSubmit();
    });
    return timed;
  }, [templateData]);

  function handleChange(templatePart, templateType) {
    props.registry.setRegistry();
    setTemplateData((prevTemplate) => {
      return ({template: {
        ...prevTemplate.template,
        [templateType]: templatePart
      }, change: true});
    });
  }

  function handleSubmit() {
    const submit = {ref: props.selection.ref, rel: props.selection.rel, data: {...templateData.template}};
    props.onTemplateSubmit(submit, () => props.registry.unsetRegistry(() => {}));
  }

  function insert(path) {
    if (templateType === "page") {
      if (htmlActiveKey === "head") editorRefs.head.current.insert(path);
      if (htmlActiveKey === "body") editorRefs.body.current.insert(path);
    }
    else editorRefs.body.current.insert(path); 
  }

  function importPredefined() {
    const section = props.page.getSection(props.selection.rel.split("/").map(Number));
    props.onTemplateChosen({ref: "type", rel: {custom: section.custom, name: section.type}}, () => {
      const predefinedTemplate = props.templates.get({ref: "type", rel: {custom: section.custom, name: section.type}});
      if (predefinedTemplate !== null && predefinedTemplate.body !== undefined) dialogRef.current.showDialog({
        title: props.labels.get("confirmation"),
        text: props.labels.get("importPredefinedTemplateConfirm"),
        callback: () => editorRefs.body.current.set(predefinedTemplate.body)
      });
    });
  }

  const view = () => {
    return (
      <>
        {
          templateType === "page" ?
          <Tabs
            activeKey={htmlActiveKey}
            onSelect={(key) => setHtmlActiveKey(key)}
          >
            <Tab eventKey="head" title={props.labels.get("head")}>
              <CodeEditor
                ref={editorRefs.head}
                defaultValue={templateData.template.head}
                onChange={handleChange}
                templatePart="head"
                templateType="head"
                height="100vh"
                {...props}
              />
            </Tab>
            <Tab eventKey="body" title={props.labels.get("body")}>
              <CodeEditor
                ref={editorRefs.body}
                defaultValue={templateData.template.body}
                onChange={handleChange}
                templatePart="body"
                templateType={templateType}
                height="100vh"
                {...props}
              />
            </Tab>
          </Tabs>
          :
          <CodeEditor
            ref={editorRefs.body}
            defaultValue={templateData.template.body}
            onChange={handleChange}
            templatePart="body"
            templateType={templateType}
            height="100vh"
            {...props}
          />
        }
      </>      
    )
  };

  return (
    <>
      <Dialog
        ref={dialogRef}
      />
      <Dialog
        ref={registryDialogRef}
      />
      <Nav
        className="my-3"
        variant="tabs"
        activeKey={secondEditor.editor}
      >
        <Nav.Item>
          <Nav.Link eventKey="possiblePaths" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "possiblePaths"}))}><AccountTree /> {props.labels.get("possiblePaths")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="selectPath" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "selectPath"}))}><Route /> {props.labels.get("selectPath")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="page" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "page"}))}><Pages /> {props.labels.get("pageManager")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="css" onClick={() => secondRegistry.getRegistry(() => (setSecondEditor({editor: "css"})))}><Css /> {props.labels.get("cssManager")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="fonts" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "fonts"}))}><FontDownload /> {props.labels.get("fontManager")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="js" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "js"}))}><Javascript /> {props.labels.get("jsManager")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="templateSnippets" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "templateSnippets"}))}><TextSnippetIcon /> {props.labels.get("templateSnippets")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="importTemplateSnippet" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "importTemplateSnippet"}))}><TextSnippetIcon /> {props.labels.get("importTemplateSnippet")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="textSnippets" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "textSnippets"}))}><Title /> {props.labels.get("textSnippets")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="importTextSnippet" onClick={() => secondRegistry.getRegistry(() => setSecondEditor({editor: "importTextSnippet"}))}><Title /> {props.labels.get("importTextSnippet")}</Nav.Link>
        </Nav.Item>
        {
          props.selection.ref === "path" && templateType !== "page" ?
          <Nav.Item>
            <Nav.Link eventKey="importPredefinedTemplate" onClick={importPredefined}><ListAlt /> {props.labels.get("importPredefinedTemplate")}</Nav.Link>
          </Nav.Item>
          :
          null
        }
        <Nav.Item>
          <Nav.Link eventKey="preview" onClick={() => props.onGetPreview(props.selection.ref, props.selection.rel, templateData.template)}><Preview /> {props.labels.get("preview")}</Nav.Link>
        </Nav.Item>
      </Nav>
      <div style={{height: "100vh"}}>
        <ReflexContainer orientation="vertical">
          <ReflexElement flex={0.9} className="left-pane me-3">
            <div className="pane-content">
              <div style={{minWidth: '50vw', overflow: 'scroll'}}>
                {
                  view()
                }
              </div>
            </div>
          </ReflexElement>
          <ReflexSplitter />
          <ReflexElement className="right-pane ms-3">
            <div style={{minWidth: '30vw', overflow: 'scroll'}}>
              <div className="pane-content">
                <SecondEditor 
                  editor={secondEditor.editor}
                  insert={insert}
                  secondRegistry={secondRegistry}
                  {...props}
                />
              </div>
            </div>
          </ReflexElement>
        </ReflexContainer>
      </div>
    </>
  );
}

function SchemaList(props) {
  return (
    <Stack gap={3}>
      {
        props.schemas.sort((a, b) => sortSectionSchemas(a.name, b.name, props.labels)).map((schema, i) => <Button key={i} onClick={() => props.onSelect({ref: "type", rel: {name: schema.name, custom: props.custom}})}> {(props.custom === true ? schema.name : props.labels.get(schema.name))}</Button>)
      }
    </Stack>
  );
}

export function TemplateFileHandler(props) {
  return (
    <>
      <FileManager
        onLoadFileList={(callback) => props.onLoadList("templateFiles", callback)}
        onRemove={(id, callback) => props.onRemoveFile("templateFiles", "id", id, callback)}
        onUpload={(file, name, callback) => props.onUploadFile("templateFiles", file, name, callback)}
        onGetThumbnail={props.onGetThumbnail}
        managerName="templateFileManager"
        title="templateFiles"
        fileNameLabel="filename"
        accept="video/*,audio/*,image/*"
        data={props.templateFilesData}
        labels={props.labels}
      />
    </>
  );
}

export function HomepageSelector(props) {
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>{props.labels.get("selectHomepage")}</Breadcrumb.Item>
      </Breadcrumb>
      {
        props.page.hasProperty("homepage") ?
        <Alert>{props.page.getSection(props.page.getSection().homepage.split("/").map(Number)).title[props.languages.defaultLanguage]}</Alert>
        :
        null
      }
      <PageTreeView
        languages={props.languages}
        page={props.page}
        defaultExpanded={[props.page.getSection().id.toString()]}
        onNodeSelect={(path) => {
          props.onHomepageSelect(path);
        }}
        buttonText={props.labels.get("select")}
        exclude={[props.page.getSection().id.toString()]}
      />
    </>
  );
}

export function SectionTemplateChooser(props) {
  const registryDialogRef = React.useRef(null);
  const [selectedSchema, setSelectedSchema] = React.useState(null);
  const getTemplate = (schemaRef) => {
    props.onTemplateChosen(schemaRef, () => setSelectedSchema(schemaRef));
  };
  return (
    <>
      <Dialog
        ref={registryDialogRef}
      />
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setSelectedSchema(null))}>{props.labels.get("editSectionTemplates")}</Breadcrumb.Item>
        {
          selectedSchema !== null ?
          <Breadcrumb.Item active>{props.labels.get("editTemplate", (selectedSchema.ref === "path" ? props.page.getSection(selectedSchema.rel.split("/").map(Number)).title[props.languages.defaultLanguage] : props.labels.get(selectedSchema.rel.name)))}</Breadcrumb.Item>
          :
          null
        }
      </Breadcrumb>
      {
        selectedSchema === null ?
        <PageTreeView
          languages={props.languages}
          page={props.page}
          defaultExpanded={[props.page.getSection().id.toString()]}
          onNodeSelect={(path) => getTemplate({ref: "path", rel: path})}
          buttonText={props.labels.get("edit")}
        />
        :
        <TemplateEditor
          selection={selectedSchema}
          addTranslations={(translations) => props.onAddTranslations(translations, () => {})}
          {...props}
        />
      }
    </>
  );
}

export function TemplateSnippetHandler(props) {
  const editorRef = React.useRef();
  const monacoRef = React.useRef();
  const [name, setName] = React.useState("");
  const [snippet, setSnippet] = React.useState("");
  const [activeKey, setActiveKey] = React.useState({nav: null, key: null});
  const [rerender, setRerender] = React.useState(false);
  React.useEffect(() => {
    props.onGetSnippetList((snippetList) => {
      if (activeKey.key === "snippetList" || (activeKey.key === "editSnippet" && !snippetList.includes(activeKey.name))) setActiveKey({nav: "snippetList", key: "snippetList", list: snippetList});
      else if (activeKey.key === "editSnippet") props.registry.unsetRegistry(() => setRerender(true));
    });
  },[props.snippets]);
  React.useEffect(() => {
    setRerender(true);
  },[activeKey]);
  React.useEffect(() => {
    if (rerender === true) setRerender(false);
  },[rerender]);
  React.useEffect(() => {
    const timed = timely(3000, () => {
      if (activeKey.key === "editSnippet") props.registry.unsetRegistry(() => props.onEditSnippet(snippet, activeKey.name));
    });
    return timed;
  }, [snippet]);
  if (rerender === true) return null;
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setActiveKey({nav: null, key: null}))}>{props.labels.get("templateSnippets")}</Breadcrumb.Item>
        {
          activeKey.key === "newSnippet" ?
          <Breadcrumb.Item active>{props.labels.get("newSnippet")}</Breadcrumb.Item>
          :
          activeKey.key === "snippetList" ?
          <Breadcrumb.Item active>{props.labels.get("snippetList")}</Breadcrumb.Item>
          :
          activeKey.key === "editSnippet" ?
          <>
            <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => props.onGetSnippetList((snippetList) => setActiveKey({nav: "snippetList", key: "snippetList", list: snippetList})))}>{props.labels.get("snippetList")}</Breadcrumb.Item>
            <Breadcrumb.Item active>{activeKey.name}</Breadcrumb.Item> 
          </>     
          :
          null
        }
      </Breadcrumb>
      <Nav
        className="my-3"
        variant="tabs"
        activeKey={activeKey.nav}
      >
        <Nav.Item>
          <Nav.Link eventKey="newSnippet" onClick={() => props.registry.getRegistry(() => setActiveKey({nav: "newSnippet", key: "newSnippet"}))}><TextSnippetIcon /> {props.labels.get("new")}</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="snippetList" onClick={() => props.registry.getRegistry(() => props.onGetSnippetList((snippetList) => setActiveKey({nav: "snippetList", key: "snippetList", list: snippetList})))}><ListAlt /> {props.labels.get("snippetList")}</Nav.Link>
        </Nav.Item>
      </Nav>
      {
        activeKey.nav === "newSnippet" ?
        <>
          <InputGroup>
            <Form.Control placeholder={props.labels.get("name")} type="text" onChange={(e) => props.registry.setRegistry(() => setName(e.target.value))} />
            <Button onClick={() => props.registry.unsetRegistry(() => props.onAddSnippet(editorRef.current.getValue(), name))}>{props.labels.get("submit")}</Button>
          </InputGroup>
          <Editor
            className="my-3"
            height="100vh"
            defaultLanguage="html"
            onMount={(e, m) => {
              editorRef.current = e; 
              monacoRef.current = m; 
              editorRef.current.getModel().updateOptions({tabSize: 2})
            }}
            onChange={() => props.registry.setRegistry()}
          />          
        </>
        :
        activeKey.key === "snippetList" ?
        <Stack gap={3}>
          {
            activeKey.list.sort().map((snippetName, i) => <ButtonGroup key={i}>
              <Button onClick={() => props.onGetSnippet(snippetName, (snippet) => setActiveKey({nav: "snippetList", key: "editSnippet", name: snippetName, snippet: snippet}))}><Edit /> {snippetName}</Button>
              <Button className="btn-danger" onClick={() => props.onRemoveSnippet(snippetName, () => props.onGetSnippetList((snippetList) => setActiveKey({nav: "snippetList", key: "snippetList", list: snippetList})))}><Delete /> {props.labels.get("remove")}</Button>
            </ButtonGroup>)
          }
        </Stack>
        :
        activeKey.key === "editSnippet" ?
        <>
          <Editor
            className="my-3"
            height="100vh"
            defaultLanguage="html"
            defaultValue={activeKey.snippet}
            onMount={(e, m) => {
              editorRef.current = e; 
              monacoRef.current = m; 
              editorRef.current.getModel().updateOptions({tabSize: 2})
            }}
            onChange={() => props.registry.setRegistry(() => setSnippet(editorRef.current.getValue()))}
          />          
        </>
        :
        null
      }
    </>
  );
}

export function PredefinedTemplateChooser(props) {
  const registryDialogRef = React.useRef(null);
  const [selectedSchema, setSelectedSchema] = React.useState(null);
  const getTemplate = (schemaRef) => {
    props.onTemplateChosen(schemaRef, () => setSelectedSchema(schemaRef));
  };
  return (
    <>
      <Dialog
        ref={registryDialogRef}
      />
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => props.registry.getRegistry(() => setSelectedSchema(null))}>{props.labels.get("editPredefinedTemplates")}</Breadcrumb.Item>
        {
          selectedSchema !== null ?
          <Breadcrumb.Item active>{props.labels.get("editTemplate", (selectedSchema.rel.custom !== true ? props.labels.get(selectedSchema.rel.name) : selectedSchema.rel.name))}</Breadcrumb.Item>
          :
          null
        }
      </Breadcrumb>
      {
        selectedSchema === null ?
        <Row>
          <Col sm>
            <Container className="p-3 my-3 border">
              <h4>{props.labels.get("predefined")}</h4>
              <SchemaList
                schemas={props.sectionSchemas}
                custom={false}
                onSelect={(schemaRef) => getTemplate(schemaRef)}
                {...props}
              />
            </Container>
          </Col>
          <Col sm>
            <Container className="p-3 my-3 border">
              <h4>{props.labels.get("custom")}</h4>
              <SchemaList
                schemas={props.customSectionSchemas}
                custom={true}
                onSelect={(schemaRef) => getTemplate(schemaRef)}
                {...props}
              />
            </Container>
          </Col>
        </Row>
        :
        <TemplateEditor
          selection={selectedSchema}
          addTranslations={(translations) => props.onAddTranslations(translations, () => {})}
          {...props}
        />
      }
    </>
  );
}
