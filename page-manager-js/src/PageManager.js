import React, { useEffect, useState } from 'react';
import './index.css';
import {LanguageEditor, DefaultLanguageChooser} from './languageManager.js';
import PageEditor from './pageEditor.js';
import Page from './page.js';
import Dialog from './dialog.js';
import {UserAdder, UserEditor} from './userManager.js';
import {PredefinedTemplateChooser, SectionTemplateChooser, TemplateSnippetHandler, HomepageSelector, TemplateFileHandler} from './templateManager.js';
import TextSnippetManager from './textSnippetManager';
import {SectionAdder, SchemaEditor, ContentAdder} from './schemaManager.js';
import InformationHandler from './informationHandler.js';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Alert from 'react-bootstrap/Alert';
import Offcanvas from 'react-bootstrap/Offcanvas';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import Labels from './labels.js';
import Templates from './templates.js';
import Cookies from 'universal-cookie';
import Registry from './registry.js';
import ConflictList from './conflictManager.js';
import FileDataManager from './fileDataManager.js';
import FileManager from './fileManager.js';
import Confirmator from './confirmator.js';
import PasswordResetter from './passwordResetter.js';
import SettingsManager from './settingsManager.js';
import axios from 'axios';

var cloneDeep = require('lodash.clonedeep');
const rootPath = process.env.REACT_APP_SERVER_IP;
const serverPath = rootPath + 'server.php';
const previewPath = rootPath + 'preview.php';

// PHP DEPENDENCIES
// php 7.4>
// intl
// gd

// HIER WEITER
// 1) issues: check by try

function TotalFreeze(props) {
  if (props.frozen === true)
  return (
    <div className='total-freeze'>
      <div className='anti-waiter'>
      <div className='anti-dotter'></div>
      </div>
      <div className='waiter'>
        <div className='dotter'></div>
      </div>
    </div>
  );
  else return null;
}

function Manual(props) {
  const [ref, setRef] = useState(null);
  useEffect(() => {
    if (ref !== null) {
      props.loadManual((manualXML) => {
        const parser = new DOMParser();
        const manual = parser.parseFromString(manualXML, "text/html");
        while (ref.firstChild) {
          ref.removeChild(ref.firstChild);
        }
        ref.appendChild(manual.getElementById("manual-section"));
      });
    }
  }, [ref]);
  return (
    <Offcanvas style={{width: '80%'}} show={props.show} onHide={() => props.onClose()}>
      <Offcanvas.Header closeButton>
        <Offcanvas.Title>{ props.labels.get("manual")}</Offcanvas.Title>
      </Offcanvas.Header>
      <Offcanvas.Body ref={newRef => setRef(newRef)}>
      </Offcanvas.Body>
    </Offcanvas>
  );
}

function PageNav(props) {
  const [expanded, setExpanded] = React.useState(false);
  const setCurrentEditor = (editor) => {
    props.setCurrentEditor(editor);
    setExpanded(false);
  }

  return (
    <Navbar expanded={expanded} expand="xxl" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand onClick={() => setCurrentEditor(null)}><img
            alt={props.labels.get("mainTitle")}
            src="logo_main.png"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{' '}{props.labels.get("mainTitle")}</Navbar.Brand>
        <Navbar.Toggle onClick={() => setExpanded(expanded ? false : "expanded")} aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <NavDropdown title={props.labels.get("languageManager")} id="language-nav-dropdown">
              <NavDropdown.Item onClick={() => setCurrentEditor("editLangs")}>{props.labels.get("editLangs")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("setDefaultLang")}>{props.labels.get("setDefaultLang")}</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link onClick={() => setCurrentEditor("editPage")}>{props.labels.get("pageManager")}</Nav.Link>
            <NavDropdown title={props.labels.get("userManager")} id="user-nav-dropdown">
              <NavDropdown.Item onClick={() => setCurrentEditor("addUser")}>{props.labels.get("addUser")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("editUsers")}>{props.labels.get("editUsers")}</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown disabled={props.user.privilege !== "a" ? true : false} title={props.labels.get("templateManager")} id="template-nav-dropdown">
              <NavDropdown.Item onClick={() => setCurrentEditor("editPredefinedTemplates")}>{props.labels.get("editPredefinedTemplates")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("editSectionTemplates")}>{props.labels.get("editSectionTemplates")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("templateSnippets")}>{props.labels.get("templateSnippets")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("textSnippets")}>{props.labels.get("textSnippets")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("selectHomepage")}>{props.labels.get("selectHomepage")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("templateFiles")}>{props.labels.get("templateFiles")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("informationTexts")}>{props.labels.get("informationTexts")}</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown disabled={props.user.privilege !== "a" ? true : false} title={props.labels.get("clientCode")} id="client-nav-dropdown">
              <NavDropdown.Item onClick={() => setCurrentEditor("editCSS")}>{props.labels.get("cssManager")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("editFonts")}>{props.labels.get("fontManager")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("editJS")}>{props.labels.get("jsManager")}</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown disabled={props.user.privilege !== "a" ? true : false} title={props.labels.get("schemaManager")} id="schema-nav-dropdown">
              <NavDropdown.Item onClick={() => setCurrentEditor("createSectionSchema")}>{props.labels.get("createSectionSchema")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("editSectionSchemas")}>{props.labels.get("editSectionSchemas")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("createContentSchema")}>{props.labels.get("createContentSchema")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("editContentSchemas")}>{props.labels.get("editContentSchemas")}</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title={props.labels.get("actions")} id="action-nav-dropdown">
              <NavDropdown.Item onClick={props.handleUndo}>{props.labels.get("undo")}</NavDropdown.Item>
              <NavDropdown.Item onClick={props.upload}>{props.labels.get("save")}</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title={`${props.user.fullName} (${props.labels.get(props.user.privilege)})`} id="user-nav-dropdown">
              <NavDropdown.Item onClick={props.logout}>{props.labels.get("logout")}</NavDropdown.Item>
              <NavDropdown.Item onClick={() => setCurrentEditor("resetPassword")}>{props.labels.get("resetPassword")}</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link onClick={() => setCurrentEditor("settings")}>{props.labels.get("settings")}</Nav.Link>
            <Nav.Link onClick={() => props.showManual()}>?</Nav.Link>
          </Nav>
          <Nav>
            {
              props.editorLanguages !== null ?
              props.editorLanguages.languages.map((language, i) => <Nav.Link key={i} onClick={() => props.setEditorLanguage(language.iso639)}>{language.name}</Nav.Link>)
              :
              null
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default class PageManager extends React.Component {
  constructor(props) {
    super(props);

    this.dialogRef = React.createRef();
    this.registryDialogRef = React.createRef();
    this.confirmatorRef = React.createRef();
    this.state = {
      currentEditor: null,
      history: [],
      languages: null,
      languageSchema: null,
      page: null,
      contents: {},
      filesLoaded: {},
      forms: {},
      css: {},
      fonts: {},
      js: {},
      templateFiles: {},
      userSchema: null,
      templates: new Templates(),
      templateActions: null,
      snippets: {},
      textSnippets: {},
      pageSchema: null,
      sectionSchemas: null,
      customSectionSchemas: null,
      contentSchemas: null,
      customContentSchemas: null,
      formSchema: null,
      informationTextSchema: null,
      translations: null,
      editorLanguages: null,
      curEditorLang: null,
      labelsList: null,
      labels: null,
      registry: null,
      loaded: false,
      uploadConflict: false,
      informationTexts: null,
      user: null,
      frozen: false,
      showManual: false,
      manual: null
    };

    this.languagesExist = this.languagesExist.bind(this);
    this.handleLanguagesEdited = this.handleLanguagesEdited.bind(this);
    this.handleChooseDefaultLanguage = this.handleChooseDefaultLanguage.bind(this);
    this.handleUndo = this.handleUndo.bind(this);
    this.setCurrentEditor = this.setCurrentEditor.bind(this);
    this.createPageUpload = this.createPageUpload.bind(this);
    this.resetData = this.resetData.bind(this);
    this.upload = this.upload.bind(this);
    this.handleResolveConflict = this.handleResolveConflict.bind(this);
    this.logout = this.logout.bind(this);
    this.resetPassword = this.resetPassword.bind(this);
    this.handleGetSettings = this.handleGetSettings.bind(this);
    this.handleUpdateSetting = this.handleUpdateSetting.bind(this);
    this.handleDataChange = this.handleDataChange.bind(this);
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
    this.handleChangeActiveStatus = this.handleChangeActiveStatus.bind(this);
    this.handleGetForms = this.handleGetForms.bind(this);
    this.handleGetFormData = this.handleGetFormData.bind(this);
    this.handleClearFormData = this.handleClearFormData.bind(this);
    this.handleRemoveForm = this.handleRemoveForm.bind(this);
    this.handleAddFile = this.handleAddFile.bind(this);
    this.handleGetContentFiles = this.handleGetContentFiles.bind(this);
    this.handleGetThumbnail = this.handleGetThumbnail.bind(this);
    this.handleRemoveContentFile = this.handleRemoveContentFile.bind(this);
    this.handleRepositionFile = this.handleRepositionFile.bind(this);
    this.handleEditFileCaptions = this.handleEditFileCaptions.bind(this);
    this.handleContentChosen = this.handleContentChosen.bind(this);
    this.handleTemplateChosen = this.handleTemplateChosen.bind(this);
    this.handleAddUser = this.handleAddUser.bind(this);
    this.handleEditUser = this.handleEditUser.bind(this);
    this.handleRemoveUser = this.handleRemoveUser.bind(this);
    this.handlePrivilegeAssigned = this.handlePrivilegeAssigned.bind(this);
    this.handlePrivilegeRemoved = this.handlePrivilegeRemoved.bind(this);
    this.handleTemplateSubmit = this.handleTemplateSubmit.bind(this);
    this.handleUseSectionTemplate = this.handleUseSectionTemplate.bind(this);
    this.handleAddSnippet = this.handleAddSnippet.bind(this);
    this.handleEditSnippet = this.handleEditSnippet.bind(this);
    this.handleGetSnippetList = this.handleGetSnippetList.bind(this);
    this.handleGetSnippet = this.handleGetSnippet.bind(this);
    this.handleRemoveSnippet = this.handleRemoveSnippet.bind(this);
    this.handleSubmitText = this.handleSubmitText.bind(this);
    this.handleGetTextSnippets = this.handleGetTextSnippets.bind(this);
    this.handleRemoveTextSnippet = this.handleRemoveTextSnippet.bind(this);
    this.handleGetTextSnippet = this.handleGetTextSnippet.bind(this);
    this.handleEditTextSnippet = this.handleEditTextSnippet.bind(this);
    this.handleHompageSelect = this.handleHompageSelect.bind(this);
    this.handleGetPreview = this.handleGetPreview.bind(this);
    this.setEditorLanguage = this.setEditorLanguage.bind(this);
    this.handleServerRequest = this.handleServerRequest.bind(this);
    this.handleUserSearch = this.handleUserSearch.bind(this);
    this.handlePrivilegeSearch = this.handlePrivilegeSearch.bind(this);
    this.handleGetUser = this.handleGetUser.bind(this);
    this.handleLoadImages = this.handleLoadImages.bind(this);
    this.handleLoadList = this.handleLoadList.bind(this);
    this.handleSubmitDefault = this.handleSubmitDefault.bind(this);
    this.handleSubmitDataFile = this.handleSubmitDataFile.bind(this);
    this.handleLoadDefault = this.handleLoadDefault.bind(this);
    this.handleLoadFile = this.handleLoadFile.bind(this);
    this.handleRemoveDataFile = this.handleRemoveDataFile.bind(this);
    this.handleCreateEmptyFile = this.handleCreateEmptyFile.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.checkRemove = this.checkRemove.bind(this);
    this.handleUploadFile = this.handleUploadFile.bind(this);
    this.handleReloadCustomSectionSchemas = this.handleReloadCustomSectionSchemas.bind(this);
    this.handleReloadCustomContentSchemas = this.handleReloadCustomContentSchemas.bind(this);
    this.handleUploadSectionSchema = this.handleUploadSectionSchema.bind(this);
    this.handleRemoveSectionSchema = this.handleRemoveSectionSchema.bind(this);
    this.handleUploadContentSchema = this.handleUploadContentSchema.bind(this);
    this.handleRemoveContentSchema = this.handleRemoveContentSchema.bind(this);
    this.handleGetSchemaUpload = this.handleGetSchemaUpload.bind(this);
    this.handleAddTranslations = this.handleAddTranslations.bind(this);
    this.handleGetInformationTexts = this.handleGetInformationTexts.bind(this);
    this.handleSubmitInformationTexts = this.handleSubmitInformationTexts.bind(this);
    this.notify = this.notify.bind(this);
    this.loadManual = this.loadManual.bind(this);
    this.showManual = this.showManual.bind(this);
  }

  languagesExist() {
    if (
      this.state.languages !== null &&
      "languages" in this.state.languages &&
      this.state.languages.languages.length > 0 &&
      "defaultLanguage" in this.state.languages &&
      this.state.languages.defaultLanguage !== null
    ) return true;
    return false;
  }
  
  componentDidMount() {
    window.onbeforeunload = () => true;
    document.title = "Page manager";
    const cookies = new Cookies();
    let langCookie = cookies.get('lang');
    if (langCookie === undefined) langCookie = null;
    var formData = new FormData();
    formData.append("jsonData", JSON.stringify({
      get: [
        {request: "PAGE_SCHEMA"},
        {request: "SECTION_SCHEMAS"},
        {request: "CUSTOM_SECTION_SCHEMAS"},
        {request: "CONTENT_SCHEMAS"},
        {request: "CUSTOM_CONTENT_SCHEMAS"},
        {request: "FORM_SCHEMA"},
        {request: "INFORMATION_TEXT_SCHEMA"},
        {request: "LANGUAGE_SCHEMA"},
        {request: "USER_SCHEMA"},
        {request: "LANGUAGES"},
        {request: "EDITOR_LANGUAGES"},
        {request: "PAGE"},
        {request: "LABELS", language: langCookie},
        {request: "TEMPLATE_ACTIONS"},
        {request: "TRANSLATIONS"},
        {request: "HISTORY"},
        {request: "USER"}
      ],
    }));
    axios.post(serverPath, formData, {
      withCredentials: true,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    .then((response) => {
      const pageSchema = response.data.PAGE_SCHEMA;
      const sectionSchemas = response.data.SECTION_SCHEMAS;
      const customSectionSchemas = response.data.CUSTOM_SECTION_SCHEMAS;
      const curEditorLang = response.data.LABELS.language;
      const labelsList = {[curEditorLang]: response.data.LABELS.labels};
      const labels = new Labels(labelsList[curEditorLang]);
      const newData = {
        "pageSchema": pageSchema,
        "sectionSchemas": sectionSchemas,
        "customSectionSchemas": customSectionSchemas,
        "contentSchemas": response.data.CONTENT_SCHEMAS,
        "customContentSchemas": response.data.CUSTOM_CONTENT_SCHEMAS,
        "formSchema": response.data.FORM_SCHEMA,
        "informationTextSchema": response.data.INFORMATION_TEXT_SCHEMA,
        "languageSchema": response.data.LANGUAGE_SCHEMA,
        "page": new Page(response.data.PAGE, pageSchema, sectionSchemas, customSectionSchemas),
        "languages": response.data.LANGUAGES,
        "editorLanguages": response.data.EDITOR_LANGUAGES,
        "curEditorLang": curEditorLang,
        "userSchema": response.data.USER_SCHEMA,
        "labelsList": labelsList,
        "labels": labels,
        "registry": new Registry(this.registryDialogRef, labels.get("back"), labels.get("backConfirmation")),
        "loaded": true,
        "templateActions": response.data.TEMPLATE_ACTIONS,
        "translations": response.data.TRANSLATIONS,
        "user": response.data.USER
      };
      this.setState(newData);
    })
    .catch((error) => {
      alert(error.message);
    });
  }

  handleServerRequest(request, files, callback) {
    this.setState({frozen: true});
    var formData = new FormData();
    formData.append("jsonData", JSON.stringify(request));
    if (files !== null) files.forEach((file) => {
      formData.append("files[]", file);
    });
    axios.post(serverPath, formData, {
      withCredentials: true,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    .then((response) => {
      this.setState({frozen: false});
      callback(response.data);
    })
    .catch((error) => {
      this.setState({frozen: false});
      let errMsg = "";
      errMsg = error.message;
      if (error.response && error.response.data) {
        const data = error.response.data;
        errMsg += " --- " + this.state.labels.get(data[0]);
        if (data.length > 1) {
          if (error.response.status === 409) errMsg += " --- " + data[1].filename + " (" + data[1].index + ")";
          else errMsg += " --- " + data[1];
        }
      }
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: errMsg
      });
    });
  }

  setCurrentEditor(name) {
    const currentEditor = this.state.currentEditor;
    if (currentEditor === name) return;
    if (currentEditor === null) this.setState({currentEditor: name});
    else {
      this.state.registry.getRegistry(() => this.setState({currentEditor: name}));
    }
  }

  setEditorLanguage(iso639) {
    const cookies = new Cookies();
    cookies.set('lang', iso639, { path: '/' });
    const labelsList = this.state.labelsList;
    if (!(iso639 in labelsList)) {
      this.handleServerRequest({
        get: [
          {request: "LABELS", language: iso639}
        ]
      }, null, (response) => {
        labelsList[iso639] = response.LABELS.labels;
        this.setState({curEditorLang: iso639, labelsList: labelsList, labels: new Labels(labelsList[iso639])}, () => this.loadManual(() => {}, true));
      });
    }
    else this.setState({curEditorLang: iso639, labels: new Labels(labelsList[iso639])}, () => this.loadManual(() => {}, true));
  }

  notify() {
    this.handleServerRequest({
      get: [
        {request: "NOTIFY"}
      ]
    }, null, () => {});
  }

  handleUndo() {
    const history = this.state.history;
    if (history.length === 0) return;
    const last = history.pop();
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("undo"),
      text: this.state.labels.get(last.update),
      callback: () => {
        last.history = history;
        const newState = this.state;
        if ("page" in last) newState.page = new Page(last.page, this.state.pageSchema, this.state.sectionSchemas, this.state.customSectionSchemas);
        if ("contents" in last) newState.contents = last.contents;
        if ("languages" in last) newState.languages = last.languages;
        if ("templates" in last) newState.templates = last.templates;
        if ("snippets" in last) newState.snippets = last.snippets;
        if ("textSnippets" in last) newState.textSnippets = last.textSnippets;
        if ("filesLoaded" in last) newState.filesLoaded = last.filesLoaded;
        if ("forms" in last) newState.forms = last.forms;
        if ("css" in last) newState.css = last.css;
        if ("fonts" in last) newState.fonts = last.fonts;
        if ("js" in last) newState.js = last.js;
        if ("templateFiles" in last) newState.templateFiles = last.templateFiles;
        this.setState(newState, this.confirmatorRef.current.setMsg(this.state.labels.get("undone")));
      }
    });
  }

  createPageUpload() {
    return {
      requests: [
        {request: "PAGE", data: this.state.page.getSection()},
        {request: "LANGUAGES", data: this.state.languages},
        {request: "CONTENTS", data: this.state.contents},
        {request: "TEMPLATES", data: this.state.templates.getAll()},
        {request: "SNIPPETS", data: this.state.snippets},
        {request: "TEXT_SNIPPETS", data: this.state.textSnippets},
        {request: "FILES", data: this.state.filesLoaded},
        {request: "FORMS", data: this.state.forms},
        {request: "CSS", data: this.state.css},
        {request: "FONTS", data: this.state.fonts},
        {request: "JS", data: this.state.js},
        {request: "TEMPLATE_FILES", data: this.state.templateFiles}
      ],
      files: null
    };
  }

  resetData(response) {
    if (response.CREATE_PAGE !== undefined && response.CREATE_PAGE !== true) this.dialogRef.current.showDialog({
      title: this.state.labels.get("error"),
      text: this.state.labels.get("uploadSuccessfulCreateError", this.state.labels.get(response.CREATE_PAGE))
    });
    else {
      this.setState({history: [], uploadConflict: false}, () => {
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("confirmation"),
          text: this.state.labels.get("uploadSuccessful")
        });
      });
    }
  }

  upload() {
    this.state.registry.getRegistry(() => {
      this.dialogRef.current.showDialog({
        "title": this.state.labels.get("save"),
        "text": this.state.labels.get("uploadConfirmation"),
        "callback": () => {
          const pageUpload = this.createPageUpload();
          pageUpload.requests.push({request: "HISTORY"});
          this.handleServerRequest({
            set: pageUpload.requests
          }, pageUpload.files, (response) => {
            if (typeof response === 'string' || response instanceof String) {
              this.dialogRef.current.showDialog({
                "title": this.state.labels.get("error"),
                "text": response
              });
              return;
            }
            else if (response.HISTORY !== false) {
              this.setState({uploadConflict: response.HISTORY});
              return;
            }
            this.resetData(response);
          })
        }
      });
    });
  }

  handleResolveConflict(decisions) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("resolveConflict"),
      callback: () => {
        const pageUpload = this.createPageUpload();
        pageUpload.requests.push({
          request: "RESOLVE_CONFLICT",
          id: this.state.uploadConflict.id,
          decisions: decisions
        });
        this.handleServerRequest({set: pageUpload.requests}, pageUpload.files, (response) => {
          this.resetData(response);
        });
      }
    });
  }

  logout() {
    this.state.registry.getRegistry(() => {
      this.handleServerRequest({set: [
        {request: "LOGOUT"}
      ]}, null, (response) => {
        window.onbeforeunload = undefined;
        window.location.assign(rootPath);
      });
    });
  }

  resetPassword(currentPassword, newPassword, repeatPassword) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("resetPasswordConfirm"),
      callback: () => {
        this.handleServerRequest({set: [
          {
            request: "RESET_PASSWORD",
            user: this.state.user.username,
            currentPassword: currentPassword,
            newPassword: newPassword,
            repeatPassword: repeatPassword
          }
        ]}, null, (response) => {
          this.dialogRef.current.showDialog({
            title: this.state.labels.get("confirmation"),
            text: this.state.labels.get("passwordSet")
          });
        });
      }
    });
  }

  handleGetSettings(callback) {
    this.handleServerRequest({get: [
      {
        request: "SETTINGS"
      }
    ]}, null, (response) => {
      callback(response.SETTINGS);
    });
  }

  handleUpdateSetting(name, value, callback) {
    this.handleServerRequest({set: [
      {
        request: "SETTING",
        name: name,
        value: value
      }
    ]}, null, (response) => {
      callback(response.SETTING);
    });
  }

  handleDataChange(data, callback) {
    const history = this.state.history;
    const clones = {};
    switch (data.action) {
      case "pc":
        this.handleServerRequest({get: [
          {
            request: "NEW_PAGE_ID"
          }
        ]}, null, (response) => {
          clones.page = cloneDeep(this.state.page.getSection());
          clones.update = "create-page";
          data.formData.id = response.NEW_PAGE_ID;
          this.state.page.setPageTree(data.formData);
          this.setState({page: this.state.page}, callback());
          this.confirmatorRef.current.setMsg(this.state.labels.get(clones.update));
        });
        break;
      case "sd":
        clones.page = cloneDeep(this.state.page.getSection());
        clones.update = "remove-section";
        this.state.page.removeSection(data.path);
        this.setState({page: this.state.page}, callback());
        this.confirmatorRef.current.setMsg(this.state.labels.get(clones.update));
        break;
      case "sa":
        const parentSection = this.state.page.getSection(data.path);
        if ("sections" in parentSection && parentSection.sections.filter((section) => section.name === data.formData.name).length > 0) {
          this.dialogRef.current.showDialog({
            "title": this.state.labels.get("error"),
            "text": this.state.labels.get("nameExists")
          });
          return;
        }
        const page = this.state.page.getSection();
        this.handleServerRequest({get: [
          {
            request: "NEW_PAGE_ID",
            page: page
          }
        ]}, null, (response) => {
          clones.page = cloneDeep(page);
          clones.update = "add-section";
          data.formData.type = data.type;
          data.formData.custom = data.custom;
          data.formData.id = response.NEW_PAGE_ID;
          this.state.page.addSection(data.path, data.formData);
          this.setState({page: this.state.page}, callback(data.formData.id));
          this.confirmatorRef.current.setMsg(this.state.labels.get(clones.update));
        });
        break;
      case "se":
        clones.page = cloneDeep(this.state.page.getSection());
        clones.update = "edit-section";
        this.state.page.replaceSection(data.path, data.formData);
        this.setState({page: this.state.page}, callback());
        this.confirmatorRef.current.setMsg(this.state.labels.get(clones.update));
        break;
      case "sm":
        clones.page = cloneDeep(this.state.page.getSection());
        clones.update = "move-section";
        this.state.page.repositionSection(data.path, data.dropped, data.current);
        this.setState({page: this.state.page}, callback());
        this.confirmatorRef.current.setMsg(this.state.labels.get(clones.update));
        break;
      case "ca":
        if (data.type === null || data.type.length < 1 || data.name === null || data.name.length < 1) {
          this.dialogRef.current.showDialog({
            "title": this.state.labels.get("error"),
            "text": this.state.labels.get("emptyContent")
          });
          return;
        }
        const section = this.state.page.getSection(data.path);
        if ("content" in section && section.content[data.name]) {
          this.dialogRef.current.showDialog({
            "title": this.state.labels.get("error"),
            "text": this.state.labels.get("nameExists")
          });
          return;
        }
        clones.page = cloneDeep(this.state.page.getSection());
        clones.contents = cloneDeep(this.state.contents);
        clones.update = "add-content";
        this.state.page.addContent(data.path, data.name, data.type, data.custom);
        this.setState({page: this.state.page}, callback());
        this.confirmatorRef.current.setMsg(this.state.labels.get(clones.update));
        break;
      case "ce": {
          clones.contents = cloneDeep(this.state.contents);
          clones.update = "edit-content";
          const contents = this.state.contents;
          const pathStr = data.path.join("/");
          const part = (data.custom === true ? "custom" : "predefined");
          if (!(pathStr in contents)) contents[pathStr] = {};
          if (!(part in contents[pathStr])) contents[pathStr][part] = {};
          contents[pathStr][part][data.name] = data.formData;
          this.setState({contents: contents}, callback());
          this.confirmatorRef.current.setMsg(this.state.labels.get(clones.update));
        }
        break;
      case "cr": {
          clones.page = cloneDeep(this.state.page.getSection());
          clones.contents = cloneDeep(this.state.contents);
          clones.update = "remove-content";
          const contents = this.state.contents;
          const pathStr = data.path.join("/");
          const part = (data.custom === true ? "custom" : "predefined");
          contents[pathStr][part][data.name] = null;
          this.state.page.removeContent(data.path, data.name, data.custom);
          this.setState({contents: contents, page: this.state.page}, callback());
          this.confirmatorRef.current.setMsg(this.state.labels.get(clones.update));
        }
        break;
      default:
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("error"),
          text: this.state.labels.get("noSuchAction"),
        });
        return;
    }
    history.push(clones);
    this.notify();
  }

  handleSubmitForm(formData, sectionPath, index) {
    const forms = this.state.forms;
    const sectionPathStr = sectionPath.join("/");
    const addByPath = () => {
      if (forms[sectionPathStr] === undefined) forms[sectionPathStr] = [];
    };
    if (index === null) {
      const history = this.state.history;
      history.push({
        forms: cloneDeep(this.state.forms),
        update: "add-form"
      });
      addByPath();
      forms[sectionPathStr].push(formData);
      this.setState({forms: forms});
      this.confirmatorRef.current.setMsg(this.state.labels.get("formAdded"));
      this.notify();
    }
    else {
      this.handleServerRequest({get: [
        {
          request: "HAS_FORM_DATA",
          sectionPath: sectionPathStr
        }
      ]}, null, (response) => {
        const forms = this.state.forms;
        if (response.HAS_FORM_DATA !== null) {
          for (let i = 0; i < forms[sectionPathStr].length; i++) {
            if (response.HAS_FORM_DATA.includes(forms[sectionPathStr][i].id)) forms[sectionPathStr][i].hasData = true;
            else forms[sectionPathStr][i].hasData = false;           
          }
        }
        this.setState({forms: forms}, () => {
          if (forms[sectionPathStr][index].hasData === true) {
            this.dialogRef.current.showDialog({
              title: this.state.labels.get("error"),
              text: this.state.labels.get("FORM_DATA_EXISTS")
            });
          }
          else {
            const history = this.state.history;
            history.push({
              forms: cloneDeep(this.state.forms),
              update: "edit-form"
            });
            addByPath();
            forms[sectionPathStr][index] = formData;
            this.setState({forms: forms});
            this.confirmatorRef.current.setMsg(this.state.labels.get("formEdited"));
          }
        });
      });
    }
  }

  handleChangeActiveStatus(sectionPath, index) {
    const history = this.state.history;
    history.push({
      forms: cloneDeep(this.state.forms),
      update: "edit-form"
    });
    const forms = this.state.forms;
    const sectionPathStr = sectionPath.join("/");
    const form = forms[sectionPathStr][index];
    if (form.active !== true) form.active = true;
    else form.active = false;
    this.setState({forms: forms});
    this.confirmatorRef.current.setMsg(this.state.labels.get("formEdited"));
    this.notify();
  }

  handleGetForms(sectionPath, callback) {
    const sectionPathStr = sectionPath.join("/");
    const forms = this.state.forms;
    if (forms[sectionPathStr] === undefined) {
      this.handleServerRequest({get: [
        {
          request: "FORMS",
          sectionPath: sectionPathStr
        }
      ]}, null, (response) => {
        forms[sectionPathStr] = response.FORMS;
        this.setState({forms: forms}, callback());
      });
    }
    else {
      this.handleServerRequest({get: [
        {
          request: "HAS_FORM_DATA",
          sectionPath: sectionPathStr
        }
      ]}, null, (response) => {
        if (response.HAS_FORM_DATA !== null) {
          for (let i = 0; i < forms[sectionPathStr].length; i++) {
            if (response.HAS_FORM_DATA.includes(forms[sectionPathStr][i].id)) forms[sectionPathStr][i].hasData = true;
            else forms[sectionPathStr][i].hasData = false;           
          }
        }
        this.setState({forms: forms}, callback());
      });
    }
  }

  handleGetFormData(sectionPath, id, callback) {
    this.handleServerRequest({get: [
      {
        request: "FORM_DATA",
        sectionPath: sectionPath.join("/"),
        id: id
      }
    ]}, null, (response) => {
      callback(response.FORM_DATA);
    });
  }

  handleClearFormData(sectionPath, id, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("clearForms"),
      callback: () => {
        const set = {set: [
          {
            request: "CLEAR_FORM_DATA",
            sectionPath: sectionPath.join("/"),
            id: id,
            overwrite: false
          }
        ]};
        this.handleServerRequest(set, null, (response) => {
          if (response.CLEAR_FORM_DATA === false) {
            set.set[0].overwrite = true;
            this.dialogRef.current.showDialog({
              title: this.state.labels.get("uploadConflicts"),
              text: this.state.labels.get("overwrite"),
              callback: () => {
                this.handleServerRequest(set, null, (response) => {
                  callback();
                  this.confirmatorRef.current.setMsg(this.state.labels.get("formDataCleared"));
                });            
              }
            });
            return;
          }
          callback();
          this.confirmatorRef.current.setMsg(this.state.labels.get("formDataCleared"));
        });
      }
    });
  }

  handleRemoveForm(sectionPath, index) {
    const sectionPathStr = sectionPath.join("/");
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("removeForm"),
      callback: () => {
        this.handleServerRequest({get: [
          {
            request: "HAS_FORM_DATA",
            sectionPath: sectionPathStr
          }
        ]}, null, (response) => {
          const forms = this.state.forms;
          if (response.HAS_FORM_DATA !== null) {
            for (let i = 0; i < forms[sectionPathStr].length; i++) {
              if (response.HAS_FORM_DATA.includes(forms[sectionPathStr][i].id)) forms[sectionPathStr][i].hasData = true;
              else forms[sectionPathStr][i].hasData = false;           
            }
          }
          this.setState({forms: forms}, () => {
            if (forms[sectionPathStr][index].hasData === true) {
              this.dialogRef.current.showDialog({
                title: this.state.labels.get("error"),
                text: this.state.labels.get("FORM_DATA_EXISTS")
              });
            }
            else {
              const history = this.state.history;
              history.push({
                forms: cloneDeep(this.state.forms),
                update: "remove-form"
              });
              const forms = this.state.forms;
              forms[sectionPathStr].splice(index, 1);
              this.setState({forms: forms});
              this.confirmatorRef.current.setMsg(this.state.labels.get("formRemoved"));
            }
          });
        });
      }
    });    
  }

  handleGetContentFiles(path, callback) {
    if (path in this.state.filesLoaded) {
      callback(this.state.filesLoaded[path]);
      return;
    }
    this.handleServerRequest({get: [
      {
        request: "CONTENT_FILES",
        path: path
      }
    ]}, null, (response) => {
      const filesLoaded = this.state.filesLoaded;
      filesLoaded[path] = response.CONTENT_FILES;
      this.setState({filesLoaded: {...filesLoaded}}, callback(response.CONTENT_FILES));
    });
  }

  handleGetThumbnail(id, area, size, callback) {
    this.handleServerRequest({get: [
      {
        request: "CONTENT_FILE_THUMBNAIL",
        id: id,
        area: area,
        size: size
      }
    ]}, null, (response) => {
      callback(response.CONTENT_FILE_THUMBNAIL);
    });
  };

  handleAddFile(path, fileCollection, caption, file, occurrence, callback) {
    path = path.join("/");
    const addFilesLoaded = (response) => {
      const filesLoaded = this.state.filesLoaded;
      if (!(path in filesLoaded) || filesLoaded[path] === null) filesLoaded[path] = {};
      if (!(fileCollection in filesLoaded[path]) || filesLoaded[path][fileCollection] === null) filesLoaded[path][fileCollection] = [];
      if (occurrence === "single") filesLoaded[path][fileCollection] = [response];
      else filesLoaded[path][fileCollection].push(response);
      this.setState({filesLoaded: {...filesLoaded}}, callback);
    };
    if (file === null) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("noFile")
      });
      return;
    }
    if (!(this.state.languages.defaultLanguage in caption) || caption[this.state.languages.defaultLanguage] === '') {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("emptyField")
      });
      return;
    }
    const set = {set: [
      {
        request: "CONTENT_FILE",
        path: path,
        fileCollection: fileCollection,
        caption: caption,
        page: this.state.page.getSection(),
        languages: this.state.languages
      }
    ]};
    this.handleServerRequest(set, [file], (response) => {
      if (response.CONTENT_FILE === false) {
        set.set[0].overwrite = true;
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("uploadConflicts"),
          text: this.state.labels.get("overwrite"),
          callback: () => {
            this.handleServerRequest(set, [file], (response) => {
              addFilesLoaded(response.CONTENT_FILE);
            });
            this.confirmatorRef.current.setMsg(this.state.labels.get("fileAdded"));
          }
        });
        return;
      }
      addFilesLoaded(response.CONTENT_FILE);
    });
  }

  handleRemoveContentFile(path, fileCollection, id, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("removeFile"),
      callback: () => {
        const history = this.state.history;
        history.push({
          filesLoaded: cloneDeep(this.state.filesLoaded),
          update: "remove-file"
        });
        const filesLoaded = this.state.filesLoaded;
        const files = filesLoaded[path][fileCollection];
        filesLoaded[path][fileCollection] = files.filter((fileDesc) => fileDesc.id !== id);
        this.setState({filesLoaded: {...filesLoaded}, history: history}, callback());
        this.confirmatorRef.current.setMsg(this.state.labels.get("remove-file"));
      }
    });
    this.notify();
  }

  handleRepositionFile(path, fileCollection, dropped, current, callback) {
    const history = this.state.history;
    history.push({
      filesLoaded: cloneDeep(this.state.filesLoaded),
      update: "reposition-file"
    });
    const filesLoaded = this.state.filesLoaded;
    const files = filesLoaded[path][fileCollection];
    const droppedFile = files.splice(dropped, 1)[0];
    files.splice(current, 0, droppedFile);
    this.setState({filesLoaded: {...filesLoaded}, history: history}, callback());
    this.confirmatorRef.current.setMsg(this.state.labels.get("reposition-file"));
    this.notify();
  }

  handleEditFileCaptions(path, fileCollection, id, captions, callback) {
    const history = this.state.history;
    history.push({
      filesLoaded: cloneDeep(this.state.filesLoaded),
      update: "edit-caption"
    });
    const filesLoaded = this.state.filesLoaded;
    const files = filesLoaded[path][fileCollection];
    const file = files.filter((file) => file.id === id)[0];
    file.caption = captions;
    this.setState({filesLoaded: {...filesLoaded}, history: history}, callback());
    this.confirmatorRef.current.setMsg(this.state.labels.get("edit-caption"));
    this.notify();
  }

  handleLanguagesEdited(formData) {
    let languages = this.state.languages;
    if (languages === null) languages = {};
    else if ("defaultLanguage" in languages) {
      const index = formData.findIndex((language) => language.iso639 === languages.defaultLanguage);
      if (index < 0) {
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("error"),
          text: this.state.labels.get("obligatoryDefaultLang")
        });
        this.setState({
          languages: languages,
        });
        return;
      }
    }

    let valueArr = formData.map(function(language){ return language.iso639 });
    let isDuplicate = valueArr.some(function(item, idx){
      return valueArr.indexOf(item) !== idx
    });
    if (isDuplicate) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("duplicateLang")
      });
      this.setState({
        languages: languages,
      });
      return;
    }

    const history = this.state.history;
    history.push({languages: cloneDeep(languages), update: "edit-language"});
    languages.languages = formData;
    this.setState({
      history: history,
      languages: languages
    }, this.confirmatorRef.current.setMsg(this.state.labels.get("edit-language")));
    this.notify();
  }

  handleChooseDefaultLanguage(chosen) {
    const languages = this.state.languages;
    if (languages !== null && "languages" in languages) {
      const index = languages.languages.findIndex((language) => language.iso639 === chosen);
      if (index < 0) {
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("error"),
          text: this.state.labels.get("obligatoryDefaultLang")
        });
        this.setState({
          languages: languages,
        });
        return;
      }
    }
    const history = this.state.history;
    history.push({languages: cloneDeep(languages), update: "choose-defaultLanguage"});
    languages.defaultLanguage = chosen;
    this.setState({
      history: history,
      languages: languages
    }, () => this.confirmatorRef.current.setMsg(this.state.labels.get("choose-defaultLanguage")));
    this.notify();
  }

  handleContentChosen(path, name, custom, callback) {
    const pathStr = path.join("/");
    const contents = this.state.contents;
    const part = (custom === true ? "custom" : "predefined");
    if (!(pathStr in contents)) contents[pathStr] = {};
    if (!(part in contents[pathStr])) contents[pathStr][part] = {};
    if (!(name in contents[pathStr][part])) {
      this.handleServerRequest({
        get: [
          {request: "CONTENT", path: path, name: name, custom: custom}
        ]
      }, null, (response) => {
        contents[pathStr][part][name] = response.CONTENT;
        this.setState({contents: contents}, callback());
      });
    }
    else callback();
  }

  handleTemplateChosen(data, callback) {
    const template = this.state.templates.get(data);
    if (template === null) {
      this.handleServerRequest({
        get: [
          {request: "TEMPLATE", data: data}
        ],
      }, null, (response) => {
        if (response.template !== null) {
          const loadedTemplate = {...data, data: {...response.TEMPLATE}};
          this.setState({templates: this.state.templates.append(loadedTemplate)}, callback());
        }
        else callback();
      });
    }
    else callback();
  }

  handlePrivilegeSearch(privilege, callback) {
    this.handleServerRequest({
      get: [
        {
          request: "USERS_BY_PRIVILEGE",
          privilege: privilege
        }
      ]
    }, null, (response) => callback(response.USERS_BY_PRIVILEGE));
  }

  handleUserSearch(searchStr, full, callback) {
    this.handleServerRequest({
      get: [
        {
          request: "USERS_BY_NAME",
          searchStr: searchStr,
          full: full
        }
      ]
    }, null, (response) => callback(response.USERS_BY_NAME));
  }

  handleGetUser(id, callback) {
    this.handleServerRequest({
      get: [
        {
          request: "USER_BY_ID",
          id: id
        }
      ]
    }, null, (response) => {
      if (response.USER_BY_ID === null) {
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("error"),
          text: this.state.labels.get("noSuchUser")
        });
        return;
      }
      callback(response.USER_BY_ID);
    });
  }

  handleUseSectionTemplate(path, useTemplate) {
    const history = this.state.history;
    history.push({
      page: cloneDeep(this.state.page.getSection()),
      update: "edit-useSectionTemplate"
    });
    const section = this.state.page.getSection(path);
    section.useSectionTemplate = useTemplate;
    this.setState({page: this.state.page}, this.confirmatorRef.current.setMsg(this.state.labels.get("edit-useSectionTemplate")));
    this.notify();
  }

  handleTemplateSubmit(data, callback) {
    const history = this.state.history;
    history.push({templates: cloneDeep(this.state.templates), update: "edit-template"});
    this.setState({templates: this.state.templates.append(data)}, callback);
    this.confirmatorRef.current.setMsg(this.state.labels.get("edit-template"));
    this.notify();
  }

  handleAddSnippet(snippet, name) {
    if (snippet === "" || name === "") {
        this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("emptySnippet")
      });
      return;
    }
    const regex = /^[a-zA-Z0-9_]+$/g;
    if (!regex.test(name)) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("noSpecialChars")
      });
      return;
    }
    this.handleServerRequest({get: [
      {
        request: "SNIPPET_NAMES"
      }
    ]}, null, (response) => {
      if (response.SNIPPET_NAMES.includes(name)) {
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("error"),
          text: this.state.labels.get("nameExists")
        });
        return;
      }
      const history = this.state.history;
      history.push({snippets: cloneDeep(this.state.snippets), update: "add-snippet"});
      const snippets = this.state.snippets;
      snippets[name] = snippet;
      this.setState({snippets: snippets}, () => this.confirmatorRef.current.setMsg(this.state.labels.get("snippetAdded")));
    });
  }

  handleEditSnippet(snippet, name) {
    const history = this.state.history;
    history.push({snippets: cloneDeep(this.state.snippets), update: "edit-snippet"});
    const snippets = this.state.snippets;
    snippets[name] = snippet;
    this.setState({snippets: snippets}, () => this.confirmatorRef.current.setMsg(this.state.labels.get("snippetEdited")));
  }

  handleGetSnippetList(callback) {
    this.handleServerRequest({get: [
      {
        request: "SNIPPET_NAMES"
      }
    ]}, null, (response) => {
      const snippetNames = [];
      Object.keys(this.state.snippets).forEach((name) => {
        if (this.state.snippets[name] !== null) snippetNames.push(name);
      });
      response.SNIPPET_NAMES.forEach((snippetName) => {
        if (!snippetNames.includes(snippetName) && this.state.snippets[snippetName] !== null) snippetNames.push(snippetName);
      });
      snippetNames.sort();
      callback(snippetNames);
    });
  }

  handleGetSnippet(name, callback) {
    const snippetNames = Object.keys(this.state.snippets);
    if (snippetNames.includes(name) && this.state.snippets[name] !== null) callback(this.state.snippets[name]);
    else {
      this.handleServerRequest({get: [
        {
          request: "GET_SNIPPET",
          name: name
        }
      ]}, null, (response) => {
        callback(response.GET_SNIPPET);
      });
    }
  }

  handleRemoveSnippet(name, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("error"),
      text: this.state.labels.get("removeSnippet"),
      callback: () => {
        const history = this.state.history;
        history.push({snippets: cloneDeep(this.state.snippets), update: "remove-snippet"});
        const snippets = this.state.snippets;
        snippets[name] = null;
        this.setState({snippets: snippets}, () => callback());
        this.confirmatorRef.current.setMsg(this.state.labels.get("snippetRemoved"));
      }
    });
    this.notify();
  }

  handleSubmitText(type, name, text, callback) {
    if (type === null || name === "" || text === null || text[this.state.languages.defaultLanguage] === undefined || text[this.state.languages.defaultLanguage].length < 1) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("emptyField")
      });
      return;
    }
    if (this.state.textSnippets[name] !== undefined) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("nameExists")
      });
      return;
    }
    this.handleServerRequest({get: [
      {
        request: "TEXT_SNIPPET_NAMES"
      }
    ]}, null, (response) => {
      if (response.TEXT_SNIPPET_NAMES.includes(name)) {
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("error"),
          text: this.state.labels.get("nameExists")
        });
        return;
      }
      const history = this.state.history;
      history.push({textSnippets: cloneDeep(this.state.textSnippets), update: "add-text-snippet"});
      const textSnippets = this.state.textSnippets;
      textSnippets[name] = {type: type, text: text};
      this.setState({textSnippets: textSnippets}, callback());
      this.confirmatorRef.current.setMsg(this.state.labels.get("textSnippetAdded"));
    });
  }

  handleGetTextSnippets(callback) {
    this.handleServerRequest({get: [
      {
        request: "TEXT_SNIPPET_NAMES"
      }
    ]}, null, (response) => {
      const textSnippets = this.state.textSnippets;
      response.TEXT_SNIPPET_NAMES.forEach((snippetName) => {
        if (textSnippets[snippetName] === undefined) textSnippets[snippetName] = {}; 
      });
      this.setState({textSnippets: textSnippets}, callback(Object.keys(textSnippets)));
    });
  }

  handleEditTextSnippet(type, name, text, callback) {
    if (text === null || text[this.state.languages.defaultLanguage] === undefined || text[this.state.languages.defaultLanguage].length < 1) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("emptyField")
      });
      return;
    }
    const history = this.state.history;
    history.push({textSnippets: cloneDeep(this.state.textSnippets), update: "edit-text-snippet"});
    const textSnippets = this.state.textSnippets;
    textSnippets[name] = {type: type, text: text};
    this.confirmatorRef.current.setMsg(this.state.labels.get("textSnippetEdited"));
    this.notify();
  }

  handleRemoveTextSnippet(name) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("removeTextSnippet"),
      callback: () => {
        const history = this.state.history;
        history.push({textSnippets: cloneDeep(this.state.textSnippets), update: "remove-text-snippet"});
        const textSnippets = this.state.textSnippets;
        textSnippets[name] = {remove: true};
        this.setState({textSnippets: textSnippets});
        this.confirmatorRef.current.setMsg(this.state.labels.get("textSnippetRemoved"));
      }
    });
    this.notify();
  }

  handleGetTextSnippet(name, callback) {
    if (this.state.textSnippets[name].type === undefined) {
      this.handleServerRequest({get: [
        {
          request: "TEXT_SNIPPET",
          name: name
        }
      ]}, null, (response) => {
        const textSnippets = this.state.textSnippets;
        textSnippets[name] = response.TEXT_SNIPPET;
        this.setState({textSnippets: textSnippets}, callback());
      });
    }
    else callback();
  }

  handleHompageSelect(value) {
    const history = this.state.history;
    history.push({page: cloneDeep(this.state.page.getSection()), update: "edit-homepage"});
    const page = this.state.page;
    page.setProperty("homepage", value);
    this.setState({page: page}, () => this.confirmatorRef.current.setMsg(this.state.labels.get("homepageSelected")));
    this.notify();
  }

  handleGetPreview(ref, rel, data) {
    let section = null;
    if (data === null && ref === "path") {
      const path = rel.split("/");
      const curSection = this.state.page.getSection(path);
      if (!this.state.page.isPage(path) && curSection.useSectionTemplate !== true) {
        section = curSection;
        section.path = rel;
        ref = "type";
        rel = {
          name: curSection.type,
          custom: curSection.custom
        };
      }
    }
    const open = (verb, url, data, target) => {
      var form = document.createElement("form");
      form.action = url;
      form.method = verb;
      form.target = target || "_self";
      if (data) {
        for (var key in data) {
          var input = document.createElement("textarea");
          input.name = key;
          input.value = typeof data[key] === "object"
            ? JSON.stringify(data[key])
            : data[key];
          form.appendChild(input);
        }
      }
      form.style.display = 'none';
      document.body.appendChild(form);
      form.submit();
      document.body.removeChild(form);
    }
    open('POST', previewPath, { 
      request: {
        ref: ref,
        rel: rel,
        section: section,
        page: this.state.page.getSection(),
        contents: this.state.contents,
        template: (data === null ? this.state.templates.get({ref, rel}) : data),
        languages: this.state.languages,
        css: this.state.css,
        fonts: this.state.fonts,
        js: this.state.js,
        templateFiles: this.state.templateFiles,
        textSnippets: this.state.textSnippets,
        snippets: this.state.snippets
      }
    }, "preview");
  }

  handleAddUser(data, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("uploadNewUser"),
      callback: () => this.handleServerRequest({set: [
        {
          request: "ADD_USER",
          data: data
        }
      ]}, null, (response) => {
        callback(response.ADD_USER);
        this.confirmatorRef.current.setMsg(this.state.labels.get("userAdded"));
      })
    });
  }

  handleEditUser(data, id, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("uploadEditUser"),
      callback: () => this.handleServerRequest({set: [
        {
          request: "EDIT_USER",
          id: id,
          data: data
        }
      ]}, null, (response) => {
        callback(response.EDIT_USER);
        this.confirmatorRef.current.setMsg(this.state.labels.get("userEdited"));
      }
    )});
  }

  handleRemoveUser(id, name, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("removeUser", name),
      callback: () => this.handleServerRequest({set: [
        {
          request: "REMOVE_USER",
          id: id,
        }
      ]}, null, (response) => {
        callback(response.REMOVE_USER);
        this.confirmatorRef.current.setMsg(this.state.labels.get("userRemoved"));
      }
    )});
  }

  handlePrivilegeAssigned(path, id, callback) {
    this.handleServerRequest({set: [
      {
        request: "ASSIGN_PRIVILEGE",
        id: id,
        path: path
      }
    ]}, null, (response) => {
      callback(response.ASSIGN_PRIVILEGE);
      this.confirmatorRef.current.setMsg(this.state.labels.get("privilegeAssigned"));
    });
  }

  handlePrivilegeRemoved(privilege, id, name, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("removePrivilege", name),
      callback: () => this.handleServerRequest({set: [
        {
          request: "REMOVE_PRIVILEGE",
          id: id,
          path: privilege
        }
      ]}, null, (response) => {
        callback(response.REMOVE_PRIVILEGE);
        this.confirmatorRef.current.setMsg(this.state.labels.get("privilegeRemoved"));
      }
    )});
  }

  handleLoadDefault(data, callback) {
    if ("default" in this.state[data]) {
      callback(this.state[data].default);
      return;
    }
    this.handleServerRequest({get: [
      {
        request: "DEFAULT_DATA",
        data: data
      }
    ]}, null, (response) => {
      const curData = this.state[data];
      curData.default = response.DEFAULT_DATA;
      this.setState({[data]: curData}, () => callback(this.state[data].default));
    });
  }

  handleLoadImages(selection, callback) {
    this.handleServerRequest({get: [
      {
        request: "IMAGE_LIST",
        data: selection
      }
    ]}, null, (response) => callback(response.IMAGE_LIST));
  }

  handleLoadList(data, callback) {
    this.handleServerRequest({get: [
      {
        request: "DATA_LIST",
        data: data
      }
    ]}, null, (response) => {
      const curData = this.state[data];
      if ("list" in curData) {
        const difference = response.DATA_LIST.filter(x => curData.list.findIndex((item) => item.name === x.name) === -1);
        curData.list = curData.list.concat(difference);
      }
      else curData.list = response.DATA_LIST;
      this.setState({[data]: curData}, () => callback(this.state[data].list));
    });
  }

  handleLoadFile(data, name, callback) {
    if ("list" in this.state[data]) {
      const index = this.state[data].list.findIndex((item) => item.name === name);
      if (index !== -1 && "data" in this.state[data].list[index]) {
        callback(this.state[data].list[index].data);
        return;
      }
    }
    this.handleServerRequest({get: [
      {
        request: "DATA_FILE",
        data: data,
        name: name
      }
    ]}, null, (response) => {
      const curData = this.state[data], newData = response.DATA_FILE;
      if (!("list" in curData)) curData.list = [];
      const index = curData.list.findIndex((item) => item.name === name);
      if (index === -1) curData.list.push({name: name, data: newData});
      else curData.list[index].data = newData;
      this.setState({[data]: curData}, () => callback(newData));
    });
  }

  handleSubmitDefault(data, defaultData, callback) {
    const history = this.state.history;
    history.push({
      [data]: cloneDeep(this.state[data]),
      update: "edit-default-" + data
    });
    const curData = this.state[data];
    curData.default = defaultData;
    this.setState({[data]: curData, history: history}, () => callback());
    this.confirmatorRef.current.setMsg(this.state.labels.get("editDefaultFile"));
    this.notify();
  }

  handleSubmitDataFile(data, name, file, callback) {
    const history = this.state.history;
    history.push({
      [data]: cloneDeep(this.state[data]),
      update: "edit-data-file-" + data
    });
    let curData = this.state[data];
    const index = curData.list.findIndex((item) => item.name === name);
    if (index === -1) return;
    curData.list[index].data = file;
    this.setState({[data]: curData, history: history}, () => callback());
    this.confirmatorRef.current.setMsg(this.state.labels.get("editDataFile"));
    this.notify();
  }

  handleRemoveDataFile(data, propertyName, property, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("removeFile"),
      callback: () => {
        const history = this.state.history;
        history.push({
          [data]: cloneDeep(this.state[data]),
          update: "remove-data-file-" + data
        });
        const curData = this.state[data];
        const index = curData.list.findIndex((item) => item[propertyName] === property);
        if (index === -1) return;
        curData.list[index].remove = true;
        this.setState({[data]: curData, history: history}, () => callback());
        this.confirmatorRef.current.setMsg(this.state.labels.get("removeDataFile"));
      }
    });
    this.notify();
  }

  handleCreateEmptyFile(data, filename, callback) {
    const regex = /^[a-zA-Z0-9_]+$/g;
    if (filename === null || filename === "" || !regex.test(filename)) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("setFilename")
      });
      return;
    }
    this.handleServerRequest({set: [
      {
        request: "EMPTY_FILE",
        data: data,
        filename: filename
      }
    ]}, null, (response) => {
      callback(response.EMPTY_FILE);
      this.confirmatorRef.current.setMsg(this.state.labels.get("fileCreated"));
    });
  }

  handleUpload(data, file, callback) {
    if (file === null) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("chooseFile")
      });
      return;
    }
    this.handleServerRequest({set: [
      {
        request: "DATA_FILE",
        data: data
      }
    ]}, [file], (response) => {
      callback(response.DATA_FILE);
      this.confirmatorRef.current.setMsg(this.state.labels.get("fileUploaded"));
    });
  }

  checkRemove(data, name, callback) {
    const fileData = this.state[data];
    let removed = false;
    if (fileData.list !== undefined) {
      const filtered = fileData.list?.filter((listFile) => listFile.name === name);
      if (filtered.length > 0 && filtered[0].remove === true) {
        removed = true;
        delete filtered[0].remove;
        this.setState({[data]: fileData}, callback(removed));
        return;
      }
    }
    callback(removed);
  }

  handleUploadFile(data, file, callback) {
    if (file === null) {
      this.dialogRef.current.showDialog({
        title: this.state.labels.get("error"),
        text: this.state.labels.get("chooseFile")
      });
      return;
    }
    this.checkRemove(data, file.name, (removed) => {
      this.handleServerRequest({set: [
        {
          request: "UPLOAD_FILE",
          data: data,
          removed: removed
        }
      ]}, [file], (response) => {
        callback(response.UPLOAD_FILE);
        this.confirmatorRef.current.setMsg(this.state.labels.get("fileUploaded"));
      });
    });
  }

  handleReloadCustomSectionSchemas(callback) {
    this.handleServerRequest({get: [
      {
        request: "CUSTOM_SECTION_SCHEMAS"
      }
    ]}, null, (response) => {
      this.setState({customSectionSchemas: response.CUSTOM_SECTION_SCHEMAS}, callback(response.CUSTOM_SECTION_SCHEMAS));
    });
  }

  handleUploadSectionSchema(sectionSchema, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("uploadNewSchema"),
      callback: () => this.handleServerRequest({set: [
        {
          request: "SECTION_SCHEMA",
          data: sectionSchema
        }
      ]}, null, (response) => {
        this.handleReloadCustomSectionSchemas(() => {
          callback(response.SECTION_SCHEMA);
          this.confirmatorRef.current.setMsg(this.state.labels.get("sectionSchemaUploaded"));
        }
      )})
    });
  }

  handleRemoveSectionSchema(sectionSchema, overwrite, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("removeSchema"),
      callback: () => this.handleServerRequest({set: [
        {
          request: "REMOVE_SECTION_SCHEMA",
          data: sectionSchema,
          overwrite: overwrite
        }
      ]}, null, (response) => {
        if (response.REMOVE_SECTION_SCHEMA === false) {
          this.dialogRef.current.showDialog({
            title: this.state.labels.get("uploadConflicts"),
            text: this.state.labels.get("overwrite"),
            callback: () => {
              this.handleServerRequest({set: [
                {
                  request: "REMOVE_SECTION_SCHEMA",
                  data: sectionSchema,
                  overwrite: true
                }
              ]}, null, (response) => this.handleReloadCustomSectionSchemas(() => {
                callback(response.REMOVE_SECTION_SCHEMA);
                this.confirmatorRef.current.setMsg(this.state.labels.get("sectionSchemaRemoved"));
              }));
            }
          });
        }
        else this.handleReloadCustomSectionSchemas(() => {
          callback(response.REMOVE_SECTION_SCHEMA);
          this.confirmatorRef.current.setMsg(this.state.labels.get("sectionSchemaRemoved"));
        });
      })
    });
  }

  handleReloadCustomContentSchemas(callback) {
    this.handleServerRequest({get: [
      {
        request: "CUSTOM_CONTENT_SCHEMAS"
      }
    ]}, null, (response) => {
      this.setState({customContentSchemas: response.CUSTOM_CONTENT_SCHEMAS}, callback(response.CUSTOM_CONTENT_SCHEMAS));
    });
  }

  handleUploadContentSchema(contentSchema, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("uploadNewSchema"),
      callback: () => this.handleServerRequest({set: [
        {
          request: "CONTENT_SCHEMA",
          data: contentSchema
        }
      ]}, null, (response) => {
        this.handleReloadCustomContentSchemas(() => {
          callback(response.CONTENT_SCHEMA);
          this.confirmatorRef.current.setMsg(this.state.labels.get("contentSchemaUploaded"));
        });
      })
    });
  }

  handleRemoveContentSchema(contentSchema, overwrite, callback) {
    this.dialogRef.current.showDialog({
      title: this.state.labels.get("confirmation"),
      text: this.state.labels.get("removeSchema"),
      callback: () => this.handleServerRequest({set: [
        {
          request: "REMOVE_CONTENT_SCHEMA",
          data: contentSchema,
          overwrite: overwrite
        }
      ]}, null, (response) => {
        if (response.REMOVE_SECTION_SCHEMA === false) {
          this.dialogRef.current.showDialog({
            title: this.state.labels.get("uploadConflicts"),
            text: this.state.labels.get("overwrite"),
            callback: () => {
              this.handleServerRequest({set: [
                {
                  request: "REMOVE_CONTENT_SCHEMA",
                  data: contentSchema,
                  overwrite: overwrite
                }
              ]}, null, this.handleReloadCustomContentSchemas(() => {
                callback(response.REMOVE_CONTENT_SCHEMA);
                this.confirmatorRef.current.setMsg(this.state.labels.get("contentSchemaRemoved"));
              }));
            }
          });
        }
        else this.handleReloadCustomContentSchemas(() => {
          callback(response.REMOVE_CONTENT_SCHEMA);
          this.confirmatorRef.current.setMsg(this.state.labels.get("contentSchemaRemoved"));
        });
      })
    });
  }

  handleGetSchemaUpload(type, schema, callback) {
    this.handleServerRequest({get: [
      {
        request: "SCHEMA_UPLOAD",
        type: type,
        schema: schema
      }
    ]}, null, (response) => callback(response.SCHEMA_UPLOAD));
  }

  handleAddTranslations(translations, callback) {
    const set = {
      request: "TRANSLATIONS",
      data: translations,
      overwrite: false
    };
    this.handleServerRequest({set: [set]}, null, (response) => {
      if (response.TRANSLATIONS === false) {
        set.overwrite = true;
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("uploadConflicts"),
          text: this.state.labels.get("overwrite"),
          callback: () => {
            this.handleServerRequest({set: [set]}, null, (overwriteResponse) => {
              this.handleServerRequest({get: [
                {
                  request: "TRANSLATIONS"
                }
              ]}, null, (getResponse) => {
                this.setState({translations: getResponse.TRANSLATIONS}, () => {
                  callback(overwriteResponse.TRANSLATIONS);
                  this.confirmatorRef.current.setMsg(this.state.labels.get("translationEdited"));
                });
              });
            });
          }
        });
      }
      else {
        this.handleServerRequest({get: [
          {
            request: "TRANSLATIONS"
          }
        ]}, null, (getResponse) => {
          this.setState({translations: getResponse.TRANSLATIONS}, () => {
            callback(response.TRANSLATIONS);
            this.confirmatorRef.current.setMsg(this.state.labels.get("translationEdited"));
          });
        });
      }
    });
  }

  handleGetInformationTexts(callback) {
    this.handleServerRequest({
      get: [
        {
          request: "INFORMATION_TEXTS"
        }
      ]
    }, null, (response) => {
      this.setState({informationTexts: response.INFORMATION_TEXTS}, callback);
    });
  }

  handleSubmitInformationTexts(formData) {
    this.handleServerRequest({
      set: [
        {
          request: "INFORMATION_TEXTS",
          data: formData,
          overwrite: false
        }
      ]
    }, null, (response) => {
      if (response.INFORMATION_TEXTS === false) {
        this.dialogRef.current.showDialog({
          title: this.state.labels.get("uploadConflicts"),
          text: this.state.labels.get("overwrite"),
          callback: () => {
            this.handleServerRequest({
              set: [
                {
                  request: "INFORMATION_TEXTS",
                  data: formData,
                  overwrite: true
                }
              ]
            }, null, () => this.handleGetInformationTexts(() => {}));
          }
        });
      }
      else this.handleGetInformationTexts(() => {});
    });
  }

  loadManual(callback, reload = false) {
    if (this.state.manual === null || reload === true) {
      this.handleServerRequest({
        get: [
          {
            request: "MANUAL",
            lang: this.state.curEditorLang
          }
        ]
      }, null, (response) => {
        this.setState({manual: response.MANUAL});
        callback(this.state.manual);
      });
    }
    else callback(this.state.manual);
  }

  showManual() {
    this.setState({showManual: !this.state.showManual});
  }

  render() {
    if (this.state.loaded === false) return null;
    return (
      <Container className="pageManager">
        <Manual
          labels={this.state.labels}
          loadManual={this.loadManual}
          show={this.state.showManual}
          onClose={this.showManual}
        />
        <TotalFreeze
          frozen={this.state.frozen}
        />
        <Dialog
          ref={this.dialogRef}
        />
        <Dialog
          ref={this.registryDialogRef}
        />
        <Confirmator
          ref={this.confirmatorRef}
        />
        <Modal dialogClassName="modal-90w" show={this.state.uploadConflict !== false} onHide={() => this.setState({uploadConflict: false})}>
          <Modal.Header closeButton>
            <Modal.Title>{this.state.labels.get("uploadConflicts")}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.uploadConflict !== false && "conflicts" in this.state.uploadConflict ?
              <ConflictList
                uploadConflict={this.state.uploadConflict}
                page={this.state.page}
                labels={this.state.labels}
                onResolveConflict={(decisions) => this.handleResolveConflict(decisions)}
              />
              :
              null
            }
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => this.setState({uploadConflict: false})}>{this.state.labels.get("cancel")}</Button>
          </Modal.Footer>
        </Modal>
        <PageNav
          labels={this.state.labels}
          editorLanguages={this.state.editorLanguages}
          handleUndo={this.handleUndo}
          setCurrentEditor={this.setCurrentEditor}
          setEditorLanguage={this.setEditorLanguage}
          upload={this.upload}
          registry={this.state.registry}
          user={this.state.user}
          logout={this.logout}
          showManual={this.showManual}
        />
        <Container fluid className="py-3 px-0">
          {
            this.state.currentEditor === "editLangs" ?
            <LanguageEditor
              languages={this.state.languages}
              languageSchema={this.state.languageSchema}
              onLanguagesEdit={this.handleLanguagesEdited}
              labels={this.state.labels}
              registry={this.state.registry}
            />
            : null
          }
          {
            this.state.currentEditor === "setDefaultLang" ?
            <DefaultLanguageChooser
              languages={this.state.languages}
              onChooseDefaultLang={this.handleChooseDefaultLanguage}
              labels={this.state.labels}
            />
            : null
          }
          {
            this.state.currentEditor === "editPage" ?
            this.languagesExist() ?
            <PageEditor
              page={this.state.page}
              pageSchema={this.state.pageSchema}
              sectionSchemas={this.state.sectionSchemas}
              customSectionSchemas={this.state.customSectionSchemas}
              contentSchemas={this.state.contentSchemas}
              customContentSchemas={this.state.customContentSchemas}
              formSchema={this.state.formSchema}
              contents={this.state.contents}
              forms={this.state.forms}
              onSubmitForm={this.handleSubmitForm}
              onChangeActiveStatus={this.handleChangeActiveStatus}
              onGetForms={this.handleGetForms}
              onGetFormData={this.handleGetFormData}
              onClearFormData={this.handleClearFormData}
              onRemoveForm={this.handleRemoveForm}
              languages={this.state.languages}
              onGetPreview={this.handleGetPreview}
              onDataChange={this.handleDataChange}
              onContentChosen={this.handleContentChosen}
              onUserSearch={this.handleUserSearch}
              onPrivilegeSearch={this.handlePrivilegeSearch}
              onPrivilegeAssigned={this.handlePrivilegeAssigned}
              onPrivilegeRemoved={this.handlePrivilegeRemoved}
              filesLoaded={this.state.filesLoaded}
              onAddFile={this.handleAddFile}
              onGetContentFiles={this.handleGetContentFiles}
              onGetThumbnail={this.handleGetThumbnail}
              onRemoveFile={this.handleRemoveContentFile}
              onRepositionFile={this.handleRepositionFile}
              onEditFileCaptions={this.handleEditFileCaptions}
              labels={this.state.labels}
              onUseSectionTemplate={this.handleUseSectionTemplate}
              registry={this.state.registry}
            />
            :
            <Alert variant="warning">{this.state.labels.get("noLanguage")}</Alert>
            :
            null
          }
          {
            this.state.currentEditor === "addUser" ?
            <UserAdder
              userSchema={this.state.userSchema}
              onAddUser={this.handleAddUser}
              labels={this.state.labels}
              registry={this.state.registry}
            />
            :
            null
          }
          {
            this.state.currentEditor === "editUsers" ?
            <UserEditor
              page={this.state.page}
              languages={this.state.languages}
              onUserSearch={this.handleUserSearch}
              onGetUser={this.handleGetUser}
              onAddUser={this.handleAddUser}
              onEditUser={this.handleEditUser}
              onRemoveUser={this.handleRemoveUser}
              userSchema={this.state.userSchema}
              onPrivilegeAssigned={this.handlePrivilegeAssigned}
              onPrivilegeRemoved={this.handlePrivilegeRemoved}
              labels={this.state.labels}
              registry={this.state.registry}
            />
            :
            null
          }
          {
            this.state.currentEditor === "editPredefinedTemplates" ?
              <PredefinedTemplateChooser
                onGetContentFiles={this.handleGetContentFiles}
                onRemoveContentFile={this.handleRemoveContentFile}
                onRepositionFile={this.handleRepositionFile}
                onTemplateSubmit={this.handleTemplateSubmit}
                onTemplateChosen={this.handleTemplateChosen}
                onUseSectionTemplate={this.handleUseSectionTemplate}
                onGetPreview={this.handleGetPreview}
                onDataChange={this.handleDataChange}
                onContentChosen={this.handleContentChosen}
                onUserSearch={this.handleUserSearch}
                onPrivilegeSearch={this.handlePrivilegeSearch}
                onPrivilegeAssigned={this.handlePrivilegeAssigned}
                onPrivilegeRemoved={this.handlePrivilegeRemoved}
                onAddFile={this.handleAddFile}
                onEditFileCaptions={this.handleEditFileCaptions}
                onLoadFile={this.handleLoadFile}
                onLoadDefault={this.handleLoadDefault}
                onLoadList={this.handleLoadList}
                onLoadImages={this.handleLoadImages}
                onSubmitDefault={this.handleSubmitDefault}
                onUpload={this.handleUpload}
                onUploadFile={this.handleUploadFile}
                onSubmitDataFile={this.handleSubmitDataFile}
                onRemoveFile={this.handleRemoveDataFile}
                onCreateEmptyFile={this.handleCreateEmptyFile}
                onAddTranslations={this.handleAddTranslations}
                onSubmitForm={this.handleSubmitForm}
                onChangeActiveStatus={this.handleChangeActiveStatus}
                onGetForms={this.handleGetForms}
                onGetFormData={this.handleGetFormData}
                onClearFormData={this.handleClearFormData}
                onRemoveForm={this.handleRemoveForm}
                onAddSnippet={this.handleAddSnippet}
                onEditSnippet={this.handleEditSnippet}
                onGetSnippetList={this.handleGetSnippetList}
                onGetSnippet={this.handleGetSnippet}
                onRemoveSnippet={this.handleRemoveSnippet}
                cssData={this.state.css}
                jsData={this.state.js}
                fontData={this.state.fonts}
                onGetTextSnippets={this.handleGetTextSnippets}
                onSubmitText={this.handleSubmitText}
                onRemoveTextSnippet={this.handleRemoveTextSnippet}
                onGetTextSnippet={this.handleGetTextSnippet}
                onEditTextSnippet={this.handleEditTextSnippet}
                {...this.state}
              />
            :
            null
          }
          {
            this.state.currentEditor === "editSectionTemplates" ?
              this.languagesExist() &&
              this.state.page.getSection() !== null ?
              <SectionTemplateChooser
                onGetContentFiles={this.handleGetContentFiles}
                onRemoveContentFile={this.handleRemoveContentFile}
                onRepositionFile={this.handleRepositionFile}
                onTemplateSubmit={this.handleTemplateSubmit}
                onTemplateChosen={this.handleTemplateChosen}
                onUseSectionTemplate={this.handleUseSectionTemplate}
                onGetPreview={this.handleGetPreview}
                onDataChange={this.handleDataChange}
                onContentChosen={this.handleContentChosen}
                onUserSearch={this.handleUserSearch}
                onPrivilegeSearch={this.handlePrivilegeSearch}
                onPrivilegeAssigned={this.handlePrivilegeAssigned}
                onPrivilegeRemoved={this.handlePrivilegeRemoved}
                onAddFile={this.handleAddFile}
                onEditFileCaptions={this.handleEditFileCaptions}
                onLoadFile={this.handleLoadFile}
                onLoadDefault={this.handleLoadDefault}
                onLoadList={this.handleLoadList}
                onLoadImages={this.handleLoadImages}
                onSubmitDefault={this.handleSubmitDefault}
                onUpload={this.handleUpload}
                onUploadFile={this.handleUploadFile}
                onSubmitDataFile={this.handleSubmitDataFile}
                onRemoveFile={this.handleRemoveDataFile}
                onCreateEmptyFile={this.handleCreateEmptyFile}
                onAddTranslations={this.handleAddTranslations}
                onSubmitForm={this.handleSubmitForm}
                onChangeActiveStatus={this.handleChangeActiveStatus}
                onGetForms={this.handleGetForms}
                onGetFormData={this.handleGetFormData}
                onClearFormData={this.handleClearFormData}
                onRemoveForm={this.handleRemoveForm}
                onAddSnippet={this.handleAddSnippet}
                onEditSnippet={this.handleEditSnippet}
                onGetSnippetList={this.handleGetSnippetList}
                onGetSnippet={this.handleGetSnippet}
                onRemoveSnippet={this.handleRemoveSnippet}
                cssData={this.state.css}
                jsData={this.state.js}
                fontData={this.state.fonts}
                onGetTextSnippets={this.handleGetTextSnippets}
                onSubmitText={this.handleSubmitText}
                onRemoveTextSnippet={this.handleRemoveTextSnippet}
                onGetTextSnippet={this.handleGetTextSnippet}
                onEditTextSnippet={this.handleEditTextSnippet}
                {...this.state}
              />
              :
              <Alert variant="warning">{this.state.labels.get("noLanguagePage")}</Alert>
            :
            null
          }
          {
            this.state.currentEditor === "selectHomepage" ?
              this.languagesExist() &&
              this.state.page.getSection() !== null ?
              <HomepageSelector
                onHomepageSelect={this.handleHompageSelect}
                {...this.state}
              />
              :
              <Alert variant="warning">{this.state.labels.get("noLanguagePage")}</Alert>
            :
            null
          }
          {
            this.state.currentEditor === "templateSnippets" ?
            <TemplateSnippetHandler
              onAddSnippet={this.handleAddSnippet}
              onEditSnippet={this.handleEditSnippet}
              onGetSnippetList={this.handleGetSnippetList}
              onGetSnippet={this.handleGetSnippet}
              onRemoveSnippet={this.handleRemoveSnippet}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "textSnippets" ?
            <TextSnippetManager
              onSubmitText={this.handleSubmitText}
              onGetTextSnippets={this.handleGetTextSnippets}
              onRemoveTextSnippet={this.handleRemoveTextSnippet}
              onGetTextSnippet={this.handleGetTextSnippet}
              onEditTextSnippet={this.handleEditTextSnippet}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "templateFiles" ?
            <TemplateFileHandler
              onLoadList={this.handleLoadList}
              onRemoveFile={this.handleRemoveDataFile}
              onUploadFile={this.handleUploadFile}
              onGetThumbnail={this.handleGetThumbnail}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "informationTexts" ?
              this.languagesExist() ?
              <InformationHandler
                onSubmitInformationTexts={this.handleSubmitInformationTexts}
                onGetInformationTexts={this.handleGetInformationTexts}
                {...this.state}
              />
              :
              <Alert variant="warning">{this.state.labels.get("noLanguage")}</Alert>
            :
            null
          }
          {
            this.state.currentEditor === "editCSS" ?
            <FileDataManager
              onLoadFile={(name, callback) => this.handleLoadFile("css", name, callback)}
              onLoadDefault={(callback) => this.handleLoadDefault("css", callback)}
              onLoadList={(callback) => this.handleLoadList("css", callback)}
              onSubmitDefault={(defaultData, callback) => this.handleSubmitDefault("css", defaultData, callback)}
              onUpload={(file, callback) => this.handleUpload("css", file, callback)}
              onSubmitDataFile={(name, file, callback) => this.handleSubmitDataFile("css", name, file, callback)}
              onRemoveFile={(name, callback) => this.handleRemoveDataFile("css", "name", name, callback)}
              onCreateEmptyFile={(filename, callback) => this.handleCreateEmptyFile("css", filename, callback)}
              syntax="css"
              managerName="cssManager"
              title="cssManager"
              defaultName="defaultCSS"
              listName="cssList"
              uploadLabel="uploadCSSFile"
              accept=".css,text/css"
              data={this.state.css}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "editFonts" ?
            <FileManager
              onLoadFileList={(callback) => this.handleLoadList("fonts", callback)}
              onRemove={(id, callback) => this.handleRemoveDataFile("fonts", "id", id, callback)}
              onUpload={(file, name, callback) => this.handleUploadFile("fonts", file, name, callback)}
              managerName="cssManager"
              title="fonts"
              fileNameLabel="fontName"
              accept="font/collection,font/otf,font/sfnt,font/ttf,font/woff,font/woff2"
              data={this.state.fonts}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "editJS" ?
            <FileDataManager
              onLoadFile={(name, callback) => this.handleLoadFile("js", name, callback)}
              onLoadDefault={(callback) => this.handleLoadDefault("js", callback)}
              onLoadList={(callback) => this.handleLoadList("js", callback)}
              onSubmitDefault={(defaultData, callback) => this.handleSubmitDefault("js", defaultData, callback)}
              onUpload={(file, callback) => this.handleUpload("js", file, callback)}
              onSubmitDataFile={(name, file, callback) => this.handleSubmitDataFile("js", name, file, callback)}
              onRemoveFile={(name, callback) => this.handleRemoveDataFile("js", "name", name, callback)}
              onCreateEmptyFile={(filename, callback) => this.handleCreateEmptyFile("js", filename, callback)}
              syntax="javascript"
              managerName="jsManager"
              title="jsManager"
              defaultName="defaultJS"
              listName="jsList"
              uploadLabel="uploadJSFile"
              accept=".js,text/javascript"
              data={this.state.js}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "createSectionSchema" ?
            <SectionAdder
              onUploadSectionSchema={this.handleUploadSectionSchema}
              onSetList={() => {}}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "editSectionSchemas" ?
            <SchemaEditor
              schemaType="section"
              onGetSchemaUpload={this.handleGetSchemaUpload}
              schemas={this.state.customSectionSchemas}
              onRemoveSchema={this.handleRemoveSectionSchema}
              onUploadSectionSchema={this.handleUploadSectionSchema}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "createContentSchema" ?
            <ContentAdder
              onUploadContentSchema={this.handleUploadContentSchema}
              onSetList={() => {}}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "editContentSchemas" ?
            <SchemaEditor
              schemaType="content"
              onGetSchemaUpload={this.handleGetSchemaUpload}
              schemas={this.state.customContentSchemas}
              onRemoveSchema={this.handleRemoveContentSchema}
              onUploadContentSchema={this.handleUploadContentSchema}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "resetPassword" ?
            <PasswordResetter
              onSubmitPassword={this.resetPassword}
              {...this.state}
            />
            :
            null
          }
          {
            this.state.currentEditor === "settings" ?
            <SettingsManager
              {...this.state}
              onGetSettings={this.handleGetSettings}
              onUpdateSetting={this.handleUpdateSetting}
            />
            :
            null
          }
          {
            this.state.currentEditor === null ?
            <Row>
              <Col><Image src={"logo_main.png"} fluid className="rounded mx-auto d-block" /></Col>
              <Col><Image src={"logo_main_blue.png"} fluid className="rounded mx-auto d-block" /></Col>
            </Row>
            :
            null
          }
        </Container>
      </Container>
    );
  }
}
