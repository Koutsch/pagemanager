import React from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { useDrag, useDrop } from 'react-dnd';

const ItemTypes = {
  ELEMENT: 'element'
};

function Element({i, children}) {
  const [{isDragging}, drag] = useDrag(() => ({
    type: ItemTypes.ELEMENT,
    item: {i:i},
    collect: monitor => ({
      isDragging: !!monitor.isDragging(),
    }),
  }));

  return (
    <div
      ref={drag}
      style={{
        opacity: isDragging ? 0.5 : 1,
        cursor: 'move',
        marginRight: 10
      }}
    >
      {children}
    </div>
  )
}

function Square({ i, onReposition, children}) {
  const [{ isOver }, drop] = useDrop(() => ({
    accept: ItemTypes.ELEMENT,
    drop: (item, monitor) => onReposition(item.i, i),
    collect: monitor => ({
      isOver: !!monitor.isOver(),
    }),
  }), []);

  return (
    <div ref={drop}>
      <Element i={i}>
        {children}
      </Element>
    </div>
  );
}

export default function DragAndDrop(props) {

  function renderSquares() {
    return props.elements.map((element, i) => <Square key={i} i={i} onReposition={props.onReposition}>
      {element}
    </Square>);
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <div
        style={{
          width: '100%',
          height: '100%',
          display: 'flex',
          flexWrap: 'wrap'
        }}
      >
        {renderSquares()}
      </div>
    </DndProvider>
  );
}
