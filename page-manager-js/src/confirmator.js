import React from 'react';

export default class Confirmator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      msg: null
    }
  }

  setMsg(msg) {
    this.setState({msg: msg, show: true}, () => {
      setTimeout(() => {
        this.setState({show: false});
      }, 3000);
    });
  }

  render() {
    return (
      <div className={`confirmator ${this.state.show === true ? 'confirmator-shown' : 'confirmator-hidden'}`}>{this.state.msg}</div>
    );
  };
}
