function addLangInput(languages) {
  const langInput = {
    "type": "object",
    "required": [languages.defaultLanguage],
    "properties": {}
  }
  for (const language of languages.languages) {
    langInput.properties[language.iso639] = {type: "string", title: language.name};
  }
  return langInput;
}

function addTitles(schemaJson, labels) {
  let schemaStr = JSON.stringify(schemaJson);
  const regExp = new RegExp("{{{([a-zA-Z0-9]+)}}}",'g');
  const titles = schemaStr.match(regExp);
  if (titles === null) return JSON.parse(schemaStr);
  for (let bracketTitle of titles) {
    const title = bracketTitle.substr(3, bracketTitle.length - 6);
    schemaStr = schemaStr.replaceAll(bracketTitle, labels.get(title));
  }
  return JSON.parse(schemaStr);
}

function createBasicSectionSchema(title, dataSchema, languages, labels) {
  const schema = {
    "title": title,
    "type": "object",
    "required": ["title","name"],
    "properties": {
      "title": {"$ref": "#/$defs/langInput", "title": labels.get("title")},
      "name": {"type": "string", "pattern": "^[a-zA-Z0-9_]*$", "title": labels.get("internalName")}
    },
    "$defs": {}
  };
  if (dataSchema !== undefined && dataSchema !== null) {
    const titledDataSchema = addTitles(dataSchema, labels);
    schema.properties.data = titledDataSchema;
    schema.required.push("data");
  }
  schema.$defs.langInput = addLangInput(languages);
  return schema;
}

function _deriveProperty(name, property, defs, parentPath) {
  const pType = Object.prototype.toString.call(property);
  if (pType !== "[object Object]") return null;
  let path = [];
  if (name === null) path = parentPath;
  else {
    if (name.match(/\s/g) === null) path = parentPath + "." + name;
    else path = `attribute(${parentPath}, '${name}')`;
  }
  const pathDesc = {
    path: path,
    property: property
  };
  if ("$ref" in property) {
    const def = property["$ref"].split("/").slice(-1)[0];
    pathDesc.def = {"name": def, "schema": defs[def]};
    pathDesc.subnodes = _derivePathsFromSchema(defs[def], defs, path, true);
  }
  else if (name !== null && "type" in property) {
    pathDesc.subnodes = _derivePathsFromSchema(property, defs, path, true);
  }
  return pathDesc;
}

function _derivePathsFromSchema(schema, defs, parentPath, ignore) {
  let paths = [], hasProps = false;
  const type = Object.prototype.toString.call(schema);
  switch (type) {
    case "[object Object]":
      if ("type" in schema && schema.type === "array") {
        parentPath += "[*]";
        if ("items" in schema) {
          const pathDesc = _deriveProperty(null, schema.items, defs, parentPath);
          if (pathDesc === null) return;
          paths.push(pathDesc);
        }
        if ("prefixItems" in schema) {
          Object.keys(schema.prefixItems).forEach((key) => {
            const pathDesc = _deriveProperty(key, schema.prefixItems[key], defs, parentPath);
            if (pathDesc === null) return;
            paths.push(pathDesc);
          });
        };
      }
      else if ("properties" in schema) {
        hasProps = true;
        const psType = Object.prototype.toString.call(schema.properties);
        if (psType !== "[object Object]") break;
        Object.keys(schema.properties).forEach((key) => {
          const pathDesc = _deriveProperty(key, schema.properties[key], defs, parentPath);
          if (pathDesc === null) return;
          paths.push(pathDesc);
        });
      }
      else if (ignore !== true) {
        const pathDesc = _deriveProperty(null, schema, defs, parentPath);
        if (pathDesc === null) return;
        paths.push(pathDesc);
      }
      Object.keys(schema).forEach((property) => {
        if (property === "$defs" || (hasProps && property === "properties")) return;
        paths = paths.concat(_derivePathsFromSchema(schema[property], defs, parentPath, true));
      });
      break;
    case "[object Array]":
      schema.forEach((item) => {
        paths = paths.concat(_derivePathsFromSchema(item, defs, parentPath, true));
      });
      break;
    default:
      break;
  }
  return paths;
}
const derivePathsFromSchema = (schema, rootPath) => _derivePathsFromSchema(schema, schema["$defs"], rootPath);

export { addLangInput, createBasicSectionSchema, derivePathsFromSchema, addTitles };
