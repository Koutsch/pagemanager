<?php
	require_once __DIR__ . DIRECTORY_SEPARATOR . "basic.php";

	if (session_status() !== PHP_SESSION_ACTIVE) session_start();

	ob_start();
	header("Content-Transfer-Encoding: binary"); 
	header("Content-Type: audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3");
	header('Content-Disposition: attachment; filename="stream.mp3"');
	header('X-Pad: avoid browser bug');
	header('Cache-Control: no-cache');
	

	foreach (str_split($_SESSION['text_captcha']) as $char) {
		$handle = fopen(SOUNDS_PATH . $char . ".mp3", "r");
		while ($data = fread($handle, 500)) { 
			//Buffer size needs to be large enough to not break audio up while getting the next part
			echo $data;
			ob_flush();
			flush();
			set_time_limit(30); // Reset the script execution time to prevent timeouts since this page will probably never terminate. 
		}
	}
?>