<?php
	require_once __DIR__ . DIRECTORY_SEPARATOR . "basic.php";

	// Starten der Sitzung
	if (session_status() !== PHP_SESSION_ACTIVE) session_start();

	$basic_handler = new Basic_Handler();
	$width = $basic_handler->get_setting("CAPTCHA_WIDTH");
	$height = $basic_handler->get_setting("CAPTCHA_HEIGHT");

	// Neues Bild erstellen und Transparenz aktivieren
	$image = imagecreatetruecolor($width, $height);
	imagealphablending($image, true);
	imagesavealpha($image, true);

	// Hintergrundfarbe festlegen
	$bg_color = imagecolorallocatealpha($image, 128, 128, 128, 0);

	// Hintergrund mit der Hintergrundfarbe füllen
	imagefill($image, 0, 0, $bg_color);

	// Captcha-Text erstellen
	$chars = "abcdefghijklmnopqrstuvwxyz23456789";
	$text = "";

	for ($i = 0; $i < 5; $i++) {
		// Zufälligen Buchstaben auswählen
		$char = $chars[rand(0, strlen($chars) - 1)];
		$text .= $char;

		// Schriftgröße festlegen
		$size = 20;

		// Schriftfarbe festlegen
		$color = imagecolorallocate($image, 0, 0, 0);

		// Text auf das Bild schreiben
		imagettftext($image, $size, 0, 30 + $i * 30, 35, $color, CAPTCHA_FONT_PATH, $char);
	}

	for ($i = 0; $i < 10; $i++) {
		imageline($image, rand(0, $width), rand(0, $height), rand(0, $width), rand(0, $height), $color);
	}

	// Captcha-Text in der Sitzung speichern
	$_SESSION["text_captcha"] = $text;

	// Captcha-Bild an den Browser senden
	header("Content-Type: image/png");
	imagepng($image);

	// Ressourcen freigeben
	imagedestroy($image);
?>