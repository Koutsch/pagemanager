<?php
	require_once __DIR__ . DIRECTORY_SEPARATOR . "basic.php";

	class Cleanup_Handler extends Basic_Handler {

		private ?object $page;

		function __construct(){
			$this->page = $this->get_json_from_file(PAGE_FILE_PATH);
		}

		public function cleanup() {
			if ($this->page === null) {
				if (file_exists(CONTENTS_PATH)) $this->safe_remove_dir(CONTENTS_PATH);
				if (file_exists(TEMPLATES_PATH)) $this->safe_remove_dir(TEMPLATES_PATH);
				if (file_exists(FILES_DIR_PATH)) $this->safe_remove_dir(FILES_DIR_PATH);
				if (file_exists(FORMS_DIR_PATH)) $this->safe_remove_dir(FORMS_DIR_PATH);
				if (file_exists(EVENTS_PATH)) $this->safe_remove_dir(EVENTS_PATH);
				if (file_exists(FORMS_DIR_PATH)) $this->safe_remove_dir(FORMS_DIR_PATH);
			}
			else {
				$content_list = $this->get_json_from_file(CONTENTS_LIST_PATH);
				if ($content_list !== null) {
					foreach ($content_list as $path => $parts) {
						$section = $this->get_section($this->page, $path);
						if ($section === null) {
							foreach ($parts as $part => $contents) {
								foreach ($contents as $name => $filename) {
									$this->safe_remove(CONTENTS_PATH . $filename);
								}
							}
							unset($content_list->{$path});
						}
					}
					$this->safe_save(json_encode($content_list, JSON_PRETTY_PRINT), CONTENTS_LIST_PATH);
				}
				$event_list = $this->get_json_from_file(EVENT_LIST_PATH);
				if ($event_list !== null) {
					foreach ($event_list as $path => $files) {
						$removed = [];
						$section = $this->get_section($this->page, $path);
						if ($section === null && property_exists($event_list, $path)) {
							unset($event_list->{$path});
						}
						if ($files !== null){
							if (count($files) > 0 && $content_list === null) exit_error(500, INTERNAL_ERR);
							foreach ($files as $filename) {
								$exists = false;
								// loop through contents list
								foreach ($content_list as $path => $parts) {
									foreach ($parts as $part => $contents) {
										foreach ($contents as $content => $content_filename) {
											if ($content_filename === $filename) {
												$exists = true;
												break;
											}
										}
									}
								}
								if (!$exists) array_push($removed, $filename);
							}
						}
						// remove from array
						foreach ($removed as $filename) {
							if (property_exists($event_list, $path) && $event_list->{$path} !== null) {
								$index = array_search($filename, $event_list->{$path});
								array_splice($event_list->{$path}, $index, 1);
							}							
						}
					}
					$this->safe_save(json_encode($event_list, JSON_PRETTY_PRINT), EVENT_LIST_PATH);
				}
	
				$templates_list = $this->get_json_from_file(TEMPLATES_LIST_PATH);
				if ($templates_list !== null) {
					if (property_exists($templates_list, "path")) {
						foreach ($templates_list->path as $path => $template) {
							$section = $this->get_section($this->page, $path);
							if ($section === null) {
								foreach ($template as $type => $filename) {
									$this->safe_remove(TEMPLATES_PATH . $filename);
								}
								unset($templates_list->path->{$path});
							}
						}
					}
					if (property_exists($templates_list, "type") && property_exists($templates_list->type, "custom")) {
						$custom_section_schemas = $this->get_json_from_file(CUSTOM_SECTION_SCHEMAS_PATH);
						if ($custom_section_schemas !== null) {
							foreach ($templates_list->type->custom as $name => $template) {
								$schema_exists = (count(array_filter($custom_section_schemas, function($schema) use($name) {
									return $schema->name === $name;
								})) > 0);
								if (!$schema_exists) unset($templates_list->type->custom->{$name});
							}
						}
					}
					$this->safe_save(json_encode($templates_list, JSON_PRETTY_PRINT), TEMPLATES_LIST_PATH);
				}
	
				$files_list = $this->get_json_from_file(FILES_LIST_PATH);
				if ($files_list !== null) {
					foreach ($files_list as $path => $file_collections) {
						$section = $this->get_section($this->page, $path);
						if ($section === null) {
							foreach ($file_collections as $coll_name => $files) {
								foreach ($files as $file) {
									$this->safe_remove(FILES_DIR_PATH . $file->filename);
								}
							}
							unset($files_list->{$path});
						}
					}
					$this->safe_save(json_encode($files_list, JSON_PRETTY_PRINT), FILES_LIST_PATH);
				}

				$forms_list = $this->get_json_from_file(FORMS_PATH);
				if ($forms_list !== null) {
					foreach ($forms_list as $path => $forms) {
						$section = $this->get_section($this->page, $path);
						if ($section === null) {
							unset($forms_list->{$path});
						}
					}
					$this->safe_save(json_encode($forms_list, JSON_PRETTY_PRINT), FORMS_PATH);
				}

				$form_data = $this->get_json_from_file(FORM_DATA_PATH);
				if ($form_data !== null) {
					foreach ($form_data as $path => $data) {
						$section = $this->get_section($this->page, $path);
						if ($section === null) {
							unset($form_data->{$path});
						}
					}
					$this->safe_save(json_encode($form_data, JSON_PRETTY_PRINT), FORM_DATA_PATH);
				}
			}

			$users = $this->get_json_from_file(USERS_PATH);
			if ($users !== null) {
				foreach ($users as $id => $user) {
					if (property_exists($user, "privileges")) {
						for ($i = 0; $i < count($user->privileges); $i++) {
							if ($this->page) $section = $this->get_section($this->page, $user->privileges[$i]);
							if ($section === null) $user->privileges[$i] = null;
						}
						$user->privileges = array_filter($user->privileges, function($privilege) {
							return ($privilege !== null);
						});
					}
				}
				$this->safe_save(json_encode($users, JSON_PRETTY_PRINT), USERS_PATH);
			}

			$this->flush_files();
		}
	}
?>