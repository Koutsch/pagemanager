<?php
	require_once __DIR__ . DIRECTORY_SEPARATOR . 'basic.php';
	require_once __DIR__ . DIRECTORY_SEPARATOR . 'login.php';

  if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET["f"])) {
		$handler = new Basic_Handler();
		$file_downloads = $handler->get_json_from_file(FILE_DOWNLOAD_LIST_PATH, true);
		$id = intval($_GET["f"]);
		$existing = array_values(array_filter($file_downloads, function($file_download) use($id) {
			return $file_download["id"] === $id;
		}));
		if (count($existing) < 1) exit_error(500, INVALID_REQUEST_ERR);
		$file = $existing[0];
		// access check
		if ($file["checkAccess"] === true) {
			$login_handler = new LoginHandler();
    	$has_access = $login_handler->user_has_access($file["path"]);
			if (!$has_access)	exit_error(500, FORBIDDEN_ERR);
		}
		// get content file list
		$files = $handler->get_collection_files($file["path"], $file["collection"], $file["fileId"]);
		if ($files === null || count($files) < 1) exit_error(500, INVALID_REQUEST_ERR);
		// send to client
		header("Content-type: " . $files[0]->type);
		header("Content-length: " . $files[0]->size);
  	header("Content-disposition: attachment;filename=" . $files[0]->name);
		readfile(FILES_DIR_PATH . $files[0]->filename);
		exit();
	}
?>