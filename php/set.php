<?php
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'basic.php';
  require_once __DIR__ . '/vendor/autoload.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'conflicts.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'templateHandler.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'cleanup.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'login.php';

	use Swaggest\JsonSchema\Schema;
  use JsonPath\JsonObject;

  class Set_Handler extends Handler {
    private array $handled = [];
    private bool $do_cleanup = true;
    private $conflict_handler;
    private $login_handler;

    function __construct($requests) {
      parent::__construct($requests);
      $this->requests = $requests;
      $this->conflict_handler = new Conflict_Handler();
      $this->login_handler = new LoginHandler();
    }

    private function add_page_ids(object &$section, array &$given_ids): void {
      // add id if not exists
      if (!property_exists($section, "id")) {
        $id = $this->generate_rand_id($given_ids);
        $section->id = $id;
      }
      // add to given ids
      array_push($given_ids, $section->id);
      // recurse subsections if exist
      if (property_exists($section, "sections")) foreach ($section->sections as $subsection) {
        $this->add_page_ids($subsection, $given_ids);
      }
    }

    private function check_other_request(string $func_name, ?string $path=null) {
      $other_request = null;
      // check if handled and return result
      if (array_key_exists($func_name, $this->handled)) $other_request = $this->handled[$func_name];
      else {
        // check for function name in requests
        $requests = array_values(array_filter($this->requests, function($item) use ($func_name) {
          return $item->request === $func_name;
        }));
        if (count($requests) > 0) {
          $request = $requests[0];
          // process request
          $this->response[$func_name] = $this->{$func_name}($request);
          // return result if exists and path is given
          if ($path !== null && array_key_exists($func_name, $this->handled)) $other_request = $this->handled[$func_name];
        }
      }
      
      // return saved data if path is given
      if ($other_request === null && $path !== null) return $this->get_json_from_file($path);
      return $other_request;
    }
    
    private function create_sub_schemas(object &$full_schema, object $basic_page_schema, object $section_schema, bool $custom): void {
      $part = $this->get_part($custom);
      $sub_schema = json_decode(json_encode($basic_page_schema->basicSchema));
      $sub_schema->properties->type = (object) ['const' => $section_schema->name];
      $sub_schema->properties->custom = (object) ['const' => $custom];
      $sub_schema->properties->useSectionTemplate = (object) ['$type' => 'boolean'];
      array_push($sub_schema->required, "type");
      array_push($sub_schema->required, "custom");
      if (property_exists($section_schema, "schema")) $sub_schema->properties->data = $section_schema->schema;
      if (property_exists($section_schema, "content") && $section_schema->content === true) $sub_schema->properties->content = (object) ['$ref' => '#/$defs/content'];
      // add subsections
      if ($section_schema->subsections === true) $sub_schema->properties->sections = (object) ['$ref' => '#/$defs/sections'];
      else if ($section_schema->subsections !== false) {
        $sub_schema->properties->sections = json_decode(json_encode($basic_page_schema->sections));
        if (property_exists($section_schema->subsections, "predefined")) {
          foreach ($section_schema->subsections->predefined as $subsection) {
            array_push($sub_schema->properties->sections->items->anyOf, (object) ['$ref' => '#/$schemaDefspredefined/' . $subsection]);
          }
        }
        if (property_exists($section_schema->subsections, "custom")) {
          foreach ($section_schema->subsections->custom as $subsection) {
            array_push($sub_schema->properties->sections->items->anyOf, (object) ['$ref' => '#/$schemaDefscustom/' . $subsection]);
          }
        }
      }
      // add to full schema
      array_push($full_schema->{'$defs'}->sections->items->anyOf, (object) ['$ref' => '#/$schemaDefs' . $part . '/' . $section_schema->name]);
      if (!property_exists($full_schema, '$schemaDefs' . $part)) $full_schema->{'$schemaDefs' . $part} = (object) [];
      $full_schema->{'$schemaDefs' . $part}->{$section_schema->name} = $sub_schema;
    }
    
    private function create_page_schema(object $languages): object {
      // get data
      $basic_page_schema = $this->get_system_json(BASIC_PAGE_SCHEMA_PATH);
      $page_schema = $this->get_system_json(PAGE_SCHEMA_PATH);
      $section_schemas = $this->get_system_json(SECTION_SCHEMAS_PATH);
      $custom_section_schemas = $this->get_json_from_file(CUSTOM_SECTION_SCHEMAS_PATH);
      $content_schemas = $this->get_system_json(CONTENT_SCHEMAS_PATH);
      $custom_content_schemas = $this->get_json_from_file(CUSTOM_CONTENT_SCHEMAS_PATH);
      // prepare full schema
      $full_schema = json_decode(json_encode($basic_page_schema->basicSchema));
      $full_schema->{'$defs'} = json_decode(json_encode($basic_page_schema->defs));
      $full_schema->{'$defs'}->langInput->required = [$languages->defaultLanguage];
      foreach ($languages->languages as $language) $full_schema->{'$defs'}->langInput->properties->{$language->iso639} = (object) ["type" => "string"];
      $full_schema->properties->data = $page_schema->schema;
      $full_schema->properties->sections = (object) ['$ref' => '#/$defs/sections'];
      // add subschemas
      foreach ($section_schemas as $section_schema) {
        $this->create_sub_schemas($full_schema, $basic_page_schema, $section_schema, false);
      }
      if ($custom_section_schemas !== null) {
        foreach ($custom_section_schemas as $section_schema) {
          $this->create_sub_schemas($full_schema, $basic_page_schema, $section_schema, true);
        }
      }
      // add content types
      $full_schema->{'$defs'}->contentTypesPredefined = json_decode(json_encode($basic_page_schema->contentTypes));
      $full_schema->{'$defs'}->contentPatternPredefined = json_decode(json_encode($basic_page_schema->contentPattern));
      foreach ($full_schema->{'$defs'}->contentPatternPredefined->patternProperties as $key => $value) $full_schema->{'$defs'}->contentPatternPredefined->patternProperties = (object) [
        $key => (object) ['$ref' => '#/$defs/contentTypesPredefined']
      ];
      foreach ($content_schemas as $name => $content) {
        array_push($full_schema->{'$defs'}->contentTypesPredefined->properties->type->enum, $name);
      }
      if ($custom_content_schemas !== null && count(get_object_vars($custom_content_schemas)) > 0) {
        $full_schema->{'$defs'}->content->properties->custom = (object) ['$ref' => '#/$defs/contentPatternCustom'];
        $full_schema->{'$defs'}->contentTypesCustom = json_decode(json_encode($basic_page_schema->contentTypes));
        $full_schema->{'$defs'}->contentPatternCustom = json_decode(json_encode($basic_page_schema->contentPattern));
        foreach ($full_schema->{'$defs'}->contentPatternCustom->patternProperties as $key => $value) $full_schema->{'$defs'}->contentPatternCustom->patternProperties = (object) [
          $key => (object) ['$ref' => '#/$defs/contentTypesCustom']
        ];
        foreach ($custom_content_schemas as $name => $content) {
          array_push($full_schema->{'$defs'}->contentTypesCustom->properties->type->enum, $name);
        }
      }
      // add homepage
      $full_schema->properties->homepage = json_decode(json_encode($basic_page_schema->defs->homepage));
      return $full_schema;
    }
    
    private function save_template(object &$templates_list, object $template): void {
      // set path according to reference and relation
      switch ($template->ref) {
        case "path":
          $path = [$template->ref, $template->rel];
          break;
        case "type":
          $path = [$template->ref, $this->get_part($template->rel->custom), $template->rel->name];
          break;
        default:
          exit_error(400, INVALID_REQUEST_ERR);
      }
      // set path in list
      $new = false;
      $target = &$templates_list;
      foreach ($path as $item) {
        if (!property_exists($target, $item)) {
          $target->{$item} = (object) [];
          $new = true;
        }
        $target = &$target->{$item};
      }
      // add to template list and save template files
      foreach ($template->data as $template_part => $data) {
        if ($new || !property_exists($target, $template_part)) {
          $filename = $this->get_filename(TEMPLATES_PATH);
          $target->{$template_part} = $filename;
        }
        else $filename = $target->{$template_part};
        $this->safe_save($data, TEMPLATES_PATH . $filename);
      }
    }
    
    private function get_mime_main_type(string $mime_type): string {
      $mime_type = explode("/", $mime_type);
      if (count($mime_type) !== 2) exit_error(400, INVALID_REQUEST_ERR);
      return $mime_type[0];
    }
    
    private function check_upload_file(string $name, int $max_size, ?array $exts=null, ?array $mime_subtypes=null, ?array $mime_types=null, ?int $i=null): string {
      if ($i !== null) {
        if (!isset($_FILES[$name]['error'][$i])) exit_error(400, INVALID_FILE_DESC_ERR);
        $error = $_FILES[$name]['error'][$i];
        $size = $_FILES[$name]['size'][$i];
        $filename = $_FILES[$name]['name'][$i];
        $tmp_name = $_FILES[$name]['tmp_name'][$i];
      }
      else {
        if (!isset($_FILES[$name]['error']) || is_array($_FILES[$name]['error'])) exit_error(400, INVALID_FILE_DESC_ERR);
        $error = $_FILES[$name]['error'];
        $size = $_FILES[$name]['size'];
        $filename = $_FILES[$name]['name'];
        $tmp_name = $_FILES[$name]['tmp_name'];
      }
      $error_data = array("index" => $i, "filename" => $filename);
      // Check value
      switch ($error) {
        case UPLOAD_ERR_OK:
          break;
        case UPLOAD_ERR_NO_FILE:
          exit_error(409, NO_FILE_ERR, $error_data);
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
          exit_error(409, FILE_TOO_BIG_ERR, $error_data);
        default:
          exit_error(409, UNKOWN_FILE_ERR, $error_data);
      }
      // file size
      if ($size > $max_size) exit_error(409, FILE_TOO_BIG_ERR, $error_data);
      // extension, mime
      $ext = pathinfo($filename, PATHINFO_EXTENSION);
      if ($exts !== null && !in_array($ext, $exts)) exit_error(409, INVALID_FILE_TYPE_ERR, $error_data);
      // get file mime type
      $finfo = new finfo(FILEINFO_MIME_TYPE);
      $mime_type = $finfo->file($tmp_name);
      // check subtypes
      if ($mime_subtypes !== null && !in_array($mime_type, $mime_subtypes)) exit_error(409, INVALID_FILE_TYPE_ERR, $error_data);
      // check main types
      if ($mime_types !== null) {
        $main_type = $this->get_mime_main_type($mime_type);
        if (!in_array($main_type, $mime_types)) exit_error(409, INVALID_FILE_TYPE_ERR, $error_data);
        // check if image
        if ($main_type === 'image') {
          try {
          	$image_valid = getimagesize($tmp_name);
          	if ($image_valid === false || $image_valid[0] === 0) exit_error(409, INVALID_FILE_TYPE_ERR, $error_data);
          }
          catch (\Throwable $e) {
            exit_error(409, INVALID_FILE_TYPE_ERR, $error_data);
          }
        }
      }
      return $mime_type;
    }
    
    private function save_data_file(object $data, string $dir_path, string $list_path, ?string $default_path=null): void {
      // save default data if exists and set parameter
      if ($default_path !== null) {
        if (property_exists($data, "default")) $this->safe_save($data->default, $default_path);
        $parameter = "name";
      }
      else $parameter = "id";
      // save from list if exists
      if (property_exists($data, "list")) {
        $list = $this->get_json_from_file($list_path);
        if ($list === null) $list = [];
        foreach ($data->list as $new_data) {
          // check if exists
          $files = array_filter($list, function($file) use($new_data, $parameter) {
            return $new_data->{$parameter} === $file->{$parameter};
          });
          if (count($files) > 0) {
            foreach ($files as $index => $file_data) {
              if (property_exists($new_data, "remove") && $new_data->remove === true) {
                $this->safe_remove($dir_path . $file_data->filename);
                array_splice($list, $index, 1);
              }
              else if (property_exists($new_data, "data")) $this->safe_save($new_data->data, $dir_path . $file_data->filename);
            }
          }
        }
        $this->safe_save(json_encode($list, JSON_PRETTY_PRINT), $list_path);
      }
    }
    
    private function create_schema_upload($upload_schema): object {
      $schema_part = null;
      $ui_schema_part = null;
      switch ($upload_schema->type) {
        case "singleLine":
          $schema_part = (object) [
            '$ref' => '#/$defs/langInput'
          ];
          break;
        case "multiLine":
          $schema_part = (object) [
            '$ref' => '#/$defs/langInput'
          ];
          $ui_schema_part = (object) [
            "ui:field" => "quillTextarea"
          ];
          break;
        case "number":
          $schema_part = (object) [
            'type' => 'number'
          ];
          break;
        case "regex":
          $schema_part = (object) [
            'type' => 'string',
            'pattern' => $upload_schema->regex
          ];
          break;
        case "date":
        case "date-time":
        case "email":
        case "uri":
          $schema_part = (object) [
            'type' => 'string',
            'format' => $upload_schema->type
          ];
          break;
        case "selector":
          $schema_part = (object) ["enum" => []];
          if (property_exists($upload_schema, "options"))
          foreach ($upload_schema->options as $option) {
            array_push($schema_part->enum, $option->name);
          }
          else exit_error(400, "OPTIONS_MISSING_ERR");
          break;
        case "array":
          $schema_part = (object) [
            'type' => 'array'
          ];
          if (property_exists($upload_schema, "items")) {
            $items_part = $this->create_schema_upload($upload_schema->items);
            $schema_part->items = $items_part->schema;
            if ($items_part->ui_schema !== null) $ui_schema_part = (object) ["items" => $items_part->ui_schema];
          }
          break;
        case "object":
          $schema_part = (object) [
            'type' => 'object'
          ];
          if (property_exists($upload_schema, "properties")) {
            if (!(property_exists($schema_part, "properties"))) $schema_part->properties = (object) [];
            $required = [];
            foreach ($upload_schema->properties as $property) {
              $property_part = $this->create_schema_upload($property);
              $schema_part->properties->{$property->name} = $property_part->schema;
              if ($property_part->ui_schema !== null) {
                $ui_schema_part = (object) [];
                $ui_schema_part->{$property->name} = $property_part->ui_schema;
              }
              if (property_exists($property, "required") && $property->required === true) array_push($required, $property->name);
            }
            if (count($required) > 0) $schema_part->required = $required;
          }
          break;
        default:
          break;
      }
      $schema_part->title = $upload_schema->name;
      return (object) ["schema" => $schema_part, "ui_schema" => $ui_schema_part];
    }

    private function validate_json(object $schema, $data) {
      $validator = Schema::import($schema);
      try {
        $validator->in($data);
      } catch (\Throwable $e) {
        return $e->getMessage();
      }
      return true;
    }

    private function check_schema_validity($schema) {
      $content_schema_upload_schema = $this->get_system_json(CONTENT_SCHEMA_UPLOAD_PATH);
      return $this->validate_json($content_schema_upload_schema, $schema);
    }
    
    private function check_string(string $string, ?string $regex=null): bool {
      // if no regex given, check for ASCII alphanumeric and _
      if ($regex === null) $regex = "/^[a-zA-Z0-9_]+$/";
      if (!preg_match($regex, $string)) return false;
      return true;
    }
    
    private function filter_section_schemas(array $sections, array $section_schemas): ?array {
      $new_sections = [];
      foreach ($sections as $section) {
        $existing_schemas = array_filter($section_schemas, function($saved_section) use($section) {
          return $saved_section->name === $section;
        });
        if (count($existing_schemas) < 1) exit_error(400, NO_SUCH_SECTION_ERR);
        array_push($new_sections, $section);
      }
      if (count($new_sections) < 1) return null;
      return $new_sections;
    }
    
    private function filter_content_schemas(array $contents, object $content_schemas): object {
      $new_contents = (object) [];
      $counter = 0;
      foreach ($contents as $content) {
        if (!property_exists($content, "name") || !property_exists($content, "type")) exit_error(400, INVALID_REQUEST_ERR);
        if (!property_exists($content_schemas, $content->type)) exit_error(400, INVALID_REQUEST_ERR);
        if (property_exists($new_contents, $content->name)) exit_error(400, INVALID_REQUEST_ERR);
        $new_contents->{$content->name} = (object) ["type" => $content->type];
        $counter++;
      }
      if ($counter === 0) return null;
      return $new_contents;
    }

    private function get_schemas($schema_def, $func_name, $predefined_path, $custom_path) {
      if (!is_bool($schema_def)) {
        $schemas = (object) [];
        if (property_exists($schema_def, "predefined")) {
          $filtered_schemas = $this->{$func_name}($schema_def->predefined, $this->get_system_json($predefined_path));
          if ($filtered_schemas !== null) $schemas->predefined = $filtered_schemas;
        }
        if (property_exists($schema_def, "custom")) {
          $filtered_schemas = $this->{$func_name}($schema_def->custom, $this->get_system_json($custom_path));
          if ($filtered_schemas !== null) $schemas->custom = $filtered_schemas;
        }
        if (!property_exists($schemas, "predefined") && !property_exists($schemas, "custom")) $schemas = false;
        return $schemas;
      }
      return $schema_def;
    }

    private function get_content_desc(object $section_schema, object $section) {
      if (!property_exists($section_schema, "content")) exit_error(400, NO_SUCH_CONTENT_ERR);
      if ($section_schema->content === true) $content_desc = $section->content;
      else $content_desc = $section_schema->content;
      return $content_desc;
    }

    private function handle_file_data_request(object $request): bool {
      // abort if no files to handle
      if (count(get_object_vars($request->data)) === 0) {
        $this->handled[$request->request] = null;
        return false;
      }
      else {
        // check permission
        $privilege = $this->login_handler->get_admin_privilege();
        if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      }
      // check if already handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      if (!property_exists($request, "data")) exit_error(400, INVALID_REQUEST_ERR);
      $this->handled[$request->request] = $request->data;
      return true;
    }
    
    public function LANGUAGES(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // check if already handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      // check if empty
      if ($request->data === null) {
        $this->handled[$request->request] = null;
        return false;
      }
      // check if diff
      $cur_langs = $this->get_json_from_file(LANG_FILE_PATH);
      if ($cur_langs !== null && $this->get_diff_count($request->data, $cur_langs) === 0) {
        $this->handled[$request->request] = null;
        return false;
      }
      // validate request
      $lang_schema = $this->get_system_json(LANG_SCHEMA_PATH);
      $valid = $this->validate_json($lang_schema, $request->data->languages);
      if ($valid !== true) exit_error(400, LANGUAGES_INVALID_ERR, $valid);
  		// check if correct default language
  		if (!(property_exists($request->data, "defaultLanguage")) || !in_array($request->data->defaultLanguage, array_column((array) $request->data->languages, "iso639"))) {
  			exit_error(400, DEFAULT_LANGUAGE_INVALID_ERR);
  		}
      $this->handled[$request->request] = $request->data;
  		return true;
    }

    public function LOGOUT(object $request): bool {
      $this->login_handler->logout();
      return true;
    }
    
    public function PAGE(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // check if handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      // check if data
      if ($request->data === null) {
        $this->handled[$request->request] = null;
        return false;
      }
      // check if diff
      $cur_page = $this->get_json_from_file(PAGE_FILE_PATH);
      if ($cur_page !== null && $this->get_diff_count($request->data, $cur_page, false) === 0) return false;
      // check languages
      $languages = $this->check_other_request("LANGUAGES", LANG_FILE_PATH);
      if ($languages === null) exit_error(500, INTERNAL_ERR);
      // create page schema
      $page_schema = $this->create_page_schema($languages);
      // validate
      $valid = $this->validate_json($page_schema, $request->data);
      if ($valid !== true) exit_error(400, PAGE_INVALID_ERR, $valid);
      // check if homepage exists and not main page
      if (property_exists($request->data, "homepage"))  {
        $section = $this->get_section($request->data, $request->data->homepage);
        if ($section === null) exit_error(400, NO_SUCH_SECTION_ERR);
        if ($section->id === $request->data->id) exit_error(400, NO_SUCH_SECTION_ERR);
      }
      // add ids recursively
      $given_ids = [];
      $this->add_page_ids($request->data, $given_ids);
      $this->handled[$request->request] = $request->data;
  		return true;
    }

    public function CONTENTS($request) {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // check already handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      // check if empty
      if (count(array_keys(get_object_vars($request->data))) === 0) {
        $this->handled[$request->request] = null;
        return false;
      }
      // check language request
      $languages = $this->check_other_request("LANGUAGES", LANG_FILE_PATH);
      if ($languages === null) exit_error(500, INTERNAL_ERR);
      // check page request
      $page = $this->check_other_request("PAGE", PAGE_FILE_PATH);
      if ($page === null) exit_error(500, INTERNAL_ERR);
      // get content list
      $content_list = $this->get_json_from_file(CONTENTS_LIST_PATH);
      if ($content_list === null) $content_list = (object) [];
      // get event list
      $event_list = $this->get_json_from_file(EVENT_LIST_PATH);
      if ($event_list === null) $event_list = (object) [];
      // prepare data array
      $contents_data = [];
      // loop request content data
      foreach ($request->data as $path => $contents) {
        $section = $this->get_section($page, $path);
        if ($section === null) exit_error(400, NO_SUCH_SECTION_ERR);
        // get section schema
        $section_schema = $this->get_section_schema($section->type, $section->custom);
        // get content description
        $content_desc = $this->get_content_desc($section_schema, $section);
        // loop parts
        foreach ($contents as $part => $part_contents) {
          // loop contents
          foreach ($part_contents as $content => $data) {
            $filename = null;
            // if data is null, remove
            if ($data === null) {
              $filename = $this->content_search($content_list, $path, $part, $content);
              if ($filename !== null) {
                unset($content_list->{$path}->{$part}->{$content});                
              }
            }
            else {
              // check if in content description
              if (!(property_exists($content_desc->{$part}, $content))) exit_error(400, NO_SUCH_CONTENT_ERR);
              $content_type = $content_desc->{$part}->{$content}->type;
              // get content schema
              $content_schema = json_decode(json_encode($this->get_content_schema($part, $content_type)));
              $this->add_lang_input($content_schema, $languages, $languages->defaultLanguage);
              // validate
              $valid = $this->validate_json($content_schema, $data);
              if ($valid !== true) exit_error(400, CONTENT_INVALID_ERR, $valid);
              // get file name
              $filename = $this->content_search($content_list, $path, $part, $content);
              if ($filename === null) {
                $filename = $this->get_filename(CONTENTS_PATH);
                $this->content_create($content_list, $path, $part, $content, $filename);
              }
              // add to event list
              if ($content_type === "dateTimePlace") {
                if (!property_exists($event_list, $path)) $event_list->{$path} = [];
                if (!in_array($filename, $event_list->{$path})) array_push($event_list->{$path}, $filename);
              }
            }
            if ($filename !== null) array_push($contents_data, ["filename" => $filename, "data" => $data]);
          }
        }
      }
      $this->handled[$request->request] = (object) ["list" => $content_list, "contents" => $contents_data, "events" => $event_list];
      return true;
    }
    
    public function TEMPLATES($request) {
      // check if already handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      // check if empty
      if (count($request->data) === 0) {
        $this->handled[$request->request] = $request->data;
        return false;
      }
      else {
        // check permission
        $privilege = $this->login_handler->get_admin_privilege();
        if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      }
      // check templates json
      $template_schema = $this->get_system_json(TEMPLATE_SCHEMA_PATH);
      $valid = $this->validate_json($template_schema, $request->data);
      if ($valid !== true) exit_error(400, TEMPLATES_INVALID_ERR, $valid);
      // try to render to see if error
      $i = 0;
  		try {
        $loader = new \Twig\Loader\ArrayLoader(array());
        $twig = new \Twig\Environment($loader);
        foreach ($request->data as $template) {
          if (property_exists($template->data, "head")) $twig->tokenize(new \Twig\Source($template->data->head, "head"));
          if (property_exists($template->data, "body")) $twig->tokenize(new \Twig\Source($template->data->body, "body"));
          $i++;
        }
  		}
  		catch (\Throwable $e) {
        exit_error(400, TEMPLATES_INVALID_ERR, $request->data[$i]->ref . " " . json_encode($request->data[$i]->rel) . ": " . $e->getMessage());
      }
      $this->handled[$request->request] = $request->data;
      return true;
    }
    
    public function SNIPPETS(object $request): bool {
      // check permission
      if (count(get_object_vars($request->data)) > 0) {
        $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      }
      // check if already handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      // check template request
      $this->check_other_request("TEMPLATES");
      // get snippet list
      $snippet_list = $this->get_json_from_file(SNIPPETS_LIST_PATH);
      if ($snippet_list === null) $snippet_list = (object) [];
      // get template snippets for save
      $new_snippets = (object) [];
      $removed_snippets = [];
      // prepare changes bool
      $has_changes = false;
      foreach ($request->data as $name => $snippet) {
        // remove
        if ($snippet === null) {
          if (!property_exists($snippet_list, $name)) exit_error(400, INVALID_REQUEST_ERR);
          array_push($removed_snippets, $snippet_list->{$name});
          unset($snippet_list->{$name});
          $has_changes = true;
        }
        // add and edit
        else {
          if (!property_exists($snippet_list, $name)) {
            $filename = $this->get_filename(SNIPPETS_DIR_PATH);
            $snippet_list->{$name} = $filename;          
          }
          $new_snippets->{$name} = $snippet;
          $has_changes = true;
        }
      }
      // set handled
      $this->handled[$request->request] = (object) [
        "list" => $snippet_list,
        "new" => $new_snippets,
        "remove" => $removed_snippets
      ];
      return $has_changes;
    }

    public function TEXT_SNIPPETS(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // check if already handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      // abort if no files to handle
      if (count(get_object_vars($request->data)) === 0) {
        $this->handled[$request->request] = null;
        return false;
      }
      // check language request
      $languages = $this->check_other_request("LANGUAGES", LANG_FILE_PATH);
      if ($languages === null) exit_error(500, INTERNAL_ERR);
      // get saved data
      $text_snipppets = $this->get_json_from_file(TEXT_SNIPPETS_PATH);
      if ($text_snipppets === null) $text_snipppets = (object) [];
      // loop request data
      foreach ($request->data as $name => $snippet) {
        // check remove
        if (property_exists($text_snipppets, $name) && property_exists($snippet, "remove") && $snippet->remove === true) {
          unset($text_snipppets->{$name});
        }
        // check add/edit
        if (property_exists($snippet, "type") && property_exists($snippet, "text")) {
          if ($snippet->type !== "singleLine" && $snippet->type !== "multiLine") exit_error(400, INVALID_REQUEST_ERR);
          if (!property_exists($snippet->text, $languages->defaultLanguage) || $snippet->text->{$languages->defaultLanguage} === "") exit_error(400, MISSING_DATA_ERR);
          $text_snipppets->{$name} = $snippet;
        }
      }
      $this->handled[$request->request] = $text_snipppets;
      return true;
    }
    
    public function FILES(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // check if already handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      // abort if no files to handle
      if (count(get_object_vars($request->data)) === 0) {
        $this->handled[$request->request] = null;
        return false;
      }
      // check languages
      $languages = $this->check_other_request("LANGUAGES", LANG_FILE_PATH);
      if ($languages === null) exit_error(500, INTERNAL_ERR);
      // check page
      $page = $this->check_other_request("PAGE", PAGE_FILE_PATH);
      if ($page === null) exit_error(500, INTERNAL_ERR);
      // get file list
      $files_list = $this->get_json_from_file(FILES_LIST_PATH);
      if ($files_list === null) exit_error(400, INVALID_REQUEST_ERR);
      // prepare file remove list
      $removed_files = [];
      // loop data
      foreach ($request->data as $path => $file_collections) {
        // check if has file collections
        if ($file_collections !== null) {
          // loop file collections
          foreach ($file_collections as $file_collection => $file_descs) {
            // check if has files
            if ($file_descs !== null) {
              // check if file collection exists
              if (!property_exists($files_list, $path)) exit_error(400, INVALID_REQUEST_ERR);
              if (!property_exists($files_list->{$path}, $file_collection)) exit_error(400, INVALID_REQUEST_ERR);
              // get saved files
              $saved_files = $files_list->{$path}->{$file_collection};
              // get file ids from request
              $ids = array_column($file_descs, "id");
              // get files to remove
              foreach ($saved_files as $saved_file) {
                if (!in_array($saved_file->id, $ids)) array_push($removed_files, $saved_file->filename);
              }
              // get new files to save
              $new_files = [];
              foreach ($ids as $id) {
                $new_files = array_merge($new_files, array_filter($saved_files, function($saved_file) use($id) {
                  return $saved_file->id === $id;
                }));
              }
              // add captions
              $captions = array_column($file_descs, "caption");
              foreach ($new_files as $index => $new_file) {
                $new_file->caption = $captions[$index];
              }
              $files_list->{$path}->{$file_collection} = $new_files;
            }
          }
        }
      }
      $this->handled[$request->request] = (object) ["files_list" => $files_list, "removed_files" => $removed_files];
      return true;
    }

    public function FORMS(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // check if already handled
      if (array_key_exists($request->request, $this->handled)) return $this->response[$request->request];
      // abort if no files to handle
      if (count(get_object_vars($request->data)) === 0) {
        $this->handled[$request->request] = null;
        return false;
      }
      // check languages
      $languages = $this->check_other_request("LANGUAGES", LANG_FILE_PATH);
      if ($languages === null) exit_error(500, INTERNAL_ERR);
      // check page
      $page = $this->check_other_request("PAGE", PAGE_FILE_PATH);
      if ($page === null) exit_error(500, INTERNAL_ERR);
      // get forms schema
      $forms_schema = $this->get_system_json(FORM_SCHEMA_PATH);
      $this->add_lang_input($forms_schema, $languages, $languages->defaultLanguage);
      // get form data
      $form_data = $this->get_json_from_file(FORM_DATA_PATH);
      // get saved forms
      $saved_forms = $this->get_json_from_file(FORMS_PATH);
      // loop data
      foreach ($request->data as $section_path => $forms) {
        // check if section has forms
        $section = $this->get_section($page, $section_path);
        if ($section === null) exit_error(400, INVALID_REQUEST_ERR);
        $section_schema = $this->get_section_schema($section->type, $section->custom);
        if ($section_schema === null) exit_error(400, INVALID_REQUEST_ERR);
        if (!property_exists($section_schema, "forms") || $section_schema->forms === false) exit_error(400, INVALID_REQUEST_ERR);
        // get form ids
        $form_ids = [];
        foreach ($forms as $form) {
          if (property_exists($form, "id")) array_push($form_ids, $form->id);
        }
        // get form data ids
        $form_data_ids = null;
        if ($form_data !== null && property_exists($form_data, $section_path)) {
          $form_data_ids = array_keys( (array) $form_data->{$section_path});
        }
        // loop forms
        foreach ($forms as $form) {
          // remove has data
          if (property_exists($form, "hasData")) unset($form->hasData);
          // check if edited although form data exists
          if ($saved_forms !== null && $form_data_ids !== null && in_array($form->id, $form_data_ids)) {
            $saved_path_forms = $saved_forms->{$section_path};
            $old_form = array_values(array_filter($saved_path_forms, function($old_form) use($form) {
              return $form->id === $old_form->id;
            }));
            if (count($old_form) > 0) {
              $old_form = $old_form[0];
              $diffs = $this->get_diffs($old_form, $form);
              if (array_key_exists("removed", $diffs)) exit_error(400, FORM_DATA_EXISTS_ERR);
              if (array_key_exists("modified", $diffs)) {
                if (count($diffs["modified"]) > 1) exit_error(400, FORM_DATA_EXISTS_ERR);
                if ($diffs["modified"][0] !== "/active") exit_error(400, FORM_DATA_EXISTS_ERR);
              }
            }
          }
          // validate
          $valid = $this->validate_json($forms_schema, $form);
          if ($valid !== true) exit_error(400, INVALID_REQUEST_ERR, $valid);
          // add id
          if (!property_exists($form, "id")) {
            $id = $this->generate_rand_id($form_ids);
            $form->id = $id;
            array_push($form_ids, $id);
          }
        }
        // check if id is missing in form data
        if ($form_data_ids !== null) {
          $form_data_ids = array_keys( (array) $form_data->{$section_path});
          $diff = array_diff($form_data_ids, $form_ids);
          if (count($diff) > 0) exit_error(400, FORM_DATA_EXISTS_ERR);
        }
      }
      // create new forms if not saved
      if ($saved_forms === null) $saved_forms = (object) [];
      // add forms
      foreach ($request->data as $section_path => $new_forms) {
        $saved_forms->{$section_path} = $new_forms;
      }
      $this->handled[$request->request] = $saved_forms;
      return true;
    }

    public function CLEAR_FORM_DATA(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // check if conflict
      if ($this->conflict_handler->check_overwrite_conflict($request) === false) return false;
      // get form data
      $form_data = $this->get_json_from_file(FORM_DATA_PATH);
      if ($form_data === null) exit_error(400, INVALID_REQUEST_ERR);
      // check if exists
      if (!(property_exists($form_data, $request->sectionPath))) exit_error(400, INVALID_REQUEST_ERR);
      $form_data_content = $form_data->{$request->sectionPath};
      // check if exists
      if ($form_data_content === null) exit_error(400, INVALID_REQUEST_ERR);
      if (!property_exists($form_data_content, $request->id)) exit_error(400, INVALID_REQUEST_ERR);
      // remove data
      unset($form_data_content->{$request->id});
      $this->safe_save(json_encode($form_data, JSON_PRETTY_PRINT), FORM_DATA_PATH);
      return true;
    }
    
    public function CONTENT_FILE(object $request): object {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // check if conflict
      if ($this->conflict_handler->check_overwrite_conflict($request) === false) return false;
      // check if valid request
      if (
        !property_exists($request, "path") ||
        !property_exists($request, "fileCollection") ||
        !property_exists($request, "caption") ||
        !property_exists($request, "page") ||
        !property_exists($request, "languages")
      ) exit_error(400, INVALID_REQUEST_ERR);
      // check if default language caption given
      if ($request->caption->{$request->languages->defaultLanguage} === "") exit_error(400, NO_CAPTION_ERR);
      // get section
      $section = $this->get_section($request->page, $request->path);
      // get section schema
      $section_schema = $this->get_section_schema($section->type, $section->custom);
      // check if file definition exists
      if (!(property_exists($section_schema, "files"))) exit_error(400, INVALID_REQUEST_ERR);
      // check if file collection exists
      if (!(property_exists($section_schema->files, $request->fileCollection))) exit_error(400, INVALID_REQUEST_ERR);
      // check mime types definition
      if (property_exists($section_schema->files->{$request->fileCollection}, "mimeTypes")) $mime_types = $section_schema->files->{$request->fileCollection}->mimeTypes;
      else $mime_types = null;
      if (property_exists($section_schema->files->{$request->fileCollection}, "mimeSubtypes")) $mime_subtypes = $section_schema->files->{$request->fileCollection}->mimeSubtypes;
      else $mime_subtypes = null;
      // check uploaded file and retrieve actual mime type
      $mime_type = $this->check_upload_file("files", $this->get_setting("MAX_FILE_SIZE"), null, $mime_subtypes, $mime_types, 0);
      // get list
      $files_list = $this->get_json_from_file(FILES_LIST_PATH);
      // prepare files list
      if ($files_list === null) $files_list = (object) [];
      if (!(property_exists($files_list, $request->path))) $files_list->{$request->path} = (object) [];
      if (!(property_exists($files_list->{$request->path}, $request->fileCollection))) $files_list->{$request->path}->{$request->fileCollection} = [];
      // get file name
      $filename = $this->get_filename(FILES_DIR_PATH);
      // get all file ids
      $ids = [];
      foreach ($files_list as $path => $fileCollections) {
        foreach ($fileCollections as $fileCollection) {
          foreach ($fileCollection as $file_desc) {
            array_push($ids, $file_desc->id);
          }
        }
      }
      // create file info
      $file_info = (object) [
        "name" => $_FILES["files"]['name'][0],
        "type" => $mime_type,
        "size" => $_FILES["files"]['size'][0],
        "caption" => $request->caption,
        "id" => $this->generate_rand_id($ids)
      ];
      $new_file = $file_info;
      $new_file->filename = $filename;
      // save file and remove saved if single occurrence
      if ($section_schema->files->{$request->fileCollection}->occurrence === "single") {
        if (count($files_list->{$request->path}->{$request->fileCollection}) > 0) $this->safe_remove(FILES_DIR_PATH . $files_list->{$request->path}->{$request->fileCollection}[0]->filename);
        $files_list->{$request->path}->{$request->fileCollection} = [$new_file];
      }
      // or add file
      else if ($section_schema->files->{$request->fileCollection}->occurrence === "multiple") array_push($files_list->{$request->path}->{$request->fileCollection}, $new_file);
      $this->safe_move("files", FILES_DIR_PATH . $filename, 0, true);
      $this->safe_save(json_encode($files_list, JSON_PRETTY_PRINT), FILES_LIST_PATH);
      // no cleanup since content might not be saved yet
      $this->do_cleanup = false;
      return $file_info;
    }
    
    public function ADD_USER(object $request): string {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // validate user
      $valid = $this->validate_json($this->get_system_json(USER_SCHEMA_PATH), $request->data);
      if ($valid !== true) exit_error(400, INVALID_JSON_ERR, $valid);
      // prepare user list
      $users = $this->get_json_from_file(USERS_PATH, true); 
      if ($users === null) $users = [];
      // check if user name exists
      foreach ($users as $id => $user) {
				if ($user["username"] === $request->data->username) exit_error(400, USER_EXISTS_ERR);
			}
      // create pin for later password choice
      $pin = $this->get_pin();
      $request->data->pin = $pin;
      $user_id = null;
			do {
				$user_id = strval(rand());
			} while (in_array($user_id, array_keys($users)));
      $users[$user_id] = $request->data;
      // send pin via email
      $this->send_user_mail($request->data->email, $request->data->username, $request->data->pin);
      // save to file
      $this->safe_save(json_encode($users, JSON_PRETTY_PRINT), USERS_PATH);
      return $user_id;
    }
    
    public function EDIT_USER(object $request): string {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // validate user
      $valid = $this->validate_json($this->get_system_json(USER_SCHEMA_PATH), $request->data);
      if ($valid !== true) exit_error(400, INVALID_JSON_ERR, $valid);
      // check if user exists
      $users = $this->get_json_from_file(USERS_PATH, true);
      if ($users === null) exit_error(400, NO_SUCH_USER_ERR);
      if (!array_key_exists($request->id, $users)) exit_error(400, NO_SUCH_USER_ERR);
      // check if existing user name chosen
      foreach ($users as $id => $user) {
				if ($user["username"] === $request->data->username && $id != $request->id) exit_error(400, USER_EXISTS_ERR);
			}
      // save to file
      $users[$request->id] = $request->data;
      $this->safe_save(json_encode($users, JSON_PRETTY_PRINT), USERS_PATH);
      return $request->id;
    }
    
    public function REMOVE_USER(object $request): string {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // remove user
      $users = $this->get_json_from_file(USERS_PATH, true);
      if ($users === null) exit_error(400, NO_SUCH_USER_ERR);
      if (!array_key_exists($request->id, $users)) exit_error(400, NO_SUCH_USER_ERR);
			unset($users[$request->id]);
      $this->safe_save(json_encode($users, JSON_PRETTY_PRINT), USERS_PATH);
      return $request->id;
    }
    
    public function ASSIGN_PRIVILEGE(object $request): string {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // assgin prvilege to user
      $users = $this->get_json_from_file(USERS_PATH);
      if ($users === null) exit_error(400, NO_SUCH_USER_ERR);
      if (!property_exists($users, $request->id)) exit_error(400, NO_SUCH_USER_ERR);
      $user = &$users->{(string) $request->id};
      if (!property_exists($user, "privileges")) $user->privileges = array($request->path);
      else array_push($user->privileges, $request->path);
      $this->safe_save(json_encode($users, JSON_PRETTY_PRINT), USERS_PATH);
      return $request->id;
    }
    
    public function REMOVE_PRIVILEGE(object $request): string {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // remove privilege from user
      $users = $this->get_json_from_file(USERS_PATH);
      if ($users === null) exit_error(400, NO_SUCH_USER_ERR);
      if (!property_exists($users, $request->id)) exit_error(400, NO_SUCH_USER_ERR);
      $user = &$users->{(string) $request->id};
      $user->privileges = array_values(array_diff($user->privileges, [$request->path]));
      $this->safe_save(json_encode($users, JSON_PRETTY_PRINT), USERS_PATH);
      return $request->id;
    }

    public function RESET_PASSWORD(object $request): string {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // get users
      $users = $this->get_json_from_file(USERS_PATH);
      if ($users === null) exit_error(400, NO_SUCH_USER_ERR);
      // get user
      $user = null;
      foreach ($users as $id => $user_data) {
        if ($user_data->username === $request->user && $user_data->password === md5($request->currentPassword)) {
          $user = &$user_data;
          break;
        }
      }
      if ($user === null) exit_error(400, WRONG_PASSWORD_ERR);
      // check new password
      if ($request->newPassword === null || strlen($request->newPassword) < 1) exit_error(400, MISSING_NEW_PASSWORD_ERR);
      // check password match
      if ($request->newPassword !== $request->repeatPassword) exit_error(400, PASSWORD_MISMATCH_ERR);
      // change password
      $user->password = md5($request->newPassword);
      // save users
      $this->safe_save(json_encode($users, JSON_PRETTY_PRINT), USERS_PATH);
      return true;
    }

    public function CSS(object $request): bool {
      return $this->handle_file_data_request($request);
    }

    public function FONTS(object $request): bool {
      return $this->handle_file_data_request($request);
    }

    public function JS(object $request): bool {
      return $this->handle_file_data_request($request);
    }

    public function TEMPLATE_FILES(object $request): bool {
      return $this->handle_file_data_request($request);
    }

    public function DATA_FILE(object $request): array {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // choose by file type
      if ($request->data === "css") {
        $dir_path = CSS_DIR_PATH;
        $list_path = CSS_LIST_PATH;
        $ext = ["css"];
        $mime_base = ["text"];
        $mime_full = null;
      }
      else if ($request->data === "js") {
        $dir_path = JS_DIR_PATH;
        $list_path = JS_LIST_PATH;
        $ext = ["js"];
        $mime_base = null;
        $mime_full = [
          "text/javascript",
          "text/ecmascript",
          "text/plain",
          "text/x-java",
          "application/ecmascript",
          "application/javascript",
          "application/x-javascript"
        ];
      }
      else exit_error(400, INVALID_REQUEST_ERR);
      $original_name = $_FILES["files"]['name'][0];
      // check if file name not too long
      if (strlen($original_name) > $this->get_setting("MAX_FILENAME_LEN")) exit_error(400, TOO_LONG_NAME_ERR);
      // check if upload valid
      $this->check_upload_file("files", $this->get_setting("MAX_FILE_SIZE"), $ext, $mime_full, $mime_base, 0);
      // check if file can be opened
      file_get_contents($_FILES['files']['tmp_name'][0]);
      // get file name
      $filename = $this->get_filename($dir_path);
      // save file
      $this->safe_move("files", $dir_path . $filename, 0, false);
      // add to list
      $list = $this->get_json_from_file($list_path);
      if ($list === null) $list = [];
      $files = array_values(array_filter($list, function($file) use($original_name) {
        return $original_name === $file->name;
      }));
      if (count($files) > 0) exit_error(400, FILE_EXISTS_ERR);
      array_push($list, array("name" => $original_name, "filename" => $filename));
      $this->safe_save(json_encode($list, JSON_PRETTY_PRINT), $list_path);
      return array("name" => $original_name);
    }

    public function EMPTY_FILE(object $request): array {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // choose by file type
      if ($request->data === "css") {
        $dir_path = CSS_DIR_PATH;
        $list_path = CSS_LIST_PATH;
        $ext = "css";
      }
      else if ($request->data === "js") {
        $dir_path = JS_DIR_PATH;
        $list_path = JS_LIST_PATH;
        $ext = "js";
      }
      else exit_error(400, INVALID_REQUEST_ERR);
      // check if file name given
      if ($request->filename === null || $request->filename === "") exit_error(400, INVALID_REQUEST_ERR);
      // check if file name not too long
      if (strlen($request->filename) > $this->get_setting("MAX_FILENAME_LEN")) exit_error(400, TOO_LONG_NAME_ERR);
      // check if valid file name
      if (!$this->check_string($request->filename)) exit_error(400, INVALID_NAME_ERR);
      $filename = $this->get_filename($dir_path);
      // assemble full file name
      $new_filename = $request->filename . "." . $ext;
      // add to list
      $list = $this->get_json_from_file($list_path);
      if ($list === null) $list = [];
      // check if file with same exists
      $files = array_values(array_filter($list, function($file) use($request) {
        return $request->filename === $file->name;
      }));
      if (count($files) > 0) exit_error(400, FILE_EXISTS_ERR);
      // save file
      array_push($list, array("name" => $new_filename, "filename" => $filename));
      $this->safe_save(json_encode($list, JSON_PRETTY_PRINT), $list_path);
      $this->safe_save("", $dir_path . $filename);
      return array("name" => $new_filename);
    }
    
    public function UPLOAD_FILE(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // choose by file type
      if ($request->data === "fonts") {
        $dir_path = FONT_DIR_PATH;
        $list_path = FONT_LIST_PATH;
        $mime_type = $this->check_upload_file("files", $this->get_setting("MAX_FILE_SIZE"), null, [
          "font/collection",
          "font/otf",
          "font/ttf",
          "font/woff",
          "font/woff2",
          "font/sfnt"
        ], null, 0);
      }
      else if ($request->data === "templateFiles") {
        $dir_path = TEMPLATE_FILES_DIR_PATH;
        $list_path = TEMPLATE_FILES_LIST_PATH;
        $mime_type = $this->check_upload_file("files", $this->get_setting("MAX_FILE_SIZE"), null, null, [
          "image",
          "audio",
          "video"
        ], 0);
      }
      else exit_error(400, INVALID_REQUEST_ERR);
      // check name validity
      $original_name = $_FILES["files"]['name'][0];
      if (strlen($original_name) > $this->get_setting("MAX_FILENAME_LEN")) exit_error(400, TOO_LONG_NAME_ERR);
      // get file list
      $list = $this->get_json_from_file($list_path);
      if ($list === null) $list = [];
      // check if file with this name exists
      $files = array_values(array_filter($list, function($file) use($original_name) {
        return $original_name === $file->name;
      }));
      // check if to replace
      if (count($files) > 0 && property_exists($request, "removed") && $request->removed === true) {
        // save file
        $this->safe_move("files", $dir_path . $files[0]->filename, 0, false);
        return true;
      }
      // abort if exists otherwise
      if (count($files) > 0) exit_error(400, FILE_EXISTS_ERR);
      // get file name
      $filename = $this->get_filename($dir_path);
      // save file
      $this->safe_move("files", $dir_path . $filename, 0, false);
      // get ids
      $ids = array_map(function($font) {
        return $font->id;
      }, $list);
      // add to list
      array_push($list, (object) [
        "name" => $original_name,
        "filename" => $filename,
        "type" => $mime_type,
        "id" => $this->generate_rand_id($ids)
      ]);
      $this->safe_save(json_encode($list, JSON_PRETTY_PRINT), $list_path);
      return true;
    }

    public function SECTION_SCHEMA(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // check if request valid
      if (!property_exists($request->data, "name") || !property_exists($request->data, "subsections") || !property_exists($request->data, "contents") || !property_exists($request->data, "fileCollections") || !property_exists($request->data, "forms") || !property_exists($request->data, "edit")) exit_error(400, INVALID_REQUEST_ERR);
      // check if schema name valid
      if (!$this->check_string($request->data->name, "/^[\\p{L}0-9_ ]+$/u")) exit_error(400, INVALID_NAME_ERR);
      // prepare data and list
      $section_schema = null;
      $subsections = false;
      $contents = false;
      $content_files = null;
      $custom_section_schemas = $this->get_json_from_file(CUSTOM_SECTION_SCHEMAS_PATH);
      if ($custom_section_schemas === null) $custom_section_schemas = [];
      // check if add
      if ($request->data->edit === false) {
        // check if name exists
        $existing_schemas = array_filter($custom_section_schemas, function($section_schema) use($request) {
          return $section_schema->name === $request->data->name;
        });
        if (count($existing_schemas) > 0) exit_error(400, SCHEMA_EXISTS_ERR);
      }
      // or edit
      else {
        $existing_schemas = array_filter($custom_section_schemas, function($section_schema) use($request) {
          return $section_schema->name === $request->data->edit;
        });
        if (count($existing_schemas) < 1) exit_error(400, INVALID_REQUEST_ERR);
      }
      // check file collections
      if (property_exists($request->data, "fileCollections") && count($request->data->fileCollections) > 0) {
        $content_files = (object) [];
        foreach ($request->data->fileCollections as $fileCollection) {
          if (property_exists($content_files, $fileCollection->name)) exit_error(400, INVALID_REQUEST_ERR);
          if (!$this->check_string($fileCollection->name, "/^[\\p{L}0-9_ ]+$/u")) exit_error(400, INVALID_NAME_ERR);
          if ($fileCollection->occurrence !== "single" && $fileCollection->occurrence !== "multiple") exit_error(400, INVALID_REQUEST_ERR);
          $content_files->{$fileCollection->name} = (object) [
            "occurrence" => $fileCollection->occurrence
          ];
          if ($fileCollection->fileType !== "") {
            if (!$this->check_string($fileCollection->fileType, "/^[a-z0-9\/\.\-]+$/")) exit_error(400, INVALID_MIME_ERR);
            $content_files->{$fileCollection->name}->mimeTypes = explode(",", $fileCollection->fileType);
          }          
        }
      }
      // check if schema exists and validate
      if (property_exists($request->data, "schema") && $request->data->schema !== null) {
        $valid = $this->check_schema_validity($request->data->schema);
        if ($valid !== true) exit_error(400, SECTION_INVALID_ERR, $valid);
        $section_schema = $this->create_schema_upload($request->data->schema);
      }
      // check sub section schemas
      $subsections = $this->get_schemas($request->data->subsections, "filter_section_schemas", SECTION_SCHEMAS_PATH, CUSTOM_SECTION_SCHEMAS_PATH);
      // check content schemas
      $contents = $this->get_schemas($request->data->contents, "filter_content_schemas", CONTENT_SCHEMAS_PATH, CUSTOM_CONTENT_SCHEMAS_PATH);
      // create schema
      $new_section_schema = (object) [
        "name" => $request->data->name,
        "subsections" => $subsections,
        "content" => $contents
      ];
      if ($content_files !== null) $new_section_schema->files = $content_files;
      // add forms boolean
      $new_section_schema->forms = $request->data->forms;
      // add data to schema
      if ($section_schema !== null) {
        $new_section_schema->schema = $section_schema->schema;
        $new_section_schema->schema->title = $request->data->name;
        if ($section_schema->ui_schema !== null) $new_section_schema->uiSchema = $section_schema->ui_schema;
      }
      // check if edit or add
      if ($request->data->edit !== false) {
        $index = 0;
        foreach ($custom_section_schemas as $schema) {
          if ($schema->name === $request->data->edit) break;
          $index++;
        }
        $new_section_schema->name = $request->data->edit;
        $custom_section_schemas[$index] = $new_section_schema;
      }
      else array_push($custom_section_schemas, $new_section_schema);
      // save schema
      $this->safe_save(json_encode($custom_section_schemas, JSON_PRETTY_PRINT), CUSTOM_SECTION_SCHEMAS_PATH);
      // save upload
      $section_schema_uploads = $this->get_json_from_file(CUSTOM_SECTION_SCHEMA_UPLOADS_PATH);
      if ($section_schema_uploads === null) $section_schema_uploads = (object) [];
      $section_schema_uploads->{$request->data->name} = (object) [
        "subsections" => $request->data->subsections,
        "content" => $request->data->contents,
        "fileCollections" => $request->data->fileCollections,
        "forms" => $request->data->forms,
        "schema" => $request->data->schema
      ];
      $this->safe_save(json_encode($section_schema_uploads, JSON_PRETTY_PRINT), CUSTOM_SECTION_SCHEMA_UPLOADS_PATH);
      return true;
    }
    
    public function REMOVE_SECTION_SCHEMA(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // check if conflict with other user
      $histories = $this->conflict_handler->check_conflict();
      if ($histories["conflict"] === true && $request->overwrite !== true) return false;
      // check if there are costum section schemas
      if (!file_exists(CUSTOM_SCHEMAS_PATH)) exit_error(400, INVALID_REQUEST_ERR);
      $custom_section_schemas = $this->get_json_from_file(CUSTOM_SECTION_SCHEMAS_PATH);
      if ($custom_section_schemas === null) exit_error(400, INVALID_REQUEST_ERR);
      // get schema index
      $index = false;
      foreach($custom_section_schemas as $i => $schema) {
        if ($schema->name === $request->data) {
          $index = $i;
          break;
        }
      }
      if ($index === false) exit_error(400, INVALID_REQUEST_ERR);
      // check if in use
      $page = $this->get_json_from_file(PAGE_FILE_PATH);
      $page_json_object = new JsonObject($page);
      $json_path = "$..sections[?(@.type=='" . $request->data . "')]";
      $sections_used = $page_json_object->get($json_path);
      if ($sections_used !== false && count($sections_used) > 0) exit_error(400, SCHEMA_IN_USE_ERR);
      // remove schema upload
      $schema_uploads = $this->get_json_from_file(CUSTOM_SECTION_SCHEMA_UPLOADS_PATH);
      if ($schema_uploads === null || !property_exists($schema_uploads, $request->data)) exit_error(400, INVALID_REQUEST_ERR);
      unset($schema_uploads->{$request->data});
      $this->safe_save(json_encode($schema_uploads, JSON_PRETTY_PRINT), CUSTOM_SECTION_SCHEMA_UPLOADS_PATH);
      // remove associated translations
      $saved_translations = $this->get_json_from_file(TRANSLATIONS_PATH);
      if ($saved_translations !== null) {
        foreach ($saved_translations as $key => &$translation) {
          if ($translation->type === "section" && $translation->custom === true && $translation->name === $custom_section_schemas[$index]->name) unset($saved_translations->{$key});
        }
        $this->safe_save(json_encode($saved_translations, JSON_PRETTY_PRINT), TRANSLATIONS_PATH);
      }
      array_splice($custom_section_schemas, $index, 1);
      // save section schemas
      $this->safe_save(json_encode($custom_section_schemas, JSON_PRETTY_PRINT), CUSTOM_SECTION_SCHEMAS_PATH);
      return true;
    }
    
    public function CONTENT_SCHEMA(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // prepare schema
      $content_schema = null;
      // check if valid request
      if (!property_exists($request->data, "name")) exit_error(400, INVALID_REQUEST_ERR);
      if (!$this->check_string($request->data->name, "/^[\\p{L}0-9_ ]+$/u")) exit_error(400, INVALID_NAME_ERR);
      // prepare custom content schemas
      $custom_content_schemas = $this->get_json_from_file(CUSTOM_CONTENT_SCHEMAS_PATH);
      if ($custom_content_schemas === null) $custom_content_schemas = (object) [];
      // check if add or edit
      if ($request->data->edit === false) {
        if (property_exists($custom_content_schemas, $request->data->name)) exit_error(400, SCHEMA_EXISTS_ERR);
        $name = $request->data->name;
      }
      else {
        if (!property_exists($custom_content_schemas, $request->data->edit)) exit_error(400, INVALID_REQUEST_ERR);
        $name = $request->data->edit;
      }
      // check schema validity if given
      if (property_exists($request->data, "schema") && $request->data->schema !== null) {
        $valid = $this->check_schema_validity($request->data->schema);
        if ($valid !== true) exit_error(400, CONTENT_INVALID_ERR, $valid);
        $content_schema = $this->create_schema_upload($request->data->schema);
      }
      
      $new_content_schema = (object) [];
      // add schema if given
      if ($content_schema === null || $content_schema->schema === null) $new_content_schema->schema = false;
      else {
        $new_content_schema->schema = $content_schema->schema;
        $new_content_schema->schema->title = $name;
        if ($content_schema->ui_schema !== null) $new_content_schema->uiSchema = $content_schema->ui_schema;
        $new_content_schema->schema->{'$defs'} = (object) [];
      }
      $custom_content_schemas->{$name} = $new_content_schema;
      $this->safe_save(json_encode($custom_content_schemas, JSON_PRETTY_PRINT), CUSTOM_CONTENT_SCHEMAS_PATH);
      // save upload
      $content_schema_uploads = $this->get_json_from_file(CUSTOM_CONTENT_SCHEMA_UPLOADS_PATH);
      if ($content_schema_uploads === null) $content_schema_uploads = (object) [];
      $content_schema_uploads->{$name} = (object) [
        "schema" => $request->data->schema
      ];
      $this->safe_save(json_encode($content_schema_uploads, JSON_PRETTY_PRINT), CUSTOM_CONTENT_SCHEMA_UPLOADS_PATH);
      return true;
    }
    
    public function REMOVE_CONTENT_SCHEMA(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // check if conflict with other user
      $histories = $this->conflict_handler->check_conflict();
      if ($histories["conflict"] === true && $request->overwrite !== true) return false;
      // prepare content schemas
      if (!file_exists(CUSTOM_SCHEMAS_PATH)) exit_error(400, INVALID_REQUEST_ERR);
      $custom_content_schemas = $this->get_json_from_file(CUSTOM_CONTENT_SCHEMAS_PATH);
      if ($custom_content_schemas === null) exit_error(400, INVALID_REQUEST_ERR);
      // check if schema exists
      if (!property_exists($custom_content_schemas, $request->data)) exit_error(400, INVALID_REQUEST_ERR);
      // check if in use in sections
      $page = $this->get_json_from_file(PAGE_FILE_PATH);
      if ($page !== null){
        $page_json_object = new JsonObject($page);
        $json_path = "$..sections[*].content.custom[?(@.type=='" . $request->data . "')]";
        $contents_used = $page_json_object->get($json_path);
        if ($contents_used !== false && count($contents_used) > 0) exit_error(400, CONTENT_IN_USE_ERR);
      }
      // check if in use in schemas
      $custom_section_schemas = $this->get_json_from_file(CUSTOM_SECTION_SCHEMAS_PATH);
      if ($custom_section_schemas !== null) {
        foreach ($custom_section_schemas as $schema) {
          if (property_exists($schema, "content") && property_exists($schema->content, "custom")) {
            foreach ($schema->content->custom as $content_name => $type) {
              if ($type->type === $request->data) exit_error(400, CONTENT_IN_USE_ERR);
            }
          }
        }
      }
      // remove schema upload
      $schema_uploads = $this->get_json_from_file(CUSTOM_CONTENT_SCHEMA_UPLOADS_PATH);
      if ($schema_uploads === null || !property_exists($schema_uploads, $request->data)) exit_error(400, INVALID_REQUEST_ERR);
      unset($schema_uploads->{$request->data});
      $this->safe_save(json_encode($schema_uploads, JSON_PRETTY_PRINT), CUSTOM_CONTENT_SCHEMA_UPLOADS_PATH);
      // remove associated translations
      $saved_translations = $this->get_json_from_file(TRANSLATIONS_PATH);
      if ($saved_translations !== null) {
        foreach ($saved_translations as $key => &$translation) {
          if ($translation->type === "content" && $translation->custom === true && $translation->name === $request->data) unset($saved_translations->{$key});
        }
        $this->safe_save(json_encode($saved_translations, JSON_PRETTY_PRINT), TRANSLATIONS_PATH);
      }
      unset($custom_content_schemas->{$request->data});
      $this->safe_save(json_encode($custom_content_schemas, JSON_PRETTY_PRINT), CUSTOM_CONTENT_SCHEMAS_PATH);
      return true;
    }
    
    public function TRANSLATIONS(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // check if conflict with other user
      $histories = $this->conflict_handler->check_conflict();
      if ($histories["conflict"] === true && $request->overwrite !== true) return false;
      // check vaild request
      if (!property_exists($request->data, "type") || !property_exists($request->data, "custom") || !property_exists($request->data, "name") || !property_exists($request->data, "path") || !property_exists($request->data, "translations")) exit_error(400, INVALID_REQUEST_ERR);
      // create folder if not exists
      if (!file_exists(TRANSLATIONS_DIR_PATH)) mkdir(TRANSLATIONS_DIR_PATH);
      // get request data
      $type = $request->data->type;
      $name = $request->data->name;
      $custom = $request->data->custom;
      $path = $request->data->path;
      $translations = $request->data->translations;
      // check if section exists
      if ($type === "section") {
        if ($custom === true) $schemas = $this->get_json_from_file(CUSTOM_SECTION_SCHEMAS_PATH);
        else if ($custom === false) $schemas = $this->get_system_json(SECTION_SCHEMAS_PATH);
        if (count(array_filter($schemas, function($schema) use($name) {
          return $schema->name === $name;
        })) < 1) exit_error(400, INVALID_REQUEST_ERR);
      }
      // check if content exists
      else if ($type === "content") {
        if ($custom === true) $schemas = $this->get_json_from_file(CUSTOM_CONTENT_SCHEMAS_PATH);
        else if ($custom === false) $schemas = $this->get_system_json(CONTENT_SCHEMAS_PATH);
        if (!property_exists($schemas, $name)) exit_error(400, INVALID_REQUEST_ERR);
      }
      // create new translation
      $saved_translations = $this->get_json_from_file(TRANSLATIONS_PATH);
      if ($saved_translations === null) $saved_translations = (object) [];
      $new_translation = (object) [
        "type" => $type,
        "name" => $name,
        "custom" => $custom,
        "path" => $path,
        "translations" => $translations
      ];
      $rand_key = null;
      // add translation
      foreach ($saved_translations as $key => &$translation) {
        if ($translation->type === $type && $translation->custom === $custom && $translation->name === $name && $translation->path === $path) {
          foreach ($translations as $name => $languages) {
            if (!property_exists($translation->translations, $name)) $translation->translations->{$name} = (object) [];
            foreach ($languages as $language => $value) {
              $translation->translations->{$name}->{$language} = $value;
            }
          }
          $rand_key = $key;
        }
        break;
      }
      // add new key
      if ($rand_key === null) {
        do {
          $rand_key = rand();
        } while (property_exists($saved_translations, $rand_key));
        $saved_translations->{$rand_key} = $new_translation;
      }
      $this->safe_save(json_encode($saved_translations, JSON_PRETTY_PRINT), TRANSLATIONS_PATH);
      return true;
    }

    public function INFORMATION_TEXTS(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "a") exit_error(403, FORBIDDEN_ERR);
      // check if conflict with other user
      $histories = $this->conflict_handler->check_conflict();
      if ($histories["conflict"] === true && $request->overwrite !== true) return false;
      // get schema and languages
      $information_text_schema = $this->get_system_json(INFORMATION_TEXT_SCHEMA_PATH);
      $languages = $this->get_json_from_file(LANG_FILE_PATH);
      if ($languages === null) exit_error(400, INVALID_REQUEST_ERR);
      // validate request data
      $this->add_lang_input($information_text_schema, $languages, $languages->defaultLanguage);
      $valid = $this->validate_json($information_text_schema, $request->data);
      if ($valid !== true) exit_error(400, INVALID_REQUEST_ERR, $valid);
      // save information texts
      $this->safe_save(json_encode($request->data, JSON_PRETTY_PRINT), INFORMATION_TEXT_PATH);
      return true;
    }

    public function SETTING(object $request): bool {
      // check permission
      $privilege = $this->login_handler->get_admin_privilege();
      if ($privilege === null || $privilege > "b") exit_error(403, FORBIDDEN_ERR);
      // get settings
      $settings = $this->get_system_json(SETTINGS_PATH);
      // get setting
      $setting = array_values(array_filter($settings, function($setting) use($request) {
        return ($setting->name === $request->name);
      }));
      if (count($setting) < 1) exit_error(400, INVALID_REQUEST_ERR);
      // check type
      if ($setting[0]->type === "int" && $request->value !== null) $new_value = intval($request->value);
      else $new_value = $request->value;
      $setting[0]->value = $new_value;
      $this->safe_save(json_encode($settings, JSON_PRETTY_PRINT), SETTINGS_PATH);
      return true;
    }   

    public function save_LANGUAGES(?object $data): void {
      if (!file_exists(LANG_PATH)) mkdir(LANG_PATH);
      if ($data === null) return;
      $this->safe_save(json_encode($data, JSON_PRETTY_PRINT), LANG_FILE_PATH);
    }

    public function save_PAGE(?object $data): void {
      if (!file_exists(PAGE_PATH)) mkdir(PAGE_PATH);
      if ($data === null) return;
      $this->safe_save(json_encode($data, JSON_PRETTY_PRINT), PAGE_FILE_PATH);
    }

    public function save_CONTENTS(?object $data): void {
      if (!file_exists(EVENTS_PATH)) mkdir(EVENTS_PATH);
      if (!file_exists(CONTENTS_PATH)) mkdir(CONTENTS_PATH);
      if ($data === null) return;
      foreach ($data->contents as $content) {
        if ($content["data"] === null) $this->safe_remove(CONTENTS_PATH . $content["filename"]);
        else $this->safe_save(json_encode($content["data"], JSON_PRETTY_PRINT), CONTENTS_PATH . $content["filename"]);
      }
      $this->safe_save(json_encode($data->list, JSON_PRETTY_PRINT), CONTENTS_LIST_PATH);
      $this->safe_save(json_encode($data->events, JSON_PRETTY_PRINT), EVENT_LIST_PATH);
    }

    public function save_FILES(?object $data): void {
      if (!file_exists(FILES_DIR_PATH)) mkdir(FILES_DIR_PATH);
      if ($data === null) return;
      $this->safe_save(json_encode($data->files_list, JSON_PRETTY_PRINT), FILES_LIST_PATH);
      // remove files
      foreach ($data->removed_files as $filename) {
        $this->safe_remove(FILES_DIR_PATH . $filename);
      }
    }

    public function save_FORMS(?object $data): void {
      if (!file_exists(FORMS_DIR_PATH)) mkdir(FORMS_DIR_PATH);
      if ($data === null) return;
      $this->safe_save(json_encode($data, JSON_PRETTY_PRINT), FORMS_PATH);
    }

    public function save_TEMPLATES(array $data): void {
      if (!file_exists(TEMPLATES_PATH)) mkdir(TEMPLATES_PATH);
      $templates_list = $this->get_json_from_file(TEMPLATES_LIST_PATH);
      if ($templates_list === null) $templates_list = (object) ["path" => (object) [], "type" => (object) []];
      foreach ($data as $template) {
        $this->save_template($templates_list, $template);
      }
      $this->safe_save(json_encode($templates_list, JSON_PRETTY_PRINT), TEMPLATES_LIST_PATH);
    }

    public function save_SNIPPETS(object $data): void {
      if (!file_exists(TEMPLATES_PATH)) mkdir(TEMPLATES_PATH);
      if (!file_exists(SNIPPETS_DIR_PATH)) mkdir(SNIPPETS_DIR_PATH);
      $this->safe_save(json_encode($data->list, JSON_PRETTY_PRINT), SNIPPETS_LIST_PATH);
      foreach ($data->new as $name => $snippet) {
        $this->safe_save($snippet, SNIPPETS_DIR_PATH . $data->list->{$name});
      }
      foreach ($data->remove as $filename) {
        $this->safe_remove(SNIPPETS_DIR_PATH . $filename);
      }
    }

    public function save_TEXT_SNIPPETS(?object $data): void {
      if (!file_exists(TEMPLATES_PATH)) mkdir(TEMPLATES_PATH);
      if (!file_exists(TEXT_SNIPPETS_DIR_PATH)) mkdir(TEXT_SNIPPETS_DIR_PATH);
      // check if empty
      if ($data === null) return;
      // save to file
      $this->safe_save(json_encode($data, JSON_PRETTY_PRINT), TEXT_SNIPPETS_PATH);
    }

    public function save_CSS(?object $data): void {
      if (!file_exists(CSS_DIR_PATH)) mkdir(CSS_DIR_PATH);
      if ($data === null) return;
      $this->save_data_file($data, CSS_DIR_PATH, CSS_LIST_PATH, CSS_PATH);
    }

    public function save_FONTS(?object $data): void {
      if (!file_exists(FONT_DIR_PATH)) mkdir(FONT_DIR_PATH);
      if ($data === null) return;
      $this->save_data_file($data, FONT_DIR_PATH, FONT_LIST_PATH);
    }

    public function save_JS(?object $data): void {
      if (!file_exists(JS_DIR_PATH)) mkdir(JS_DIR_PATH);
      if ($data === null) return;
      $this->save_data_file($data, JS_DIR_PATH, JS_LIST_PATH, DEFAULT_JS_PATH);
    }

    public function save_TEMPLATE_FILES(?object $data): void {
      if (!file_exists(TEMPLATES_PATH)) mkdir(TEMPLATES_PATH);
      if (!file_exists(TEMPLATE_FILES_DIR_PATH)) mkdir(TEMPLATE_FILES_DIR_PATH);
      if ($data === null) return;
      $this->save_data_file($data, TEMPLATE_FILES_DIR_PATH, TEMPLATE_FILES_LIST_PATH);
    }

    private function check_page_create(array $page_parts): bool {
      $create_page = false;
      foreach ($page_parts as $page_part) {
        if (array_key_exists($page_part, $this->response) && $this->response[$page_part] !== false) $create_page = true;
      }
      return $create_page;
    }

    public function response() {

      // $STDERR = fopen("php://stderr", "w");
  		// fwrite($STDERR, "\n".json_encode([$this], JSON_PRETTY_PRINT)."\n\n");
  		// fclose($STDERR);
  		
      // create data root path
      if (!file_exists(DATA_ROOT_PATH)) mkdir(DATA_ROOT_PATH);
      // check if there are conflicts with other user
      $conflict = $this->conflict_handler->check_history($this->requests);
      if ($conflict !== false) return ["HISTORY" => $conflict];
      // check if conflict has been resolved
      $resolve_conflict = $this->conflict_handler->check_resolve_conflict($this->requests);
      // process requests
      parent::response();
      // unset history to signal processing has finished
      $this->response["HISTORY"] = false;
      // check if to apply conflict resolution
      if ($resolve_conflict !== false) {
        $this->conflict_handler->apply_decisions($resolve_conflict, $this->handled);
        $this->response["RESOLVE_CONFLICT"] = true;
      }
      else $this->response["RESOLVE_CONFLICT"] = false;
      // save data
      foreach ($this->handled as $request => $data) {
        $this->{"save_" . $request}($data);
      }
      // save to file and remove file locks
      $this->flush_files();
      // check removed sections
      if ($this->do_cleanup === true) {
        $cleanup_handler = new Cleanup_Handler();
        $cleanup_handler->cleanup();
      }
      // set new session if resolved
      if ($this->response["RESOLVE_CONFLICT"] === true) $this->conflict_handler->get_current_history();
      // create the page
      $create_page = $this->check_page_create(["LANGUAGES", "PAGE", "CONTENTS", "TEMPLATES", "SNIPPETS", "FILES", "FORMS", "CSS", "FONTS", "JS", "TEMPLATE_FILES"]);
      if ($create_page) {
        try {
          $languages = $this->get_json_from_file(LANG_FILE_PATH);
          if ($languages === null) exit_error(400, MISSING_DATA_ERR);
          $template_handler = new Template_Handler($request, $languages);
          $template_handler->create_page();
          $this->response["CREATE_PAGE"] = true;
        } catch (\Throwable $e) {
          $this->response["CREATE_PAGE"] = $e->getMessage();
        }
      }
      // return result to client
      return $this->response;
    }
  }
?>
