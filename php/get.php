<?php
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'basic.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'twigExtensions.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'login.php';
  require_once __DIR__ . '/vendor/autoload.php';

  use JsonPath\JsonObject;

  class Get_Handler extends Handler {
    private function get_page_ids(object $page): array {
      $ids = [];
      array_push($ids, $page->id);
      if (property_exists($page, "sections")) foreach ($page->sections as $subsection) {
        $ids = array_merge($ids, $this->get_page_ids($subsection));
      }
      if (property_exists($page, "reservedIds")) $ids = array_merge($ids, $page->reservedIds);
      return $ids;
    }

    private function get_file_lists($file_collections) {
      $collection_list = (object) [];
      foreach ($file_collections as $file_collection => $data) {
        $collection_list->{$file_collection} = [];
        foreach ($data as $file_desc) {
          array_push($collection_list->{$file_collection}, (object) ["name" => $file_desc->name, "type" => $file_desc->type, "size" => $file_desc->size, "caption" => $file_desc->caption, "id" => $file_desc->id]);
        }
      }
      return $collection_list;
    }

    public function PAGE_SCHEMA($request) {
      return $this->get_system_json(PAGE_SCHEMA_PATH);
    }

    public function SECTION_SCHEMAS($request) {
      return $this->get_system_json(SECTION_SCHEMAS_PATH);
    }

    public function CUSTOM_SECTION_SCHEMAS($request) {
      $schemas = $this->get_json_from_file(CUSTOM_SECTION_SCHEMAS_PATH);
      if ($schemas === null) return [];
      return $schemas;
    }

    public function CONTENT_SCHEMAS($request) {
      return $this->get_system_json(CONTENT_SCHEMAS_PATH);
    }

    public function CUSTOM_CONTENT_SCHEMAS($request) {
      $schemas = $this->get_json_from_file(CUSTOM_CONTENT_SCHEMAS_PATH);
      if ($schemas === null) return (object) [];
      return $schemas;
    }

    public function FORM_SCHEMA($request) {
      return $this->get_system_json(FORM_SCHEMA_PATH);
    }

    public function INFORMATION_TEXT_SCHEMA($request) {
      return $this->get_system_json(INFORMATION_TEXT_SCHEMA_PATH);
    }

    public function INFORMATION_TEXTS($request) {
      return $this->get_json_from_file(INFORMATION_TEXT_PATH);
    }

    public function SCHEMA_UPLOAD($request) {
      if ($request->type === "section") $schema_uploads_path = CUSTOM_SECTION_SCHEMA_UPLOADS_PATH;
      if ($request->type === "content") $schema_uploads_path = CUSTOM_CONTENT_SCHEMA_UPLOADS_PATH;
      $schema_uploads = $this->get_json_from_file($schema_uploads_path);
      if ($schema_uploads === null) return null;
      if (!property_exists($schema_uploads, $request->schema)) return null;
      return $schema_uploads->{$request->schema};
    }

    public function LANGUAGE_SCHEMA($request) {
      return $this->get_system_json(LANG_SCHEMA_PATH);
    }

    public function USER_SCHEMA($request) {
      return $this->get_system_json(USER_SCHEMA_PATH);
    }

    public function LANGUAGES($request) {
      return $this->get_json_from_file(LANG_FILE_PATH);
    }

    public function EDITOR_LANGUAGES($request) {
      $editor_languages = $this->get_system_json(EDITOR_LANG_PATH);
      return $editor_languages;
    }

    public function PAGE($request) {
      $page = $this->get_json_from_file(PAGE_FILE_PATH);
      // remove reserved ids
      if ($page !== null && property_exists($page, "reservedIds")) unset($page->reserved_ids);
      return $page;
    }

    public function NEW_PAGE_ID($request) {
      if (property_exists($request, "page") && $request->page !== null && $request->page !== false) $page = $request->page;
      else $page = $this->get_json_from_file(PAGE_FILE_PATH);
      if ($page === null) return 0;
      $ids = $this->get_page_ids($page);
      $id = $this->generate_rand_id($ids);
      // save it to reserved
      if (!property_exists($page, "reservedIds") || $page->reservedIds === null) $page->reservedIds = [];
      array_push($page->reservedIds, $id);
      $this->safe_save(json_encode($page, JSON_PRETTY_PRINT), PAGE_FILE_PATH);
      $this->flush_files();
      return $id;
    }

    public function LABELS($request) {
      $editor_languages = $this->get_system_json(EDITOR_LANG_PATH);
			$label_lang = $request->language;
			if ($label_lang === null) $label_lang = $editor_languages->defaultLanguage;
			$labels = $this->get_system_json(LABELS_PATH, true);
			$lang_labels = array();
			foreach ($labels as $name => $texts) {
				if (!(array_key_exists($label_lang, $texts))) $lang_labels[$name] = $texts[$editor_languages->defaultLanguage];
				else $lang_labels[$name] = $texts[$label_lang];
			}
      return array("language" => $label_lang, "labels" => $lang_labels);
    }

    public function CONTENT($request) {
      $content_list = $this->get_json_from_file(CONTENTS_LIST_PATH);
      if ($content_list === null) return null;
      $path_str = implode("/", $request->path);
      if ($request->custom === true) $part = "custom";
      else $part = "predefined";
      if (!property_exists($content_list, $path_str)) return null;
      if (!property_exists($content_list->{$path_str}, $part)) return null;
      if (!property_exists($content_list->{$path_str}->{$part}, $request->name)) return null;
      return $this->get_json_from_file(CONTENTS_PATH . $content_list->{$path_str}->{$part}->{$request->name});
    }

    public function CONTENT_FILES($request) {
      $files_list = $this->get_json_from_file(FILES_LIST_PATH);
      if ($files_list === null) return null;
      if (!property_exists($files_list, $request->path)) return null;
      $response_file_list = $this->get_file_lists($files_list->{$request->path});
      if (count((array) $response_file_list) === 0) return null;
      return $response_file_list;
    }

    public function CONTENT_FILE_THUMBNAIL($request) {
      if ($request->area === "content") {
        $list_path = FILES_LIST_PATH;
        $dir_path = FILES_DIR_PATH;
      }
      else if ($request->area === "template") {
        $list_path = TEMPLATE_FILES_LIST_PATH;
        $dir_path = TEMPLATE_FILES_DIR_PATH;
      }
      else return null;
      $files_list = $this->get_json_from_file($list_path);
      if ($files_list === null) return null;
      $files_list_json_object = new JsonObject((object) ["list" => $files_list]);
      $json_path = "$..*[?(@.id==" . $request->id . ")]";
      $file = $files_list_json_object->get($json_path);

      if ($file !== false && count($file) > 0) $file = $file[0];
      else return null;
      $thumbnail = null;
      if (strpos($file['type'], 'image') === 0) $thumbnail = $this->get_resized_image($dir_path . $file['filename'], $request->size);
      return $thumbnail;
    }

    public function FORMS($request) {
      if (!property_exists($request, "sectionPath")) exit_error(400, INVALID_REQUEST_ERR);
      $forms = $this->get_json_from_file(FORMS_PATH);
      if ($forms === null) return [];
      if (!property_exists($forms, $request->sectionPath)) return [];
      $forms = &$forms->{$request->sectionPath};
      $form_data = $this->get_json_from_file(FORM_DATA_PATH);
      if ($form_data !== null && property_exists($form_data, $request->sectionPath)) {
        $form_data = $form_data->{$request->sectionPath};
        foreach ($forms as $form) {
          if (property_exists($form_data, $form->id)) $form->hasData = true;
          else $form->hasData = false;
        }
      }      
      return $forms;
    }

    public function FORM_DATA($request) {
      if (!property_exists($request, "sectionPath") || !property_exists($request, "id")) exit_error(400, INVALID_REQUEST_ERR);
      $form_data = $this->get_json_from_file(FORM_DATA_PATH);
      if ($form_data === null) return [];
      if (!property_exists($form_data, $request->sectionPath)) return [];
      if (!property_exists($form_data->{$request->sectionPath}, $request->id)) return [];
      return $form_data->{$request->sectionPath}->{$request->id};
    }

    public function HAS_FORM_DATA($request) {
      if (!property_exists($request, "sectionPath")) exit_error(400, INVALID_REQUEST_ERR);
      $form_data = $this->get_json_from_file(FORM_DATA_PATH);
      if ($form_data === null) return null;
      if (!property_exists($form_data, $request->sectionPath)) return null;
      return array_keys( (array) $form_data->{$request->sectionPath});
    }

    public function TEMPLATE($request) {
      return $this->get_template($request->data->ref, $request->data->rel);
    }

    public function SNIPPET_NAMES($request) {
      $snippet_list = $this->get_json_from_file(SNIPPETS_LIST_PATH);
      if ($snippet_list === null) return [];
      return array_keys( (array) $snippet_list);
    }

    public function GET_SNIPPET($request) {
      $snippet_list = $this->get_json_from_file(SNIPPETS_LIST_PATH);
      if ($snippet_list === null) return "";
      return file_get_contents(SNIPPETS_DIR_PATH . $snippet_list->{$request->name});
    }

    public function TEXT_SNIPPET_NAMES($request) {
      $snippet_list = $this->get_json_from_file(TEXT_SNIPPETS_PATH);
      if ($snippet_list === null) return [];
      return array_keys( (array) $snippet_list);
    }

    public function TEXT_SNIPPET($request) {
      $snippet_list = $this->get_json_from_file(TEXT_SNIPPETS_PATH);
      if ($snippet_list === null || !property_exists($snippet_list, $request->name)) return (object) [];
      return $snippet_list->{$request->name};
    }

    public function DEFAULT_DATA($request) {
      if ($request->data === "css") {
        $path = CSS_PATH;
      }
      else if ($request->data === "js") {
        $path = DEFAULT_JS_PATH;
      }
      else exit_error(400, INVALID_REQUEST_ERR);
      $default_data = "";
      if (file_exists($path)) $default_data = file_get_contents($path);
      return $default_data;
    }

    public function DATA_LIST($request) {
      if ($request->data === "css") {
        $path = CSS_LIST_PATH;
      }
      else if ($request->data === "js") {
        $path = JS_LIST_PATH;
      }
      else if ($request->data === "fonts") {
        $path = FONT_LIST_PATH;
      }
      else if ($request->data === "templateFiles") {
        $path = TEMPLATE_FILES_LIST_PATH;
      }
      else exit_error(400, INVALID_REQUEST_ERR);
      $list = $this->get_json_from_file($path);
      if ($list !== null) $list = array_map(function($item) {
        $data_list = [];
        foreach ($item as $key => $value) if ($key !== "filename") $data_list[$key] = $value;
        return $data_list;
      }, $list);
      else $list = [];
      return $list;
    }

    public function DATA_FILE($request) {
      if ($request->data === "css") {
        $list_path = CSS_LIST_PATH;
        $dir_path = CSS_DIR_PATH;
      }
      else if ($request->data === "js") {
        $list_path = JS_LIST_PATH;
        $dir_path = JS_DIR_PATH;
      }
      else exit_error(400, INVALID_REQUEST_ERR);
      $list = $this->get_json_from_file($list_path);
      $file = array_values(array_filter($list, function($file) use($request) {
        return $request->name === $file->name;
      }));
      if (count($file) === 0) exit_error(400, INVALID_REQUEST_ERR);
      $file = file_get_contents($dir_path . DIRECTORY_SEPARATOR . $file[0]->filename);
      if ($file === false) exit_error(500);
      return $file;
    }

    public function USERS_BY_PRIVILEGE($request) {
      $filtered = array();
      if (!file_exists(USERS_PATH)) return $filtered;
      $users = $this->get_json_from_file(USERS_PATH);
      if (property_exists($users, "root")) unset($users->root);
      foreach ($users as $id => $user) {
				if (property_exists($user, "privileges") && in_array($request->privilege, $user->privileges)) $filtered[$id] = $user;
			}
      return $filtered;
    }

    public function USERS_BY_NAME($request) {
      if (!file_exists(USERS_PATH)) return null;
      $users = $this->get_json_from_file(USERS_PATH);
      if (property_exists($users, "root")) unset($users->root);
      $filtered = array();
      foreach ($users as $id => $user) {
        if ($request->searchStr === null || (stripos($user->username, $request->searchStr) !== false || stripos($user->fullName, $request->searchStr) !== false)) $filtered[$id] = array("username" => $user->username, "fullName" => $user->fullName);
      }
      return $filtered;
    }

    public function USER_BY_ID($request) {
      if (!file_exists(USERS_PATH)) return null;
      $users = $this->get_json_from_file(USERS_PATH);
      if (property_exists($users, "root")) unset($users->root);
      // remove password
      $user = $users->{(string) $request->id};
      if (property_exists($user, "password")) unset($user->password);
      return $user;
    }

    public function TEMPLATE_ACTIONS($request) {
      global $twig_extensions;
      $actions = [];
      $toolbar = [];
      foreach ($twig_extensions as $name => $extension) {
        if (array_key_exists("toolbar", $extension)) {
          array_push($toolbar, ["template" => $extension["template"], "tool" => $extension["toolbar"]]);
        }
        else $actions[$name] = $extension["template"];
      }
      return ["actions" => $actions, "toolbar" => $toolbar];
    }

    public function IMAGE_LIST($request) {
      $template_file_list = $this->get_system_json(TEMPLATE_FILES_LIST_PATH);
      $template_files = [];
      $section_files = null;
      foreach ($template_file_list as $file) {
        if (substr($file->type, 0, 5) === "image") array_push($template_files, ["name" => $file->name, "id" => $file->id]);
      }
      if (property_exists($request, "data") && $request->data->ref === "path") {
        $section_files = [];
        $page = $this->get_json_from_file(PAGE_FILE_PATH);
        $section = $this->get_section($page, $request->data->rel);
        if (property_exists($section, "type")) {
          $section_schema = $this->get_section_schema($section->type, $section->custom);
          if (property_exists($section_schema, "files")) {
            foreach ($section_schema->files as $file_collection_name => $file_collection) {
              if ($file_collection->occurrence === "single" && count(array_filter($file_collection->mimeTypes, function($mime_type) {
                return (substr($mime_type, 0, 5) === "image");
              })) > 0) array_push($section_files, ["path" => $request->data->rel, "fileCollection" => $file_collection_name]);
            }
          }
        }
      }
      return ["template" => $template_files, "section" => $section_files];
    }

    public function TRANSLATIONS($request) {
      return $this->get_json_from_file(TRANSLATIONS_PATH);
    }

    public function HISTORY($request) {
      $_SESSION['time'] = time();
    }

    public function USER($request) {
      $login_handler = new LoginHandler();
      $user = $login_handler->is_admin();
      if ($user === false) exit_error(400, INVALID_REQUEST_ERR);
      else return ["username" => $user->username, "fullName" => $user->fullName, "privilege" => $user->privilege];
    }

    public function NOTIFY($request) {
      
    }

    public function MANUAL($request) {
      if (!property_exists($request, "lang")) exit_error(400, INVALID_REQUEST_ERR);
      $manual = simplexml_load_string(file_get_contents(MANUAL_PATH));

      $lang_tags = $manual->xpath('//*[@lang]');
      foreach ($lang_tags as $tag) {
        if ($tag['lang'] != $request->lang) {
          $dom = dom_import_simplexml($tag);
          $dom->parentNode->removeChild($dom);
        }
      }
      return $manual->asXML();
    }

    public function SETTINGS($request) {
      $settings = $this->get_json_from_file(SETTINGS_PATH);
      if ($settings === null) exit_error(500, INTERNAL_ERR);
      return $settings;
    }
  }
?>
