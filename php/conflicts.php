<?php
  use Swaggest\JsonDiff\JsonPointer;
  use Jfcherng\Diff\DiffHelper;
	use JsonPath\JsonObject;
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'error.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'basic.php';

  /*
  FORMAT for client overview:
    [
      {
        sectionPath: either: the pageManger style path,
        sectionJsonPointer: or: the jsonpointer style path,
        oldSectionName: name of section in old page,
        newSectionName: name of section in new page,
        contentName: name of the content,
        templateType: name of template type,
        head: the head diff,
        body: the body diff,
        objectModifications: true/false,
        objectRemovals: true/false,
        stringModifications: diff list (not applicable if among removed...)
        data: {type: css/js, default: strDiff, list: normal, listItems: strDiff, if not removed}
      }
    ]
  */

  /**
   * Utility class for handling ulpoads conflicts in case another user has uploaded data while editing.  
   */
  class Conflict_Handler extends Basic_Handler {

    private $conflicts = [];

    /**
     * @return array conflict: bool Indicates whether there is a conflict between current and previous uploads
     */
    public function check_conflict(): array {
      // get last history
      $last_history = $this->get_json_from_file(HISTORY_PATH);
      // get current history
      $current_history = $this->get_current_history();
      // check if possible overwrite
      return [
        "conflict" => ($last_history !== null && $last_history->user !== $current_history->user && $last_history->end > $current_history->start),
        "current" => $current_history,
        "last" => $last_history
      ];
    }

    /**
     * @param array $requests the client requests
     * 
     * @return mixed false if no history existing or an array of conflicts to be resolved
     */
    public function check_history(array &$requests) {
      $history = array_filter($requests, function($request) {
        return $request->request === "HISTORY";
      });
      if (count($history) === 0) return false;
      $index = array_keys($history)[0];
      array_splice($requests, $index, 1);
      // check if conflict
      $histories = $this->check_conflict();
      if ($histories["conflict"] === true) {
        // get last history
        $conflicts = $this->get_conflicts($requests, $histories["last"]);
        if ($conflicts !== false) return $conflicts;
      }
      $this->safe_save(json_encode($histories["current"]), HISTORY_PATH);
      $this->flush_files();
      $_SESSION['time'] = $histories["current"]->end;
      return false;
    }

    /**
     * @param array $requests the client requests
     * 
     * @return mixed false if no conflicts to resolve or object holding decisions and last history
     */
    public function check_resolve_conflict(array &$requests) {
      $resolve_conflict = array_filter($requests, function($request) {
        return $request->request === "RESOLVE_CONFLICT";
      });
      if (count($resolve_conflict) === 0) return false;
      $index = array_keys($resolve_conflict)[0];
      $request = array_splice($requests, $index, 1)[0];
      return $this->resolve_conflicts($request);
    }

    public function get_current_history() {
      $session_start_time = $_SESSION["time"];
      $session_user = $_SESSION['user'];
      $session_end = time();
      $history = (object) ["id" => rand() . time(), "user" => $session_user, "start" => $session_start_time, "end" => $session_end];
      return $history;
    }

    public function check_overwrite_conflict(object $request): bool {
      if (!property_exists($request, "overwrite") || $request->overwrite !== true) {
        $histories = $this->check_conflict();
        if ($histories["conflict"] === true) return false;
      }
      return true;
    }

    private function get_diff_value($json, $path) {
      if ($path === "/") return $json;
      $diff_value = null;
      try {
        $pointer = new JsonPointer();
        $pointer_path = $pointer->splitPath($path);
        $diff_value = $pointer->get($json, $pointer_path);
      }
      catch (Exception $e) {
      }
      return $diff_value;
    }

    private function split_section_json_pointer($json_pointer) {
      $status = preg_match('/\/(sections\/[\/0-9]+)*/', $json_pointer, $matches);
      if ($status !== 1) exit_error(500, INTERNAL_ERR);
      $length = strlen($matches[0]);
      if ($length === 1 || $matches[0][$length - 1] !== "/") return $matches[0];
      else return substr($matches[0], 0, $length - 1);
    }

    private function get_str_diff($old, $new) {
      $str_diffs = [];
      $json_result = DiffHelper::calculate($old, $new, 'Json', [], ['detailLevel' => 'char']);
      $json_result = json_decode($json_result);
      foreach ($json_result as $diff_block) {
        foreach ($diff_block as $diff) {
          if (property_exists($diff, "tag") && $diff->tag !== 1) array_push($str_diffs, ["id" => $this->get_conflict_id(), "diff" => $diff]);
        }
      }
      return $str_diffs;
    }

    private function get_conflict_ids($conflicts) {
      $json_object = new JsonObject($conflicts);
      return $json_object->get("$..id");
    }

    private function get_conflict_id() {
      $conflict_ids = $this->get_conflict_ids($this->conflicts);
      if ($conflict_ids === false) return rand();
      do {
        $id = rand();
      } while (in_array($id, $conflict_ids));
      return $id;
    }

    private function add_lines($html_str_1, $html_str_2) {
      $changed = false;
      if ($html_str_1 !== null && strlen($html_str_1) > 500 && strpos($html_str_1, "\n") === false && $html_str_1 != strip_tags($html_str_1)) {
        $pattern = '/(<\/[a-z0-9]+>)/';
        $replacement = "$1\n";
        $html_str_1 = preg_replace($pattern,$replacement,$html_str_1);
        if ($html_str_2 !== null) $html_str_2 = preg_replace($pattern,$replacement,$html_str_2);
        $changed = true;
      }
      return [0 => $html_str_1, 1 => $html_str_2, "changed" => $changed];
    }

    private function set_action(&$conflict, $action, $json_pointer, $old, $new) {
      if ($action === "removed") {
        if (!array_key_exists("objectRemovals", $conflict)) $conflict["objectRemovals"] = ["id" => $this->get_conflict_id(), "modifications" => []];
        array_push($conflict["objectRemovals"]["modifications"], $json_pointer);
      }
      else if ($action === "modified") {
        // check data
        $new_data = $this->get_diff_value($new, $json_pointer);
        if (is_string($new_data)) {
          // add to string modifications
          if (!array_key_exists("stringModifications", $conflict)) $conflict["stringModifications"] = [];
          $old_data = $this->get_diff_value($old, $json_pointer);
          $data = $this->add_lines($new_data, $old_data);
          array_push($conflict["stringModifications"], ["jsonPointer" => $json_pointer, "diff" => $this->get_str_diff($data[1], $data[0])]);
        }
        else {
          if (!array_key_exists("objectModifications", $conflict)) $conflict["objectModifications"] = ["id" => $this->get_conflict_id(), "modifications" => []];
          array_push($conflict["objectModifications"]["modifications"], $json_pointer);
        }
      }
    }

    private function get_page_conflicts($old, $new) {
      $page_conflicts = [];
      // get json diff
      $json_diff = $this->get_diffs($old, $new);
      // loop json diff
      foreach ($json_diff as $action => $section_diffs) {
        // get section json pointer
        foreach ($section_diffs as $json_pointer) {
          // check if total removal
          if ($action === "modified" && $json_pointer === "") {
            $action = "removed";
            $json_pointer = "/";
          }
          // split pointer
          $section_pointer = $this->split_section_json_pointer($json_pointer);
          // add section to conflicts
          $index = array_search($section_pointer, array_column($page_conflicts, 'sectionJsonPointer'));
          if ($index === false) {
            array_push($page_conflicts, ['sectionJsonPointer' => $section_pointer]);
            $index = count($page_conflicts) - 1;
          }
          $old_section = $this->get_diff_value($old, $section_pointer);
          if ($old_section !== null) $page_conflicts[$index]["oldSectionName"] = $old_section->name;
          $new_section = $this->get_diff_value($new, $section_pointer);
          if ($new_section !== null) $page_conflicts[$index]["newSectionName"] = $new_section->name;
          $this->set_action($page_conflicts[$index], $action, $json_pointer, $old, $new);
        }
      }
      return $page_conflicts;
    }

    private function get_diff_conflicts($old, $new) {
      $conflict = [];
      $json_diff = $this->get_diffs($old, $new);
      foreach ($json_diff as $action => $diffs) {
        foreach ($diffs as $json_pointer) {
          $this->set_action($conflict, $action, $json_pointer, $old, $new);
        }
      }
      return $conflict;
    }

    private function get_lang_conflicts($old, $new) {
      $lang_conflicts = [];
      if (property_exists($new, "languages") && property_exists($old, "languages")) {
        $diff_conflict = $this->get_diff_conflicts(["languages" => $old->languages], ["languages" => $new->languages]);
        if (count($diff_conflict) > 0) array_push($lang_conflicts, $diff_conflict);
        if ($old->defaultLanguage !== $new->defaultLanguage) array_push($lang_conflicts, ["objectModifications" => ["id" => $this->get_conflict_id(), "modifications" => ["/defaultLanguage"]]]);
      }
      return $lang_conflicts;
    }

    private function get_data_file_conflicts($old, $new) {
      $data_conflicts = [];
      if (property_exists($new, "list")) {
        $removed = [];
        foreach ($new->list as $item) {
          if (property_exists($item, "remove") && $item->remove === true) array_push($removed, $item->id);
        }
        if (count($removed) > 0) $data_conflicts["objectRemovals"] = ["id" => $this->get_conflict_id(), "modifications" => $removed];
      }
      if (count($data_conflicts) > 0) return [$data_conflicts];
      return [];
    }

    private function get_data_conflicts($default_path, $dir_path, $old, $new) {
      $data_conflicts = [];
      if (property_exists($new, "default") && file_exists($default_path)) {
        $old_default = file_get_contents($default_path);
        $diff = $this->get_str_diff($old_default, $new->default);
        if (count($diff) > 0) $data_conflicts["default"] = $diff;
      }
      if (property_exists($new, "list")) {
        $removed = [];
        $modified = [];
        foreach ($new->list as $item) {
          if (property_exists($item, "remove") && $item->remove === true) array_push($removed, $item->name);
          else if (property_exists($item, "data")) {
            $old_data = array_values(array_filter($old, function($old_item) use($item) {
              return $old_item->name === $item->name;
            }));
            if (count($old_data) < 1) exit_error(400, INVALID_REQUEST_ERR);
            $old_file_data = file_get_contents($dir_path . $old_data[0]->filename);
            $diff = $this->get_str_diff($old_file_data, $item->data);
            array_push($modified, ["name" => $item->name, "diff" => $diff]);
          }
        }
        if (count($removed) > 0) $data_conflicts["objectRemovals"] = ["id" => $this->get_conflict_id(), "modifications" => $removed];
        if (count($modified) > 0) $data_conflicts["stringModifications"] = $modified;
      }
      if (count($data_conflicts) > 0) return [$data_conflicts];
      return [];
    }

    private function get_content_conflicts($old, $new) {
      $contents_conflicts = [];
      foreach ($new as $path => $parts) {
        foreach ($parts as $part => $contents) {
          foreach ($contents as $name => $new_content) {
            if (property_exists($old, $path) && property_exists($old->{$path}, $part) && property_exists($old->{$path}->{$part}, $name)) {
              $old_content = $this->get_json_from_file(CONTENTS_PATH . $old->{$path}->{$part}->{$name});
              if ($old_content !== null) {
                $diff_conflict = $this->get_diff_conflicts($old_content, $new_content);
                if (count($diff_conflict) > 0) array_push($contents_conflicts, array_merge(["sectionPath" => $path, "part" => $part, "contentName" => $name], $diff_conflict));
              }
            }
          }
        }        
      }
      return $contents_conflicts;
    }

    private function get_template_conflicts($old, $new) {
      $template_conflicts = [];
      foreach ($new as $template) {
        $old_template_info = null;
        if ($template->ref === "path" && property_exists($old, $template->ref) && property_exists($old->{$template->ref}, $template->rel)) $old_template_info = $old->{$template->ref}->{$template->rel};
        else if ($template->ref === "type") {
          $part = ($template->rel->custom === true ? "custom" : "predefined");
          $name = $template->rel->name;
          if (property_exists($old->{$template->ref}, $part) && property_exists($old->{$template->ref}->{$part}, $name)) $old_template_info = $old->{$template->ref}->{$part}->{$name};
        }
        else exit_error(400, INVALID_REQUEST_ERR);
        if ($old_template_info !== null) {
          $diffs = [];
          foreach ($template->data as $part => $data) {
            if (property_exists($old_template_info, $part)) {
              $old_template = $this->get_template($template->ref, $template->rel);
              $new_template = $template->data->{$part};
              $diff = $this->get_str_diff($old_template->{$part}, $new_template);
              if (count($diff) > 0) $diffs[$part] = $diff;
            }
          }
          if (count($diffs) > 0) {
            $template_conflict = $diffs;
            if ($template->ref === "type") $template_conflict["templateType"] = $template->rel;
            else $template_conflict["sectionPath"] = $template->rel;
            array_push($template_conflicts, $template_conflict);
          }
        }
      }
      return $template_conflicts;
    }

    private function get_snippet_conflicts($old, $new) {
      $snippet_conflicts = [];
      $removed = [];
      $modified = [];
      foreach ($new as $name => $snippet) {
        if (property_exists($old, $name)) {
          if ($snippet === null) array_push($removed, ["name" => $name, "filename" => $old->{$name}]);
          else {
            $old_snippet = file_get_contents(SNIPPETS_DIR_PATH . $old->{$name});
            $diff = $this->get_str_diff($old_snippet, $new->{$name});
            array_push($modified, ["name" => $name, "diff" => $diff]);
          }
        }
      }
      if (count($removed) > 0) $snippet_conflicts["objectRemovals"] = ["id" => $this->get_conflict_id(), "modifications" => $removed];
      if (count($modified) > 0) $snippet_conflicts["stringModifications"] = $modified;
      if (count($snippet_conflicts) > 0) return [$snippet_conflicts];
      else return [];
    }

    private function get_text_snippet_conflicts($old, $new) {
      $snippet_conflicts = [];
      $removed = [];
      $modified = [];
      foreach ($new as $name => $snippet) {
        if (property_exists($old, $name)) {
          // check if to remove
          if (property_exists($snippet, "remove") && $snippet->remove === true) array_push($removed, ["name" => $name]);
          else if (property_exists($snippet, "text")) {
            foreach ($snippet->text as $lang => $text) {
              if (property_exists($old->{$name}->text, $lang)) {
                $diff = $this->get_str_diff($old->{$name}->text->{$lang}, $text);
                array_push($modified, ["name" => $name, "language" => $lang, "diff" => $diff]);
              }
            }
          }
        }
      }
      if (count($removed) > 0) $snippet_conflicts["objectRemovals"] = ["id" => $this->get_conflict_id(), "modifications" => $removed];
      if (count($modified) > 0) $snippet_conflicts["stringModifications"] = $modified;
      if (count($snippet_conflicts) > 0) return [$snippet_conflicts];
      else return [];
    }

    private function get_file_conflicts($old, $new) {
      $file_conflicts = [];
      foreach ($new as $path => $file_collections) {
        if ($file_collections !== null) {
          if (property_exists($old, $path)) {
            // remove filenames
            foreach ($old->{$path} as &$collection) {
              foreach ($collection as $collection_name => $file_desc) {
                unset($file_desc->filename);
              }
            }
            $diff_conflict = $this->get_diff_conflicts($old->{$path}, $file_collections);
            if (count($diff_conflict) > 0) {
              $diff_conflict["sectionPath"] = $path;
              array_push($file_conflicts, $diff_conflict);
            }
          }
        }       
      }
      return $file_conflicts;
    }

    private function get_form_conflicts($old, $new) {
      $form_conflicts = [];
      foreach ($new as $path => $forms) {
        if (property_exists($old, $path)) {
          $diff_conflict = $this->get_diff_conflicts($old->{$path}, $new->{$path});
          if (count($diff_conflict) > 0) {                
            $diff_conflict["sectionPath"] = $path;
            array_push($form_conflicts, $diff_conflict);
          }
        }
      }
      return $form_conflicts;
    }

    public function get_conflicts($requests, $last_history) {
      $this->conflicts = [];
      foreach ($requests as $request) {
        switch ($request->request) {
          case "PAGE":
            $cur_page = $this->get_json_from_file(PAGE_FILE_PATH);
            if ($cur_page !== null) {
              $page_conflicts = $this->get_page_conflicts($cur_page, $request->data);
              if (count($page_conflicts) > 0) $this->conflicts["PAGE"] = $page_conflicts;
            }
            break;
          case "LANGUAGES":
            $cur_langs = $this->get_json_from_file(LANG_FILE_PATH);
            if ($cur_langs !== null) {
              $lang_conflicts = $this->get_lang_conflicts($cur_langs, $request->data);
              if (count($lang_conflicts) > 0) $this->conflicts["LANGUAGES"] = $lang_conflicts;
            }
            break;
          case "CONTENTS":
            $content_list = $this->get_json_from_file(CONTENTS_LIST_PATH);
            if ($content_list !== null) {
              $content_conflicts = $this->get_content_conflicts($content_list, $request->data);
              if (count($content_conflicts) > 0) $this->conflicts["CONTENTS"] = $content_conflicts;
            }
            break;
          case "TEMPLATES":
            $templates_list = $this->get_json_from_file(TEMPLATES_LIST_PATH);
            if ($templates_list !== null) {
              $template_conflicts = $this->get_template_conflicts($templates_list, $request->data);
              if (count($template_conflicts) > 0) $this->conflicts["TEMPLATES"] = $template_conflicts;
            }
            break;
          case "SNIPPETS":
            $snippet_list = $this->get_json_from_file(SNIPPETS_LIST_PATH);
            if ($snippet_list !== null) {
              $snippet_conflicts = $this->get_snippet_conflicts($snippet_list, $request->data);
              if (count($snippet_conflicts) > 0) $this->conflicts["SNIPPETS"] = $snippet_conflicts;
            }
            break;
          case "TEXT_SNIPPETS":
            $text_snippets = $this->get_json_from_file((TEXT_SNIPPETS_PATH));
            if ($text_snippets !== null) {
              $snippet_conflicts = $this->get_text_snippet_conflicts($text_snippets, $request->data);
              if (count($snippet_conflicts) > 0) $this->conflicts["TEXT_SNIPPETS"] = $snippet_conflicts;
            }
            break;
          case "FILES":
            $files_list = $this->get_json_from_file(FILES_LIST_PATH);
            if ($files_list !== null) {
              $file_conflicts = $this->get_file_conflicts($files_list, $request->data);
              if (count($file_conflicts) > 0) $this->conflicts["FILES"] = $file_conflicts;
            }
            break;
          case "FORMS":
            $forms = $this->get_json_from_file(FORMS_PATH);
            if ($forms !== null) {
              $form_conflicts = $this->get_form_conflicts($forms, $request->data);
              if (count($form_conflicts) > 0) $this->conflicts["FORMS"] = $form_conflicts;
            }
            break;
          case "CSS":
            $css_list = $this->get_json_from_file(CSS_LIST_PATH);
            if ($css_list !== null) {
              $data_conflicts = $this->get_data_conflicts(CSS_PATH, CSS_DIR_PATH, $css_list, $request->data);
              if (count($data_conflicts) > 0) $this->conflicts["CSS"] = $data_conflicts;
            }
            break;
          case "FONTS":
            $fonts_list = $this->get_json_from_file(FONT_LIST_PATH);
            if ($fonts_list !== null) {
              $data_conflicts = $this->get_data_file_conflicts($fonts_list, $request->data);
              if (count($data_conflicts) > 0) $this->conflicts["FONTS"] = $data_conflicts;
            }
            break;
          case "JS":
            $js_list = $this->get_json_from_file(JS_LIST_PATH);
            if ($js_list !== null) {
              $data_conflicts = $this->get_data_conflicts(DEFAULT_JS_PATH, JS_DIR_PATH, $js_list, $request->data);
              if (count($data_conflicts) > 0) $this->conflicts["JS"] = $data_conflicts;
            }
            break;
          case "TEMPLATE_FILES":
            $template_file_list = $this->get_json_from_file(TEMPLATE_FILES_LIST_PATH);
            if ($template_file_list !== null) {
              $data_conflicts = $this->get_data_file_conflicts($template_file_list, $request->data);
              if (count($data_conflicts) > 0) $this->conflicts["TEMPLATE_FILES"] = $data_conflicts;
            }
            break;
          default:
            exit_error(400, INVALID_REQUEST_ERR);
            break;
        }
      }
      if (count($this->conflicts) > 0) {
        $last_history->conflicts = $this->conflicts;
        file_put_contents(HISTORY_PATH, json_encode($last_history), LOCK_EX);
        return ["id" => $last_history->id, "conflicts" => $this->conflicts];
      }
      return false;
    }

    private function resolve_conflicts($request) {
      $last_history = $this->get_json_from_file(HISTORY_PATH);
      if ($last_history === null || !property_exists($last_history, "conflicts")) exit_error(400, CONFLICT_ID_MISMATCH_ERR);
      if ($last_history->id !== $request->id) exit_error(400, CONFLICT_ID_MISMATCH_ERR);
      // check all ids
      $conflict_ids = $this->get_conflict_ids($last_history->conflicts);
      foreach ($conflict_ids as $id) {
        if (!property_exists($request->decisions, $id)) exit_error(400, INVALID_REQUEST_ERR);
      }
      return (object) ["decisions" => $request->decisions, "last_history" => $last_history];
    }

    private function resolve_string_modification($old_text, $new_text, $diff, $decisions) {
      $text = $this->add_lines($new_text, $old_text);
      $old_lines = explode("\n", $text[1]);
      $new_lines = explode("\n", $text[0]);
      foreach ($diff as $line) {
        $decision = $decisions->{$line->id};
        if ($decision === false) {
          $old_line = $line->diff->old;
          $new_line = $line->diff->new;
          array_splice($new_lines, $new_line->offset, count($new_line->lines), array_slice($old_lines, $old_line->offset, count($old_line->lines)));
        }
      }
      if ($text["changed"] === true) return implode("", $new_lines);
      else return implode("\n", $new_lines);
    }

    private function apply_on_conflict($conflict, $decisions, $old_data, &$new_data) {
      if (property_exists($conflict, "objectRemovals") && $decisions->{$conflict->objectRemovals->id} === false) {
        foreach ($conflict->objectRemovals->modifications as $path) {
          $old_value = $this->get_diff_value($old_data, $path);
          if ($path === "/") $path = "";
          try {
            $pointer = new JsonPointer();
            $pointer_path = $pointer->splitPath($path);
            $pointer->add($new_data, $pointer_path, $old_value);
          }
          catch (Exception $e) {
            exit_error(500, INTERNAL_ERR);
          }
        }
      }
      if (property_exists($conflict, "objectModifications") && $decisions->{$conflict->objectModifications->id} === false) {
        foreach ($conflict->objectModifications->modifications as $path) {
          $old_value = $this->get_diff_value($old_data, $path);
          try {
            $pointer = new JsonPointer();
            $pointer_path = $pointer->splitPath($path);
            $pointer->remove($new_data, $pointer_path);
            $pointer->add($new_data, $pointer_path, $old_value);
          }
          catch (Exception $e) {
            exit_error(500, INTERNAL_ERR);
          }
        }
      }
      if (property_exists($conflict, "stringModifications")) {
        foreach ($conflict->stringModifications as $modification) {
          $old_text = $this->get_diff_value($old_data, $modification->jsonPointer);
          $new_text = $this->get_diff_value($new_data, $modification->jsonPointer);
          $new_text = $this->resolve_string_modification($old_text, $new_text, $modification->diff, $decisions);
          try {
            $pointer = new JsonPointer();
            $pointer_path = $pointer->splitPath($modification->jsonPointer);
            $pointer->remove($new_data, $pointer_path);
            $pointer->add($new_data, $pointer_path, $new_text);
          }
          catch (Exception $e) {
            exit_error(500, INTERNAL_ERR);
          }
        }
      }
    }

    private function apply_data_file_decision($conflicts, &$handled, $resolve_conflict, $conflict_type, $dir_path, $list_path, $default_path = null) {
      $list = $this->get_json_from_file($list_path);
      if ($list === null) exit_error(400, INVALID_REQUEST_ERR);
      foreach ($conflicts as $conflict) {
        if (property_exists($conflict, "default") && $default_path !== null) {
          $old_default_data = file_get_contents($default_path);
          if ($old_default_data === false) exit_error(400, INVALID_REQUEST_ERR);
          $new_text = $this->resolve_string_modification($old_default_data, $handled[$conflict_type]->default, $conflict->default, $resolve_conflict->decisions);
          $handled[$conflict_type]->default = $new_text;
        }
        if (property_exists($conflict, "stringModifications")) {
          foreach ($conflict->stringModifications as $modification) {
            $old_data = array_values(array_filter($list, function($data) use($modification) {
              return $data->name === $modification->name;
            }));
            if (count($old_data) < 1) exit_error(400, INVALID_REQUEST_ERR);
            $old_data = file_get_contents($dir_path . $old_data[0]->filename);
            if ($old_data === false) exit_error(400, INVALID_REQUEST_ERR);
            $new_data = array_filter($handled[$conflict_type]->list, function($data) use($modification) {
              return $data->name === $modification->name;
            });
            if (count($new_data) < 1) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($new_data as $index => $data_data) {
              $new_text = $this->resolve_string_modification($old_data, $data_data->data, $modification->diff, $resolve_conflict->decisions);
              $handled[$conflict_type]->list[$index]->data = $new_text;
            }
          }
        }
        if ($default_path === null) $parameter = "id";
        else $parameter = "name";
        if (property_exists($conflict, "objectRemovals")) {
          if ($resolve_conflict->decisions->{$conflict->objectRemovals->id} === false) {
            foreach ($conflict->objectRemovals->modifications as $identifier) {
              $index = array_search($identifier, array_column($handled[$conflict_type]->list, $parameter));
              if ($index === false) exit_error(400, INVALID_REQUEST_ERR);
              unset($handled[$conflict_type]->list[$index]->remove);
            }
          }
        }
      }
    }

    public function apply_decisions($resolve_conflict, &$handled) {
      if (!property_exists($resolve_conflict, "last_history") || !property_exists($resolve_conflict->last_history, "conflicts")) exit_error(500, INTERNAL_ERR);
      foreach ($resolve_conflict->last_history->conflicts as $request => $conflicts) {
        switch ($request) {
          case "LANGUAGES":
            $cur_langs = $this->get_json_from_file(LANG_FILE_PATH);
            if ($cur_langs === null) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($conflicts as $conflict) {
              $this->apply_on_conflict($conflict, $resolve_conflict->decisions, $cur_langs, $handled["LANGUAGES"]);
            }
            break;
          case "PAGE":
            $cur_page = $this->get_json_from_file(PAGE_FILE_PATH);
            if ($cur_page === null) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($conflicts as $conflict) {
              $this->apply_on_conflict($conflict, $resolve_conflict->decisions, $cur_page, $handled["PAGE"]);
            }
            break;
          case "CONTENTS":
            if ($handled["CONTENTS"] === null) return;
            $content_list = $this->get_json_from_file(CONTENTS_LIST_PATH);
            if ($content_list === null) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($conflicts as $conflict) {
              if (!property_exists($conflict, "sectionPath") || !property_exists($content_list->{$conflict->sectionPath}, $conflict->part) || !property_exists($content_list->{$conflict->sectionPath}->{$conflict->part}, $conflict->contentName)) exit_error(400, INVALID_REQUEST_ERR);
              $filename = $content_list->{$conflict->sectionPath}->{$conflict->part}->{$conflict->contentName};
              $old_content = $this->get_json_from_file(CONTENTS_PATH . $filename);
              $index = -1;
              for ($i=0; $i < count($handled["CONTENTS"]->contents) && $index === -1; $i++) {
                if ($handled["CONTENTS"]->contents[$i]["filename"] === $filename) $index = $i;
              }
              if ($index === -1) exit_error(500, INTERNAL_ERR);
              $this->apply_on_conflict($conflict, $resolve_conflict->decisions, $old_content, $handled["CONTENTS"]->contents[$index]["data"]);
            }
            break;
          case "TEMPLATES":
            $templates_list = $this->get_json_from_file(TEMPLATES_LIST_PATH);
            if ($templates_list === null) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($conflicts as $conflict) {
              $index = -1;
              if (property_exists($conflict, "sectionPath")) {
                $ref = "path";
                $rel = $conflict->sectionPath;
                for ($i=0; $i < count($handled["TEMPLATES"]) && $index === -1; $i++) {
                  if ($handled["TEMPLATES"][$i]->ref === $ref && $handled["TEMPLATES"][$i]->rel === $rel) $index = $i;
                  break;
                }
              }
              else {
                $ref = "type";
                $rel_part = ($conflict->templateType->custom === true ? "custom": "predefined");
                $rel_name = $conflict->templateType->name;
                for ($i=0; $i < count($handled["TEMPLATES"]) && $index === -1; $i++) {
                  if (
                    $handled["TEMPLATES"][$i]->ref === $ref && 
                    $handled["TEMPLATES"][$i]->rel->custom === $conflict->templateType->custom && 
                    $handled["TEMPLATES"][$i]->rel->name === $conflict->templateType->name
                  ) $index = $i;
                }
              }
              if ($index === -1) exit_error(500, INTERNAL_ERR);
              $parts = ["head", "body"];
              foreach ($parts as $part) {
                if (property_exists($conflict, $part)) {
                  if (property_exists($conflict, "sectionPath")) $old_template = file_get_contents(TEMPLATES_PATH . $templates_list->{$ref}->{$rel}->{$part});
                  else $old_template = file_get_contents(TEMPLATES_PATH . $templates_list->{$ref}->{$rel_part}->{$rel_name}->{$part});
                  if ($old_template === false) exit_error(400, INVALID_REQUEST_ERR);
                  $new_text = $this->resolve_string_modification($old_template, $handled["TEMPLATES"][$index]->data->{$part}, $conflict->{$part}, $resolve_conflict->decisions);
                  $handled["TEMPLATES"][$index]->data->{$part} = $new_text;
                }
              }
            }
            break;
          case "SNIPPETS":
            $snippet_list = $this->get_json_from_file(SNIPPETS_LIST_PATH);
            if ($snippet_list === null) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($conflicts as $conflict) {
              if (property_exists($conflict, "stringModifications")) {
                foreach ($conflict->stringModifications as $modification) {
                  $old_snippet = file_get_contents(SNIPPETS_DIR_PATH . $snippet_list->{$modification->name});
                  if ($old_snippet === null) exit_error(400, INVALID_REQUEST_ERR);
                  $new_snippet = $this->resolve_string_modification($old_snippet, $handled["SNIPPETS"]->new->{$modification->name}, $modification->diff, $resolve_conflict->decisions);
                  $handled["SNIPPETS"]->new->{$modification->name} = $new_snippet;
                }
              }
              if (property_exists($conflict, "objectRemovals")) {
                if ($resolve_conflict->decisions->{$conflict->objectRemovals->id} === false) {
                  foreach ($conflict->objectRemovals->modifications as $removed_snippet) {
                    $handled["SNIPPETS"]->list->{$removed_snippet->name} = $removed_snippet->filename;
                    $index = array_search($removed_snippet->filename, $handled["SNIPPETS"]->remove);
                    array_splice($handled["SNIPPETS"]->remove, 0, 1);
                  }
                }
              }
            }
            break;
          case "TEXT_SNIPPETS":
            $saved_snippets = $this->get_json_from_file(TEXT_SNIPPETS_PATH);
            if ($saved_snippets === null) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($conflicts as $conflict) {              
              if (property_exists($conflict, "stringModifications")) {
                foreach ($conflict->stringModifications as $modification) {
                  $old_text = $saved_snippets->{$modification->name}->text->{$modification->language};
                  if ($old_text === null) exit_error(400, INVALID_REQUEST_ERR);
                  $new_text = $this->resolve_string_modification($old_text, $handled["TEXT_SNIPPETS"]->{$modification->name}->text->{$modification->language}, $modification->diff, $resolve_conflict->decisions);
                  $handled["TEXT_SNIPPETS"]->{$modification->name}->text->{$modification->language} = $new_text;
                }
              }
              if (property_exists($conflict, "objectRemovals")) {
                if ($resolve_conflict->decisions->{$conflict->objectRemovals->id} === false) {
                  foreach ($conflict->objectRemovals->modifications as $removed_snippet) {
                    $handled["TEXT_SNIPPETS"]->{$removed_snippet->name} = $saved_snippets->{$removed_snippet->name};
                  }
                }
              }
            }
            break;
          case "FILES":
            $files_list = $this->get_json_from_file(FILES_LIST_PATH);
            if ($files_list === null) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($conflicts as $conflict) {
              if (!property_exists($conflict, "sectionPath") || !property_exists($files_list, $conflict->sectionPath)) exit_error(400, INVALID_REQUEST_ERR);
              $this->apply_on_conflict($conflict, $resolve_conflict->decisions, $files_list->{$conflict->sectionPath}, $handled["FILES"]->files_list->{$conflict->sectionPath});
            }
            break;
          case "FORMS":
            $forms = $this->get_json_from_file(FORMS_PATH);
            if ($forms === null) exit_error(400, INVALID_REQUEST_ERR);
            foreach ($conflicts as $conflict) {
              if (!property_exists($conflict, "sectionPath") || !property_exists($forms, $conflict->sectionPath)) exit_error(400, INVALID_REQUEST_ERR);
              $this->apply_on_conflict($conflict, $resolve_conflict->decisions, $forms->{$conflict->sectionPath}, $handled["FORMS"]->{$conflict->sectionPath});
            }
            break;
          case "CSS":
            $this->apply_data_file_decision($conflicts, $handled, $resolve_conflict, "CSS", CSS_DIR_PATH, CSS_LIST_PATH, CSS_PATH);
            break;
          case "FONTS":
            $this->apply_data_file_decision($conflicts, $handled, $resolve_conflict, "FONTS", FONT_DIR_PATH, FONT_LIST_PATH);
            break;
          case "CSS":
            $this->apply_data_file_decision($conflicts, $handled, $resolve_conflict, "JS", JS_DIR_PATH, JS_LIST_PATH, DEFAULT_JS_PATH);
            break;
          case "TEMPLATE_FILES":
            $this->apply_data_file_decision($conflicts, $handled, $resolve_conflict, "TEMPLATE_FILES", TEMPLATE_FILES_DIR_PATH, TEMPLATE_FILES_LIST_PATH);
            break;
          default:
            exit_error(400, INVALID_REQUEST_ERR);
            break;
        }
      }
      // get current history and save it
      $current_history = $this->get_current_history();
      $this->safe_save(json_encode($current_history), HISTORY_PATH);
      $this->flush_files();
      $_SESSION['time'] = $current_history->end;
    }
  }
?>
