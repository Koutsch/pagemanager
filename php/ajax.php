<?php
	require_once __DIR__ . DIRECTORY_SEPARATOR . "error.php";
  require_once __DIR__ . DIRECTORY_SEPARATOR . "basic.php";
  require_once __DIR__ . DIRECTORY_SEPARATOR . "login.php";
  require_once __DIR__ . DIRECTORY_SEPARATOR . "form.php";
  require_once __DIR__ . DIRECTORY_SEPARATOR . "templateHandler.php";
  use JsonPath\JsonObject;

  class AjaxResponse extends Basic_Handler {
    private $preview;
    function __construct($preview = true) {
      if ($preview === true) {
        $this->preview = true;
      }
      else {
        $this->preview = false;
      }
    }

    private function get_events($paths, $language) {
      $filtered_events = [];
      $languages = $this->get_json_from_file(LANG_FILE_PATH);
      $events = $this->get_json_from_file(EVENT_LIST_PATH);
      $page = $this->get_json_from_file(PAGE_FILE_PATH);
      if ($events === null) return [];
      foreach ($events as $path => $files) {
        $filtered = true;
        if ($paths === null) $filtered = false;
        else if (is_array($paths)) {
          // check if in paths
          foreach ($paths as $request_path) {
            $length = strlen($request_path);
            if (substr($path, 0, $length) === $request_path) {
              $filtered = false;
              break;
            }
          }
        }
        if ($filtered === false) {
          $section = $this->get_section($page, $path);
          foreach ($files as $filename) {
            $content = $this->get_json_from_file(CONTENTS_PATH . $filename);
            if ($content !== null) {
              $name_path = $this->get_named_path($path, $language);
              if (!property_exists($section->title, $language)) $title = $section->title->{$languages->defaultLanguage};
              else $title = $section->title->{$language};
              // loop event array
              foreach ($content as $event_info) {
                $event = ["path" => $name_path, "title" => $title, "dateTime" => $event_info->dateTime, "place" => $event_info->place];
                array_push($filtered_events, $event);
              }
            }
          }
        }
      }
      return $filtered_events;
    }

    public function process_request() {
      // check request
      if ($_SERVER["REQUEST_METHOD"] !== "POST" || !isset($_POST["request"])) exit_error(400, INVALID_REQUEST_ERR);
      // get request
      $request = json_decode($_POST["request"]);
      // process accordingly
      switch ($request->request) {
        case "lang":
          $languages = $this->get_json_from_file(LANG_FILE_PATH);
          if ($languages !== null) {
            $cur_language = array_filter($languages->languages, function($language) use($request) {
              return $language->iso639 === $request->lang;
            });
            if (count($cur_language) > 0) {
              $_SESSION["cur_lang"] = $request->lang;
              return true;   
            }
          }
          $_SESSION["cur_lang"] = $languages->defaultLanguage;
          return true;
        case "login":
          $login_handler = new LoginHandler();
          $status = $login_handler->login($request->username, $request->password);
          return json_encode($status);
        case "newPassword":
          $login_handler = new LoginHandler();
          $status = $login_handler->set_new_password($request->username, $request->password, $request->newPassword, $request->newPasswordRepeat);
          if ($status !== null) exit_error(401, $status);
          return json_encode(true);
        case "logout":
          $login_handler = new LoginHandler();
          $login_handler->logout();
          return json_encode(true);
        case "resetPassword":
          $login_handler = new LoginHandler();
          $status = $login_handler->reset_password($request->username);
          if ($status === false) exit_error(401, INVALID_CREDENTIALS_ERR);          
          return json_encode(true);
        case "sections":
          if (!property_exists($request, "path") || !property_exists($request, "schema") || !property_exists($request, "custom") || !property_exists($request, "start") || !property_exists($request, "length") || !property_exists($request, "reverse") || !property_exists($request, "language") || !property_exists($request, "template")) exit_error(400, INVALID_REQUEST_ERR);
          // create template and send to user
          $template_handler = new Template_Handler();
          $child_templates = $template_handler->create_children($request->path, $request->schema, $request->custom, $request->reverse, $request->start, $request->length, $request->language, $request->template, $this->preview);
          return json_encode($child_templates);
        case "calendarByDate":
          if (!property_exists($request, "begin") || !property_exists($request, "end") || !property_exists($request, "backward") || !property_exists($request, "language") || !property_exists($request, "paths")) exit_error(400, INVALID_REQUEST_ERR);
          $events = $this->get_events($request->paths, $request->language);
          $filtered = array_values(array_filter($events, function($event) use($request) {
            $time = strtotime($event["dateTime"]);
            $begin = strtotime($request->begin);
            $end = strtotime($request->end);
            return ($time >= $begin && $time <= $end);
          }));
          usort($filtered, function($a, $b) {
            if ($a['dateTime'] > $b['dateTime']) {
              return 1;
            } elseif ($a['dateTime'] < $b['dateTime']) {
              return -1;
            }
            return 0;
          });
          if ($request->backward === true) $filtered = array_reverse($filtered);
          return json_encode([
            "list" => $filtered,
            "number" => count($events)
          ]);
        case "calendar":
          if (!property_exists($request, "start") || !property_exists($request, "length") || !property_exists($request, "backward") || !property_exists($request, "language") || !property_exists($request, "paths")) exit_error(400, INVALID_REQUEST_ERR);
          if ($request->start < 0) exit_error(400, INVALID_REQUEST_ERR);
          $sorted = $this->get_events($request->paths, $request->language);
          if (count($sorted) === 0) return [];
          usort($sorted, function($a, $b) {
            if ($a['dateTime'] > $b['dateTime']) {
              return 1;
            } elseif ($a['dateTime'] < $b['dateTime']) {
              return -1;
            }
            return 0;
          });          
          if ($request->backward === true) $sorted = array_reverse($sorted);
          return json_encode([
            "list" => array_slice($sorted, $request->start, $request->length),
            "number" => count($sorted)
          ]);
        case "form":
          $form_handler = new FormHandler();
          $form_handler->save_form($request->form, $request->path, $request->formId);
          return json_encode(true);
        default:
          exit_error(400, INVALID_REQUEST_ERR);
          break;
      }
    }
  }
?>