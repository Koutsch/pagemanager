<?php
  ini_set( "session.gc_maxlifetime", 60 * 60);
  session_start();

  require_once __DIR__ . DIRECTORY_SEPARATOR . 'error.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'basic.php';

  class LoginHandler extends Basic_Handler {
    protected object $users;

    function __construct() {
      parent::__construct();
      $users = $this->get_json_from_file(USERS_PATH);
      if ($users === null) {
        // create folders
        if (!file_exists(DATA_ROOT_PATH)) mkdir(DATA_ROOT_PATH);
        if (!file_exists(USERS_DIR_PATH)) mkdir(USERS_DIR_PATH);
        $users = (object) [];
      }
      $this->users = $users;
    }

    protected function check_user(string $username): ?object {
      if ($this->users === null) exit_error(400, INVALID_REQUEST_ERR);
      $cur_user = null;
      foreach ($this->users as $id => $user) {
        if ($user->username === $username) $cur_user = $user;
      }
      return $cur_user;
    }

    public function get_user(): ?string {
      if (!isset($_SESSION["user"])) return null;
      else return $_SESSION["user"];
    }

    private function check_password_valid(object $user, string $password): bool {
      return (property_exists($user, "password") && $user->password === md5($password));
    }

    private function check_password(object $user, string $password): bool {
      if (property_exists($user, "pin")) {
        if ($user->pin !== intval($password)) exit_error(401, INVALID_CREDENTIALS_ERR);
        return false;
      }
      else if (property_exists($user, "password")) {
        if (!$this->check_password_valid($user, $password)) exit_error(401, INVALID_CREDENTIALS_ERR);
        return true;
      }
      exit_error(400, INVALID_REQUEST_ERR);
    }

    private function save_user_session(string $username): void {
      $_SESSION['user'] = $username;
      $_SESSION['time'] = time();
    }

    public function login(string $username, string $password): bool {
      $user = $this->check_user($username);
      if ($user === null) exit_error(401, INVALID_CREDENTIALS_ERR);
      $is_password = $this->check_password($user, $password);
      if ($is_password) $this->save_user_session($username);
      return $is_password;
    }

    public function password_login(string $username, string $password): int {
      $user = $this->check_user($username);
      if ($user === null) return false;
      if (property_exists($user, "password")) {
        if (!$this->check_password_valid($user, $password)) return 0;
        $this->save_user_session($username);
        return 1;
      }
      else if (property_exists($user, "pin")) {
        if ($user->pin !== intval($password)) return 0;
        else return -1;
      }
      return 0;
    }

    public function set_new_password(string $username, $pin, string $new_password, string $new_password_repeat): ?string {
      $user = $this->check_user($username);
      if ($user === null) return INVALID_CREDENTIALS_ERR;
      if (!property_exists($user, "pin") || $user->pin !== intval($pin)) return INVALID_CREDENTIALS_ERR;
      if ($new_password === null || strlen($new_password) < 1) return NO_PASSWORD_ERR;
      if ($new_password !== $new_password_repeat) return PASSWORD_MISMATCH_ERR;
      unset($user->pin);
      $user->password = md5($new_password);
      $this->safe_save(json_encode($this->users, JSON_PRETTY_PRINT), USERS_PATH);
      $this->flush_files();
      return null;
    }

    public function logout(): void {
      if ($this->user_not_signed_in()) exit_error(400, INVALID_REQUEST_ERR);
      unset($_SESSION['user']);
      if (isset($_SESSION['time'])) unset($_SESSION['time']);
    }

    public function user_signed_in(): bool {
      return (isset($_SESSION['user']));
    }

    public function user_not_signed_in(): bool {
      return (!isset($_SESSION['user']));
    }

    public function user_has_access(?string $section_path): bool {
      if ($section_path === null) return false;
      if ($this->user_not_signed_in()) return false;
      $user = $this->check_user($_SESSION['user']);
      if ($user === null) return false; 
      if (!property_exists($user, "privileges")) return false;
      if (!in_array($section_path, $user->privileges)) return false;
      return true;
    }

    public function reset_password($username): bool {
      $user = $this->check_user($username);
      if ($user === null) return false;
      $pin = $this->get_pin();
      // send pin via email
      $this->send_user_mail($user->email, $username, $pin);
      if (property_exists($user, "password")) unset($user->password);
      if (property_exists($user, "pin")) unset($user->pin);
      $user->pin = $pin;
      $this->safe_save(json_encode($this->users, JSON_PRETTY_PRINT), USERS_PATH);
      $this->flush_files();
      return true;
    }

    public function is_admin() {
      if ($this->user_not_signed_in()) return false;
      $user = $this->check_user($_SESSION['user']);
      if ($user === null) return false;
      if ($user->privilege !== "a" && $user->privilege !== "b") return false;
      return $user;
    }

    public function get_admin_privilege(): ?string {
      $user = $this->is_admin();
      if ($user === null) return null;
      return $user->privilege;
    }
  }
?>
