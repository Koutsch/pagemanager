<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf0c8c6ad3e92b362a8c9f698d5a2fcce
{
    public static $files = array (
        '6e3fae29631ef280660b3cdad06f25a8' => __DIR__ . '/..' . '/symfony/deprecation-contracts/function.php',
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        '320cde22f66dd4f5d3fd621d3e88b98f' => __DIR__ . '/..' . '/symfony/polyfill-ctype/bootstrap.php',
    );

    public static $prefixLengthsPsr4 = array (
        'U' => 
        array (
            'Utilities\\' => 10,
        ),
        'T' => 
        array (
            'Twig\\' => 5,
        ),
        'S' => 
        array (
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Polyfill\\Ctype\\' => 23,
            'Swaggest\\JsonSchema\\' => 20,
            'Swaggest\\JsonDiff\\' => 18,
        ),
        'P' => 
        array (
            'Psr\\Container\\' => 14,
            'PhpLang\\' => 8,
        ),
        'J' => 
        array (
            'JsonPath\\' => 9,
            'Jfcherng\\Utility\\' => 17,
            'Jfcherng\\Diff\\' => 14,
            'JSONSchemaFaker\\' => 16,
        ),
        'F' => 
        array (
            'Faker\\' => 6,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Utilities\\' => 
        array (
            0 => __DIR__ . '/..' . '/galbar/jsonpath/src/Galbar/Utilities',
        ),
        'Twig\\' => 
        array (
            0 => __DIR__ . '/..' . '/twig/twig/src',
        ),
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Polyfill\\Ctype\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-ctype',
        ),
        'Swaggest\\JsonSchema\\' => 
        array (
            0 => __DIR__ . '/..' . '/swaggest/json-schema/src',
        ),
        'Swaggest\\JsonDiff\\' => 
        array (
            0 => __DIR__ . '/..' . '/swaggest/json-diff/src',
        ),
        'Psr\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/container/src',
        ),
        'PhpLang\\' => 
        array (
            0 => __DIR__ . '/..' . '/phplang/scope-exit/src',
        ),
        'JsonPath\\' => 
        array (
            0 => __DIR__ . '/..' . '/galbar/jsonpath/src/Galbar/JsonPath',
        ),
        'Jfcherng\\Utility\\' => 
        array (
            0 => __DIR__ . '/..' . '/jfcherng/php-color-output/src',
            1 => __DIR__ . '/..' . '/jfcherng/php-mb-string/src',
        ),
        'Jfcherng\\Diff\\' => 
        array (
            0 => __DIR__ . '/..' . '/jfcherng/php-diff/src',
            1 => __DIR__ . '/..' . '/jfcherng/php-sequence-matcher/src',
        ),
        'JSONSchemaFaker\\' => 
        array (
            0 => __DIR__ . '/..' . '/koriym/json-schema-faker/src',
        ),
        'Faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/fakerphp/faker/src/Faker',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf0c8c6ad3e92b362a8c9f698d5a2fcce::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf0c8c6ad3e92b362a8c9f698d5a2fcce::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
