<?php
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'login.php';

  function signed_in() {
    $login_handler = new LoginHandler();
    return $login_handler->user_signed_in();
  }

  function not_signed_in() {
    $login_handler = new LoginHandler();
    return $login_handler->user_not_signed_in();
  }

  function has_access(string $section_path): bool {
    $login_handler = new LoginHandler();
    return $login_handler->user_has_access($section_path);
  }
?>
