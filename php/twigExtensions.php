<?php
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'basic.php';
  require_once __DIR__ . '/vendor/autoload.php';
  use JSONSchemaFaker\Faker;

  $twig_extensions = [
    "getText" => [ 
      "template" => "[[[0]]]|getText",
      "type" => "filter",
      "name" => "getText",
      "php" => function($context, $text) {
        if ($text !== null) {
          if (!property_exists($text, $context["lang"]) || $text->{$context["lang"]} === "") return $text->{$context["class"]->languages->defaultLanguage};
          else return $text->{$context["lang"]};
        }
        return null;
      }
    ],
    "getDateTime" => [
      "template" => "[[[0]]]|getDateTime('FULL|LONG|MEDIUM|SHORT', 'FULL|LONG|MEDIUM|SHORT')",
      "type" => "filter",
      "name" => "getDateTime",
      "php" => function($context, $text, $date_constant, $time_constant=null) {
        $format_constant_date = $context["class"]->get_date_constant($date_constant);
        $format_constant_time = $context["class"]->get_date_constant($time_constant);
        $date_time = new DateTime($text);
        $formatter = new IntlDateFormatter($context["class"]->display_language, $format_constant_date, $format_constant_time);
        return $formatter->format($date_time);
      }
    ],
    "getSections" => [
      "template" => "[[[0]]]|getSections",
      "type" => "filter",
      "name" => "getSections",
      "php" => function($context, $section) {
        $sections = [];
        $section_path = explode("/", $section->path);
        foreach ($section->sections as $subsection) {
          $subsection->path = implode("/", array_merge($section_path, [$subsection->id]));
          array_push($sections, $subsection);
        }
        return $sections;
      }
    ],
    "getSavedSection" => [
      "template" => "getSection('[[[0]]]')",
      "type" => "function",
      "name" => "getSection",
      "php" => function($context, $path) {
        $section = $context["class"]->get_section($context["page"], $path);
        $section->path = $path;
        return json_decode(json_encode($section));
      }
    ],
    "getContent" => [
      "template" => "|content('[[[0]]]',[[[1]]])",
      "type" => "filter",
      "name" => "content",
      "php" => function($context, $section, $name, $custom) {
        if ($section === null) return null;
        if ($context["class"]->ref === "type" && $context["class"]->include === true) {
          if (property_exists($section, "content")) $content = $section->content;
          else {
            $section_schema = $context["class"]->get_section_schema($section->type, $section->custom);
            $content = $section_schema->content;
          }
          if ($custom === true) {
            $part = "custom";
            $content_schemas = $context["class"]->get_json_from_file(CUSTOM_CONTENT_SCHEMAS_PATH);
          }
          else {
            $part = "predefined";
            $content_schemas = $context["class"]->get_json_from_file(CONTENT_SCHEMAS_PATH);
          }
          $type = $content->{$part}->{$name}->type;
          $content_schema = $content_schemas->{$type}->schema;
          $context["class"]->add_lang_input($content_schema, $context["class"]->languages, $context["class"]->display_language);
          $fake_content = (new Faker)->generate($content_schema);
          return $fake_content;
        }
        $contents_uploaded = $context["class"]->contents_uploaded;
        $part = ($custom === true ? "custom" : "predefined");
        if (
          $contents_uploaded !== null &&
          property_exists($contents_uploaded, $section->path) && $contents_uploaded->{$section->path} !== null &&
          property_exists($contents_uploaded->{$section->path}, $part) && $contents_uploaded->{$section->path}->{$part} !== null &&
          property_exists($contents_uploaded->{$section->path}->{$part}, $name) && $contents_uploaded->{$section->path}->{$part}->{$name} !== null
        ) {
          return $contents_uploaded->{$section->path}->{$part}->{$name};
        }
        else {
          $content_list = $context["class"]->get_json_from_file(CONTENTS_LIST_PATH);
          if ($content_list !== null && property_exists($content_list, $section->path) && property_exists($content_list->{$section->path}, $part) && property_exists($content_list->{$section->path}->{$part}, $name)) {
            return $context["class"]->get_json_from_file(CONTENTS_PATH . $content_list->{$section->path}->{$part}->{$name});
          }
        }
        return null;
      }
    ],
    "getContentFileLinks" => [
      "template" => "|getContentFileLinks('[[[0]]]', true)",
      "type" => "filter",
      "name" => "getContentFileLinks",
      "php" => function($context, $section, $collection, $check_access) {
        // create dummy file array
        if ($context["class"]->ref === "type" && $context["class"]->include === true) {
          return [
            [
              "src" => "",
              "caption" => $context["class"]->get_setting("LOREM_IPSUM")
            ]
          ];
        }
        // get real paths and captions
        return $context["class"]->get_content_file_links_captions($section->path, $collection, $check_access);
      }
    ],
    "getContentFiles" => [
      "template" => "|getContentFiles('[[[0]]]')",
      "type" => "filter",
      "name" => "getContentFiles",
      "php" => function($context, $section, $collection) {
        // create dummy file array
        if ($context["class"]->ref === "type" && $context["class"]->include === true) {
          $mime_types = $context["class"]->get_file_type($section, $collection);
          if ($mime_types === null) return null;
          return [
            [
              "src" => $context["class"]->get_dummy_file($mime_types),
              "caption" => $context["class"]->get_setting("LOREM_IPSUM")
            ]
          ];
        }
        // get real paths and captions
        return $context["class"]->get_content_files_captions($section->path, $collection);
      }
    ],
    "getContentFile" => [
      "template" => "|getContentFile('[[[0]]]', [[[1]]])",
      "type" => "filter",
      "name" => "getContentFile",
      "php" => function($context, $section, $collection, $file_id) {
        // create dummy file
        if ($context["class"]->ref === "type" && $context["class"]->include === true) {
          $mime_types = $context["class"]->get_file_type($section, $collection);
          if ($mime_types === null) return null;
          return $context["class"]->get_dummy_file($mime_types);
        }
        // get real path
        $content_file = $context["class"]->get_content_files($section->path, $collection, $file_id);
        if ($content_file === null || count($content_file) < 1) return null;
        return $content_file[0];
      }
    ],
    "getCaption" => [
      "template" => "|caption('[[[0]]]', [[[1]]])",
      "type" => "filter",
      "name" => "caption",
      "php" => function($context, $section, $collection, $file_id) {
        if ($context["class"]->ref === "type" && $context["class"]->include === true) {
          return $context["class"]->get_setting("LOREM_IPSUM");
        }
        $caption = $context["class"]->get_caption($section->path, $collection, $file_id);
        return $caption;
      }
    ],
    "getType" => [
      "template" => "|getType('[[[0]]]',[[[1]]])[*]",
      "type" => "filter",
      "name" => "getType",
      "php" => function($context, $section, $type, $custom) {
        if (!property_exists($section, "sections")) return null;
        $sections = [];
        $section_path = explode("/", $section->path);
        foreach ($section->sections as $subsection) {
          if ($subsection->custom === $custom && $subsection->type === $type) {
            $subsection->path = implode("/", array_merge($section_path, [$subsection->id]));
            array_push($sections, json_decode(json_encode($subsection)));
          }
        }
        return $sections;
      }
    ],
    "getTemplateFile" => [
      "template" => "[[[0]]]|getTemplateFile",
      "type" => "filter",
      "name" => "getTemplateFile",
      "php" => function($context, $id) {
        $template_file = $context["class"]->get_template_file($id);
        return sprintf("%s?v=%d", $template_file, $context["class"]->build);
      }
    ],
    "getTextSnippet" => [
      "template" => "'[[[0]]]'|getTextSnippet",
      "type" => "filter",
      "name" => "getTextSnippet",
      "php" => function($context, $name) {
        $text_snippet = $context["class"]->get_text_snippet($name);
        if ($text_snippet === null) return "";
        else return $text_snippet;
      },
      "raw" => true
    ],
    "getSelector" => [
      "template" => "[[[0]]]|getSelector('[[[1]]]')",
      "type" => "filter",
      "name" => "getSelector",
      "php" => function($context, $value, $selector) {
        if (!is_string($value)) return null;
        $translations = $context["class"]->get_json_from_file(TRANSLATIONS_PATH);
        if ($translations === null || !property_exists($translations, $selector)) return $value;
        $translation = $translations->{$selector};
        return $translation->translations->{$value}->{$context["class"]->display_language};
      }
    ],
    "getLink" => [
      "template" => "<a href='{{\"[[[0]]]\"|getLink}}'>\n\t\n</a>",
      "type" => "filter",
      "name" => "getLink",
      "php" => function($context, $path) {
        return $context["class"]->create_named_link($path);
      }
    ],
    "getUnspecifiedLink" => [
      "template" => "<a href='{{[[[0]]]|getUnspecLink}}'>\n\t\n</a>",
      "type" => "filter",
      "name" => "getUnspecLink",
      "php" => function($context, $section) {
        return $context["class"]->create_named_link($section->path);
      }
    ],
    "getIncludeSection" => [
      "template" => "{{ include('section.template') }}",
      "type" => "text",
      "toolbar" => [
        "label" => "contents",
        "logo" => "ArtTrack",
        "apply" => ["page"],
        "area" => "twig"
      ]
    ],
    "getIf" => [
      "template" => "{% if x %}\n\t\n{% endif %}",
      "type" => "text",
      "toolbar" => [
        "label" => "if",
        "logo" => "QuestionMark",
        "apply" => ["section", "page"],
        "area" => "twig"
      ]
    ],
    "getFor" => [
      "template" => "{% for x in y %}\n\t\n{% endfor %}",
      "type" => "text",
      "toolbar" => [
        "label" => "for",
        "logo" => "Loop",
        "apply" => ["section", "page"],
        "area" => "twig"
      ]
    ],
    "getCurrentSection" => [
      "template" => "section",
      "type" => "text",
      "toolbar" => [
        "label" => "currentSection",
        "logo" => "Archive",
        "apply" => ["page"],
        "area" => "twig"
      ]
    ],
    "getSectionsToolbar" => [
      "template" => "|getSections",
      "type" => "text",
      "toolbar" => [
        "label" => "getSections",
        "logo" => "SubdirectoryArrowRight",
        "apply" => ["page", "section"],
        "area" => "twig"
      ]
    ],
    "getParentPath" => [
      "template" => "section|getParentPath",
      "type" => "filter",
      "name" => "getParentPath",
      "php" => function($context, $section) {
        $path = explode("/", $section->path);
        array_pop($path);
        return implode("/", $path);
      },
      "toolbar" => [
        "label" => "getParentPath",
        "logo" => "DataObject",
        "apply" => ["section"],
        "area" => "twig"
      ]
    ],
    "getHomepage" => [
      "template" => "getHomepage()",
      "type" => "function",
      "name" => "getHomepage",
      "php" => function($context) {
        if (!property_exists($context['page'], "homepage")) return null;
        $homepage = $context["class"]->get_section($context["page"], $context["page"]->homepage);
        if (!property_exists($homepage, "path")) $homepage->path = $context["page"]->homepage;
        return $homepage;
      },
      "toolbar" => [
        "label" => "getHomepage",
        "logo" => "Home",
        "apply" => ["page", "section"],
        "area" => "twig"
      ]
    ],
    "getLinkToolbar" => [
      "template" => "|getLink",
      "type" => "text",
      "toolbar" => [
        "label" => "addLink",
        "logo" => "AddLink",
        "apply" => ["page", "section"],
        "area" => "twig"
      ]
    ],
    "getLanguageLinks" => [
      "template" => "languageLinkList()",
      "type" => "function",
      "name" => "languageLinkList",
      "php" => function($context) {
        if ($context["class"]->include === true) $path = "";
        else if ($context["section"] === null) $path = $context["page"]->id;
        else $path = $context["section"]->path;
        return $context["class"]->create_language_links($path);
      },
      "toolbar" => [
        "label" => "languageLinks",
        "logo" => "Translate",
        "apply" => ["section", "page"],
        "area" => "twig"
      ]
    ],
    "base64Encode" => [
      "template" => "|base64Encode",
      "type" => "filter",
      "name" => "base64Encode",
      "php" => function($context, $string) {
        return base64_encode($string);
      },
      "toolbar" => [
        "label" => "base64Encode",
        "logo" => "VpnKey",
        "apply" => ["section", "page"],
        "area" => "twig"
      ]
    ],
    "getCalendar" => [
      "template" => "getCalendar(start, length, backward, [paths], callback)",
      "type" => "text",
      "toolbar" => [
        "label" => "calendar",
        "logo" => "CalendarTodayIcon",
        "apply" => ["section", "page"],
        "area" => "js"
      ]
    ],
    "getCalendarByDate" => [
      "template" => "getCalendarByDate(begin, end, backward, [paths], callback)",
      "type" => "text",
      "toolbar" => [
        "label" => "calendarByDate",
        "logo" => "CalendarMonth",
        "apply" => ["section", "page"],
        "area" => "js"
      ]
    ],
    "lazySectionLoading" => [
      "template" => "lazySectionLoading('path', schema, custom, start, length, reverse, template, callback)",
      "type" => "text",
      "toolbar" => [
        "label" => "lazySectionLoading",
        "logo" => "DownloadingIcon",
        "apply" => ["section", "page"],
        "area" => "js"
      ]
    ],
    "base64Decode" => [
      "template" => "decryptBase64(encoded)",
      "type" => "text",
      "toolbar" => [
        "label" => "base64Decode",
        "logo" => "VpnKeyOff",
        "apply" => ["section", "page"],
        "area" => "js"
      ]
    ],
    "getloginForm" => [
      "template" => "loginForm()",
      "type" => "function",
      "name" => "loginForm",
      "php" => function($context) {
        $form_index = $context["class"]->form_index;
        $login_form = <<<EOL
          <form id="loginForm-{$form_index}">
            <label for="loginForm-{$form_index}">{$context["class"]->info_texts->login}</label>
            <label for="loginForm-{$form_index}-username">{$context["class"]->info_texts->username}*</label>
            <input id="loginForm-{$form_index}-username" type="text" required />
            <label for="loginForm-{$form_index}-password">{$context["class"]->info_texts->password}*</label>
            <input id="loginForm-{$form_index}-password" type="password" required />
            <button type="submit" id="loginForm-{$form_index}-submit">{$context["class"]->info_texts->submit}</button>
            <div id="loginForm-{$form_index}-status"></div>
          </form>
          <button id="loginForm-{$form_index}-reset" onClick="resetPassword(this)">{$context["class"]->info_texts->resetPassword}</button>
          <script>
            var loginForm{$form_index}Texts = {
              passwordLabelNew: '{$context["class"]->info_texts->passwordLabelNew}',
              passwordLabelNewRepeat: '{$context["class"]->info_texts->passwordLabelNewRepeat}',
              strongPassword: '{$context["class"]->info_texts->strongPassword}',
              mediumPassword: '{$context["class"]->info_texts->mediumPassword}',
              weakPassword: '{$context["class"]->info_texts->weakPassword}',
              error: '{$context["class"]->info_texts->error}',
              loginError: '{$context["class"]->info_texts->loginError}',
              passwordMismatch: '{$context["class"]->info_texts->passwordMismatch}',
              noPassword: '{$context["class"]->info_texts->noPassword}',
              passwordSet: '{$context["class"]->info_texts->passwordSet}',
              passwordReset: '{$context["class"]->info_texts->passwordReset}'
            };
            document.getElementById("loginForm-{$form_index}").addEventListener("submit", submitLoginForm);
          </script>
        EOL;
        $context["class"]->form_index++;
        return $login_form;
      },
      "raw" => true,
      "toolbar" => [
        "label" => "loginForm",
        "logo" => "Login",
        "apply" => ["section", "page"],
        "area" => "twig"
      ]
    ],
    "getLogoutButton" => [
      "template" => "logoutButton()",
      "type" => "function",
      "name" => "logoutButton",
      "php" => function($context) {
        $form_index = $context["class"]->form_index;
        $logout_button = <<<EOL
          <button id="logout-{$form_index}" onClick="logout(this)">{$context["class"]->info_texts->logout}</button>
          <div id="logout-{$form_index}-status"></div>
          <script>
            var logoutButton{$form_index}Texts = {
              error: '{$context["class"]->info_texts->error}'
            };
          </script>
        EOL;
        $context["class"]->form_index++;
        return $logout_button;
      },
      "raw" => true,
      "toolbar" => [
        "label" => "logoutForm",
        "logo" => "Logout",
        "apply" => ["section", "page"],
        "area" => "twig"
      ]
    ],
    "getCustomForms" => [
      "template" => "customForms([[[0]]], '[[[1]]]', '[[[2]]]')",
      "type" => "function",
      "name" => "customForms",
      "php" => function($context, $section, $js_callback, $err_callback) {
        // create dummy form for preview
        if ($context["class"]->include === true) {
          $id = rand(0,1000);
          $idf = $id . "f";
          $ida = $id . "a";
          $idb = $id . "b";
          $width = $context["class"]->get_setting("CAPTCHA_WIDTH");
          $height = $context["class"]->get_setting("CAPTCHA_HEIGHT");
          return <<<EOT
            <div>
              <form id="$idf">
                <label class="form-label" for="$idf">abc</label>
                <label class="input-label" for="$ida">def</label>
                <input id="$ida" type="text" />                
                <label class="input-label" for="$idb">ghi</label>
                <input id="$idb" type="checkbox" />
                <img width="$width" height="$height" class="captcha-img" />
                <button class="captcha-refresh">jkl</button>        
                <input type="submit" value="{$context["class"]->info_texts->submit}" />
              </form>
            </div>
          EOT;
        }
        return "[[[CUSTOM_FORMS $section->path $js_callback $err_callback ]]]";
      },
      "requirePHP" => true,
      "postReplace" => [
        "command" => "CUSTOM_FORMS",
        "php" => function($class, $data, $index) {
          // load forms
          if ($class->forms === null) $class->forms = $class->get_json_from_file(FORMS_PATH);
          if ($class->forms === null) return "";
          // check if exists
          if (!property_exists($class->forms, $data[0])) return "";
          $forms = $class->forms->{$data[0]};
          $html_forms = [];
          foreach ($forms as $form) {
            // check if active
            if ($form->active === true) {
              // create form
              $inputs = [];
              for ($i=0; $i < count($form->inputs); $i++) { 
                $input = $form->inputs[$i];
                $required = "";
                $aria_required = "false";
                if (property_exists($input, "required") && $input->required === true) {
                  $required = "*";
                  $aria_required = "true";
                }                
                if ($input->type === "select") {
                  $options = [];
                  array_push($options, "<option value=''></option>");
                  for ($o=0; $o < count($input->options); $o++) { 
                    array_push($options, sprintf("<option value='%s'>%s</option>", $o, $input->options[$o]->title->{$class->display_language}));
                  }
                  $select = sprintf("<label class='input-label' id='form-%s-%s-%s-label' for='form-%s-%s-%s'>%s%s</label><select name='%s' id='form-%s-%s-%s' aria-required='%s'>%s</select>", $index, $form->id, $i, $index, $form->id, $i, $input->title->{$class->display_language}, $required, $i, $index, $form->id, $i, $aria_required, join("", $options));
                  array_push($inputs, $select);
                }
                else {
                  array_push($inputs, sprintf("<label class='input-label' id='form-%s-%s-%s-label' for='form-%s-%s-%s'>%s%s</label><input type='%s' name='%s' id='form-%s-%s-%s' aria-required='%s' />", $index, $form->id, $i, $index, $form->id, $i, $input->title->{$class->display_language}, $required, $input->type, $i, $index, $form->id, $i, $aria_required));
                }
              }
              $access_start = "";
              $access_end = "";
              if (property_exists($form, "accessRestriction") && $form->accessRestriction === true) {
                $access_start = "<?php if (has_access('" . $data[0] . "')) { ?>";
                $access_end = "<?php } ?>";
              }
              array_push($html_forms, sprintf("%s<div>
              <form id='form-%s-%s' onSubmit='return submitForm(this, \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", %s, %s)'><label class='form-label' form='form-%s-%s'>%s</label>%s%s%s</form></div>%s", 
              $access_start, 
              $index, 
              $form->id, 
              $data[0], 
              $form->id, 
              $form->responseMsg->{$class->display_language}, 
              $class->info_texts->required, 
              $class->info_texts->error, 
              $data[1], 
              $data[2], 
              $index, 
              $form->id, 
              $form->title->{$class->display_language}, 
              implode("", $inputs), 
              sprintf('<img class="captcha-img" aria-hidden="true" src="/captcha.php" id="captcha-%s-%s-img" /><button type="button" class="captcha-audio" onclick="audioCaptcha()">%s</button><button class="captcha-refresh" onclick="refreshCaptcha(\'captcha-%s-%s-img\')" type="button">%s</button><label class="input-label" for="captcha-%s-%s">%s</label><input type="text" id="captcha-%s-%s" name="text_captcha" />',
                $index, 
                $form->id,
                $class->info_texts->audioCaptcha,
                $index, 
                $form->id, 
                $class->info_texts->refreshCaptcha,                
                $index, 
                $form->id, 
                $class->info_texts->captcha, 
                $index, 
                $form->id
              ), 
              sprintf('<input type="submit" value="%s" />', 
              $class->info_texts->submit), 
              $access_end));
            }
          }
          return implode("", $html_forms);
        }
      ]
    ],
    "getSignedIn" => [
      "template" => "[[[SIGNED_IN]]]\n[[[END_SIGNED_IN]]]",
      "type" => "replace",
      "command" => "SIGNED_IN",
      "by" => [
        "start" => "<?php if (signed_in()) { ?>",
        "end" => "<?php } ?>"
      ],
      "toolbar" => [
        "label" => "signedIn",
        "logo" => "LockOpen",
        "apply" => ["section", "page"],
        "area" => "user"
      ]
    ],
    "getNotSignedIn" => [
      "template" => "[[[NOT_SIGNED_IN]]]\n[[[END_NOT_SIGNED_IN]]]",
      "type" => "replace",
      "command" => "NOT_SIGNED_IN",
      "by" => [
        "start" => "<?php if (non_signed_in()) { ?>",
        "end" => "<?php } ?>"
      ],
      "toolbar" => [
        "label" => "notSignedIn",
        "logo" => "NoEncryptionGmailerrorred",
        "apply" => ["section", "page"],
        "area" => "user"
      ]
    ],
    "getPrivateAccess" => [
      "template" => "[[[HAS_ACCESS {{section.path}} ]]]\n[[[END_HAS_ACCESS]]]",
      "type" => "replace",
      "command" => "HAS_ACCESS",
      "by" => [
        "start" => "<?php if (has_access('[[[0]]]')) { ?>",
        "end" => "<?php } ?>"
      ],
      "toolbar" => [
        "label" => "privateAccess",
        "logo" => "Lock",
        "apply" => ["section"],
        "area" => "user"
      ]
    ]
  ];
?>
