<?php
	require_once __DIR__ . DIRECTORY_SEPARATOR . "login.php";
	require_once __DIR__ . DIRECTORY_SEPARATOR . "error.php";
	require_once __DIR__ . DIRECTORY_SEPARATOR . "basic.php";

	class AdminLoginHandler extends LoginHandler {
		public object $languages;
		public string $language;
		private object $labels;

		function __construct() {
			parent::__construct();
			$this->labels = $this->get_system_json(LABELS_PATH);
			$this->languages = $this->get_system_json(EDITOR_LANG_PATH);
			$this->language = $this->languages->defaultLanguage;
    }

		private function check_root_user(): bool {
			// check if root user set
			if ($this->users === null || !property_exists($this->users, "root")) return false;
			return true;
		}

		private function get_current_url() {
			$url =  isset($_SERVER['HTTPS']) &&
    	$_SERVER['HTTPS'] === 'on' ? "https://" : "http://";  
			$url .=  $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			return $url;
		}

		private function get_language_links() {
			$links = [];
			foreach ($this->languages->languages as $language) {
				array_push($links, sprintf("<li><a href='?lang=%s'>%s</li>", $language->iso639, $language->name));
			}
			return join("", $links);
		}

		private function create_password_subform() {
			$pw_strength_js = file_get_contents(PW_STRENGTH_JS_PATH);
			return <<<EOL
				<label for="password">{$this->labels->password->{$this->language}}</label>
				<input type="password" name="password" id="password" />
				<div id="strength-badge"></div>
				<label for="passwordRepeat">{$this->labels->passwordLabelNewRepeat->{$this->language}}</label>
				<input type="password" name="passwordRepeat" id="passwordRepeat" />
				<script>
					{$pw_strength_js}
					var texts = {
						strongPassword: '{$this->labels->strongPassword->{$this->language}}',
						mediumPassword: '{$this->labels->mediumPassword->{$this->language}}',
						weakPassword: '{$this->labels->weakPassword->{$this->language}}'
					};
					window.onload = () => {
						document.getElementById("password").addEventListener("input", (e) => {
							checkPasswordStrength(e.target.value, "strength-badge", "texts");
						});
					};					
				</script>
			EOL;
		}

		private function create_root_user_form(string $error=""): string {
			$links = $this->get_language_links();
			$password_form = $this->create_password_subform();
			$username = (isset($_POST["usernameRoot"]) ? $_POST["usernameRoot"] : null);
			$email = (isset($_POST["email"]) ? $_POST["email"] : null);
			$public_dir = (isset($_POST["publicDir"]) ? $_POST["publicDir"] : null);
			return <<<EOL
				<img id="logo" src="logo_main_blue.png" alt="{$this->labels->mainTitle->{$this->language}}" />
				<form id="root-form" method="post" action="{$this->get_current_url()}">
					<label id="form-label" for="root-form">{$this->labels->rootUserForm->{$this->language}}</label>
					<div id="status">{$error}</div>
					<label for="usernameRoot">{$this->labels->username->{$this->language}}</label>
					<input type="text" name="usernameRoot" id="usernameRoot" value="{$username}" />
					{$password_form}
					<label for="address">{$this->labels->email->{$this->language}}</label>
					<input type="email" name="email" id="email" value="{$email}" />
					<label for="publicDir">{$this->labels->publicDir->{$this->language}}</label>
					<input type="text" name="publicDir" id="publicDir" value="{$public_dir}" />
					<input type="hidden" name="request" value="rootUser" />
					<button type="submit">{$this->labels->submit->{$this->language}}</button>
				</form>
				<ul>
					{$links}
				</ul>
			EOL;
		}

		private function create_login_form(string $error="") {
			$links = $this->get_language_links();
			$username = (isset($_POST["username"]) ? $_POST["username"] : "");
			return <<<EOL
				<img id="logo" src="logo_main.png" alt="{$this->labels->mainTitle->{$this->language}}" />
				<form id="root-form" method="post" action="{$this->get_current_url()}">
					<label id="form-label" for="root-form">{$this->labels->login->{$this->language}}</label>
					<div id="status">{$error}</div>
					<label for="username">{$this->labels->username->{$this->language}}</label>
					<input type="text" name="username" id="username" value="{$username}" />
					<label for="password">{$this->labels->passwordPin->{$this->language}}</label>
					<input type="password" name="password" id="password" />
					<input type="hidden" name="request" value="login" />
					<button type="submit">{$this->labels->submit->{$this->language}}</button>
				</form>
				<form id="root-form" method="post" action="{$this->get_current_url()}">
					<input type="hidden" name="request" value="resetPassword" />
					<button type="submit" name="reset" value="reset">{$this->labels->resetPassword->{$this->language}}</button>
				</form>
				<ul>
					{$links}
				</ul>
			EOL;
		}

		private function create_reset_password_form($language, string $error="") {
			$back_url = explode("?", $this->get_current_url())[0] . "?lang=" . $language;
			return <<<EOL
				<img id="logo" src="logo_main_blue.png" alt="{$this->labels->mainTitle->{$language}}" />
				<form id="root-form" method="post" action="{$this->get_current_url()}">
					<label id="form-label" for="root-form">{$this->labels->resetPassword->{$language}}</label>
					<div id="status">{$error}</div>
					<label for="username">{$this->labels->username->{$language}}</label>
					<input type="text" name="username" id="username" />
					<button type="submit">{$this->labels->submit->{$language}}</button>
					<input type="hidden" name="request" value="confirmResetPassword" />
					<a href="{$back_url}">{$this->labels->back->{$language}}</a>
				</form>
			EOL;
		}

		private function create_new_password_form($language, $username, $pin, string $error=""): string {
			$back_url = explode("?", $this->get_current_url())[0] . "?lang=" . $language;
			return <<<EOL
				<img id="logo" src="logo_main_blue.png" alt="{$this->labels->mainTitle->{$this->language}}" />
				<form id="new-form" method="post" action="{$this->get_current_url()}">
					<label id="form-label" for="new-form">{$this->labels->passwordLabelNew->{$this->language}}</label>
					<div id="status">{$error}</div>
					<label for="username">{$this->labels->username->{$this->language}}</label>
					<input type="text" id="username" value="{$username}" disabled />
					{$this->create_password_subform()}
					<input type="hidden" name="username" value="{$username}" />
					<input type="hidden" name="pin" value="{$pin}" />
					<input type="hidden" name="request" value="createNewPassword" />
					<button type="submit">{$this->labels->submit->{$this->language}}</button>
					<a href="{$back_url}">{$this->labels->back->{$language}}</a>
				</form>
			EOL;
		}

		private function set_root_user($username, $password, $email) {
			// send as email
			$url = $this->get_admin_url();
			$this->send_html_mail($email, $this->labels->rootUser->{$this->language}, <<<EOL
				<table>
					<tr>
						<th>{$this->labels->username->{$this->language}}</th>
						<th>{$this->labels->password->{$this->language}}</th>
					</tr>
					<tr>
						<td>{$username}</td>
						<td>{$password}</td>
					</tr>
				</table>
				<a href="{$url}">{$url}</a>
			EOL);
			// save to users
			$this->users->root = (object) [
				"username" => $username,
				"fullName" => $username,
				"email" => $email,
        		"privilege" => "a",
				"password" => md5($password)
			];
			$this->safe_save(json_encode($this->users, JSON_PRETTY_PRINT), USERS_PATH);
			// save public directory
			$this->set_setting("PUBLIC_DIR", $_POST["publicDir"]);
		}

		private function check_root_user_request() {
			// check username
			if (!isset($_POST["usernameRoot"])) return INVALID_USERNAME_ERR;
			$username = $_POST["usernameRoot"];
			if (strlen($username) > $this->get_setting("USERNAME_MAX_LENGTH")) return USERNAME_TOO_LONG_ERR;
			if (strlen($username) < 1 || preg_match("/^[a-zA-Z0-9_]+$/", $username) === 0) return INVALID_USERNAME_ERR;
			// check password
			if (!isset($_POST["password"])) return NO_PASSWORD_ERR;
			$password = $_POST["password"];
			if (strlen($password) < 1) return NO_PASSWORD_ERR;
			// check if match
			if ($password !== $_POST["passwordRepeat"]) return PASSWORD_MISMATCH_ERR;
			// check email
			if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) return INVALID_ADDRESS_ERR;
			$this->set_root_user($username, $password, $_POST["email"]);
			// check public directory
			if (!isset($_POST["publicDir"]) || strlen($_POST["publicDir"]) < 1) return NO_PUBLIC_DIR_ERR;
			return true;
		}

		private function check_new_password_request() {
			$status = $this->set_new_password($_POST["username"], $_POST["pin"], $_POST["password"], $_POST["passwordRepeat"]);
			if ($status !== null) {
				switch ($status) {
					case "INVALID_CREDENTIALS":
					default:
						$error = $this->labels->loginError->{$this->language};
						break;
					case "PASSWORD_MISMATCH":
						$error = $this->labels->passwordMismatch->{$this->language};
						break;
					case "NO_PASSWORD":
						$error = $this->labels->noPassword->{$this->language};
						break;
				}
				return $error;
			}
			return true;
		}

		public function check_user_request() {
			// check if language changed
			if (isset($_GET["lang"]) && in_array($_GET["lang"], array_column($this->languages->languages, "iso639"))) {
				$this->language = $_GET["lang"];
			}
			// check post requests
			if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST["request"])) {
				// check root user request
				if ($_POST["request"] === "rootUser") {
					$status = $this->check_root_user_request();
					if ($status !== true) return $this->create_root_user_form($this->labels->{$status}->{$this->language});
					else return $this->labels->passwordReload->{$this->language};
				}
				// check login request
				else if ($_POST["request"] === "login") {
					$status = $this->password_login($_POST["username"], $_POST["password"]);
					if ($status === 1) return null;
					else if ($status === 0) return $this->create_login_form($this->labels->loginError->{$this->language});
					else return $this->create_new_password_form($this->language, $_POST["username"], $_POST["password"]);
				}
				// check reset password request
				else if ($_POST["request"] === "resetPassword") {
					return $this->create_reset_password_form($this->language);
				}
				// check user reset password request
				else if ($_POST["request"] === "confirmResetPassword") {
					// check if user exists
					if (!isset($_POST["username"])) $status = false;
					else {
						$username = $_POST["username"];
						$status = $this->reset_password($username);
					}
					if ($status === false) return $this->create_reset_password_form($this->language, $this->labels->NO_SUCH_USER->{$this->language});
					else {
						$this->reset_password($username);
						$user = $this->check_user($username);
						$email = explode("@", $user->email);
						$email = substr($email[0], 0, 1) . implode("", array_fill(0, strlen($email[0]) - 1, '*')) . "@" . $email[1];
						return str_replace("{{{0}}}", $email, $this->labels->pinSent->{$this->language});
					}
				}
				else if ($_POST["request"] === "createNewPassword") {
					$status = $this->check_new_password_request();
					if ($status !== true) return $this->create_new_password_form($this->language, $_POST["username"], $_POST["password"], $status);
					else return $this->labels->passwordReload->{$this->language};
				}
				else $this->create_login_form();
			}
			else if (!$this->check_root_user()) {
				// create root user form, then send pin
				return $this->create_root_user_form();
			}
			else if (!$this->is_admin()) {
				return $this->create_login_form();
			}
			return null;
		}
	}

	$admin_handler = new AdminLoginHandler();
	$html = $admin_handler->check_user_request();
	if ($html !== null) {
		?>
		<!DOCTYPE html>
		<html lang="<?php echo $admin_handler->language; ?>">
			<head>
				<link rel="stylesheet" href="style.css">
			</head>
			<body>
				<section>
					<?php
						echo $html;
					?>
				</section>
			</body>
		</html>		
		<?php
		exit();
	}
?>