<?php
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'basic.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'twigExtensions.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'login.php';
  require_once __DIR__ . '/vendor/autoload.php';
  use JSONSchemaFaker\Faker;

  define("CLIENT_REQUEST_PATH", __DIR__ . DIRECTORY_SEPARATOR . "clientRequests.php");
  define("AJAX_PATH", __DIR__ . DIRECTORY_SEPARATOR . "ajax.php");
  define("CAPTCHA_PATH", __DIR__ . DIRECTORY_SEPARATOR . "captcha.php");
  define("AUDIO_CAPTCHA_PATH", __DIR__ . DIRECTORY_SEPARATOR . "audioCaptcha.php");
  define("FILES_PHP_PATH", __DIR__ . DIRECTORY_SEPARATOR . "files.php");

  class Template_Handler extends Basic_Handler {
    public ?string $display_language = null;
    public ?object $languages = null;
    private ?object $page = null;
    private ?object $page_template = null;
    private ?string $section_template = null;
    public ?object $contents_uploaded = null;
    public ?object $css_uploaded = null;
    public ?object $js_uploaded = null;
    public ?object $fonts_uploaded = null;
    public ?object $template_files_uploaded = null;
    public ?object $text_snippets_uploaded = null;
    public ?object $template_snippets_uploaded = null;
    public $server_path = false;
    public $client_path = false;
    public bool $include = false;
    public ?string $ref = null;
    public ?array $path_list = null;
    public ?object $info_texts = null;
    public ?object $forms = null;
    private $client_code_created = false;
    public int $form_index = 0;
    public int $build = 0;
    public ?array $file_downloads = null;

    private function add_extensions(object &$twig): void {
      global $twig_extensions;
      foreach ($twig_extensions as $extension) {
        $options = ['needs_context' => true];
        if ((array_key_exists("raw", $extension) && $extension["raw"] === true) || array_key_exists("postReplace", $extension)) $options["is_safe"] = array('html');
        if ($extension["type"] === "filter") $twig->addFilter(new \Twig\TwigFilter($extension["name"], $extension["php"], $options));
        if ($extension["type"] === "function") $twig->addFunction(new \Twig\TwigFunction($extension["name"], $extension["php"], $options));
      }
    }

    private function get_snippet_includes(): array {
      $template_snippets = [];
      if ($this->template_snippets_uploaded !== null) {
        foreach ($this->template_snippets_uploaded as $name => $snippet) {
          $template_snippets[$name] = $snippet;
        }
      }
      $snippet_list = $this->get_json_from_file(SNIPPETS_LIST_PATH);
      if ($snippet_list !== null) {
        foreach ($snippet_list as $name => $filename) {
          if (!array_key_exists($name, $template_snippets)) $template_snippets[$name] = file_get_contents(SNIPPETS_DIR_PATH . $filename);
        }
      }
      $includes = [];
      foreach ($template_snippets as $name => $snippet) {
        $includes[$name . "_snippet.template"] = $snippet;
      }
      return $includes;
    }

    /**
     * @param string $head_template=null the HTML head
     * @param string $body_template=null the page HTML body
     * @param string $section_template=null the section HTML
     * 
     * @return object the twig object created
     */
    private function create_twig(string $head_template=null, string $body_template=null, string $section_template=null): object {
      // set templates to empty string if null
      if ($head_template === null) $head_template = "";
      if ($body_template === null) $body_template = "";
      if ($section_template === null) $section_template = "";
      // load templates into twig
      $array_loader = new \Twig\Loader\ArrayLoader(array_merge([
        'head.template' => $head_template,
        'body.template'  => $body_template,
        'section.template' => $section_template
      ], $this->get_snippet_includes()));
      $file_loader = new \Twig\Loader\FilesystemLoader(BASIC_TEMPLATES_PATH);
      $loader = new \Twig\Loader\ChainLoader([$file_loader, $array_loader]);
      $twig = new \Twig\Environment($loader);
      // add extensions
      $this->add_extensions($twig);
      // add client code
      $twig->addFunction(new \Twig\TwigFunction("getClientCode", function() {
        return $this->get_client_code();
      }));
      return $twig;
    }

    /**
     * Used for previews. Sets data from client request if given, from file otherwise
     * 
     * @param object $request the client request holding the data
     * 
     * @return void
     */
    public function set_data(object $request): void {
      // set languages
      if (property_exists($request, "languages")) $this->languages = $request->languages;
      // set page
      if (property_exists($request, "page")) {
        $this->page = $request->page;
        // if page is set, set template
        if (property_exists($request, "template") && $request->template !== null && property_exists($request, "ref") && property_exists($request, "rel")) {
          if ($request->ref === "path" && $request->rel === strval($this->page->id)) $this->page_template = $request->template;
          else if (property_exists($request->template, "body")) $this->section_template = $request->template->body;
        }
      }
      // set contents
      if (property_exists($request, "contents")) $this->contents_uploaded = $request->contents;
      // set css
      if (property_exists($request, "css")) $this->css_uploaded = $request->css;
      // set js
      if (property_exists($request, "js")) $this->js_uploaded = $request->js;
      // set fonts
      if (property_exists($request, "fonts")) $this->fonts_uploaded = $request->fonts;
      // set template files
      if (property_exists($request, "templateFiles")) $this->template_files_uploaded = $request->templateFiles;
      // set text snippets
      if (property_exists($request, "textSnippets")) $this->text_snippets_uploaded = $request->textSnippets;
      // set template snippets
      if (property_exists($request, "textSnippets")) $this->template_snippets_uploaded = $request->snippets;
    }

    private function check_text_snippet($text_snippets, $name) {
      if ($text_snippets !== null && property_exists($text_snippets, $name) && property_exists($text_snippets->{$name}, "text") && property_exists($text_snippets->{$name}->text, $this->display_language)) {
        $text = $text_snippets->{$name}->text->{$this->display_language};
        if ($text_snippets->{$name}->type === "singleLine") return htmlspecialchars($text);
        else return $text;
      }
      return null;
    }

    public function get_text_snippet(string $name): ?string {
      $text_snippet = $this->check_text_snippet($this->text_snippets_uploaded, $name);
      if ($text_snippet === null && file_exists(TEXT_SNIPPETS_PATH)) $text_snippet = $this->check_text_snippet($this->get_json_from_file(TEXT_SNIPPETS_PATH), $name);
      return $text_snippet;
    }

    /**
     * Get a list of data files
     * 
     * @param string $list_path he path to the list
     * @param string $dir_path=null the path to the file directory
     * @param object|null $uploaded=null data upload from client
     * 
     * @return array a list of the file data
     */
    private function get_data_list(string $list_path, string $dir_path=null, ?object $uploaded=null): array {
      // get the file list saved on the server or an empty array
      $saved_list = $this->get_json_from_file($list_path);
      if ($saved_list === null) $saved_list = [];
      // get the list of uploaded data or an empty array if not exists
      $upload_list = ($uploaded !== null && property_exists($uploaded, "list") ? $uploaded->list : []);
      // prepare data and removed array
      $data_arr = [];
      $removed = [];
      // if no dir path is set, use id parameter, otherwise name parameter
      if ($dir_path === null) $parameter = "id";
      else $parameter = "name";
      // get uploaded data
      foreach ($upload_list as $new) {
        if (property_exists($new, "remove") && $new->remove === true) array_push($removed, $new->{$parameter});
        else if ($dir_path !== null) {
          if (property_exists($new, "data") && $new->data !== null) $data_arr[$new->{$parameter}] = $new->data;
        }
        else array_push($data_arr, $new->{$parameter});
      }
      // get saved data if not uploaded
      foreach ($saved_list as $old) {
        if (!in_array($old->{$parameter}, $removed)) {
          if ($dir_path !== null) {
            $contains = array_key_exists($old->{$parameter}, $data_arr);
            if (!$contains) $data_arr[$old->{$parameter}] = file_get_contents($dir_path . $old->filename);
          }
          else {
            $key = false;
            for ($i=0; $i < count($data_arr); $i++) {
              if ($data_arr[$i] === $old->{$parameter}) {
                $key = $i;
                break;
              }
            }
            if ($key !== false) $data_arr[$key] = (object) ["name" => $old->name, "filename" => $old->filename];
            else array_push($data_arr, (object) ["name" => $old->name, "filename" => $old->filename]);
          }
        }
      }
      return $data_arr;
    }

    public function get_dummy_file($mime_types) {
      $is_image = false;
      foreach ($mime_types as $mime_type) {
        if (strpos($mime_type, "image") !== false) {
          $is_image = true;
          break;
        } 
      }
      if ($is_image === false) return "";
      $server_path = $this->server_path . DIRECTORY_SEPARATOR . DUMMY_DIR_PATH;
      if (!file_exists($server_path)) mkdir($server_path);
      if (!file_exists($server_path . DIRECTORY_SEPARATOR . DUMMY_IMAGE_NAME)) copy(EDITOR_PATH . DUMMY_IMAGE_NAME, $server_path . DIRECTORY_SEPARATOR . DUMMY_IMAGE_NAME);
      return $this->client_path . DIRECTORY_SEPARATOR . DUMMY_DIR_PATH . DIRECTORY_SEPARATOR . DUMMY_IMAGE_NAME;
    }

    private function create_basic_named_link($path) {
      // return dummy link for preview
      if ($this->include === true) return "#";
      // assemble link
      $name_path = null;
      foreach ($this->path_list as $list_path) {
        if ($list_path["id"] === $path) $name_path = $list_path["name"];
      }
      if ($name_path === null) $name_path = "";
      else $name_path = "/" . join("/", $name_path);
      return $name_path;
    }

    public function create_language_links($path) {
      $link = $this->create_basic_named_link($path);
      $language_links = [];
      foreach ($this->languages->languages as $language) {
        array_push($language_links, ["iso639" => $language->iso639, "name" => $language->name, "link" => $link . "/" . $language->iso639]);
      }
      return $language_links;
    }

    public function create_named_link($path) {
      return $this->create_basic_named_link($path) . "/" . $this->display_language;
    }

    public function get_file_type($section, $collection) {
      $schema = $this->get_section_schema($section->type, $section->custom);
      if ($schema === null) return null;
      if (!property_exists($schema, "files") || !property_exists($schema->files, $collection)) return null;
        $file_desc = $schema->files->{$collection};
        if (!property_exists($file_desc, "mimeTypes")) return null;
        return $file_desc->mimeTypes;
    }

    public function get_content_files($path, $collection, $file_id=null) {
      $files = $this->get_collection_files($path, $collection, $file_id);
      if ($files === null) return null;
      return $this->set_files("f", FILES_DIR_PATH, $this->client_path, $this->server_path, $files);
    }

    private function get_captions($files, $file_links) {
      $files_captions = [];
      for ($i=0; $i < count($files); $i++) {
        if (property_exists($files[$i]->caption, $this->display_language) && $files[$i]->caption->{$this->display_language} !== "") $caption = $files[$i]->caption->{$this->display_language};
        else $caption = $files[$i]->caption->{$this->languages->defaultLanguage};
        array_push($files_captions, [
          "src" => $file_links[$i],
          "caption" => $caption
        ]);
      }
      return $files_captions;
    }

    public function get_content_file_links_captions($path, $collection, $check_access) {
      $files = $this->get_collection_files($path, $collection, null);
      if ($files === null) return null;
      $file_hashes = [];
      // loop files
      foreach ($files as $file) {
        $existing = array_values(array_filter($this->file_downloads, function($file_download) use($path, $file) {
          return $file_download["path"] === $path && $file_download["fileId"] === $file->id;
        }));
        if (count($existing) > 0) $id = $existing[0]["id"];
        else {
          $id = $this->generate_rand_id(array_column($this->file_downloads, "id"));
          array_push($this->file_downloads, [
            "path" => $path,
            "collection" => $collection,
            "fileId" => $file->id,
            "checkAccess" => $check_access,
            "id" => $id
          ]);
        }
        array_push($file_hashes, "/files.php?f=" . $id);
      }
      $files_captions = $this->get_captions($files, $file_hashes);
      return $files_captions;
    }

    public function get_content_files_captions($path, $collection) {
      $files = $this->get_collection_files($path, $collection, null);
      if ($files === null) return null;
      $files_set = $this->set_files("f", FILES_DIR_PATH, $this->client_path, $this->server_path, $files);
      $files_captions = $this->get_captions($files, $files_set);
      return $files_captions;
    }

    public function get_caption($path, $collection, $file_id) {
      $files = $this->get_collection_files($path, $collection, $file_id);
      if ($files === null) return null;
      return $files[0]->caption->{$this->display_language};
    }

    public function get_template_file($id) {
      $template_file_list = $this->get_json_from_file(TEMPLATE_FILES_LIST_PATH);
      if ($template_file_list === null) return null;
      $file = array_values(array_filter($template_file_list, function($item) use($id) {
        return $item->id === $id;
      }));
      if (count($file) === 0) return null;
      $template_file = $this->set_files("tf", TEMPLATE_FILES_DIR_PATH, $this->client_path, $this->server_path, $file);
      if (count($template_file) > 0) return $template_file[0];
      return null;
    }

    /**
     * Replace links given in code files with actual file links.
     * 
     * @param mixed $css_data the code data string
     * 
     * @return string the string with replaced dĺinks
     */
    private function replace_code_links(string $css_data): string {
      preg_match_all('/(\{\{[^\}]+\}\}|\|\|\|[^\|]+\|\|\|)/', $css_data, $matches, PREG_OFFSET_CAPTURE);
      if (count($matches) > 1 && count($matches[0])) {
        for ($i=count($matches[0]) - 1; $i >= 0; $i--) {
          $path = explode(" ", str_replace(["{{", "}}", "|||"], "", $matches[0][$i][0]));
          $length = strlen($matches[0][$i][0]);
          $offset = $matches[0][$i][1];
          if ($path[0] === "section") {
            $content_files = $this->get_content_files($path[1], $path[2]);
            if ($content_files === null) $matched_content_file = '';
            else $matched_content_file = $content_files[0];
            $css_data = substr_replace($css_data, $matched_content_file, $offset, $length);
          }
          else if ($path[0] === "template") {
            $template_file = $this->get_template_file(intval($path[1]));
            if ($template_file === null) $matched_template_file = '';
            else $matched_template_file = $template_file;
            $css_data = substr_replace($css_data, $matched_template_file, $offset, $length);
          }
        }
      }
      return $css_data;
    }

    private function get_data_filename($filenames, $filename) {
      $file = explode(".", $filename);
      $additional = "";
      while (in_array($file[0] . strval($additional), $filenames)) {
        if ($file[0] === "") $additional = 0;
        else $additional++;
      }
      return $file[0] . $additional . "." . $file[1];
    }

    private function get_client_data_files($list_path, $default_path, $dir_path, $uploaded, $replace_func=null) {
      // get list
      $list = $this->get_data_list($list_path, $dir_path, $uploaded, $default_path);
      $filenames = [];
      if ($list !== null) {
        foreach ($list as $filename => $file) {
          if ($replace_func !== null) $file = $this->{$replace_func}($file);         
          $this->safe_save($file, $this->server_path . DIRECTORY_SEPARATOR . $filename);
          array_push($filenames, $filename);
        }
      }
      // get default
      $default = ($uploaded !== null && property_exists($uploaded, "default") ? $uploaded->default : (file_exists($default_path) ? file_get_contents($default_path) : null));
      if ($default !== null) {
        if ($replace_func !== null) $default = $this->{$replace_func}($default);
        $split_path = explode(DIRECTORY_SEPARATOR, $default_path);
        $filename = end($split_path);
        $new_filename = $this->get_data_filename($filenames, $filename);
        $this->safe_save($default, $this->server_path . DIRECTORY_SEPARATOR . $new_filename);
        array_push($filenames, $new_filename);
      }
      return $filenames;
    }

    private function get_client_code(): string {
      // check if has been created
      if ($this->client_code_created !== false) {
        return $this->client_code_created;
      }
      // change client code created to array
      $this->client_code_created = [];
      // get css
      $this->client_code_created["css"] = $this->get_client_data_files(CSS_LIST_PATH, CSS_PATH, CSS_DIR_PATH, $this->css_uploaded, "replace_code_links");
      // get js
      $this->client_code_created["js"] = $this->get_client_data_files(JS_LIST_PATH, DEFAULT_JS_PATH, JS_DIR_PATH, $this->js_uploaded, "replace_code_links");
      // get predefined js
      $filename = $this->get_data_filename($this->client_code_created["js"], "predefined.js");
      copy(PREDEFINED_JS_PATH, $this->server_path . DIRECTORY_SEPARATOR. $filename);
      array_push($this->client_code_created["js"], $filename);
      $filename = $this->get_data_filename($this->client_code_created["js"], "pwStrength.js");
      copy(PW_STRENGTH_JS_PATH, $this->server_path . DIRECTORY_SEPARATOR. $filename);
      array_push($this->client_code_created["js"], $filename);
      // get fonts
      $fonts = $this->get_data_list(FONT_LIST_PATH, null, $this->fonts_uploaded);
      if ($fonts !== null) {
        $font_files = [];
        mkdir($this->server_path . DIRECTORY_SEPARATOR . "fonts");
        foreach ($fonts as $font) {
          copy(FONT_DIR_PATH . $font->filename, $this->server_path . DIRECTORY_SEPARATOR . "fonts" . DIRECTORY_SEPARATOR . $font->name);
          array_push($font_files, sprintf("@font-face {font-family: '%s'; src: url(%s/%s);}", $font->name, $this->client_path . DIRECTORY_SEPARATOR . "fonts", $font->name));
        }
        $filename = $this->get_data_filename($this->client_code_created["css"], "fonts.css");
        $this->safe_save(implode("\n", $font_files), $this->server_path . DIRECTORY_SEPARATOR. $filename);
        array_push($this->client_code_created["css"], $filename);
      }
      // get client HTML code
      $client_code = "";
      foreach ($this->client_code_created["css"] as $css) {
        $client_code .= sprintf("<link rel=\"stylesheet\" href=\"%s/%s?v=%d\">\n", $this->client_path, $css, $this->build);
      }
      foreach ($this->client_code_created["js"] as $js) {
        $client_code .= sprintf("<script src=\"%s/%s?v=%d\"></script>\n", $this->client_path, $js, $this->build);
      }
      $this->client_code_created = $client_code;
      return $client_code;
    }

    private function rrmdir($dir) {
     if (is_dir($dir)) {
       $objects = scandir($dir);
       foreach ($objects as $object) {
         if ($object != "." && $object != "..") {
           if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir . DIRECTORY_SEPARATOR . $object))
             $this->rrmdir($dir. DIRECTORY_SEPARATOR .$object);
           else
             unlink($dir. DIRECTORY_SEPARATOR .$object);
         }
       }
       rmdir($dir);
     }
    }

    private function set_data_paths($set_files = true) {
      $data_dir = DATA_DIR_PATH;
      if ($this->page !== null && $this->page->name === $data_dir) $data_dir . "_d";
      if ($this->include === true) {
        $login_handler = new LoginHandler();
        $username = $login_handler->get_user();
        if ($username === null) exit_error(400, INVALID_REQUEST_ERR);
        $preview_path_full = $this->output_path . ADMIN_DIR_NAME . DIRECTORY_SEPARATOR  . "preview";
        $preview_path = $preview_path_full . DIRECTORY_SEPARATOR . $username;
        $preview_dir_path = PREVIEW_DIR_PATH . "/" . $username;
        if (!file_exists($preview_path_full)) mkdir($preview_path_full);
        if ($set_files) {          
          $this->rrmdir($preview_path);
          mkdir($preview_path);
        }
        $server_path = $preview_path;
        $client_path = $preview_dir_path;
        $this->client_path = "/" . $client_path . "/" . $data_dir;
      }
      else {
        if ($set_files) {
          // remove everything except admin
          $files = array_diff(scandir($this->output_path), array('.', '..'));
          foreach ($files as $filename) {
            if ($filename !== ADMIN_DIR_NAME) $this->safe_remove_dir($this->output_path . $filename);
          }
          $this->flush_files();
        }
        $server_path = $this->output_path;
        $this->client_path = "/" . $data_dir;
      }
      $this->server_path = $server_path . DIRECTORY_SEPARATOR . $data_dir;
      if (!file_exists($this->server_path)) mkdir($server_path . DIRECTORY_SEPARATOR . $data_dir);      
    }

    public function get_date_constant($constant) {
      switch ($constant) {
        case 'FULL':
        default:
          return IntlDateFormatter::FULL;
        case 'LONG':
          return IntlDateFormatter::LONG;
        case 'MEDIUM':
          return IntlDateFormatter::MEDIUM;
        case 'SHORT':
          return IntlDateFormatter::SHORT;
      }
    }

    private function create_section_schema($schema, $languages, $display_language) {
      $basic_page_schema = $this->get_system_json(BASIC_PAGE_SCHEMA_PATH);
      $section_schema = json_decode(json_encode($basic_page_schema->basicSchema));
      array_push($section_schema->required, "data");
      if ($schema !== null) $section_schema->properties->data = $schema;
      $this->add_lang_input($section_schema, $languages, $display_language);
      return $section_schema;
    }

    private function throw_template_exception(string $msg) {
      throw new Exception($msg);
    }

    private function set_template_data() {
      // get languages
      if ($this->languages === null) $this->languages = $this->get_json_from_file(LANG_FILE_PATH);
      if ($this->languages === null) $this->throw_template_exception(MISSING_LANGUAGE_DATA_ERR);
      // get display language
      if ($this->display_language === null) $this->display_language = $this->languages->defaultLanguage;
      if ($this->display_language === null) $this->throw_template_exception(MISSING_LANGUAGE_DATA_ERR);
      // get page
      if ($this->page === null) $this->page = $this->get_json_from_file(PAGE_FILE_PATH);
      if ($this->page === null) $this->throw_template_exception(MISSING_PAGE_ERR);
      // get page template
      if ($this->page_template === null) $this->page_template = $this->get_template("path", strval($this->page->id));
      if ($this->page_template === null) $this->page_template = $this->create_dummy_template(true);
      if (!property_exists($this->page_template, "head")) $this->page_template->head = "";
    }

    private function create_section($ref, $rel) {
      $section = null;
      if ($ref === 'path') {
       if ($rel !== strval($this->page->id)) { // prevent infinite loops
         $section = $this->get_section($this->page, $rel);
         if ($section === null) $this->throw_template_exception(MISSING_SECTION_ERR);
         $section->path = $rel;
       }
       else $this->section_template = $this->get_setting("LOREM_IPSUM");
      }
      else if ($ref === 'type') { // create dummy section json
        $section_schema = $this->get_section_schema($rel->name, $rel->custom);
        if ($section_schema === null) $this->throw_template_exception(INVALID_REQUEST_ERR);
        $schema = null;
        if (property_exists($section_schema, "schema")) $schema = $section_schema->schema;
        $section_schema = $this->create_section_schema($schema, $this->languages, $this->display_language);
        $section = (new Faker)->generate($section_schema);
        $section->type = $rel->name;
        $section->custom = $rel->custom;
      }
      return $section;
    }

    private function create_dummy_template(bool $page=false): object {
      if ($page) $section = "page";
      else $section = "section";
      return (object) [
        "body" => sprintf("<h1>{{%s.title|getText}}</h1>", $section)
      ];
    }

    private function get_section_template($ref, $rel) {
      $template = $this->get_template($ref, $rel);
      // create dummy template if not exists yet
      if ($template === null || !property_exists($template, "body")) {
        $template = $this->create_dummy_template();
      }
      else $this->section_template = $template->body;
      return $template->body;
    }

    private function render_template($twig, $section) {
      try {
       return $twig->render('basic_template.template', [
         'lang' => $this->display_language,
         'section' => $section,
         'page' => $this->page,
         'class' => $this
       ]);
      } catch (\Throwable $e) {
        $this->throw_template_exception($e->getMessage());
      }
    }

    private function remove_replace($template) {
      global $twig_extensions;
      foreach ($twig_extensions as $name => $extension) {
        if ($extension["type"] === "replace") {
          $template = str_replace(["[[[" . $extension["command"] . "]]]", "[[[END_" . $extension["command"] . "]]]"], "", $template);
          preg_match("/\[\[\[" . $extension["command"] . " (?:[^ ]+ )+\]\]\]/", $template, $matches, PREG_OFFSET_CAPTURE);
          if (count($matches) > 0) {
            $template = substr_replace($template, "", $matches[0][1], strlen($matches[0][0]));
          }
        }
      }
      return $template;
    }

    private function apply_replace(&$template, $section) {
      global $twig_extensions;
      $has_replace = false;
      foreach ($twig_extensions as $name => $extension) {
        if ($extension["type"] === "replace") {
          if (strpos($template, "[[[" . $extension["command"] . "]]]") !== false) {
            $template = str_replace(["[[[" . $extension["command"] . "]]]", "[[[END_" . $extension["command"] . "]]]"], [$extension["by"]["start"], $extension["by"]["end"]], $template);
            $has_replace = true;
          }
          else {
            // get all in rendered
            preg_match("/\[\[\[" . $extension["command"] . " (?:[^ ]+ )+\]\]\]/", $template, $matches, PREG_OFFSET_CAPTURE);
            if (count($matches) > 0) {
              $start = $extension["by"]["start"];
              $parts = explode(" ", $matches[0][0]);
              $values = array_slice($parts, 1, count($parts) - 2);
              for ($i=0; $i < count($values); $i++) { 
                $start = str_replace("[[[" . $i . "]]]", $values[$i], $start);
              }
              $template = substr_replace($template, $start, $matches[0][1], strlen($matches[0][0]));
              $template = str_replace("[[[END_" . $extension["command"] . "]]]", $extension["by"]["end"], $template);
              $has_replace = true;
            }
          }
        }
      }
      return $has_replace;
    }

    private function apply_post_replace(&$rendered) {
      global $twig_extensions;
      $has_replace = false;
      $replaces = [];
      foreach ($twig_extensions as $name => $extension) {
        if (array_key_exists("postReplace", $extension)) {
          // get all in rendered
          preg_match_all("/\[\[\[" . $extension["postReplace"]["command"] . " (?:[^ ]+ )+\]\]\]/", $rendered, $matches, PREG_OFFSET_CAPTURE);
          // check if found
          if (count($matches) > 0 && count($matches[0]) > 0) {
            // set replace true
            $has_replace = (array_key_exists("requirePHP", $extension) && $extension["requirePHP"] === true);
            // loop matches
            foreach ($matches[0] as $match) {
              $length = strlen($match[0]);
              $offset = $match[1];
              $parts = explode(" ", $match[0]);
              $parts = array_slice($parts, 1, count($parts) - 2);
              $replace = $extension["postReplace"]["php"]($this, $parts, $this->form_index);
              array_push($replaces, [
                "offset" => $offset,
                "length" => $length,
                "replace" => $replace
              ]);
              $this->form_index++;
            }
          }
        }
      }
      // replace in rendered
      for ($i = count($replaces) - 1; $i >= 0; $i--) {
        $rendered = substr_replace($rendered, $replaces[$i]["replace"], $replaces[$i]["offset"], $replaces[$i]["length"]);
      }
      return $has_replace;
    }

    public function create_children($path, $schema, $custom, $reverse, $start, $length, $display_language, $template, $include = false) {
      // set template data
      $this->languages = $this->get_json_from_file(LANG_FILE_PATH);
      $this->display_language = $display_language;
      $this->include = $include;
      $this->info_texts = $this->get_info_texts($display_language);
      $this->get_build();
      // initialise server and client paths
      $this->set_data_paths(false);
      // get sections
      if ($path === "" || $path === null) {
        $this->ref = "type";
        $sections = [];
        for ($i=0; $i < $length; $i++) { 
          array_push($sections, (object) [
            "title" => (object) [
              $display_language => $this->get_setting("LOREM_IPSUM")
            ],
            "path" => "",
            "realpath" => "javascript:void(0)",
            "type" => $schema,
            "custom" => $custom,
            "id" => ""
          ]);
        }
      }
      else {
        // set ref
        $this->ref = "path";
        // get sections
        $this->page = $this->get_json_from_file(PAGE_FILE_PATH);
        $section = $this->get_section($this->page, $path);
        if ($section === null) exit_error(400, NO_SUCH_SECTION_ERR);
        if (!property_exists($section, "sections") || count($section->sections) < 1) return [];
        // check reverse
        if ($reverse === true) $sections = array_reverse($section->sections);
        else $sections = $section->sections;
        // filter by type
        $sections = array_values(array_filter($sections, function($subsection) use($schema, $custom) {
          return ($subsection->type === $schema && $subsection->custom === $custom); 
        }));
        // get selected
        $sections = array_slice($sections, $start, $length);
      }
      // load template into twig
      $loader = new \Twig\Loader\ArrayLoader(array_merge([
        'section.template' => $template
      ], $this->get_snippet_includes()));
      $twig = new \Twig\Environment($loader);
      // add extensions
      $this->add_extensions($twig);
      // render
      $rendered_sections = [];
      foreach ($sections as $subsection) {
        // add path
        $subsection->path = $path . "/" . $subsection->id;
        $subsection->realpath = $this->get_named_path($subsection->path, $display_language);
        try {
          $rendered_section = $twig->render('section.template', [
            'lang' => $this->display_language,
            'section' => $subsection,
            'page' => $this->page,
            'class' => $this
          ]);
          array_push($rendered_sections, $rendered_section);
         } catch (\Throwable $e) {
         }
      }
      // save files
      $this->flush_files();
      // return section list
      return $rendered_sections;
    }

    public function create($ref, $rel, $include = false, $section = null) {
      // set ref and include
      $this->ref = $ref;
      $this->include = $include;
      // set template data
      $this->set_template_data();
      // get information texts
      $this->info_texts = $this->get_info_texts($this->display_language);
      // initialise server and client paths
      $this->set_data_paths();
      // get section template
      if ($this->section_template === null) {
        $this->section_template = $this->get_section_template($ref, $rel);
      }
      // get section
      if ($section === null) $section = $this->create_section($ref, $rel);
      else $this->ref = "path"; 
      // remove checks
      $body_template = $this->remove_replace($this->page_template->body);
      $section_template = $this->remove_replace($this->section_template);
      // render template
      try {
        // create twig
        $twig = $this->create_twig($this->page_template->head, $body_template, $section_template);
        $template = $this->render_template($twig, $section);
      } catch (\Throwable $e) {
        exit_error(400, $e->getMessage());
      }
      // save files
      $this->flush_files();
      return $template;
    }

    private function get_name_paths($section, $homepage_path, $parent_id_path = "", $parent_name_path = []) {
      $paths = [];
      if ($parent_id_path === "") $id_path = $section->id;
      else $id_path = $parent_id_path . "/" . $section->id;
      $section_name = $section->name;
      if (count($parent_name_path) === 0 && $section_name === ADMIN_DIR_NAME) $section_name += "1"; 
      $name_path = array_merge($parent_name_path, [$section->name]);
      if ($parent_id_path !== "") {
        array_push($paths, ["id" => $id_path, "name" => ($id_path === $homepage_path ? null : $name_path)]);
      }
      if (property_exists($section, "sections")) {
        foreach ($section->sections as $subsection) {
          $paths = array_merge($paths, $this->get_name_paths($subsection, $homepage_path, $id_path, $name_path));
        }
      }
      return $paths;
    }

    public function get_path($name_path) {
      if ($name_path === null) $path = $this->output_path . DIRECTORY_SEPARATOR . $this->display_language;
      else {
        for ($i=0; $i <= count($name_path); $i++) {
          $dir_path = $this->output_path . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, array_slice($name_path, 0, $i));
          if (!file_exists($dir_path)) mkdir($dir_path);
        }
        $path = $this->output_path . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, $name_path) . DIRECTORY_SEPARATOR . $this->display_language;
      }
      if (!file_exists($path)) mkdir($path);
      return $path;
    }

    private function spaceless(string $template): string {
      return sprintf("{%% apply spaceless %%}%s{%% endapply %%}", $template);
    }

    private function create_section_template($id_path, $name_path) {
      $section = $this->create_section("path", $id_path);
      if (property_exists($section, "useSectionTemplate") && $section->useSectionTemplate === true) {
        $ref = "path";
        $rel = $id_path;
      }
      else {
        $ref = "type";
        $rel = (object) ["custom" => $section->custom, "name" => $section->type];
      }
      $template = $this->get_section_template($ref, $rel);
      // create twig
      $twig = $this->create_twig(
        $this->spaceless($this->page_template->head), 
        $this->spaceless($this->page_template->body), 
        $this->spaceless($template)
      );
      $rendered = $this->render_template($twig, $section);
      // replace
      $has_replace = $this->apply_replace($rendered, $section);
      // post replace
      $has_post_replace = $this->apply_post_replace($rendered);
      // get path to page
      $path = $this->get_path($name_path);
      // get file name and extension
      if ($has_replace !== false || $has_post_replace !== false) {
        $filename = "index.php";
        $rendered = "<?php require_once '"  . CLIENT_REQUEST_PATH . "' ?>" . $rendered;
      }
      else $filename = "index.html";
      // save
      $this->safe_save($rendered, $path . DIRECTORY_SEPARATOR . $filename);
    }

    private function set_build(): void {
      if (!file_exists(BUILD_PATH)) {
        $this->safe_save("0", BUILD_PATH);
      }
      else {
        $last_build = intval(file_get_contents(BUILD_PATH));
        $cur_build = $last_build + 1;
        $this->build = $cur_build;
        $this->safe_save(strval($cur_build), BUILD_PATH);
      }
    }

    private function get_build(): void {
      if (!file_exists(BUILD_PATH)) {
        $this->safe_save("0", BUILD_PATH);
      }
      else $this->build = intval(file_get_contents(BUILD_PATH));
    }

    public function create_page(): void {
      $this->ref = "path";
      // set build number
      $this->set_build();
      // set template data
      $this->set_template_data();
      // initialise server and client paths
      $this->set_data_paths();
      // get homepage and name paths
      if (!property_exists($this->page, "homepage")) $this->throw_template_exception(NO_HOMEPAGE_ERR);
      $homepage_path = $this->page->homepage;
      // get constant paths
      $const = get_defined_constants();
      // create ajax php
      $php_text = <<<EOL
      <?php
        require_once '{$const['AJAX_PATH']}';
        \$ajax_response = new AjaxResponse(false);
        echo \$ajax_response->process_request();
      ?>
      EOL;
      $this->safe_save($php_text, $this->output_path . DIRECTORY_SEPARATOR . "ajax.php");
      // create captcha php
      $php_text = <<<EOL
      <?php 
        require_once '{$const['CAPTCHA_PATH']}';
      ?>
      EOL;
      $this->safe_save($php_text, $this->output_path . DIRECTORY_SEPARATOR . "captcha.php");
      // create audio captcha
      $php_text = <<<EOL
      <?php 
        require_once '{$const['AUDIO_CAPTCHA_PATH']}';
      ?>
      EOL;
      $this->safe_save($php_text, $this->output_path . DIRECTORY_SEPARATOR . "audioCaptcha.php");
      // create files php
      $php_text = <<<EOL
      <?php 
        require_once '{$const['FILES_PHP_PATH']}';
      ?>
      EOL;
      $this->safe_save($php_text, $this->output_path . DIRECTORY_SEPARATOR . "files.php");
      // get name paths
      $paths = $this->get_name_paths($this->page, $homepage_path);
      $this->path_list = $paths;
      $this->safe_save(json_encode($paths, JSON_PRETTY_PRINT), PATH_LIST_PATH);
      // clear file downloads
      $this->file_downloads = [];
      // for each language
      foreach ($this->languages->languages as $language) {
        $this->display_language = $language->iso639;
        // get information texts
        $this->info_texts = $this->get_info_texts($this->display_language);
        foreach ($paths as $path) {
          try {
            $this->create_section_template($path["id"], $path["name"]);
          } catch (\Throwable $e) {
            $this->throw_template_exception($e->getMessage());
          }          
        }
      }
      // save file downloads
      $this->safe_save(json_encode($this->file_downloads, JSON_PRETTY_PRINT), FILE_DOWNLOAD_LIST_PATH);
      // create main page
      $page_root_path = $this->output_path;
      $main_page = <<<EOL
      <?php
        session_start();
        \$cur_lang = '{$this->languages->defaultLanguage}';
        if (isset(\$_SESSION["cur_lang"])) \$cur_lang = \$_SESSION["cur_lang"];
        \$lang_path = '{$page_root_path}' . \$cur_lang . DIRECTORY_SEPARATOR . "index.";
        if (!file_exists(\$lang_path . "php")) \$ext = "html";
        else \$ext = "php";
        require_once \$lang_path . \$ext;
      ?>
      EOL;
      $this->safe_save($main_page, $this->output_path . "index.php");
      // save files
      $this->flush_files();
    }

    // $STDERR = fopen("php://stderr", "w");
    // fwrite($STDERR, "\n".json_encode($this->contents)."\n\n");
    // fclose($STDERR);
  }
?>
