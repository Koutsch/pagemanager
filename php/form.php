<?php
	require_once __DIR__ . DIRECTORY_SEPARATOR . "basic.php";
	require_once __DIR__ . DIRECTORY_SEPARATOR . "error.php";
	require_once __DIR__ . DIRECTORY_SEPARATOR . "login.php";

	class FormHandler extends Basic_Handler {
		private object $forms;

		function __construct() {
			$this->forms = $this->get_json_from_file(FORMS_PATH);
		}

		private function get_form(string $section_path, int $form_id): object {
			if (!property_exists($this->forms, $section_path)) exit_error(400, INVALID_REQUEST_ERR);
			$content_forms = $this->forms->{$section_path};
			// get form
			$form = array_values(array_filter($content_forms, function($form) use($form_id) {
				return $form->id === $form_id;
			}));
			if (count($form) < 1) exit_error(400, INVALID_REQUEST_ERR, ["formId" => $form_id]);
			return $form[0];
		}

		private function get_inputs(object $form, object $uploaded_form_data, ?string $section_path=null): array {
			// check access
			if (property_exists($form, "accessRestriction") && $form->accessRestriction === true) {
				$login_handler = new LoginHandler();
				$status = $login_handler->user_has_access($section_path);
				if ($status === false) exit_error(403, FORBIDDEN_ERR);
			}
			$checked_inputs = [];
			for ($i=0; $i < count($form->inputs); $i++) { 
				if ($uploaded_form_data->{$i} === "" && property_exists($form->inputs[$i], "required") && $form->inputs[$i]->required === true) exit_error(401, REQUIRED_ERR, ["index" => $i]);
				array_push($checked_inputs, $uploaded_form_data->{$i});
			}
			return $checked_inputs;	
		}

		private function check_captcha(object $data): void {
			if (!property_exists($data, "text_captcha")) exit_error(401, CAPTCHA_INVALID_ERR);
			if (session_status() !== PHP_SESSION_ACTIVE || $data->text_captcha !== $_SESSION["text_captcha"]) exit_error(401, CAPTCHA_INVALID_ERR);
		}

		public function save_form(object $uploaded_form_data, string $section_path, int $form_id): void {
			$this->check_captcha($uploaded_form_data);
			$form = $this->get_form($section_path, $form_id);
			$inputs = $this->get_inputs($form, $uploaded_form_data, $section_path);
			$form_data = $this->get_json_from_file(FORM_DATA_PATH);
			if ($form_data === null) $form_data = (object) [];
			if (!property_exists($form_data, $section_path)) $form_data->$section_path = (object) [];
			if (!property_exists($form_data->$section_path, $form_id)) $form_data->{$section_path}->{$form_id} = [];
			array_push($form_data->{$section_path}->{$form_id}, $inputs);
			// send mail to admin
			$languages = $this->get_json_from_file(LANG_FILE_PATH);
			if ($languages === null) exit_error(500, INTERNAL_ERR);
			// prepare email
			$input_texts = [];
			$index = 0;
			foreach ($form->inputs as $input) {
				if ($input->type === "select") {
					$option_index = intval($inputs[$index]);
					if ($option_index >= 0 && $option_index < count($input->options)) array_push($input_texts, htmlspecialchars($input->options[$option_index]->title->{$languages->defaultLanguage}));
					else array_push($input_texts, $option_index);
				}
				else array_push($input_texts, htmlspecialchars($inputs[$index]));
				$index++;
			}
			$page = $this->get_json_from_file(PAGE_FILE_PATH);
			if ($page === null) exit_error(500, INTERNAL_ERR);
			$section = $this->get_section($page, $section_path);
			if ($section === false) exit_error(500, INTERNAL_ERR);
			$input_texts = sprintf("<h1>%s</h1><table><tr><th>%s</th></tr><tr><td>%s</td></tr></table>", htmlspecialchars($section->title->{$languages->defaultLanguage}), implode("</th><th>", array_map(function($input) use($languages) {
				return htmlspecialchars($input->title->{$languages->defaultLanguage});
			}, $form->inputs)), implode("</td><td>", $input_texts));
			$this->send_html_mail($form->notifyEmail, $form->title->{$languages->defaultLanguage}, $input_texts);
			// save to file
			$this->safe_save(json_encode($form_data, JSON_PRETTY_PRINT), FORM_DATA_PATH);
			$this->flush_files();
		}
	}
?>
