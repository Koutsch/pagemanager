<?php
  define("ROOT_PATH", __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR);
  define("DATA_ROOT_PATH", ROOT_PATH . "data" . DIRECTORY_SEPARATOR);
  define("EDITOR_PATH", ROOT_PATH . "editor" . DIRECTORY_SEPARATOR);
  define("EDITOR_LANG_PATH", EDITOR_PATH . "editorLanguages.json");
  define("PREDEFINED_JS_PATH", EDITOR_PATH . "predefined" . DIRECTORY_SEPARATOR . "predefinedJS.js");
  define("PW_STRENGTH_JS_PATH", EDITOR_PATH . "predefined" . DIRECTORY_SEPARATOR . "pwStrength.js");
  define("LABELS_PATH", EDITOR_PATH . "labels.json");
  define("MANUAL_PATH", EDITOR_PATH . "manual.html" );
  define("SCHEMAS_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "schemas" . DIRECTORY_SEPARATOR);
  define("CUSTOM_SCHEMAS_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "customSchemas" . DIRECTORY_SEPARATOR);
  define("CUSTOM_CONTENT_SCHEMAS_PATH", CUSTOM_SCHEMAS_PATH . "customContentSchemas.json");
  define("CUSTOM_SECTION_SCHEMAS_PATH", CUSTOM_SCHEMAS_PATH . "customSectionSchemas.json");
  define("CUSTOM_SECTION_SCHEMA_UPLOADS_PATH", CUSTOM_SCHEMAS_PATH . "customSectionSchemaUploads.json");
  define("CUSTOM_CONTENT_SCHEMA_UPLOADS_PATH", CUSTOM_SCHEMAS_PATH . "customContentSchemaUploads.json");
  define("LANG_SCHEMA_PATH", SCHEMAS_PATH . "languageSchema.json");
  define("BASIC_PAGE_SCHEMA_PATH", SCHEMAS_PATH . "basicPageSchema.json");
  define("PAGE_SCHEMA_PATH", SCHEMAS_PATH . "pageSchema.json");
  define("SECTION_SCHEMAS_PATH", SCHEMAS_PATH . "sectionSchemas.json");
  define("CONTENT_SCHEMAS_PATH", SCHEMAS_PATH . "contentSchemas.json");
  define("USER_SCHEMA_PATH", SCHEMAS_PATH . "userSchema.json");
  define("TEMPLATE_SCHEMA_PATH", SCHEMAS_PATH . "templateSchema.json");
  define("CONTENT_SCHEMA_UPLOAD_PATH", SCHEMAS_PATH . "contentSchemaUpload.json");
  define("FORM_SCHEMA_PATH", SCHEMAS_PATH . "formSchema.json");
  define("INFORMATION_TEXT_SCHEMA_PATH", SCHEMAS_PATH . "informationTextSchema.json");
  define("LANG_PATH", DATA_ROOT_PATH . "languages" . DIRECTORY_SEPARATOR);
  define("LANG_FILE_PATH", LANG_PATH . "languages.json");
  define("PAGE_PATH", DATA_ROOT_PATH . "page" . DIRECTORY_SEPARATOR);
  define("PAGE_FILE_PATH", PAGE_PATH . "page.json");
  define("CONTENTS_PATH", DATA_ROOT_PATH . "contents" . DIRECTORY_SEPARATOR);
  define("CONTENTS_LIST_PATH", CONTENTS_PATH . "contents_list.json");
  define("FORMS_DIR_PATH", DATA_ROOT_PATH . "forms" . DIRECTORY_SEPARATOR);
  define("FORMS_PATH", FORMS_DIR_PATH . "forms.json");
  define("FORM_DATA_PATH", FORMS_DIR_PATH . "formData.json");
  define("EVENTS_PATH", DATA_ROOT_PATH . "events" . DIRECTORY_SEPARATOR);
  define("EVENT_LIST_PATH", EVENTS_PATH . "event_list.json");
  define("FILES_DIR_PATH", DATA_ROOT_PATH . "files" . DIRECTORY_SEPARATOR);
  define("FILES_LIST_PATH", FILES_DIR_PATH . "files_list.json");
  define("USERS_DIR_PATH", DATA_ROOT_PATH . "users" . DIRECTORY_SEPARATOR);
  define("USERS_PATH", USERS_DIR_PATH . "users.json");
  define("TEMPLATES_PATH", DATA_ROOT_PATH . "templates" . DIRECTORY_SEPARATOR);
  define("TEMPLATES_LIST_PATH", TEMPLATES_PATH . "templates_list.json");
  define("TEMPLATE_FILES_DIR_PATH", TEMPLATES_PATH . "files" . DIRECTORY_SEPARATOR);
  define("TEMPLATE_FILES_LIST_PATH", TEMPLATE_FILES_DIR_PATH . "files_list.json");
  define("SNIPPETS_DIR_PATH", TEMPLATES_PATH . "snippets" . DIRECTORY_SEPARATOR);
  define("SNIPPETS_LIST_PATH", SNIPPETS_DIR_PATH . "snippetList.json");
  define("TEXT_SNIPPETS_DIR_PATH", TEMPLATES_PATH . "textSnippets" . DIRECTORY_SEPARATOR);
  define("TEXT_SNIPPETS_PATH", TEXT_SNIPPETS_DIR_PATH . "textSnippets.json");
  define("CSS_DIR_PATH", DATA_ROOT_PATH . "css" . DIRECTORY_SEPARATOR);
  define("CSS_LIST_PATH", DATA_ROOT_PATH . "css" . DIRECTORY_SEPARATOR. "css_list.json");
  define("CSS_PATH", CSS_DIR_PATH . DIRECTORY_SEPARATOR . "style.css");
  define("FONT_DIR_PATH", DATA_ROOT_PATH . DIRECTORY_SEPARATOR . "fonts" . DIRECTORY_SEPARATOR);
  define("FONT_LIST_PATH", FONT_DIR_PATH . DIRECTORY_SEPARATOR . "font_list.json");
  define("JS_DIR_PATH", DATA_ROOT_PATH . "js" . DIRECTORY_SEPARATOR);
  define("JS_LIST_PATH", JS_DIR_PATH . DIRECTORY_SEPARATOR . "js_list.json");
  define("DEFAULT_JS_PATH", JS_DIR_PATH . DIRECTORY_SEPARATOR . "defaultJS.js");
  define("BASIC_TEMPLATES_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "templates");
  define("HISTORY_PATH", EDITOR_PATH . "history.json");
  define("DEFAULT_INFORMATION_TEXT_PATH", EDITOR_PATH . "informationTexts.json");
  define("TRANSLATIONS_DIR_PATH", DATA_ROOT_PATH . "translations" . DIRECTORY_SEPARATOR);
  define("TRANSLATIONS_PATH", TRANSLATIONS_DIR_PATH . "translations.json");
  define("INFORMATION_TEXT_DIR_PATH", DATA_ROOT_PATH . "informationTexts" . DIRECTORY_SEPARATOR);
  define("INFORMATION_TEXT_PATH", INFORMATION_TEXT_DIR_PATH . "informationTexts.json");
  define("CAPTCHA_FONT_PATH", EDITOR_PATH . "captcha.ttf");
  define("FILES", "all");
  define("MEDIA_FILES", "media");
  define("BUILD_PATH", EDITOR_PATH . "build.txt");
  define("SETTINGS_PATH", EDITOR_PATH . "settings.json");

  // get public directory
  define("ADMIN_DIR_NAME", "admin");
  define("PREVIEW_DIR_PATH", ADMIN_DIR_NAME . "/preview");
  define("DATA_DIR_PATH", "data");
  define("DUMMY_DIR_PATH", "d");
  define("DUMMY_IMAGE_NAME", "dummy_logo.png");
  define("PATH_LIST_PATH", EDITOR_PATH . "pathList.json");
  define("FILE_DOWNLOAD_LIST_PATH", EDITOR_PATH . "fileDownloads.json");
  define("SOUNDS_PATH", EDITOR_PATH . "sounds" . DIRECTORY_SEPARATOR);

  require_once __DIR__ . DIRECTORY_SEPARATOR . 'error.php';
  require_once __DIR__ . '/vendor/autoload.php';
  use Swaggest\JsonDiff\JsonDiff;

  class Basic_Handler {
    protected $system_jsons = array();
    private $file_list = array();
    protected $settings;
    public $output_path;
    
    function __construct() {
      $this->settings = $this->get_system_json(SETTINGS_PATH);
      $this->output_path = ROOT_PATH . $this->get_setting("PUBLIC_DIR") . DIRECTORY_SEPARATOR;
    }

    private function get_setting_by_name($name) {
      $setting = array_values(array_filter($this->settings,  function($setting) use($name) {
        return ($name === $setting->name);
      }));
      if (count($setting) < 1) exit_error(500, INTERNAL_ERR);
      return $setting[0];
    }

    public function get_setting($name) {
      $setting = $this->get_setting_by_name($name);
      if (property_exists($setting, "value") && $setting->value !== null) return $setting->value;
      else if (property_exists($setting, "default")) return $setting->default;
      else if ($setting->type === "string") return "";
      else if ($setting->type === "int") return 0;
      else exit_error(500, INTERNAL_ERR);
    }

    public function set_setting($name, $value) {
      $setting = $this->get_setting_by_name($name);
      $setting->value = $value;
      $this->safe_save(json_encode($this->settings, JSON_PRETTY_PRINT), SETTINGS_PATH);
			$this->flush_files();
    }

    protected function generate_rand_id(array $existing_ids): int {
      $id = 0;
      while (in_array($id, $existing_ids)) $id++;
      return $id;
    }

    public function get_section(object $page, string $path): ?object {
      $path_arr = explode("/", $path);
      array_shift($path_arr);
      $cur_section = $page;
      foreach ($path_arr as $id) {
        if (!property_exists($cur_section, "sections")) return null;
        $subsections = array_values(array_filter($cur_section->sections, function($subsection) use($id) {
          return (intval($id) === $subsection->id);
        }));
        if (count($subsections) < 1) return null;
        $cur_section = $subsections[0];
      }
      return $cur_section;
    }

    public function add_lang_input(object &$schema, object $languages, string $display_language): void {
      if (!property_exists($schema, '$defs')) $schema->{'$defs'} = (object) [];
      $basic_page_schema = $this->get_system_json(BASIC_PAGE_SCHEMA_PATH);
      $schema->{'$defs'}->langInput = $basic_page_schema->defs->langInput;
      foreach ($languages->languages as $language) $schema->{'$defs'}->langInput->properties->{$language->iso639} = (object) ["type" => "string"];
      $schema->{'$defs'}->langInput->required = [$display_language];
    }

    public function get_part(bool $custom): string {
      return ($custom ? "custom": "predefined");
    }

    protected function &content_create(?object &$object, string $section_path, string $part, string $content, $add=null): object {
      if ($object === null) {
        $object = (object) [];
        return $object;
      }
      if (!property_exists($object, $section_path)) $object->{$section_path} = (object) [];
      if (!property_exists($object->{$section_path}, $part)) $object->{$section_path}->{$part} = (object) [];
      if (!property_exists($object->{$section_path}->{$part}, $content)) $object->{$section_path}->{$part}->{$content} = $add;
      return $object;
    }

    protected function &content_search(object &$object, string $section_path, string $part, string $content) {
      if (
        $object === null || 
        !property_exists($object, $section_path) || 
        !property_exists($object->{$section_path}, $part) || 
        !property_exists($object->{$section_path}->{$part}, 
        $content)
      ) {
        $null = null;
        return $null;
      }
      return $object->{$section_path}->{$part}->{$content};     
    }

    protected function send_html_mail(string $address, string $title, string $body): void {
      try {
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				mail($address, $title, $body, $header);
			} catch (\Throwable $th) {
				exit_error(500, INTERNAL_ERR);
			}
    }

    protected function get_url(): string {
      if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') $url = "https://";   
      else $url = "http://";    
      $url.= $_SERVER['HTTP_HOST']; 
      return $url;
    }

    protected function get_pin(): int {
      return rand(1000, 9999);
    }

    protected function get_admin_url(): string {
      $url = $this->get_url();
			if (substr($url, -1) !== "/") $url .= "/";
			$url .= ADMIN_DIR_NAME;
      return $url;
    }

    protected function send_user_mail(string $address, string $username, int $pin): void {
      $languages = $this->get_json_from_file(LANG_FILE_PATH);
      $info_texts = $this->get_info_texts($languages->defaultLanguage);
      $url = $this->get_admin_url();
      $this->send_html_mail($address, $info_texts->credentialsMail, <<<EOL
        <h1>{$info_texts->credentialsMail}</h1>
        <table>
          <tr>
            <th>{$info_texts->username}</th>
            <th>{$info_texts->pin}</th>
          </tr>
          <tr>
            <td>{$username}</td>
            <td>{$pin}</td>
          </tr>
        </table>
        <a href="{$url}">{$info_texts->getToSite}</a>
      EOL);
    }

    private function create_diff_object($cur, $new, $rearrange=true): object {
      if ($rearrange)
        return new JsonDiff(
          $cur,
          $new,
          JsonDiff::REARRANGE_ARRAYS
        );
      else
        return new JsonDiff(
          $cur,
          $new
        );
    }

    protected function get_diff_count($cur, $new, $rearrange=true) {
      $diff = $this->create_diff_object($cur, $new, $rearrange);
      return $diff->getDiffCnt();
    }

    protected function get_diffs($cur, $new): array {
      $conflicts = [];
      $diff = $this->create_diff_object($cur, $new);
      $modified = $diff->getModifiedPaths();
      $removed = $diff->getRemovedPaths();
      if (count($modified) > 0) $conflicts["modified"] = $modified;
      if (count($removed) > 0) $conflicts["removed"] = $removed;
      return $conflicts;
    }

    protected function get_template(string $ref, $rel): ?object {
      $templates_list = $this->get_json_from_file(TEMPLATES_LIST_PATH);
      if ($templates_list === null) return null;
      switch ($ref) {
        case "path":
          if (property_exists($templates_list, $ref) && property_exists($templates_list->{$ref}, $rel)) $template_data = $templates_list->{$ref}->{$rel};
          else return null;
          break;
        case "type":
          $part = ($rel->custom === true ? "custom" : "predefined");
          if (property_exists($templates_list, $ref) && property_exists($templates_list->{$ref}, $part) && property_exists($templates_list->{$ref}->{$part}, $rel->name)) $template_data = $templates_list->{$ref}->{$part}->{$rel->name};
          else return null;
          break;
        default:
          exit_error(400, INVALID_REQUEST_ERR);
      }
      $template = (object) [];
      foreach ($template_data as $key => $filename) {
        $template->{$key} = file_get_contents(TEMPLATES_PATH . $filename);
        if ($template->{$key} === false) exit_error(400, NO_SUCH_TEMPLATE_ERR);
      }
      return $template;
    }

    public function get_json_from_file(string $path, bool $assoc=false) {
      if (!(file_exists($path))) return null;
      $json = file_get_contents($path);
      if ($json === false) return null;
      $json = json_decode($json, $assoc);
      if ($json === null) return null;
      return $json;
    }

    public function get_named_path($path, $language) {
      $path_list = $this->get_json_from_file(PATH_LIST_PATH);
      $name_path = "javascript:void(0)";
      if ($path_list !== null) {
        $name_path = "/";
        $name_paths = array_values(array_filter($path_list, function($paths) use($path) {
          return $paths->id === $path;
        }));
        if (count($name_paths) > 0 && $name_paths[0]->name !== null) {
          $name_path .= implode("/", $name_paths[0]->name) . "/" . $language;
        }
      }
      return $name_path;
    }

    public function get_section_schema(string $name, bool $custom, bool $assoc=false) {
      if ($custom === true) {
        $schemas = $this->get_system_json(CUSTOM_SECTION_SCHEMAS_PATH, $assoc);
        if ($schemas === null) return null;
      }
      else if ($custom === false) {
        $schemas = $this->get_system_json(SECTION_SCHEMAS_PATH, $assoc);
      }
      else return null;
      for ($i=0; $i < count($schemas); $i++) {
        if (!$assoc && $schemas[$i]->name === $name) return $schemas[$i];
        if ($assoc && $schemas[$i]["name"] === $name) return $schemas[$i];
      }
      return null;
    }

    public function get_content_schema(string $part, string $content_type): object {
      if ($part === "custom") $content_schemas = $this->get_system_json(CUSTOM_CONTENT_SCHEMAS_PATH);
      else $content_schemas = $this->get_system_json(CONTENT_SCHEMAS_PATH);
      return $content_schemas->{$content_type};
    }

    public function get_collection_files($path, $collection, $file_id=null) {
      $files_list = $this->get_json_from_file(FILES_LIST_PATH);
      if ($files_list === null) return null;
      if (!property_exists($files_list, $path) || !property_exists($files_list->{$path}, $collection)) return null;
      $files = $files_list->{$path}->{$collection};
      if (count($files) < 1) return null;
      if ($file_id !== null) {
        $files = array_values(array_filter($files, function($file) use($file_id) {
          return ($file->id === $file_id);
        }));
      }
      return $files;
    }

    protected function get_resized_image($image_path, $width) {
      list($i_width, $i_height, $type) = getimagesize($image_path);
      if ($type == IMAGETYPE_JPEG) {
        $image = imagecreatefromjpeg($image_path);
      }
      elseif ($type == IMAGETYPE_PNG) {
        $image = imagecreatefrompng($image_path);
      }
      elseif ($type == IMAGETYPE_GIF) {
        $image = imagecreatefromgif($image_path);
      }
      else return null;
      $resize_ratio = $width / $i_width;
      $height = (int) ($i_height * $resize_ratio);
      try {
		    $resized_image = imagecreatetruecolor($width, $height);
		    imagesavealpha($resized_image, true);
		    $color = imagecolorallocatealpha($resized_image, 0, 0, 0, 127);
		    imagefill($resized_image, 0, 0, $color);
		    imagecopyresampled($resized_image, $image, 0, 0, 0, 0, $width, $height, $i_width, $i_height);
		  }
		  catch (\Throwable $e) {
		  	exit_error(500, $e->getMessage());
		  }
      ob_start(); 
      imagepng($resized_image);
      $image_data = ob_get_contents();
      ob_end_clean();
      imagedestroy($image);
      return "data:image/png;base64," . base64_encode($image_data);
    }

    public function set_files(string $dir, string $dir_path, string $client_path, string $server_path, array $files): array {
      $client_dir = $client_path . DIRECTORY_SEPARATOR. $dir;
      $server_dir = $server_path . DIRECTORY_SEPARATOR . $dir;
      if (!file_exists($server_dir)) mkdir($server_dir);
      $file_paths = [];
      foreach ($files as $file) {
        $filename = $file->name;
        if (!file_exists($server_dir . DIRECTORY_SEPARATOR . $file->name)) {

          copy($dir_path . $file->filename, $server_dir . DIRECTORY_SEPARATOR . $file->name);
          /* $image = false;
          // get file size
          $file_size = filesize($dir_path . $file->filename);
          if (strpos($file->type, "image") === 0 && $file_size > $this->get_setting("MAX_SIZE")) {
            switch($file->type){ 
              case 'image/jpeg':
                $image = imagecreatefromjpeg($dir_path . $file->filename); 
                break; 
              case 'image/png': 
                $image = imagecreatefrompng($dir_path . $file->filename); 
                break; 
              case 'image/gif': 
                $image = imagecreatefromgif($dir_path . $file->filename); 
                break;
            }
          }
          if ($image !== false) {
            $quality = 100;
            $new_file_size = 0;
            $filename = substr($file->name, 0, strpos($file->name, ".")) . ".jpeg";
            do {
              imagejpeg($image, $server_dir . DIRECTORY_SEPARATOR . $filename, $quality);
              clearstatcache();
              $new_file_size = filesize($server_dir . DIRECTORY_SEPARATOR . $filename);
              $ratio = $this->get_setting("MAX_SIZE") / $new_file_size;
              // calculate compression
              $quality -= 30 - (int) (30 / (1 / $ratio));
            } while ($new_file_size > $this->get_setting("MAX_SIZE") && $quality > 70);
            imagedestroy($image);
          }
          else copy($dir_path . $file->filename, $server_dir . DIRECTORY_SEPARATOR . $file->name); */
        }
        array_push($file_paths, $filename);
      }
      return array_map(function($filename) use($client_dir) {
        return $client_dir . DIRECTORY_SEPARATOR . $filename;
      }, $file_paths);
    }

    protected function get_system_json(string $path, bool $assoc=false) {
      if (!array_key_exists($path, $this->system_jsons)) $this->system_jsons[$path] = $this->get_json_from_file($path, $assoc);
      if ($this->system_jsons[$path] === null) exit_error(500);
      return $this->system_jsons[$path];
    }

    protected function get_filename(string $path): int {
      do {
        $filename = abs(crc32(uniqid(rand(), true)));
      } while (file_exists($path . $filename));
      return $filename;
    }

    protected function get_info_texts(string $language): object {
      // get info texts
      $default_info_texts = $this->get_system_json(DEFAULT_INFORMATION_TEXT_PATH);
      $info_texts = $this->get_json_from_file(INFORMATION_TEXT_PATH);
      if ($info_texts !== null) {
        foreach ($info_texts as $key => $texts) {
          if ($texts->{$language} !== null && $texts->{$language} !== "") $default_info_texts->{$key} = $texts->{$language};
        }
      }
      return $default_info_texts;
    }

    public function safe_save($data, string $path): void {
      $fp = fopen($path, 'c');
      if (!flock($fp, LOCK_EX | LOCK_NB)) exit_error(403, FILE_LOCKED_ERR);
      array_push($this->file_list, array("file_pointer" => $fp, "data" => $data));
    }

    public function safe_remove(string $path): void {
      $fp = fopen($path, 'c');
      if (!flock($fp, LOCK_EX | LOCK_NB)) exit_error(403, FILE_LOCKED_ERR);
      array_push($this->file_list, array("file_pointer" => $fp, "path" => $path));
    }

    public function safe_remove_dir(string $path): void {
      if (is_dir($path) === true) {
        $files = array_diff(scandir($path), array('.', '..'));
        foreach ($files as $file) $this->safe_remove_dir(realpath($path) . DIRECTORY_SEPARATOR . $file);
        array_push($this->file_list, array("dirpath" => $path));
      }
      else $this->safe_remove($path);
    }

    public function safe_move($upload_pointer, string $path, $i=null, bool $resize=false): void {
      $fp = fopen($path, 'c');
      if (!flock($fp, LOCK_EX | LOCK_NB)) exit_error(403, FILE_LOCKED_ERR);
      $file = array("file_pointer" => $fp, "upload_pointer" => $upload_pointer, "path" => $path, "resize" => $resize);
      if ($i !== null) $file["i"] = $i;
      array_push($this->file_list, $file);
    }

    public function flush_files(): void {
      foreach ($this->file_list as $file) {
        if (array_key_exists("data", $file) && array_key_exists("file_pointer", $file)) {
          $fp = $file["file_pointer"];
          ftruncate($fp, 0);
          fwrite($fp, $file["data"]);
          fflush($fp);
          flock($fp, LOCK_UN);
          fclose($fp);
        }
        else if (array_key_exists("file_pointer", $file) && array_key_exists("upload_pointer", $file) && array_key_exists("path", $file)) {
          if ($file["i"] !== null) {
            $tmp_name = $_FILES[$file["upload_pointer"]]['tmp_name'][$file["i"]];
            $mime_type = $_FILES[$file["upload_pointer"]]['type'][$file["i"]];
          }
          else {
            $tmp_name = $_FILES[$file["upload_pointer"]]['tmp_name'];
            $mime_type = $_FILES[$file["upload_pointer"]]['type'];
          }
          if (!(move_uploaded_file($tmp_name, $file["path"]))) exit_error(500);
          $fp = $file["file_pointer"];
          flock($fp, LOCK_UN);
          fclose($fp);
        }
        else if (array_key_exists("path", $file)) {
          $fp = $file["file_pointer"];
          unlink($file["path"]);
          flock($fp, LOCK_UN);
          fclose($fp);
        }
        else if (array_key_exists("dirpath", $file)) {
          rmdir($file["dirpath"]);
        }
      }
      $this->file_list = array();
    }
  }

  class Handler extends Basic_Handler {
    protected $requests;
    protected $response;

    function __construct($requests) {
      parent::__construct();
      $this->requests = $requests;
    }

    public function response() {
      // check if requests is array
      if (!(is_array($this->requests))) exit_error(400, INVALID_REQUEST_ERR);
      // prepare response
      $this->response = [];
      // loop all individual requests
      foreach ($this->requests as $request) {
        if (!(property_exists($request, "request"))) exit_error(400, INVALID_REQUEST_ERR);
        $request_name = (string) $request->request;
        try {
          $this->response[$request_name] = $this->{$request_name}($request);
        }
        catch (Error $e) {
          exit_error(500);
        }
      }
      return $this->response;
    }
  }
?>
