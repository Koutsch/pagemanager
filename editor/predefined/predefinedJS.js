var lang;

window.addEventListener('DOMContentLoaded', (event) => {
  lang = document.querySelector("html").getAttribute("lang");
  const langRequest = new FormData();
  langRequest.append("request", JSON.stringify({request:"lang", lang:lang}));
  makeRequest(langRequest, ()=>{});
});

function makeRequest(uploadRequest, callback, errorCallback) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      if (this.status == 200) {
        const json = JSON.parse(this.response);
        callback(json);
      }
      else if (errorCallback !== undefined) {
        const json = JSON.parse(this.response);
        errorCallback(json);
      }
    }
  };
  xhttp.open("POST", "/ajax.php", true);
  xhttp.send(uploadRequest);
}

const element = (tag, props = {}) => {
  if (props.id !== undefined && document.getElementById(props.id) !== null) return null;  
  return Object.assign(document.createElement(tag), props);
};
const safeInsert = (par, sib, after) => {
  if (sib !== null) par.insertBefore(sib, after);
};
const insert = (par, after, ...sibs) => sibs.reduce((e, sib) => (safeInsert(par, sib, e), e), after);
const setLoginStatusText = (formId, text) => document.getElementById(formId + "-status").textContent = text;
const createNewPasswordInput = (form, textVar) => {
  const submit = document.getElementById(form.id + "-submit");
  insert(form, submit, 
    element("label", {id: form.id + "-newPassword-label", for: form.id + "-newPassword", textContent: window[textVar].passwordLabelNew + "*"}),
    element("input", {id: form.id + "-newPassword", type: "password", "aria-required": "true", "aria-describedby": form.id + "-newPasswordStrength"}),
    element("div", {id: form.id + "-newPasswordStrength", "aria-atomic": "false"}),
    element("label", {id: form.id + "-newPasswordRepeat-label", for: form.id + "-newPasswordRepeat", textContent: window[textVar].passwordLabelNewRepeat + "*"}),
    element("input", {id: form.id + "-newPasswordRepeat", type: "password", "aria-required": "true"})
  );
  const newPasswordInput = document.getElementById(form.id + "-newPassword");
  newPasswordInput.addEventListener("input", (e) => checkPasswordStrength(e.target.value, form.id + "-newPasswordStrength", textVar));
  form.removeEventListener("submit", submitLoginForm);
  form.addEventListener("submit", submitNewPassword);
}
const getTextVar = (formId) => "loginForm" + formId.split("-")[1] + "Texts";
const errorMsg = (error, textVar) => {
  switch (error) {
    case "INVALID_REQUEST":
    default:
      return window[textVar].error;
    case "INVALID_CREDENTIALS":
      return window[textVar].loginError;
    case "PASSWORD_MISMATCH":
      return window[textVar].passwordMismatch;
    case "NO_PASSWORD":
      return window[textVar].noPassword;
  }
};
const submitNewPassword = (e) => {
  const form = e.target;
  setLoginStatusText(form.id, "");
  const textVar = getTextVar(form.id);
  const username = document.getElementById(form.id + "-username").value;
  const password = document.getElementById(form.id + "-password").value;
  const newPassword = document.getElementById(form.id + "-newPassword").value;
  const newPasswordRepeat = document.getElementById(form.id + "-newPasswordRepeat").value;
  const request = new FormData();
  request.append("request", JSON.stringify({request:"newPassword", username: username, password: password, newPassword: newPassword, newPasswordRepeat: newPasswordRepeat}));
  makeRequest(request, () => {
    document.getElementById(form.id + "-status").textContent = window[textVar].passwordSet;
    document.getElementById(form.id + "-newPassword").remove();
    document.getElementById(form.id + "-newPassword-label").remove();
    document.getElementById(form.id + "-newPasswordStrength").remove();
    document.getElementById(form.id + "-newPasswordRepeat").remove();
    document.getElementById(form.id + "-newPasswordRepeat-label").remove();
    document.getElementById(form.id + "-password").value = "";
    form.removeEventListener("submit", submitNewPassword);
    form.addEventListener("submit", submitLoginForm);
  }, (error) => {
    setLoginStatusText(form.id, errorMsg(error[0], textVar));
  });
  e.preventDefault();
}
const submitLoginForm = (e) => {
  const form = e.target;
  setLoginStatusText(form.id, "");
  const textVar = getTextVar(form.id);
  const username = document.getElementById(form.id + "-username").value;
  const password = document.getElementById(form.id + "-password").value;
  const request = new FormData();
  request.append("request", JSON.stringify({request:"login", username: username, password: password}));
  makeRequest(request, (response) => {
    if (response === false) {
      createNewPasswordInput(form, textVar);
    }
    else {
      location.reload();
    }
  }, (error) => {
    setLoginStatusText(form.id, errorMsg(error[0], textVar));
  });
  e.preventDefault();
}
const logout = (element) => {
  const request = new FormData();
  request.append("request", JSON.stringify({request:"logout"}));
  makeRequest(request, () => {
    location.reload();
  }, () => {
    const statusDivId = element.id + "-status";
    const textVar = "logoutButton" + element.id.split("-")[1] + "Texts"
    document.getElementById(statusDivId).textContent = window[textVar].error;
  });
};
const resetPassword = (element) => {
  const id = element.id.split("-")[1];
  const prefix = "loginForm";
  const formId = prefix + "-" + id;
  const textVar = prefix + id + "Texts";
  const username = document.getElementById(formId + "-username").value;
  const request = new FormData();
  request.append("request", JSON.stringify({request:"resetPassword", username: username}));
  makeRequest(request, () => {
    createNewPasswordInput(document.getElementById(formId), textVar);
    document.getElementById(formId + "-status").textContent = window[textVar].passwordReset;
  }, (error) => {
    setLoginStatusText(formId, errorMsg(error[0], textVar));
  });
};

function lazySectionLoading(path, schema, custom, start, length, reverse, template, callback) {
  const request = new FormData();
  request.append("request", JSON.stringify({request:"sections", path:path, schema:schema, custom:custom, start:start, length:length, reverse:reverse, template:template, language:lang}));
  makeRequest(request, callback);
}

function getCalendar(start, length, backward, paths, callback) {
  if (!Array.isArray(paths) || paths.length < 1) paths = null;
  const request = new FormData();
  request.append("request", JSON.stringify({request:"calendar", start:start, length:length, backward:backward, paths:paths, language:lang}));
  makeRequest(request, callback);
}

function getCalendarByDate(begin, end, backward, paths, callback) {
  if (!Array.isArray(paths) || paths.length < 1) paths = null;
  const request = new FormData();
  request.append("request", JSON.stringify({request:"calendarByDate", begin:begin, end:end, backward:backward, paths:paths, language:lang}));
  makeRequest(request, callback);
}

function decryptBase64(encoded) {
  return atob(encoded);
}

function audioCaptcha() {
  const audio = new Audio('/audioCaptcha.php?' + new Date().getTime());
  audio.play();
}

function refreshCaptcha(id) {
  const img = document.querySelector("#" + id);
  const src = img.src.split("?")[0];
  img.src = src + "?" + new Date().getTime();
}

function submitForm(form, path, formId, responseMsg, requiredMsg, errorMsg, callback, errCallback) {
  const request = new FormData();
  const formData = new FormData(form);
  request.append("request", JSON.stringify({
    request: "form",
    path: path,
    formId: formId,
    form: Object.fromEntries(formData)
  }));
  makeRequest(request, (response) => {
    if (callback !== null) callback(response);
    else alert(responseMsg);
  }, (error) => {
    if (errCallback !== null) errCallback(error);
    else {
      if (error[0] === "REQUIRED") {
        document.getElementById(form.id + "-" + error[1].index + "-label").style.color = "red";
        alert(requiredMsg);
      }
      else alert(errorMsg);
    }
  });
  return false;
}
