const strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
const mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
const checkPasswordStrength = (password, badgeId, textVar) => {
  const badge = document.getElementById(badgeId);
  if (password.length < 1) badge.textContent = "";
  else if (strongPassword.test(password)) {
    badge.textContent = window[textVar].strongPassword;
    badge.style.color = "#006600";
  }
  else if (mediumPassword.test(password)) {
    badge.textContent = window[textVar].mediumPassword;
    badge.style.color = "#ff9900";
  }
  else {
    badge.textContent = window[textVar].weakPassword;
    badge.style.color = "#cc0000";
  }
}